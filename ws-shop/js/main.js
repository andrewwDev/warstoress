


function sendSignup(e) {
	e.preventDefault();

	if ($('#agreement').is(':checked')) {
		$.ajax({
			url: $(this).attr('action'),
			method: $(this).attr('method'),
			data: $(this).serialize(),
			success: function(response) {
				console.log(response);
				var data = JSON.parse(response)
				if (data.status === 'success') {
					alert('Компания успешно добавлена.');
				} else {
					alert('Ошибка: компания не добавлена.');
				}
			},
			error: function(response) {
				console.log(response);
				alert('Ошибка: соединение прервано.');
			}
		});
	}
}

function getName (str, type){
	if (str.lastIndexOf('\\')){
		var i = str.lastIndexOf('\\')+1;
	}
	else{
		var i = str.lastIndexOf('/')+1;
	}
	var filename = str.slice(i);
	if (type == "upload-logo") {
		var uploaded = document.getElementById("fileformlabel-logo");
	} else if (type == "upload-news") {
        var uploaded = document.getElementById("fileformlabel-news");
    } else {
		var uploaded = document.getElementById("fileformlabel-big");
	}
	uploaded.innerHTML = filename;
}



// Для вычисления ИТОГО процент от чека:
// $percent_in_ref=6; //Процент по реф сетке максимальный
// $I9=$percent_in_ref0.01;
// $ref_line_factor=(pow(1+$I9,4)+pow(1+$I9,3)+pow(1+$I9,2)+(1+$I9)+1)$I9; //Коэффициент ветки реферальной системе.
// $ref_sys_factor=($percent_ref*(1+$ref_line_factor)/100+$ref_line_factor); //Коэффициент всей системы. Умножаем его на процент пользователю и получаем сколько процентов уйдет на всю систему (без выплаты самого бонуса)
// //Для 6% $ref_line_factor=0.3382255776; //Коэффициент ветки реферальной системы.

// ИТОГО=Процент от чека клиенту*(1+$ref_sys_factor)

// function getPercent() {
// 	var REF_PERCENT = 6;
// 	var I9 = REF_PERCENT * 0.01;
// 	var REF_LINE_FACTOR = (Math.pow(1 + I9, 4) + Math.pow(1 + I9, 3) + Math.pow(1 + I9, 2) + Math.pow(1 + I9, 1));

// 	var REF_SYS_FACTOR = (REF_PERCENT * (1 + REF_LINE_FACTOR) / 100 + REF_LINE_FACTOR);

// 	return (/* Процент от чека клиенту */ * (1 + REF_SYS_FACTOR));
// }


function setMap(address) {
	ymaps.ready(init);

    function init(){
    	var res = ymaps.geocode(address);
        res.then(
	    function (res) {
	    	var object = res.geoObjects.get(0)
	    	var coords = object.geometry.getCoordinates();

	    	window.myMap = new ymaps.Map("map", {
            	center: [coords[0], coords[1]],
        	    zoom: 16,
        	    controls: []
        	});

        	window.myMap.geoObjects.add(object);
	    },
	    function (err) {
	        alert('Ошибка');
	    });
	}
}

;$(function() {
	$('#shopRegister').on('submit', sendSignup);
	/*
	if ($('#morris-chart-network')[0] !== undefined) {
		new Morris.Line({
		  // ID of the element in which to draw the chart.
		  element: 'morris-chart-network',
		  // Chart data records -- each entry in this array corresponds to a point on
		  // the chart.
		  data: [
		    { year: '2008', value: 20 },
		    { year: '2009', value: 10 },
		    { year: '2010', value: 5 },
		    { year: '2011', value: 5 },
		    { year: '2012', value: 20 }
		  ],
		  // The name of the data record attribute that contains x-values.
		  xkey: 'year',
		  // A list of names of data record attributes that contain y-values.
		  ykeys: ['value'],
		  // Labels for the ykeys -- will be displayed when you hover over the
		  // chart.
		  labels: ['Value']
		});

		var myvalues = [10,8,5,7,4,4,1];

		$('#demo-sparkline-area').sparkline(myvalues, {type: 'bar', barColor: 'green'} );

	} */
	
	if (document.getElementById('map') !== null) {
		document.getElementById('realAddress').addEventListener('blur', function(e) {
			var address = e.target.value;

			if (address !== '') {
				document.getElementById('map').innerHTML = '';
				setMap(address);
			}
		}, false);
	}
});