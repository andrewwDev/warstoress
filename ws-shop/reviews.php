<?php
error_reporting(-1);
require_once '../ws-panel/libs/Smarty.class.php';
$smarty = new Smarty();

session_start();
if (isset($_SESSION['uid'])) {
	require_once './db.php';

	$uid = $_SESSION['uid'];
	require_once './backend/user.php';

	$smarty->assign('owner_name', $username);
    $smarty->assign('user_status', $user_status);
    $smarty->assign('avatar', $avatar);
	$smarty->assign('shops_list', $shops_list);


	if (isset($_GET['sid'])) {
		$shop_id = (int)$_GET['sid'];

        $smarty->assign('current_shop', '');

        foreach ($shops_list as $key => $value) {
            if ($value['id'] == $shop_id) {
                $smarty->assign('current_shop', $shops_list[$key]);
                require_once './backend/shop.php';

                $reviews = [];
                $users = [];
                $overall_rating = 0;
                if ($stmt = $mysqli->prepare("SELECT id, uid, date, text, rating FROM `wsq_feedback` WHERE sid=?")) {
                    $stmt->bind_param('s', $shop_id);
                    $stmt->execute();
                    $stmt->bind_result($id, $uid, $date, $text, $rating);
                    while ($stmt->fetch()) $reviews[] = array(
                        'id' => $id,
                        'uid' => $uid,
                        'date' => $date,
                        'text' => $text,
                        'rating' => $rating,
                    );
                    $stmt->close();
                    foreach ($reviews as $key => $value) {
                        $overall_rating += $value["rating"];
                        $stmt = $mysqli->prepare("SELECT name, surname FROM `wsq_users` WHERE uid=?");
                        $stmt->bind_param('s', $value["uid"]);
                        $stmt->execute();
                        $stmt->bind_result($name, $surname);
                        while ($stmt->fetch()) $users[$value["uid"]] = array($name, $surname);
                    }
                    if ($overall_rating > 0) $overall_rating = $overall_rating / count($reviews);
                    $news = array_reverse($reviews);
                    $smarty->assign('reviews', $reviews);
                    $smarty->assign('users', $users);
                    $smarty->assign('overall_rating', $overall_rating);
                }
            }
        }


		



	}

	$login = true;
} else {
	$login = false;
}








$smarty->assign('login', $login);
$smarty->display('reviews.tpl');
