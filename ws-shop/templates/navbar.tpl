﻿				<!--Brand logo & name-->
                <!--================================-->
                <div class="navbar-header">
                    <a href="http://warstores.net" class="navbar-brand">
                        <img src="img/logo.png" alt="WarStores Logo" class="brand-icon">
                        <div class="brand-title">
                            <span class="brand-text">WarStores</span>
                        </div>
                    </a>
                </div>
                <!--================================-->
                <!--End brand logo & name-->


                <!--Navbar Dropdown-->
                <!--================================-->
                <div class="navbar-content clearfix">
                    <ul class="nav navbar-top-links pull-left">

                        <!--Navigation toogle button-->
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <li class="tgl-menu-btn">
                            <a class="mainnav-toggle" href="#">
                                <i class="demo-pli-view-list"></i>
                            </a>
                        </li>
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <!--End Navigation toogle button-->
						<!--Container-->
						Сервис работает в открытом бета-тестировании. В случае найденных недочетов пишите в <a href="https://t.me/warstores">телеграмм <i class="demo-pli-paper-plane icon-2x"></i>@warstores</a>
						<!--End container-->




                    </ul>

                    <ul class="nav navbar-top-links pull-right">
					
                        <li>
                            <a href="#" class="aside-toggle navbar-aside-icon">
                                <i class="pci-ver-dots"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!--================================-->
                <!--End Navbar Dropdown-->
