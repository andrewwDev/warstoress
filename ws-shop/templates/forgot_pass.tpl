{include file='header.tpl'}
<div id="container" class="cls-container">


    <!-- BACKGROUND IMAGE -->
    <!--===================================================-->
    <div id="bg-overlay"></div>


    <!-- REGISTRATION FORM -->
    <!--===================================================-->
    <div class="cls-content">
        <div class="cls-content-lg panel">
            <div class="panel-body">
                <div id="before" class="mar-ver pad-btm">
                    <h3 class="h4 mar-no">Восстановление пароля</h3>
                    <p class="text-muted">На email адрес связанный с аккаунтом будет отправлена ссылка для восстановления пароля.</p>
                </div>
                <div id="after" style="display: none;">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4></h4>
                        </div>
                    </div>
                </div>
                <form id="reg_form" action="">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Uid или Логин или Email" name="email" id="email" required>
                            </div>
                        </div>
                    </div>
                    <button id="reg_form_finish" class="btn btn-primary btn-block" type="submit">Восстановить</button>
                </form>
            </div>
            <div class="pad-all">
                <a href="/ws-shop/" class="btn-link mar-rgt">Войти</a>
            </div>
        </div>
    </div>
    <!--===================================================-->


</div>
<!--===================================================-->
<!-- END OF CONTAINER -->
<script>
    $( '#reg_form_finish' ).click(function(event) {
        event.preventDefault();
        var before = $("#before");
        var after = $("#after");
        var email = $("#email");
        if (email.val() == "") {
            after.find("h4").text("Поле не может быть пустым.");
            after.show();
            return false;
        }

        $("#reg_form").hide();

        var formData  = new FormData($( '#reg_form' )[0]);

        $.ajax({
            url: 'backend/forgot_pass.php',
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                after.find("h4").text(data["status"]);
                after.show();
                before.hide();
            }
        });
    });
</script>


</body>
</html>
