{include file='header.tpl'}
<script src='https://www.google.com/recaptcha/api.js'></script>
	<div id="container" class="cls-container">
		
		
		<!-- BACKGROUND IMAGE -->
		<!--===================================================-->
		<div id="bg-overlay"></div>
		
		
		<!-- REGISTRATION FORM -->
		<!--===================================================-->
		<div class="cls-content">
		    <div class="cls-content-lg panel">
		        <div class="panel-body">
		            <div class="mar-ver pad-btm">
		                <h3 class="h4 mar-no">Регистрация</h3>
		                <p class="text-muted">Присоединяйтесь к warstores.</p>
		            </div>
					<div id="after" style="display: none;">
						<div class="row">
							<div class="col-sm-12">
								<h4>Вы успешно зарегистрировались</h4>
								Для начала работы подтвердите свою учетную запись. Код подтверждения был отправлен на указанный Вами e-mail.
							</div>
						</div>
					</div>
		            <form id="reg_form" action="">
						<div id="recaptcha" class="g-recaptcha" data-sitekey="6LeiPCkUAAAAADrflUCLPGARoZd1j1aYmZI0XI0g" data-callback="onSubmitReCaptcha" data-size="invisible"></div>
		                <div class="row">
		                    <div class="col-sm-6">
		                        <div class="form-group">
		                            <input type="text" class="form-control" placeholder="Имя" name="name" id="name" required>
		                        </div>
		                    </div>
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Фамилия" name="surname" id="surname" required>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input type="password" class="form-control" placeholder="Пароль" name="pwdhash" id="pwdhash" required>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input type="password" class="form-control" placeholder="Повторите пароль" name="p_password" id="p_password"  required>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text"  onkeyup="var simvols=/['А-я',':;'.,'\s','/','<>','[\]\\','#!@$%^&',''','{}?№\|_','-=+']/; if(simvols.test(this.value)) this.value=''" class="form-control" placeholder="Логин" name="login" id="login" required>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input type="email" class="form-control" placeholder="E-mail" name="email" id="email" required>
								</div>
							</div>
		                </div>

		                <div class="checkbox pad-btm text-left">
		                    <input id="offer_check" class="magic-checkbox" type="checkbox">
		                    <label for="offer_check">Я согласен с <a href="http://warstores.net/doc/oferta.doc" class="btn-link">условиями оферты</a></label>
		                </div>
		                <button id="reg_form_finish" class="btn btn-primary btn-block" type="submit">Регистрация</button>
		            </form>
		        </div>
		        <div class="pad-all">
		            Уже имеете аккаунт? <a href="/ws-shop/" class="btn-link mar-rgt">Войти</a>
		
		            <!-- <div class="media pad-top bord-top">
		                <div class="pull-right">
		                    <a href="#" class="pad-rgt"><i class="demo-psi-facebook icon-lg text-primary"></i></a>
		                    <a href="#" class="pad-rgt"><i class="demo-psi-twitter icon-lg text-info"></i></a>
		                    <a href="#" class="pad-rgt"><i class="demo-psi-google-plus icon-lg text-danger"></i></a>
		                </div>
		                <div class="media-body text-left text-muted">
		                    Sign Up with
		                </div>
		            </div> -->
		        </div>
		    </div>
		</div>
		<!--===================================================-->
		
		
	</div>
	<!--===================================================-->
	<!-- END OF CONTAINER -->
<script src="/ws-shop/js/md5.min.js"></script>
<script>
	$( '#reg_form_finish' ).click(function(event) {
		event.preventDefault();
        if ($("#name").val() == "") {
            alert("Введенные имя!");
            return false;
        }
        if ($("#surname").val() == "") {
            alert("Введенные фамилию!");
            return false;
        }
        if ($("#pwdhash").val() == "") {
            alert("Введенные пароль!");
            return false;
        }
        if ($("#p_password").val() == "") {
            alert("Введенные подтверждение пароля!");
            return false;
        }
        if ($("#login").val() == "") {
            alert("Введенные логин!");
            return false;
        }
        if ($("#email").val() == "") {
            alert("Введенные email!");
            return false;
        }
		if ($("#pwdhash").val() !== $("#p_password").val()) {
			alert("Введенные пароли не совпадают!");
			return false;
		}
        if($("#offer_check").prop("checked") == false) {
            alert('Подтвердите согласие с условиями.');
            return false;
        }
		grecaptcha.execute();
	});
    function onSubmitReCaptcha(token) {
        $("#reg_form").hide();
        $(".pad-all").hide();
        $("#pwdhash").val(md5($("#pwdhash").val()));
        $("#p_password").val("");

        var formData  = new FormData($( '#reg_form' )[0]);

        $.ajax({
            url: 'http://warstores.net/ws_srv/singup.php',
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                if (data.indexOf('Логин') != -1) {
                    $("#after").text("Ошибка! Такой логин уже занят");
                    $("#after").show();
                } else if(data.indexOf('Email') != -1) {
                    $("#after").text("Ошибка! Такой email уже занят");
                    $("#after").show();
                } else {
                    console.log(data);
                    $("#after").show();
                }

            }
        });
    }
</script>


		</body>
</html>
