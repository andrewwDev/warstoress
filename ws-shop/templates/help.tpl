﻿{include file='header.tpl'}

{if $login}
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Помощь</h1>

                    <!--Searchbox-->
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    <div class="fixed-fluid">
                        <p>Найти ответы на все интересующие Вас вопросы можно в нашей группе: <a href="https://vk.com/warstores" class="pad-rgt" target="_blank" style="text-decoration: underline;">WAR STORES</a></p>
					    {*<div class="fixed-sm-200 fixed-md-250 pull-sm-left">*}
					        {*<div class="panel">*}
					            {*<div class="pad-all bord-btm">*}
					                {*<a href="mailbox-compose.html" class="btn btn-block btn-success">Compose Mail</a>*}
					            {*</div>*}
					{**}
					            {*<p class="pad-hor mar-top text-main text-bold">Folders</p>*}
					            {*<div class="list-group bg-trans pad-btm bord-btm">*}
					                {*<a href="#" class="list-group-item text-semibold text-main">*}
					                    {*<span class="badge badge-success pull-right">7</span>*}
					                    {*<span class="text-main"><i class="demo-pli-inbox-full icon-lg icon-fw"></i> Inbox</span>*}
					                {*</a>*}
					                {*<a href="#" class="list-group-item">*}
					                    {*<i class="demo-pli-file icon-lg icon-fw"></i>*}
					                    {*Draft*}
					                {*</a>*}
					                {*<a href="#" class="list-group-item">*}
					                    {*<i class="demo-pli-mail-send icon-lg icon-fw"></i>*}
					                    {*Sent*}
					                {*</a>*}
					                {*<a href="#" class="list-group-item text-semibold">*}
					                    {*<span class="badge badge-danger pull-right">3</span>*}
					                    {*<span class="text-main"><i class="demo-pli-fire-flame-2 icon-lg icon-fw"></i> Spam</span>*}
					                {*</a>*}
					                {*<a href="#" class="list-group-item">*}
					                    {*<i class="demo-pli-recycling icon-lg icon-fw"></i>*}
					                    {*Trash*}
					                {*</a>*}
					            {*</div>*}
					{**}
					            {*<div class="list-group bg-trans pad-ver">*}
					                {*<a href="#" class="list-group-item"><i class="demo-pli-male-female icon-lg icon-fw"></i> Address Book</a>*}
					                {*<a href="#" class="list-group-item"><i class="demo-pli-folder-with-document icon-lg icon-fw"></i> User Folders</a>*}
					            {*</div>*}
					{**}
					            {*<!-- Friends -->*}
					            {*<div class="list-group bg-trans pad-ver bord-ver">*}
					                {*<p class="pad-hor text-main text-bold"><span class="pull-right label label-info">New</span>Friends</p>*}
					{**}
					                {*<!-- Menu list item -->*}
					                {*<a href="#" class="list-group-item list-item-sm">*}
					                    {*<span class="badge badge-purple badge-icon badge-fw pull-left"></span>*}
					                    {*Joey K. Greyson*}
					                {*</a>*}
					                {*<a href="#" class="list-group-item list-item-sm">*}
					                    {*<span class="badge badge-info badge-icon badge-fw pull-left"></span>*}
					                    {*Andrea Branden*}
					                {*</a>*}
					                {*<a href="#" class="list-group-item list-item-sm">*}
					                    {*<span class="badge badge-pink badge-icon badge-fw pull-left"></span>*}
					                    {*Lucy Moon*}
					                {*</a>*}
					                {*<a href="#" class="list-group-item list-item-sm">*}
					                    {*<span class="badge badge-success badge-icon badge-fw pull-left"></span>*}
					                    {*Johny Juan*}
					                {*</a>*}
					                {*<a href="#" class="list-group-item list-item-sm">*}
					                    {*<span class="badge badge-danger badge-icon badge-fw pull-left"></span>*}
					                    {*Susan Sun*}
					                {*</a>*}
					            {*</div>*}
					{**}
					            {*<p class="pad-hor text-main text-bold">Labels</p>*}
					            {*<ul class="list-inline mar-hor">*}
					                {*<li class="tag tag-sm">*}
					                    {*<a href="#"><i class="demo-pli-tag"></i> Family</a>*}
					                {*</li>*}
					                {*<li class="tag tag-sm">*}
					                    {*<a href="#"><i class="demo-pli-tag"></i> Home</a>*}
					                {*</li>*}
					                {*<li class="tag tag-sm">*}
					                    {*<a href="#"><i class="demo-pli-tag"></i> Work</a>*}
					                {*</li>*}
					                {*<li class="tag tag-sm">*}
					                    {*<a href="#"><i class="demo-pli-tag"></i> Film</a>*}
					                {*</li>*}
					                {*<li class="tag tag-sm">*}
					                    {*<a href="#"><i class="demo-pli-tag"></i> Music</a>*}
					                {*</li>*}
					                {*<li class="tag tag-sm">*}
					                    {*<a href="#"><i class="demo-pli-tag"></i> Photography</a>*}
					                {*</li>*}
					            {*</ul>*}
					        {*</div>*}
					    {*</div>*}
					    {*<div class="fluid">*}
					        {*<!-- COMPOSE EMAIL -->*}
					        {*<!--===================================================-->*}
					        {*<div class="panel panel-default panel-left">*}
					            {*<div class="panel-body">*}
					                {*<div class="mar-btm pad-btm clearfix">*}
					                    {*<!--Cc & bcc toggle buttons-->*}
					                    {*<div class="pull-right pad-btm">*}
					                        {*<div class="btn-group">*}
					                            {*<button id="demo-toggle-cc" data-toggle="button" type="button" class="btn btn-sm btn-default btn-active-info">Cc</button>*}
					                            {*<button id="demo-toggle-bcc" data-toggle="button" type="button" class="btn btn-sm btn-default btn-active-info">Bcc</button>*}
					                        {*</div>*}
					                    {*</div>*}
					{**}
					                    {*<h1 class="page-header text-overflow">*}
					                        {*Compose Message*}
					                    {*</h1>*}
					                {*</div>*}
					{**}
					{**}
					{**}
					                {*<!--Input form-->*}
					                {*<form role="form" class="form-horizontal">*}
					                    {*<div class="form-group">*}
					                        {*<label class="col-lg-1 control-label text-left" for="inputEmail">To</label>*}
					                        {*<div class="col-lg-11">*}
					                            {*<input type="email" id="inputEmail" class="form-control">*}
					                        {*</div>*}
					                    {*</div>*}
					                    {*<div id="demo-cc-input" class="hide form-group">*}
					                        {*<label class="col-lg-1 control-label text-left" for="inputCc">Cc</label>*}
					                        {*<div class="col-lg-11">*}
					                            {*<input type="text" id="inputCc" class="form-control">*}
					                        {*</div>*}
					                    {*</div>*}
					                    {*<div id="demo-bcc-input" class="hide form-group">*}
					                        {*<label class="col-lg-1 control-label text-left" for="inputBcc">Bcc</label>*}
					                        {*<div class="col-lg-11">*}
					                            {*<input type="text" id="inputBcc" class="form-control">*}
					                        {*</div>*}
					                    {*</div>*}
					                    {*<div class="form-group">*}
					                        {*<label class="col-lg-1 control-label text-left" for="inputSubject">Subject</label>*}
					                        {*<div class="col-lg-11">*}
					                            {*<input type="text" id="inputSubject" class="form-control">*}
					                        {*</div>*}
					                    {*</div>*}
					                {*</form>*}
					{**}
					{**}
					                {*<!--Attact file button-->*}
					                {*<div class="media pad-btm">*}
					                    {*<div class="media-left">*}
					                        {*<span class="btn btn-default btn-file">*}
					                            {*Attachment <input type="file">*}
					                        {*</span>*}
					                    {*</div>*}
					                    {*<div id="demo-attach-file" class="media-body"></div>*}
					                {*</div>*}
					{**}
					{**}
					                {*<!--Wysiwyg editor : Summernote placeholder-->*}
					                {*<div id="demo-mail-compose"></div>*}
					{**}
					                {*<div class="pad-ver">*}
					{**}
					                    {*<!--Send button-->*}
					                    {*<button id="mail-send-btn" type="button" class="btn btn-primary">*}
					                        {*<i class="demo-psi-mail-send icon-lg icon-fw"></i> Send Mail*}
					                    {*</button>*}
					{**}
					                    {*<!--Save draft button-->*}
					                    {*<button id="mail-save-btn" type="button" class="btn btn-default">*}
					                        {*<i class="demo-pli-mail-unread icon-lg icon-fw"></i> Save Draft*}
					                    {*</button>*}
					{**}
					                    {*<!--Discard button-->*}
					                    {*<button id="mail-discard-btn" type="button" class="btn btn-default">*}
					                        {*<i class="demo-pli-mail-remove icon-lg icon-fw"></i> Discard*}
					                    {*</button>*}
					                {*</div>*}
					            {*</div>*}
					        {*</div>*}
					        {*<!--===================================================-->*}
					        {*<!-- END COMPOSE EMAIL -->*}
					    {*</div>*}
					</div>

				
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
{include file='aside.tpl'}
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->

			<nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->
        </div>



        <!-- FOOTER -->
        <!--===================================================-->
        <footer id="footer">

            <!-- Visible when footer positions are fixed -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="show-fixed pull-right">
                You have <a href="#" class="text-bold text-main"><span class="label label-danger">3</span> pending action.</a>
            </div>



            <!-- Visible when footer positions are static -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="hide-fixed pull-right pad-rgt">
                14GB of <strong>512GB</strong> Free.
            </div>



            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

            <p class="pad-lft">&#0169; 2016 Your Company</p>



        </footer>
        <!--===================================================-->
        <!-- END FOOTER -->


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}
