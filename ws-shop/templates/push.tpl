﻿{include file='header.tpl'}

{if $login}
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Настройки</h1>

                    <!--Searchbox-->
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                 <div class="col-lg-6">
					        <!--Panel with Footer-->
					            <!--===================================================-->
					            <div class="panel">
					                <div class="panel-heading">
					                    <h3 class="panel-title">
					                       Отправить Push-сообщение
					                    </h3>
					                </div>
					                <div class="panel-body">
					                   <input type="text" class="form-control" style="width:300px; height:70px;">
                                            <div class="col-sm-offset-3 col-sm-9">
					                            <input id="demo-form-checkbox" class="magic-checkbox" type="checkbox">
					                            <label for="demo-remember-me-2">Отправить__новым клиентам</label>
					                        </div>
                                            <div class="col-sm-offset-3 col-sm-9">
					                            <input id="demo-form-checkbox-2" class="magic-checkbox" type="checkbox">
					                            <label for="demo-remember-me-2">Отправить__старым клиентам</label>
					                        </div>
                                            <div class="col-sm-offset-3 col-sm-9">
					                            <input id="demo-form-checkbox-3" class="magic-checkbox" type="checkbox">
					                            <label for="demo-remember-me-2">Отправить__всем клиентам</label>
					                        </div>
                                           
					                </div>
					                <div class="panel-footer">
                                        <div>Срок жизни Push<input type="text" class="form-control" style="width:50px;"></div>
                                        <div>Бонусов клиенту<input type="text" class="form-control" style="width:50px; "></div>
                                            И того ... бонусов
                                        <button class="btn btn-success" style="float:right; margin-top:-2% ">Отправить</button>
                                    </div>
					            </div>
					            <!--===================================================-->
					            <!--End Panel with Footer-->
                                <div class="timeline">
                                <div class="timeline-stat">
                                     <div class="timeline-icon"></div>
					                <div class="timeline-icon"><i class="demo-pli-mail icon-2x"></i></div>
					                <div class="timeline-time">15:45</div>
					            </div>
                                <div class="timeline-label">
					                    <p class="mar-no pad-btm"><a href="#" class="btn-link text-main text-bold">Отправка всем клиентам</a> сроки жизни до  <a href="#" class="text-semibold"><i>17.07.2017</i></a></p>
					                    <blockquote class="bq-sm bq-open mar-no">Новые Тапули только в Вашем магазине.Прикольные и крутые тапули.</blockquote>
					            </div>
                        </div>
					
                </div>
                <!--===================================================-->
                <!--End page content-->

                 







                <!--Default Tabs (Right Aligned)-->
					        <!--===================================================-->
					        
					
					            <!--Nav tabs-->
					            <div class="nav nav-tabs tabs-right">
					                <!--Bordered Accordion-->
					        <!--===================================================-->

                              <div class="col-lg-6">
					        <div class="panel"  style="margin-left:-1% ; margin-right:8%;" >
					            <div class="panel-heading"> 
                                    <div class="panel-control">
					                    <button id="demo-panel-network-refresh" data-toggle="panel-overlay" data-target="#demo-panel-network" class="btn"><i class="demo-pli-repeat-2 icon-lg"></i></button>
					                    <div class="btn-group">
					                        <button class="dropdown-toggle btn" data-toggle="dropdown" aria-expanded="false"><i class="demo-pli-gear icon-lg"></i></button>
					                        <ul class="dropdown-menu dropdown-menu-right">
					                            <li><a href="#">Неделя</a></li>
					                            <li><a href="#">Месяц</a></li>
					                            <li><a href="#">Год</a></li>
					                            <li class="divider"></li>
					                            <li><a href="#">За всё время</a></li>
					                        </ul>
					                    </div>
					                </div>
					                <h3 class="panel-title">Информация об отправке</h3>
					            </div>
					            <div class="panel-body">
					
					                <!--Morris Area Chart placeholder-->
					                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
					                <div id="demo-morris-area" style="height:212px"></div>
					                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
					
					            </div>
					        </div>
					    </div>






					        <div class="panel-group accordion" id="demo-acc-info-outline">
					            <div class="panel panel-bordered panel-info" style="margin-right:4%; " >
					
					                <!--Accordion title-->
					                <div class="panel-heading">
					                    <h4 class="panel-title">
					                        <a data-parent="#demo-acc-info-outline" data-toggle="collapse" href="#demo-acd-info-outline-1">Информация</a>
					                    </h4>
					                </div>
					
					                <!--Accordion content-->
					                <div class="panel-collapse collapse in" id="demo-acd-info-outline-1">
					                    <div class="panel-body">
					                      В зависимости от выбранного вами процента вознаграждения пользователю добавляется реферальное вознаграждение.
                                          Тириф можно настроить исходя из  процента который вы хотите дарить вашему клиенту  или из общего процента затрат.
                                          Обратите внимание, весь остаток от партнёрской программы является платой  за привличенного к вам клиента 
					                    </div>
					                </div>
					            </div>
					        </div>



                         
					            </div>
					            <!--===================================================-->
					            <!-- End Form wizard with Validation -->
					            </div>
					
					            
					        
					        <!--===================================================-->
					        <!--End Default Tabs (Right Aligned)-->






     
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
{include file='aside.tpl'}
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



        <!-- FOOTER -->
        <!--===================================================-->
        <footer id="footer">

            <!-- Visible when footer positions are fixed -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="show-fixed pull-right">
                You have <a href="#" class="text-bold text-main"><span class="label label-danger">3</span> pending action.</a>
            </div>



            <!-- Visible when footer positions are static -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="hide-fixed pull-right pad-rgt">
                14GB of <strong>512GB</strong> Free.
            </div>



            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

            <p class="pad-lft">&#0169; 2016 Your Company</p>



        </footer>
        <!--===================================================-->
        <!-- END FOOTER -->


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}
