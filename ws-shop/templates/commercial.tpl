﻿{include file='header.tpl'}

{if $login}
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Коммерческое предложение</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->
                
                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
				
							<div class="panel panel-primary">
					
					            <!--Panel heading-->
					            <div class="panel-heading">

					                <h3 class="panel-title">Скачать рекламные коммерческие предложения</h3>
					            </div>
					
					            <!--Panel body-->
					            <div class="panel-body">
					
					                <!--Tabs content-->
					                <div class="tab-content">
					                        <p class="text-main text-lg mar-no">Брошюра</p>
											КП создано для информационного характера. Ознакомиться может любой пользователь сервиса, так как мы за прозрачность. Агенту может самостоятельно распечатать данные педложения для работы.
											<br />											
											Для скачивания нажмите правую кнопку мыши и выберите "Сохранить изображение".
											<br />
											<br />
											<br />
											<a href="img/addoc/A4-vert1.jpg" target="_blank"><img src="img/addoc/A4-vert1.jpg" width="350px"></a>   
											<a href="img/addoc/A4-vert2.jpg" target="_blank"><img src="img/addoc/A4-vert2.jpg" width="350px"></a>    
											<br />
											<br />
											<br />
											<a href="img/addoc/A4gor.jpg" target="_blank"><img src="img/addoc/A4gor.jpg" width="350px"></a>    
											<a href="img/addoc/A4gor2.jpg" target="_blank"><img src="img/addoc/A4gor2.jpg" width="350px"></a>
					                </div>
					            </div>
					        </div>
				
				
				</div>
                     <!--===================================================-->
                <!--End page content-->

     
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
{include file='aside.tpl'} 
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



       <!-- FOOTER -->
				<!--===================================================-->
					<nav id="footer2-container">
						<div id="footer2">
						{include file='footer2.tpl'}
						</div>
					</nav>
				<!--===================================================-->
				<!--END FOOTER -->	


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}
