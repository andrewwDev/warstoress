﻿
<center style="width: 100%;table-layout: fixed;">
 <table align="center" style="border-collapse: collapse;border-spacing: 0;color: #222222;max-width: 580px;width: 100%;">
  <tbody>
  <tr style="text-align: left;" align="left">
  <td style="text-align: left;vertical-align: top;padding: 0 0 10px;" align="left" valign="top">
	<table style="border-collapse: collapse;border-spacing: 0;color: #222222;width: 100%;">
	 <tbody>
	 <tr style="text-align: left;" align="left">
	  <td style="text-align: center;vertical-align: top;font-size: 0;padding: 0;" align="center" valign="top">
	  <div style="vertical-align: middle;display: inline-block;text-align: center;width: 100%;max-width: 240px;margin: 0;padding: 0;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;" align="center">
      <div lang="l-align_left" style="margin: 0;padding: 0;">
	  <a href="http://warstores.net">
	  <img src="http://warstores.net/wp-content/uploads/2016/12/cropped-ws.jpg" height="72" alt="WarStores" style="border: 0;">
	  </a> </div>
	  </div>
 
	  <div style="vertical-align: middle;display: inline-block;text-align: center;width: 100%;max-width: 340px;margin: 0;padding: 10px 0;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;" align="center">
	  <div lang="l-align_right" style="margin: 0;padding: 0;">
	  <p style="margin: 0;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;">
	  Отчетный &nbsp;месяц<br>
	  </p>
	  </div>
	  </div>
 
	  </td>
	 </tr>
     </tbody>
	</table>
  </td>
  </tr>
<tr style="text-align: left;" align="left">
	<td style="text-align: left;vertical-align: top;background: #8bc34a;padding: 15px;" align="left" valign="top">
		<table width="100%" style="border-collapse: collapse;border-spacing: 0;color: #222222;">
			<tbody>
				<tr style="text-align: left;" align="left">
					<td style="text-align: left;vertical-align: top;background: #ffffff;padding: 25px 35px;" align="left" valign="top">
						<p style="margin: 0 0 18px;font: bold 32px/42px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;">
							Вот и месяц прошел!
						</p>
					<table style="border-collapse: collapse;border-spacing: 0;color: #222222;width: 100%;margin: 0;padding: 0;list-style: none;">
						<tbody>
							<tr style="text-align: left;" align="left">
								<td style="text-align: left;vertical-align: top;mso-line-height-rule: none;width: 22px;padding: 0 0 8px;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;" align="left" valign="top">
									<img src="{$logo_user}" alt="" style="display: block;margin: 8px 0 0;border: 0;" width=”8”>
								</td>
								<td style="text-align: left;vertical-align: top;mso-line-height-rule: none;padding: 0 0 8px;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;" align="left" valign="top">
								<strong>Немного статистики ваших дел за месяц :)</strong>
									<table style="border-collapse: collapse;border-spacing: 0;color: #222222;width: 100%;">
									<tbody>	
										<tr style="text-align: left;" align="left">
											<td style="text-align: left;vertical-align: top;background: #ededed;padding: 6px 18px;" align="left" valign="top">
											<table style="border-collapse: collapse;border-spacing: 0;color: #222222;width: 100%;">
												<tbody>
													<tr style="text-align: left;" align="left">
														<td style="text-align: center;vertical-align: top;font-size: 0;padding: 0;" align="center" valign="top">
														<div style="text-align: left;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;" >
															<p style="margin: 12px 0 6px;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;">
															Пришло клиентов: {$}<br>
															Из них новых: {$}<br>
															<hr><br>
															Потрачено бонусов: {$} <br>
															Получено бонусов: {$}<br>
															Получено рублей: {$}<br>
															<hr><br>
															Отправлено новостей: {$}<br>
															Отправлено Push: {$}<br>
															<hr><br>
															Новых друзей в команде: {$}<br>
															</p>
														</div>
 														</td>
													</tr>
												</tbody>
											</table>
											</td>
										</tr>
										<tr><td><br></td></tr>
										<tr><td><br> </td></tr>
										<tr><td><strong>Ваши действия как пользователя</strong></td></tr>
										<tr style="text-align: left;" align="left">
											<td style="text-align: left;vertical-align: top;background: #ededed;padding: 6px 18px;" align="left" valign="top">
											<table style="border-collapse: collapse;border-spacing: 0;color: #222222;width: 100%;">
												<tbody>
													<tr style="text-align: left;" align="left">
														<td style="text-align: center;vertical-align: top;font-size: 0;padding: 0;" align="center" valign="top">
														<div style="text-align: left;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;" >
															<p style="margin: 12px 0 6px;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;">
															Всего покупок: {$}<br>
															<hr><br>
															Бонусов от покупок: {$} <br>
															Реферальных бонусов: {$}<br>
															Агентских бонусов: {$}<br>
															<hr><br>
															Потрачено бонусов: {$}<br>
															Потрачено рублей: {$}<br>
															<hr><br>
															Новых друзей: {$}<br>
															Новых друзей в команде: {$}<br>
															Вы стали агентом для: {$} компаний<br>
															</p>
														</div>
 														</td>
													</tr>
												</tbody>
											</table>
											</td>
										</tr>
										<tr><td><br></td></tr>
										<tr><td><br> </td></tr>
										<tr><td><strong>Игровые действия WarStores</strong></td></tr>
										<tr style="text-align: left;" align="left">
											<td style="text-align: left;vertical-align: top;background: #ededed;padding: 6px 18px;" align="left" valign="top">
											<table style="border-collapse: collapse;border-spacing: 0;color: #222222;width: 100%;">
												<tbody>
													<tr style="text-align: left;" align="left">
														<td style="text-align: center;vertical-align: top;font-size: 0;padding: 0;" align="center" valign="top">
														<div style="text-align: left;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;" >
															<p style="margin: 12px 0 6px;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;">
															Приобретенный статус: {$}<br>
															<hr><br>
															Бонусов от захвата компании: {$addr}<br>
															Бонусов от захвата здания: {$addr}<br>
															Всего бонусов получено: {$}<br>
															<hr><br>
															Захвачено компаний: {$}<br>
															Захвачено зданий: {$}<br>
															Захвачено новых компаний: {$}<br>
															Захвачено новых зданий: {$}<br>
															</p>
														</div>
 														</td>
													</tr>
												</tbody>
											</table>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					</td>
				</tr>

			</tbody>
		</table>
	</td>
</tr>
 <tr style="text-align: left;" align="left">
	<td style="text-align: left;vertical-align: top;padding: 0;" align="left" valign="top">
		<p style="margin: 24px 0 6px;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;">
		Если у&nbsp;вас есть вопросы, вы&nbsp;можете обратиться в&nbsp; в нашу группу Вконтакте «<a href="http://vk.com/warstores" style="color: #2ba6cb;text-decoration: underline;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;" target="_blank" rel="noopener">WarStores</a>»<br>Спасибо за&nbsp;то, что вы&nbsp;с нами! Искренне ваш, WarStores.net
		</p>
	</td>
</tr>
 
 </tbody>
</table>
 </center>
