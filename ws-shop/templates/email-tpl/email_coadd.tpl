﻿
<center style="width: 100%;table-layout: fixed;">
 <table align="center" style="border-collapse: collapse;border-spacing: 0;color: #222222;max-width: 580px;width: 100%;">
  <tbody>
  <tr style="text-align: left;" align="left">
  <td style="text-align: left;vertical-align: top;padding: 0 0 10px;" align="left" valign="top">
	<table style="border-collapse: collapse;border-spacing: 0;color: #222222;width: 100%;">
	 <tbody>
	 <tr style="text-align: left;" align="left">
	  <td style="text-align: center;vertical-align: top;font-size: 0;padding: 0;" align="center" valign="top">
	  <div style="vertical-align: middle;display: inline-block;text-align: center;width: 100%;max-width: 240px;margin: 0;padding: 0;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;" align="center">
      <div lang="l-align_left" style="margin: 0;padding: 0;">
	  <a href="http://warstores.net">
	  <img src="http://warstores.net/wp-content/uploads/2016/12/cropped-ws.jpg" height="72" alt="WarStores" style="border: 0;">
	  </a> </div>
	  </div>
 
	  <div style="vertical-align: middle;display: inline-block;text-align: center;width: 100%;max-width: 340px;margin: 0;padding: 10px 0;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;" align="center">
	  <div lang="l-align_right" style="margin: 0;padding: 0;">
	  <p style="margin: 0;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;">
	  Модерация &nbsp;Компании<br>
	  </p>
	  </div>
	  </div>
 
	  </td>
	 </tr>
     </tbody>
	</table>
  </td>
  </tr>
<tr style="text-align: left;" align="left">
	<td style="text-align: left;vertical-align: top;background: #ffa726;padding: 15px;" align="left" valign="top">
		<table width="100%" style="border-collapse: collapse;border-spacing: 0;color: #222222;">
			<tbody>
				<tr style="text-align: left;" align="left">
					<td style="text-align: left;vertical-align: top;background: #ffffff;padding: 25px 35px;" align="left" valign="top">
						<p style="margin: 0 0 18px;font: bold 32px/42px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;">
							ВНИМАНИЕ!
						</p>
					<table style="border-collapse: collapse;border-spacing: 0;color: #222222;width: 100%;margin: 0;padding: 0;list-style: none;">
						<tbody>
							<tr style="text-align: left;" align="left">
								<td style="text-align: left;vertical-align: top;mso-line-height-rule: none;width: 22px;padding: 0 0 8px;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;" align="left" valign="top">
									<img src="{$logo_user}" alt="" style="display: block;margin: 8px 0 0;border: 0;" width=”8”>
								</td>
								<td style="text-align: left;vertical-align: top;mso-line-height-rule: none;padding: 0 0 8px;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;" align="left" valign="top">
								<strong>Вы подали на модерацию компанию </strong>
									<table style="border-collapse: collapse;border-spacing: 0;color: #222222;width: 100%;">
									<tbody>
										<tr style="text-align: left;" align="left">
											<td style="text-align: left;vertical-align: top;background: #ededed;padding: 6px 18px;" align="left" valign="top">
											<table style="border-collapse: collapse;border-spacing: 0;color: #222222;width: 100%;">
												<tbody>
													<tr style="text-align: left;" align="left">
														<td style="text-align: center;vertical-align: top;font-size: 0;padding: 0;" align="center" valign="top">
														<div style="vertical-align: middle;display: inline-block;text-align: center;width: 100%;max-width: 212px;margin: 0;padding: 6px 0;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;" align="center">
															<table width="100%" style="border-collapse: collapse;border-spacing: 0;color: #222222;">
																<tbody>
																	<tr style="text-align: left;" align="left">
																		<td align="left">
																		<p style="margin: 12px 0 6px;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;">
																		Компания: {$addr}<br>
																		ИНН: {$addr}<br>
																		Фактический адрес: {$addr}<br>
																		Телефон: {$addr}<br>
																		Сайт: {$addr}<br>
																		</p>
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
 														</td>
													</tr>
												</tbody>
											</table>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					
																		<p style="margin: 12px 0 6px;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;">
																		Напоминаем вам, что вам необходимо в течении 7 дней отправить соглашение о присоединение к оферте.<br>
																		В ином случае ваша Компания не будет допущена к модерации.<br>
																		Данное присоединение сделано для подтверждения реальности магазина.
																		</p>
					</td>
				</tr>

			</tbody>
		</table>
	</td>
</tr>
 <tr style="text-align: left;" align="left">
	<td style="text-align: left;vertical-align: top;padding: 0;" align="left" valign="top">
		<p style="margin: 24px 0 6px;font: 15px/24px Arial, 'Helvetica Neue', Helvetica, FreeSans, sans-serif;">
		Если у&nbsp;вас есть вопросы, вы&nbsp;можете обратиться в&nbsp; в нашу группу Вконтакте «<a href="http://vk.com/warstores" style="color: #2ba6cb;text-decoration: underline;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;" target="_blank" rel="noopener">WarStores</a>»<br>Спасибо за&nbsp;то, что вы&nbsp;с нами! Искренне ваш, WarStores.net
		</p>
	</td>
</tr>
 
 </tbody>
</table>
 </center>
