﻿{include file='header.tpl'}

{if $login}
    <div id="container" class="effect aside-float aside-bright mainnav-lg">
        
        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">
			
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                {if !empty($current_shop)}
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Статистика компании</h1>

                    <!--Searchbox-->
					<!--Searchbox-->
					<div class="searchbox" style="text-align: right;">
                        {if $moderator_flag == 0}
							<span style="background-color: blue; padding: 4px; color: #fff;">Компания на модерации</span>
                        {/if}
                        {if $moderator_flag == 1}
							<span style="background-color: green; padding: 4px; color: #fff;">Компания активна</span>
                        {/if}
                        {if $moderator_flag == 2}
							<span style="background-color: red; padding: 4px; color: #fff;">Компания не прошла модерацию</span>
                        {/if}
                        {if $moderator_flag == 3}
							<span style="background-color: black; padding: 4px; color: #fff;">Компания в архиве</span>
                        {/if}
					</div>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    
					<div class="row">
					    <div class="col-lg-7">
					
					        <!--Network Line Chart-->
					        <!--===================================================-->
					        <div id="demo-panel-network" class="panel">
					            <div class="panel-heading">
					                {*<div class="panel-control">*}
					                    {*<button id="demo-panel-network-refresh" data-toggle="panel-overlay" data-target="#demo-panel-network" class="btn"><i class="demo-pli-repeat-2 icon-lg"></i></button>*}
					                    {*<div class="btn-group">*}
					                        {*<button class="dropdown-toggle btn" data-toggle="dropdown" aria-expanded="false"><i class="demo-pli-gear icon-lg"></i></button>*}
					                        {*<ul class="dropdown-menu dropdown-menu-right">*}
					                            {*<li><a href="#">Сегодня</a></li>*}
					                            {*<li><a href="#">Неделя</a></li>*}
					                            {*<li><a href="#">Месяц</a></li>*}
					                            {*<li class="divider"></li>*}
					                            {*<li><a href="#">За всё время</a></li>*}
					                        {*</ul>*}
					                    {*</div>*}
					                {*</div>*}
					                <h3 class="panel-title">График привлечения клиентов и показатели</h3>
					            </div>
					
					            <!--Morris line chart placeholder-->
					            <script type="text/javascript">
					            	var shops_data = [];
					            	{foreach $shops_data as $key => $value}
					            		shops_data.push({
				            				"id": "{$value.id}",
											"day": "{$value.day}",
											"clients": "{$value.clients}",
											"newclients": "{$value.newclients}",
											"spend_total": "{$value.spend_total}",
											"spend_money": "{$value.spend_money}",
											"received": "{$value.received}",
											"responses": "{$value.responses}"
										});
					            	{/foreach}
					            </script>
					            <div id="morris-chart-network" class="morris-full-content" 
						            style="height: 170px;">
						            <div style="display: table; height: 100%; width: 100%;">
						            	<span style="display: table-cell; vertical-align: middle; text-align: center;">Слишком мало данных</span>
						            </div>
					            </div>
					
					            <!--Chart information-->
					            <div class="panel-body">
					                <div class="row pad-top">
					                    <div class="col-lg-12">
					                        <div class="media">
											<small> <p class="text-semibold text-main">Показатели</p></small>
					                            <div class="media-body pad-lft">
					                                <div class="media">
				                                        <div class="media-left pad-no">
					                                       <small><span class="text-2x text-semibold text-nowrap text-main">{$count_sales / 100}</span></small>
					                                    </div>
					                                    <div class="media-body">
					                                        <small><p class="mar-no">Рублей от клиентов </p></small>
					                                    </div>
					                                    <div class="media-left pad-no">
					                                       <small><span class="text-2x text-semibold text-nowrap text-main">
					                                             {$count_bonus / 100}
					                                        </span></small>
					                                    </div>
					                                    <div class="media-body">
					                                      <small><p class="mar-no">Баллов от клиентов</p></small>
					                                    </div>
                                                        <div class="media-left pad-no">
					                                      <small><span class="text-2x text-semibold text-nowrap text-main">{$count_cashback / 100}</span></small>
					                                    </div>
                                                        <div class="media-body">
					                                       <small><p class="mar-no">CashBack баллов</p></small>
					                                    </div>
                                                        {*<div class="media-left pad-no">*}
					                                         {*<small><span class="text-2x text-semibold text-nowrap text-main">452</span></small>*}
					                                    {*</div>*}
                                                        {*<div class="media-body">*}
					                                      {*<small>  <p class="mar-no">Затраты Push</p> </small></p>*}
					                                    {*</div>*}
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					
					                    {*<div class="col-lg-4">*}
					                         {*<small><p class="text-semibold text-main">Итоговая прибыль</p> </small>    *}
					                        {*<ul class="list-unstyled">*}
					                            {*<li>*}
					                                {*<div class="media">*}
					                                    {*<div class="media-left pad-no">*}
					                                       {*<small>  <span class="text-2x text-semibold text-main">6</span> </small>    *}
					                                    {*</div>*}
					                                    {*<div class="media-body">*}
					                                       {*<small>  <p class="mar-no">Рублей</p> </small>    *}
					                                    {*</div>*}
                                                         {*<div class="media-left pad-no">*}
					                                       {*<small>  <span class="text-2x text-semibold text-main">0</span> </small>    *}
					                                    {*</div>*}
					                                    {*<div class="media-body">*}
					                                       {*<small>  <p class="mar-no">Баллов</p> </small>    *}
					                                    {*</div>*}
					                                {*</div>*}
                                                    {**}
					                            {*</li>*}
					                            {**}
					                        {*</ul>*}
					                    {*</div>*}
					                </div>
					            </div>
					
					
					        </div>
					        <!--===================================================-->
					        <!--End network line chart-->
					
					    </div>
					    <div class="col-lg-5">
					        <div class="row">
					            <div class="col-sm-6 col-lg-6">
					            <script type="text/javascript">
					            	var movement_funds = [];
					            	{foreach $movement_funds as $key => $value}
					            		movement_funds.push({{$value.summ}});
					            	{/foreach}
					            </script>
					
					                <!--Sparkline Area Chart-->
					                <div class="panel panel-success panel-colorful">
					                    <div class="pad-all">
					                        <p class="text-lg text-semibold"><i class="demo-pli-data-storage icon-fw"></i>Движение баллов</p>
					                        <p class="mar-no">
					                            <span class="pull-right text-bold">{$move_bonus_down / 100}</span>
					                            Потраченные
					                        </p>
					                        <p class="mar-no">
					                            <span class="pull-right text-bold">{$move_bonus_up / 100}</span>
					                            Полученные
					                        </p>
					                    </div>
					                    <div class="pad-all text-center">
					                        <!--Placeholder-->
					                        <div id="demo-sparkline-area"></div>
					                    </div>
					                </div>
					            </div>
					            <div class="col-sm-6 col-lg-6">
					
					                <!--Sparkline Line Chart-->
					                <div class="panel panel-info panel-colorful">
					                    <div class="pad-all">
					                        <p class="text-lg text-semibold"><i class="demo-pli-wallet-2 icon-fw"></i>Прибыль</p>
					                        <p class="mar-no">
					                            <span class="pull-right text-bold">{$count_sales / 100}</span>
					                            От компании
					                        </p>
					                        <p class="mar-no">
					                            <span class="pull-right text-bold">{$profit_from_team / 100}</span>
					                            От команды
					                        </p>
					                    </div>
					                    {*<div class="pad-all text-center">*}
					{**}
					                        {*<!--Placeholder-->*}
					                        {*<div id="demo-sparkline-line"></div>*}
					{**}
					                    {*</div>*}
					                </div>
					            </div>
					        </div>
					        <div class="row">
					            <div class="col-sm-6 col-lg-6">
					            <script type="text/javascript">
					            	var sales = [];
					            	{foreach $all_sales as $key => $value}
					            		sales.push({{$value.summ}});
					            	{/foreach}
					            </script>
					
					                <!--Sparkline bar chart -->
					                <div class="panel panel-purple panel-colorful">
					                    <div class="pad-all">
					                        <p class="text-lg text-semibold"><i class="demo-pli-bag-coins icon-fw"></i>Продажи</p>
					                        <p class="mar-no">
					                            <span class="pull-right text-bold">{$num_sales}</span>
					                            Кол-во продаж
					                        </p>
					                    </div>
					                    <div class="pad-all text-center">
					
					                        <!--Placeholder-->
					                        <div id="demo-sparkline-bar" class="box-inline"></div>
					
					                    </div>
					                </div>
					            </div>
					            <div class="col-sm-6 col-lg-6">
					
					                <!--Sparkline pie chart -->
					                <div class="panel panel-warning panel-colorful">
					                    <div class="pad-all">
					                        <p class="text-lg text-semibold"><i class="demo-pli-check icon-fw"></i>Баланс</p>
					                        <p class="mar-no">
					                            <span class="pull-right text-bold">{$move_bonus_down / 100}</span>
					                            Всего потрачено
					                        </p>
					                        <p class="mar-no">
					                            <span class="pull-right text-bold">{$move_bonus_up / 100}</span>
					                           Всего получено
					                        </p>
                                             <p class="mar-no">
					                            <span class="pull-right text-bold">{($move_bonus_up - $move_bonus_down) /100}</span>
					                           Остаток
					                        </p>
					                    </div>
					                    <div class="pad-all">
					                        {*<ul class="list-group list-unstyled">*}
					                            {*<li class="mar-btm">*}
					                                {*<span class="label label-warning pull-right">15%</span>*}
					                                {*<p>Progress</p>*}
					                                {*<div class="progress progress-md">*}
					                                    {*<div style="width: 15%;" class="progress-bar progress-bar-light">*}
					                                        {*<span class="sr-only">15%</span>*}
					                                    {*</div>*}
					                                {*</div>*}
					                            {*</li>*}
					                        {*</ul>*}
					                    </div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
					
					<div class="row">
					   
					    
					    </div>
					    
					    <div class="row">
					    <div class="col-xs-12">
					        <div class="panel">
					            <div class="panel-heading">
					                <h3 class="panel-title">Детализированный список</h3>
					            </div>
					
					            <!--Data Table-->
					            <!--===================================================-->


					            <div class="panel-body">
									{if $sales}
					                <div class="pad-btm form-inline">
					                    <div class="row">
					                        <div class="col-sm-6 table-toolbar-left">
					                            <!--<button class="btn btn-purple"><i class="demo-pli-add icon-fw"></i>Add</button>-->
					                            <button class="btn btn-default"><i class="demo-pli-printer"></i></button>
					                            <!--<div class="btn-group">
					                                <button class="btn btn-default"><i class="demo-pli-information"></i></button>
					                                <button class="btn btn-default"><i class="demo-pli-recycling"></i></button>
					                            </div>-->
					                        </div>
					                    </div>
					                </div>
					                <div class="table-responsive">
										<table class="table table-striped">
											<thead>
											<tr>
												<th>Дата продажи</th>
												<th>Покупатель</th>
												<th>Сотрудник</th>
												<th>Cashback</th>
												<th class="text-center">Получено рублей</th>
												<th class="text-center">Получено баллов</th>
												<th class="text-center">Отзыв</th>
												<th class="text-center">Возврат</th>
											</tr>
											</thead>
											<tbody>
											{foreach $sales as $key => $value}
												<tr>
													<td><span class="text-muted"><i class="fa fa-clock-o"></i> {$value["date"]}</span></td>
													<td>{$user_arr[$value["user_id"]]}</td>
													<td>{$manager_arr[$value["manager_id"]]}</td>
													<td>{$value["cashback"]}</td>
													<td class="text-center">
														{$value["cash"]}
													</td>
													<td class="text-center">{$value["summ"] - $value["cash"]*100}</td>
													<td class="text-center">-</td>
													<td class="text-center">-</td>
												</tr>
											{/foreach}
											</tbody>
										</table>
					                </div>
					                <hr>
									{if $num_pages > 0 }
										<div class="pull-right">
											<ul class="pagination text-nowrap mar-no">
												{for $page = 1 to $num_pages}
													<li class="page-number {if $page==$active_page}active{/if}">
														<a href="./overallstatistics.php?sid=2&page={$page}">{$page}</a>
													</li>
												{/for}
					                    	</ul>
					                	</div>
									{/if}
									{else}
									<div class="row">
										<div class="col-md-12">
											<p>Операций по данной компании нет</p>
										</div>
									</div>
									{/if}
					            </div>
					            <!--===================================================-->
					            <!--End Data Table-->
					
					        </div>
					    </div>
					</div>

					</div>
					{/if}
					
					
					
					
                </div>
                <!--===================================================-->
                <!--End page content-->

                <!-- FOOTER -->
				<!--===================================================-->
					<nav id="footer2-container">
						<div id="footer2">
						{include file='footer2.tpl'}
						</div>
					</nav>
				<!--===================================================-->
				<!--END FOOTER -->	



            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->


            
            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
{include file='aside.tpl'}
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->

            
            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>

        



        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->


    
        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
    
    <!--===================================================-->
    <!-- END SETTINGS -->
{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}