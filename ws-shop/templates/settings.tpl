﻿{include file='header.tpl'}

{if $login}
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Настройки</h1>

                    <!--Searchbox-->
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                 <div class="col-lg-6">
					        <div class="panel">
					            <div class="panel-heading">
					                <h3 class="panel-title">Карточка магазина</h3>
					            </div>
					
					
					            <!-- BASIC FORM ELEMENTS -->
					            <!--===================================================-->
					            <form class="panel-body form-horizontal form-padding">
					                <!--First tab-->
					                            <div id="demo-bv-tab1" class="tab-pane">
					                                <div class="form-group">
					                                    <label class="col-lg-3 control-label">Юридическое название</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" class="form-control" name="shop_jtitle" id="shop_jtitle" placeholder="Юридическое название компании" value="{$row.jtitle}">
					                                    </div>
					                                </div>
													<div class="form-group">
														<label class="col-lg-3 control-label">Хэштэг компании<span class="red">*</span></label>
					                       				<div class="col-lg-7">
					                            			<input type="text" onkeyup="var simvols=/['А-я',':;'.,'\s','/','<>','[\]\\','#!@$%^&',''','{}?№\|_','-=+']/; if(simvols.test(this.value)) this.value=''" maxlength="12" class="form-control" name="shopnick" id="shopnick" placeholder="Хэштэг компании">
														</div>
					                    			</div>
					                                <div class="form-group">
					                                    <label class="col-lg-3 control-label">ИНН</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" class="form-control" name="INN" placeholder="ИНН">
					                                    </div>
					                                </div>
                                                     <div class="form-group">
					                                    <label class="col-lg-3 control-label">ОГРН</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" class="form-control" name="OGRN" placeholder="ОГРН">
					                                    </div>
					                                </div>
                                                     <div class="form-group">
					                                    <label class="col-lg-3 control-label">Телефон для связи</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" class="form-control" name="..." placeholder="Телефон для связи">
					                                    </div>
					                                </div>
                                                    <div class="form-group">
					                                    <label class="col-lg-3 control-label">Юридический адрес</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" class="form-control" name="..." placeholder="Юридический адрес">
					                                    </div>
					                                </div>
                                                    <div class="form-group">
					                                    <label class="col-lg-3 control-label">У меня есть группа Вконтакте</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" class="form-control" name="..." placeholder="id группы Вконтакте">
					                                    </div>
					                                </div>
                                                    <div class="form-group">
					                                    <label class="col-lg-3 control-label">Доставка товаров по городу</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" class="form-control" name="..." placeholder="Доставка товаров по городу">
					                                    </div>
					                                </div>
					                            </div>

                                                <!--Second tab-->

					                                <div class="form-group">
					                                    <label class="col-lg-3 control-label">Название</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" class="form-control" value="{$row.title}" id="shop_title" placeholder="Введите Название" >
					                                    </div>
					                                </div>
					                                <div class="form-group">
					                                    <label class="col-lg-3 control-label">Краткое описание</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" placeholder="Краткое описание" name="lastName" class="form-control">
					                                    </div>
					                                </div>
                                                    <div class="form-group">
					                                    <label class="col-lg-3 control-label">Рубрика</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" placeholder="Рубрика" name="lastName" class="form-control">
					                                    </div>
					                                </div>
                                                    <div class="form-group">
					                                    <label class="col-lg-3 control-label">Подробное описание</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" placeholder="Подробное описание" name="lastName" class="form-control">
					                                    </div>
					                                </div>
                                                    <div class="form-group">
					                                    <label class="col-lg-3 control-label">Время работы</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" placeholder="Время работы" name="lastName" class="form-control">
					                                    </div>
                                                    </div>
                                                    <div class="form-group">
					                                    <label class="col-lg-3 control-label">Адрес</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" placeholder="Город,Улица,Дом" name="lastName" class="form-control">
					                                    </div>
					                                </div>
                                                      <div class="form-group">
					                                    <label class="col-lg-3 control-label">Телефон для связи</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" placeholder="Телефон для связи" name="lastName" class="form-control">
					                                    </div>
					                                </div>
                                                    <div class="form-group">
					                                    <label class="col-lg-3 control-label">Фотографии</label>
					                                    <div class="col-lg-7">
					                                        <td><img class="img-md" src="img/profile-photos/1.png" alt="Sample Image"></td>
                                                            <td><img class="img-md" src="img/profile-photos/1.png" alt="Sample Image"></td>
                                                            <div class="bord-top pad-ver">
                                                                 <span class="btn btn-success fileinput-button dz-clickable">
					                                                <i class="fa fa-plus"></i>
					                                                <span>Add files...</span>
					                                            </span>     
                                                            </div>                                                            
					                                    </div>
					                                </div>
                                                    <div class="form-group">
					                                    <label class="col-lg-3 control-label">Сайт</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" placeholder="Сайт" name="lastName" class="form-control">
					                                    </div>
					                                </div>


                                                    
                                                    
					                                <div class="col-xs-6">
					                                    <p class="text-main mar-btm">Клиенту</p>
					
					                                    <!--Default Range Slider-->
					                                    <!--===================================================-->
					                                    <div id="demo-range-def"></div>
					                                    <!--===================================================-->
					
					                                    <br>
					                                    <div>
					                                        <strong>Процент от чека: </strong>
					                                        <span id="demo-range-def-val"></span>
					                                    </div>
					                                </div>
                                                    <div class="col-xs-6">
					                                    <p class="text-main mar-btm">Партнёрская программа</p>
					
					                                    <!--Default Range Slider-->
					                                    <!--===================================================-->
					                                    <div id="demo-range-def"></div>
					                                    <!--===================================================-->
					
					                                    <br>
					                                    <div>
					                                        <strong>Процент от чека: 2% </strong>
					                                        <!--<span id="demo-range-def-val"></span> -->
					                                    </div>
					                                </div>
                                                     <div class="col-xs-6">
					                                    <p class="text-main mar-btm">Итого</p>
					
					                                    <!--Default Range Slider-->
					                                    <!--===================================================-->
					                                    <div id="demo-range-def"></div>
					                                    <!--===================================================-->
					
					                                    <br>
					                                    <div>
					                                        <strong>Процент от чека: </strong>
					                                       <span id="demo-range-step-val">20.00</span>
					                                    </div>
					                                </div>
                                                     <div class="col-xs-6">
					                                    <p class="text-main mar-btm">Клиенту</p>
					
					                                    <!--Default Range Slider-->
					                                    <!--===================================================-->
					                                    <div id="demo-range-def"></div>
					                                    <!--===================================================-->
					                                    <br>
                                                     </div>
                                                     <div class="col-xs-6">
					                                    <p class="text-main mar-btm"> Вознаграждение (фиксированное) сотруднику за подключение клиента</p>
					
					                                    <!--Default Range Slider-->
					                                    <!--===================================================-->
					                                    <div id="demo-range-def"></div>
					                                    <!--===================================================-->
					
					                                    <br>
					                                    <div>
					                                        <strong>Процент от чека: </strong>
					                                        <span id="demo-range-def-val"></span>
					                                    </div>
					                                </div>

                                                    <div class="col-xs-6">
					                                    <p class="text-main mar-btm">  Вознаграждение (фиксированное) сотруднику за выполнение операции (проводка чека)</p>
					
					                                    <!--Default Range Slider-->
					                                    <!--===================================================-->
					                                    <div id="demo-range-def"></div>
					                                    <!--===================================================-->
					
					                                    <br>
					                                    <div>
					                                        <strong>Процент от чека: </strong>
					                                        <span id="demo-range-def-val"></span>
					                                    </div>
					                                </div>
                                                    <div class="col-xs-6">
					                                    <p class="text-main mar-btm"> Процент от покупки, который можно оплатить бонусами</p>
					
					                                    <!--Default Range Slider-->
					                                    <!--===================================================-->
					                                    <div id="demo-range-def"></div>
					                                    <!--===================================================-->
					
					                                    <br>
					                                    <div>
					                                        <strong>Процент от чека: </strong>
					                                        <span id="demo-range-def-val"></span>
					                                    </div>
					                                </div>
                                                    <div class="col-xs-6">
					                                    <p class="text-main mar-btm">Максимальное число бонусов, которые можно отдать за покупку (в копейках)</p>
					
					                                    <!--Default Range Slider-->
					                                    <!--===================================================-->
					                                    <div id="demo-range-def"></div>
					                                    <!--===================================================-->
					
					                                    <br>
					                                    <div>
					                                        <strong>Процент от чека: </strong>
					                                        <span id="demo-range-def-val"></span>
					                                    </div>
					                                </div>
					                                
					                              <!--Footer button-->
					                    
					                         <div class="text-right">
                                                 <div class="box-inline">
					                            <button type="button" class="finish btn btn-warning" disabled>Finish</button>
                                            </div>
					                    </div>
					                        <!-- FINISH FOOTER -->                                                    

					            </form>
					            <!--===================================================-->
					            <!-- END BASIC FORM ELEMENTS -->


					
					</div>
					
                </div>
                <!--===================================================-->
                <!--End page content-->

                <!-- FOOTER -->
				<!--===================================================-->
					<nav id="footer2-container">
						<div id="footer2">
						{include file='footer2.tpl'}
						</div>
					</nav>
				<!--===================================================-->
				<!--END FOOTER -->	







                <!--Default Tabs (Right Aligned)-->
					        <!--===================================================-->
					        
					
					            <!--Nav tabs-->
					            <div class="nav nav-tabs tabs-right">
					                <!--Bordered Accordion-->
					        <!--===================================================-->
					        <div class="panel-group accordion" id="demo-acc-info-outline">
					            <div class="panel panel-bordered panel-info">
					
					                <!--Accordion title-->
					                <div class="panel-heading">
					                    <h4 class="panel-title">
					                        <a data-parent="#demo-acc-info-outline" data-toggle="collapse" href="#demo-acd-info-outline-1">Дополнительная информация</a>
					                    </h4>
					                </div>
					
					                <!--Accordion content-->
					                <div class="panel-collapse collapse in" id="demo-acd-info-outline-1">
					                    <div class="panel-body">
					                      В зависимости от выбранного вами процента вознаграждения пользователю добавляется реферальное вознаграждение.
                                          Тириф можно настроить исходя из  процента который вы хотите дарить вашему клиенту  или из общего процента затрат.
                                          Обратите внимание, весь остаток от партнёрской программы является платой  за привличенного к вам клиента 
					                    </div>
					                </div>
					            </div>
					        </div>
					            </div>
					            <!--===================================================-->
					            <!-- End Form wizard with Validation -->
					            </div>
					
					            
					        
					        <!--===================================================-->
					        <!--End Default Tabs (Right Aligned)-->






     
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
{include file='aside.tpl'} 
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



     


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->

         



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}
