<!-- FOOTER -->
	<!--===================================================-->
	<footer id="footer">

		<!-- Visible when footer positions are fixed -->
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<div class="show-fixed pull-right">
			You have <a href="#" class="text-bold text-main"><span class="label label-danger">3</span> pending action.</a>
		</div>



		<!-- Visible when footer positions are static -->
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<div class="hide-fixed pull-right pad-rgt">
			Плати только за <strong>настоящих</strong> клиентов.
		</div>



		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

		<p class="pad-lft">&#0169; 2016 AdPoints</p>




	</footer>
	<!-- Yandex.Metrika counter -->
		<script type="text/javascript"> 
		  (function (d, w, c) { (w[c] = w[c] || []).push(function()
		  { try { w.yaCounter37854090 = new Ya.Metrika({ id:37854090, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true, ut:"noindex" }); } catch(e) { } }); 
		  var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; 
		  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } 
		  else { f(); } })(document, window, "yandex_metrika_callbacks"); 
		</script>
	<!-- /Yandex.Metrika counter -->
	<!--===================================================-->
	<!-- END FOOTER -->