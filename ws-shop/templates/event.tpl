﻿{include file='header.tpl'}

{if $login}
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">События</h1>

                    <!--Searchbox-->
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">

<div class="timeline two-column">
					    <!-- Timeline header -->
					    <div class="timeline-header">
					        <div class="timeline-header-title bg-success">Now</div>
					    </div>
					
					    <div class="timeline-entry">
					        <div class="timeline-stat">
					            <div class="timeline-icon">
					                <img alt="Profile picture" src="img/profile-photos/8.png">
					            </div>
					            <div class="timeline-time">30 Min ago</div>
					        </div>
					        <div class="timeline-label">
					            <p class="mar-no pad-btm">
					                <a class="btn-link text-semibold" href="#">Maria J.</a> shared an image</p>
					            <div class="img-holder">
					                <img alt="Image" src="img/thumbs/img2.jpg">
					            </div>
					        </div>
					    </div>
					    <div class="timeline-entry">
					        <div class="timeline-stat">
					            <div class="timeline-icon">
					                <i class="demo-pli-office icon-2x"></i>
					            </div>
					            <div class="timeline-time">2 Hours ago</div>
					        </div>
					        <div class="timeline-label">
					            <h4 class="mar-no pad-btm"><a class="text-danger" href="#">Job Meeting</a></h4>
					            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.
					        </div>
					    </div>
					    <div class="timeline-entry">
					        <div class="timeline-stat">
					            <div class="timeline-icon">
					                <img alt="Profile picture" src="img/profile-photos/9.png">
					            </div>
					            <div class="timeline-time">3 Hours ago</div>
					        </div>
					        <div class="timeline-label">
					            <p class="mar-no pad-btm">
					                <a class="btn-link text-semibold" href="#">Lisa D.</a> commented on
					                <a href="#" class="text-semibold">The Article</a>
					            </p>
					            <blockquote class="bq-sm bq-open mar-no">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</blockquote>
					        </div>
					    </div>
					    <div class="timeline-entry">
					        <div class="timeline-stat">
					            <div class="timeline-icon"><i class="demo-pli-twitter icon-2x"></i>
					            </div>
					            <div class="timeline-time">5 Hours ago</div>
					        </div>
					        <div class="timeline-label">
					            <img alt="Profile picture" src="img/profile-photos/3.png" class="img-xs img-circle">
					            <a class="btn-link text-semibold" href="#">Bobby Marz</a> followed you.
					        </div>
					    </div>
					    <div class="timeline-entry">
					        <div class="timeline-stat">
					            <div class="timeline-icon"><i class="demo-pli-mail icon-2x"></i>
					            </div>
					            <div class="timeline-time">15:45</div>
					        </div>
					        <div class="timeline-label">
					            <p class="text-main text-semibold">Lorem ipsum dolor sit amet</p>
					            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.
					        </div>
					    </div>
					    <div class="timeline-entry">
					        <div class="timeline-stat">
					            <div class="timeline-icon bg-success"><i class="demo-psi-like icon-lg"></i>
					            </div>
					            <div class="timeline-time">13:27</div>
					        </div>
					        <div class="timeline-label">
					            <img alt="Profile picture" src="img/profile-photos/2.png" class="img-xs img-circle">
					            <a class="btn-link text-semibold" href="#">Michael Both</a> Like <a href="#" class="text-semibold">The Article</a>
					        </div>
					    </div>
					    <div class="timeline-entry">
					        <div class="timeline-stat">
					            <div class="timeline-icon"></div>
					            <div class="timeline-time">11:27</div>
					        </div>
					        <div class="timeline-label">
					            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.
					        </div>
					    </div>
					    <div class="clearfix"></div>

                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
{include file='aside.tpl'}
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



        <!-- FOOTER -->
        <!--===================================================-->
        <footer id="footer">

            <!-- Visible when footer positions are fixed -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="show-fixed pull-right">
                You have <a href="#" class="text-bold text-main"><span class="label label-danger">3</span> pending action.</a>
            </div>



            <!-- Visible when footer positions are static -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="hide-fixed pull-right pad-rgt">
                14GB of <strong>512GB</strong> Free.
            </div>



            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

            <p class="pad-lft">&#0169; 2016 Your Company</p>



        </footer>
        <!--===================================================-->
        <!-- END FOOTER -->


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
    
    <!--===================================================-->
    <!-- END SETTINGS -->
{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}
