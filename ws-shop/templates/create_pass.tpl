{include file='header.tpl'}
<div id="container" class="cls-container">


    <!-- BACKGROUND IMAGE -->
    <!--===================================================-->
    <div id="bg-overlay"></div>


    <!-- REGISTRATION FORM -->
    <!--===================================================-->
    <div class="cls-content">
        <div class="cls-content-lg panel">
            <div class="panel-body">
                <div class="mar-ver pad-btm">
                    <h3 class="h4 mar-no">Восстановление пароля</h3>
                    <p class="text-muted">Задайте новый пароль.</p>
                </div>
                <div id="after" style="display: none;">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Новый пароль создан</h4>
                        </div>
                    </div>
                </div>
                <form id="reg_form" action="">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Введите новый пароль" name="pass" id="pass" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Подтвердите новый пароль" name="p_pass" id="p_pass" required>
                            </div>
                        </div>
                    </div>
                    <button id="reg_form_finish" class="btn btn-primary btn-block" type="submit">Применить</button>
                </form>
            </div>
            <div class="pad-all">
                <a href="/ws-shop/" class="btn-link mar-rgt">Войти</a>
            </div>
        </div>
    </div>
    <!--===================================================-->


</div>
<!--===================================================-->
<!-- END OF CONTAINER -->
<script src="/ws-shop/js/md5.min.js"></script>
<script>
    $( '#reg_form_finish' ).click(function(event) {
        event.preventDefault();
        if ($("#pass").val() == "") {
            alert("Введите пароль!");
            return false;
        }
        if ($("#p_pass").val() == "") {
            alert("Подтвердите пароль!");
            return false;
        }
        if ($("#pass").val() != $("#p_pass").val()) {
            alert("Введенные пароли не совпадают!");
            return false;
        }

        var post = {literal}{{/literal}code: "{$code}", pass: md5($("#pass").val()){literal}};{/literal}

        $("#reg_form").hide();

        $.ajax({
            url: 'backend/create_pass.php',
            data: post,
            type: 'POST',
            success: function (data) {
                //console.log(data);
                $("#after").show();
            }
        });
    });
</script>


</body>
</html>
