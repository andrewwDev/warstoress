﻿{include file='header.tpl'}

{if $login}
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Профиль</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    
					<div class="row">
					    <div class="col-lg-2">


                           <div class="fixed-fluid">
					    <div class="fixed-sm-160 fixed-lg-200 pull-sm-left" style="width:100%;">
					        <div class="panel">
					            <!-- Simple profile -->
					            <div class="text-center pad-all bord-btm">
					                <div class="pad-ver">
                                        {if $avatar}
											<img src="{$avatar}" class="img-lg img-border img-circle" alt="Profile Picture">
                                        {else}
											<img src="img/profile-photos/1.png" class="img-lg img-border img-circle" alt="Profile Picture">
                                        {/if}
					                </div>
					                <h4 class="text-lg text-overflow mar-no">{$owner_name}</h4>
					
					                <!-- <div class="pad-ver btn-groups">
					                    <a href="#" class="btn btn-hover-primary demo-pli-facebook icon-lg add-tooltip" data-original-title="Facebook" data-container="body"></a>
					                    <a href="#" class="btn btn-hover-info demo-pli-twitter icon-lg add-tooltip" data-original-title="Twitter" data-container="body"></a>
					                    <a href="#" class="btn btn-hover-danger demo-pli-google-plus icon-lg add-tooltip" data-original-title="Google+" data-container="body"></a>
					                    <a href="#" class="btn btn-hover-purple demo-pli-instagram icon-lg add-tooltip" data-original-title="Instagram" data-container="body"></a>
					                </div> -->
					            </div>
					            <div class="text-semibold text-main pad-all mar-no">Об игроке</div>
                                {*<div class="text-semibold text-main pad-all mar-no">Высший ранг</div>*}
                                {*<div id="two"><h4>&nbsp;&nbsp;&nbsp;Золотой игрок</h4></div>*}
                                <div class="pad-all">
                                 <p class="mar-no">
					                            <span class="pull-right text-bold">{$user_buildings_count}</span>
					                           Захвачено зданий
					                        </p> 
                                <p class="mar-no">
					                            <span class="pull-right text-bold">{$user_sectors_count}</span>
					                          Захвачено территорий
					                        </p>
                                 <p class="mar-no">
					                            <span class="pull-right text-bold">{$user_friends_count}</span>
					                         Друзей
					                        </p>            
                                </div>          
					        </div>
					    </div>
					    
                           </div>
					
					    </div>
						<div class="col-lg-5">
							<!--Network Line Chart-->
							<!--===================================================-->
							<div id="demo-panel-network" class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Личный счет</h3>
								</div>

								<!--Morris line chart placeholder-->
								<div class="row"
									 style="padding: 10px; text-align: center;">
									<div class="col-sm-12">
										На вашем счете: {$user_balance / 100}
									</div>
								</div>


							</div>
							<!--===================================================-->
							<!--End network line chart-->
						</div>
					    <div class="col-lg-5">
							<div class="panel panel-warning panel-colorful cobalance">
								<div class="pad-all">
									<p class="text-lg text-semibold"><i class="demo-pli-check icon-fw"></i> Баланс</p>
									<p class="mar-no">
										<span class="pull-right text-bold">{$user_transaction_out / 100}</span>
										Расход
									</p>
									<hr>
									<p class="mar-no" style="padding-left: 25px;">
										<span class="pull-right text-bold">{$user_transaction_up / 100}</span>
										Пополнение
									</p>
									<p class="mar-no" style="padding-left: 25px;">
										<span class="pull-right text-bold">{$user_transaction_ref / 100}</span>
										Реферальные
									</p>
									<p class="mar-no" style="padding-left: 25px;">
										<span class="pull-right text-bold">{$user_transaction_agent / 100}</span>
										Агентские
									</p>
									<p class="mar-no" style="padding-left: 25px;">
										<span class="pull-right text-bold">{$user_transaction_bonus / 100}</span>
										Бонусные
									</p>
									<hr>
									<p class="mar-no">
										<span class="pull-right text-bold">{$user_transaction_vyvod / 100}</span>
										Выведено
									</p>
								</div>
							</div>
					    </div>
					</div>
					

					    
					</div>
					
					<div class="row">
					    <div class="col-xs-12" style="width:100%;">
					        <div class="panel">
					            <div class="panel-heading">
					                <h3 class="panel-title">Детализация</h3>
					            </div>
					
					            <!--Data Table-->
					            <!--===================================================-->
					            <div class="panel-body">
					                <div class="table-responsive">
										{if $user_transaction != 0}
											<table class="table table-striped">
												<thead>
												<tr>
													<th>Дата операции</th>
													<th>Операция</th>
													<th>Магазин</th>
													<th>Баллы (руб.)</th>
												</tr>
												</thead>
												<tbody>
                                                {foreach $user_transaction as $item}
													<tr>

														<td><span class="text-muted">{$item["tdate"]}</span></td>
														<td>{$item["tcomment"]}</td>
														<td>{$item["tsid"]}</td>
														<td>{$item["tsumm"]/100}</td>
													</tr>
                                                {/foreach}
												</tbody>
											</table>
										{else}
											Операции отсутствуют
										{/if}

					                </div>
					            </div>
					            <!--===================================================-->
					            <!--End Data Table-->
					
					        </div>
					    </div>
					</div>
					
					
					
                </div>
                <!--===================================================-->
                <!--End page content-->

              





                <!-- FOOTER -->
				<!--===================================================-->
					<nav id="footer2-container">
						<div id="footer2">
						{include file='footer2.tpl'}
						</div>
					</nav>
				<!--===================================================-->
				<!--END FOOTER -->	




     
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
{include file='aside.tpl'} 
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



       


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->


{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}
