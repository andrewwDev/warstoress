﻿{include file='header.tpl'}

{if $login}
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed" style="
    position: absolute;
    display: block;
    height: 100%;
">


            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Рекламные буклеты</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
				
							<div class="panel panel-primary">
					
					            <!--Panel heading-->
					            <div class="panel-heading">
					                <div class="panel-control">
					
					                    <!--Nav tabs-->
					                    <ul class="nav nav-tabs">
					                        <li class="active"><a data-toggle="tab" href="#demo-tabs-box-1">Брошюра</a></li>
					                        <li><a data-toggle="tab" href="#demo-tabs-box-2">Тейблтент</a></li>
					                    </ul>
					
					                </div>
					                <h3 class="panel-title">Скачивание брошюр</h3>
					            </div>
					
					            <!--Panel body-->
					            <div class="panel-body">
					
					                <!--Tabs content-->
					                <div class="tab-content">
					                    <div id="demo-tabs-box-1" class="tab-pane fade in active">
					                        <p class="text-main text-lg mar-no">Брошюра</p>
											Брошюру рекомендуем положить рядом с кассовой зоной или распечатав с двух сторон использовать в тейблтент. Размер может быть выбран вами по желанию, однако стандартный размер А5.
											<br />											
											Для скачивания нажмите правую кнопку мыши и выберите "Сохранить изображение".
											<br />
											<br />
											<br />
											<a href="img/addoc/A5-1.jpg" target="_blank"><img src="img/addoc/A5-1.jpg" width="350px"></a> 
											<a href="img/addoc/A5-2.jpg" target="_blank"><img src="img/addoc/A5-2.jpg" width="350px"></a> 
					                    </div>
					                    <div id="demo-tabs-box-2" class="tab-pane fade">
					                        <p class="text-main text-lg mar-no">Тейблтент</p>
											Брошюру рекомендуем положить рядом с кассовой зоной или распечатав с двух сторон использовать в тейблтент. Размер может быть выбран вами по желанию, однако стандартный размер А5.
											<br />											
											Для скачивания нажмите правую кнопку мыши и выберите "Сохранить изображение".
											<br />
											<br />
											<br />
											<a href="img/addoc/tablet.jpg" target="_blank"><img src="img/addoc/tablet.jpg" width="350px"></a> 
					                    </div>
					                </div>
					            </div>
					        </div>
				
				
				</div>
                     <!--===================================================-->
                <!--End page content-->
            

     
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
{include file='aside.tpl'}
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



      <!-- FOOTER -->
		<!--===================================================-->
		<nav id="footer2-container">
			<div id="footer2">
				{include file='footer2.tpl'}
			</div>
		</nav>
		<!--===================================================-->
	<!--END FOOTER -->


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}