﻿{include file='header.tpl'}

{if $login}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Баланс компании</h1>

                    <!--Searchbox
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div> -->
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->
                {if !empty($current_shop)}
                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    
					<div class="row">
					    <div class="col-lg-8">
					
					        <!--Network Line Chart-->
					        <!--===================================================-->
					        <div id="demo-panel-network" class="panel">
					            <div class="panel-heading">
					                <div class="panel-control">
					                    
					                    <div class="btn-group"><h5>{(($move_bonus_up - $move_bonus_down) /100)} баллов на балансе компании</h5>
					                        
					                    </div>
					                </div>
					                <h3 class="panel-title">Действия со счётом </h3>
					            </div>
					
					            <!--Chart information-->
					            <div class="panel-body">
					                <div class="row">
					                    <div class="col-lg-12">
                                           <h5>Пополнить баланс через робокассу</h5>
					                       <p>{$robocassa}</p>
                                            <hr>
					                    </div>


					                    <div class="col-lg-12">
                                            <h5>Пополнить с личного счета:</h5>
                                            <div id="after_money_to_shop_from_user" style="display: none;">
                                                <p>Перевод выполнен успешно</p>
                                            </div>
                                            <form action="" id="money_to_shop_from_user">
                                                <p>Баланс личного счета: {($user_balance / 100)} балла</p>
                                                <div class="row">
                                                        <input type="hidden" name="type_fn" value="money_to_shop_from_user">
                                                        <input type="hidden" name="uid" value="{$uid}">
                                                        <input type="hidden" name="sid" value="{$shop_id}">
                                                        <div class="col-md-6"><input type="text" name="cash" class="form-control" placeholder="Количество баллов"></div>
                                                        <div class="col-md-6"><button id="sub_money_to_shop_from_user" class="btn btn-primary">Пополнить</button></div>
                                                </div>
                                            </form>
                                            <script>
                                                $( '#sub_money_to_shop_from_user' ).click(function(event) {
                                                    event.preventDefault();
                                                    var flag = confirm("Подтвердите перевод");
                                                    if (flag) {
                                                        var formData  = new FormData($( '#money_to_shop_from_user' )[0]);
                                                        $("#money_to_shop_from_user").hide();

                                                        $.ajax({
                                                            url: 'backend/fn_cobalance.php',
                                                            data: formData,
                                                            processData: false,
                                                            contentType: false,
                                                            type: 'POST',
                                                            success: function (data) {
                                                                console.log(data);
                                                                $("#after_money_to_shop_from_user").show();
                                                            }
                                                        });
                                                    }
                                                });
                                            </script>
                                            <h5>Перевести со счета другой компании</h5>
                                            <div id="after_money_to_shop_from_shop" style="display: none;">
                                                <p>Перевод выполнен успешно</p>
                                            </div>
                                            <form action="" id="money_to_shop_from_shop">
                                                <input type="hidden" name="type_fn" value="money_to_shop_from_shop">
                                                <input type="hidden" name="uid" value="{$uid}">
                                                <input type="hidden" name="sid_to" value="{$shop_id}">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <select id="select_comp" name="sid_from" class="selectpicker title="Выберите компанию">
                                                    {foreach $user_company as $item}
                                                        <option value="{$item["sid"]}">{$item["title"]}</option>
                                                    {/foreach}
                                                    </select>
                                                </div>
                                                <div class="col-md-8">
                                                    <p style="margin: 7px 0 15px 0;">Баланс компании: <span id="bal_comp"></span></p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="cash" class="form-control" placeholder="Количество баллов">
                                                </div>
                                                <div class="col-md-6">
                                                    <button id="sub_money_to_shop_from_shop" class="btn btn-primary">Пополнить</button>
                                                </div>
                                            </div>
                                            </form>
                                            <script>
                                                var user_company_balance = [];
                                                {foreach $user_company_balance as $key => $value}
                                                user_company_balance[{$key}] = {$value};
                                                {/foreach}
                                                $("#bal_comp").text(user_company_balance[$('#select_comp').val()]);
                                                $('#select_comp').on('changed.bs.select', function (event, clickedIndex, newValue, oldValue) {
                                                    $("#bal_comp").text(user_company_balance[$('#select_comp').val()]);
                                                });


                                                $( '#sub_money_to_shop_from_shop' ).click(function(event) {
                                                    event.preventDefault();
                                                    var flag = confirm("Подтвердите перевод");
                                                    if (flag) {
                                                        var formData  = new FormData($( '#money_to_shop_from_shop' )[0]);
                                                        $("#money_to_shop_from_shop").hide();

                                                        $.ajax({
                                                            url: 'backend/fn_cobalance.php',
                                                            data: formData,
                                                            processData: false,
                                                            contentType: false,
                                                            type: 'POST',
                                                            success: function (data) {
                                                                console.log(data);
                                                                $("#after_money_to_shop_from_shop").show();
                                                            }
                                                        });
                                                    }
                                                });
                                            </script>
                                            {*<hr>*}
                                            {*<h5>Автопополнение из личного счёта</h5>*}
                                            {*<div class="row">*}
                                                {*<div class="col-md-8">Пополнить, когда на балансе компании меньше</div>*}
                                                {*<div class="col-md-4"><input type="url" class="form-control" placeholder="" style="width:100px"></div>*}
                                            {*</div>*}
                                            {*<div class="row">*}
                                                {*<div class="col-md-8">Пополнить, когда на личном балансе больше</div>*}
                                                {*<div class="col-md-4"><input type="url" class="form-control" placeholder="" style="width:100px"></div>*}
                                            {*</div>*}
                                            {*<div class="row">*}
                                                {*<div class="col-md-8">Размер пополнения</div>*}
                                                {*<div class="col-md-4"><input type="url" class="form-control" placeholder="" style="width:100px"></div>*}
                                            {*</div>*}
                                            {*<div class="row">*}
                                                {*<div class="col-md-8"><p><input id="demo-checkbox-addons" class="magic-checkbox" type="checkbox"><label for="demo-checkbox-addons"></label> Включить втопополнение</p></div>*}
                                                {*<div class="col-md-4"><button class="btn btn-primary">Сохранить</button></div>*}
                                            {*</div>*}


					                    </div>
					                </div>
					            </div>
					
					
					        </div>
					        <!--===================================================-->
					        <!--End network line chart-->
					
					    </div>
                        <div class="col-lg-2">
                            <div class="panel panel-success panel-colorful cobalance">
                                <div class="pad-all">
                                    <p class="text-lg text-semibold"><i class="demo-pli-check icon-fw"></i> Состояние</p>
                                    <p class="mar-no">
                                        <span class="pull-right text-bold">{($move_bonus_up_clients - $move_bonus_down) / 100}</span>
                                        Прибыль
                                    </p>
                                    <ul class="list-group list-unstyled">
                                        <li class="mar-btm">
                                            <span class="label label-info pull-right" style="font-size: 1.4em;
    margin-top: 6px;">{$coefficient} %</span>
                                            <p>Коэффициент успешности</p>
                                            <div class="progress progress-md">
                                                <div style="width: {$coefficient}%;" class="progress-bar progress-bar-light">
                                                    <span class="sr-only">{$coefficient}%</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <hr>
                                    <p>Коэффициент успешности это соотношение ваших денежных затрат к бесплатным баллам которые вы вновь вкладываете в рекламный бюджет.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="panel panel-warning panel-colorful cobalance">
                                <div class="pad-all">
                                    <p class="text-lg text-semibold"><i class="demo-pli-check icon-fw"></i> Баланс</p>
                                    <p class="mar-no">
                                        <span class="pull-right text-bold">{$move_bonus_down / 100}</span>
                                        Расход
                                    <p class="mar-no">
                                        <span class="pull-right text-bold">{$move_bonus_clients / 100}</span>
                                        Клиентам
                                    </p>
                                    <p class="mar-no">
                                        <span class="pull-right text-bold">{$move_bonus_manager / 100}</span>
                                        Сотрудникам
                                    </p>
                                    <hr>
                                    <p class="mar-no">
                                        <span class="pull-right text-bold">{$move_bonus_up / 100}</span>
                                        Пополнение
                                    </p>
                                    <p class="mar-no">
                                        <span class="pull-right text-bold">{$move_bonus_up_eqvair / 100}</span>
                                        Денежные
                                    </p>
                                    <p class="mar-no">
                                        <span class="pull-right text-bold">{$move_bonus_up_clients /100}</span>
                                        От клиентов
                                    </p>
                                    <p class="mar-no">
                                        <span class="pull-right text-bold">{$move_bonus_up_ls / 100}</span>
                                        Из личного счёта
                                    </p>
                                </div>
                            </div>
                        </div>
					</div>
					
				
					
					<div class="row">
					    <div class="col-xs-12">
					        <div class="panel">
					            <div class="panel-heading">
					                <h3 class="panel-title">Детализированный список</h3>
					            </div>
					
					            <!--Data Table-->
					            <!--===================================================-->
					            <div class="panel-body">
                                    {if $movement_funds}
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Дата операции</th>
                                                    <th>Клиент</th>
                                                    <th>Сотрудник</th>
                                                    <th>Сумма</th>
                                                    <th>Комментарий</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {foreach $movement_funds as $key => $value}
                                                    <tr>
                                                        <td>{$value["date"]}</td>
                                                        <td>{$user_arr[$value["uid"]]}</td>
                                                        <td>{$manager_arr[$value["mid"]]}</td>
                                                        <td>{$value["summ"] / 100}</td>
                                                        <td>{$value["comment"]}</td>
                                                    </tr>
                                                {/foreach}
                                                </tbody>
                                            </table>
                                        </div>
                                    {else}
                                        <p>Операции отсутствуют</p>
                                    {/if}

					                <hr>
					            </div>
					            <!--===================================================-->
					            <!--End Data Table-->
					
					        </div>
					    </div>
					</div>
					
					
					
                </div>
                <!--===================================================-->
                <!--End page content-->
                 






            {/if}
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
{include file='aside.tpl'} 
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



       <!-- FOOTER -->
				<!--===================================================-->
					<nav id="footer2-container">
						<div id="footer2">
						{include file='footer2.tpl'}
						</div>
					</nav>
				<!--===================================================-->
				<!--END FOOTER -->	


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--==================================================
   
    <!--===================================================-->
    <!-- END SETTINGS -->
{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}
