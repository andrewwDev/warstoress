﻿{include file='header.tpl'}

{if $login}
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Клиенты</h1>

                    <!--Searchbox-->
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                    
                                <!--Page content-->
                <!--===================================================-->
               <div id="page-content">
                   <div class="row">
					        <div class="col-xs-12">
					        <div class="panel">
					            <div class="panel-heading">
					                <h3 class="panel-title">Клиенты</h3>
					            </div>
					
					            <!--Data Table-->
					            <!--===================================================-->
					            <div class="panel-body">
					                <div class="pad-btm form-inline">
					                    <div class="row">
					                        <div class="col-sm-6 table-toolbar-left">
					                            <button class="btn btn-purple"><i class="demo-pli-add icon-fw"></i>Add</button>
					                            <button class="btn btn-default"><i class="demo-pli-printer"></i></button>
					                            <div class="btn-group">
					                                <button class="btn btn-default"><i class="demo-pli-information"></i></button>
					                                <button class="btn btn-default"><i class="demo-pli-recycling"></i></button>
					                            </div>
					                        </div>
					                        <div class="col-sm-6 table-toolbar-right">
					                            <div class="form-group">
					                                <input type="text" autocomplete="off" class="form-control" placeholder="Search" id="demo-input-search2">
					                            </div>
					                            <div class="btn-group">
					                                <button class="btn btn-default"><i class="demo-pli-download-from-cloud"></i></button>
					                                <div class="btn-group">
					                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					                                        <i class="demo-pli-gear"></i>
					                                        <span class="caret"></span>
					                                    </button>
					                                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
					                                        <li><a href="#">Action</a></li>
					                                        <li><a href="#">Another action</a></li>
					                                        <li><a href="#">Something else here</a></li>
					                                        <li class="divider"></li>
					                                        <li><a href="#">Separated link</a></li>
					                                    </ul>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					                </div>
					                <div class="table-responsive">
					                    <table class="table table-striped">
					                        <thead>
					                            <tr>
					                                <th>Последняя дата операции</th>
                                                    <th>Клиент</th>
					                                <th>Cashback</th>
					                                <th>Оплачено.руб</th>
					                                <th>Оплачено бон.</th>
					                            </tr>
					                        </thead>
					                        <tbody>
					                            <tr>
                                                    <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 22, 2014</span></td>
                                                    <td>Steve N. Horton</td>
					                                <td>$45.00</td>
                                                    <td>$45.00</td>
                                                    <td>$45.00</td>
					                            </tr>
					                            <tr>
                                                    <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 22, 2014</span></td>
                                                    <td>Charles S Boyl</td>
					                                <td>$245.00</td>
                                                    <td>$245.00</td>
                                                    <td>$245.00</td>
					                               
					                            </tr>
					                            <tr>
					                                <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 22, 2014</span></td>
                                                    <td>Boyl Charles S </td>
					                                <td>$38.00</td>
                                                    <td>$38.00</td>
                                                    <td>$38.00</td>
					                            </tr>
					                            <tr>
                                                    <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 22, 2014</span></td>
                                                    <td>Teresa L. Doe</td>
					                                <td>$77.00</td>
                                                    <td>$77.00</td>
                                                    <td>$77.00</td>
					                            </tr>
					                            <tr>
                                                    <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 22, 2014</span></td>
                                                    <td>Charles S Boyle</td>
					                                <td>$18.00</td>
                                                    <td>$18.00</td>
                                                    <td>$18.00</td>
					                            </tr>
					                        </tbody>
					                    </table>
					                </div>
					                <hr>
					                <div class="pull-right">
					                    <ul class="pagination text-nowrap mar-no">
					                        <li class="page-pre disabled">
					                            <a href="#">&lt;</a>
					                        </li>
					                        <li class="page-number active">
					                            <span>1</span>
					                        </li>
					                        <li class="page-number">
					                            <a href="#">2</a>
					                        </li>
					                        <li class="page-number">
					                            <a href="#">3</a>
					                        </li>
					                        <li>
					                            <span>...</span>
					                        </li>
					                        <li class="page-number">
					                            <a href="#">9</a>
					                        </li>
					                        <li class="page-next">
					                            <a href="#">&gt;</a>
					                        </li>
					                    </ul>
					                </div>
					            </div>
					        </div>
					        </div>
                   </div>
               </div>
                <!--End-Page content-->
                <!--===================================================-->

                  <!--Panel with Tabs-->
					        <!--===================================================-->
					        <div class="panel panel-primary">
					
					            <!--Panel heading-->
					            <div class="panel-heading">
					                <div class="panel-control">
					
					                    <!--Nav tabs-->
					                    <ul class="nav nav-tabs">
					                        <li class="active"><a data-toggle="tab" href="#demo-tabs-box-1">Профиль</a></li>
					                        <li><a data-toggle="tab" href="#demo-tabs-box-2">Отзывы</a></li>
                                            <li><a data-toggle="tab" href="#demo-tabs-box-2">Статистика покупок</a></li>
					                    </ul>
					
					                </div>
					                <h3 class="panel-title">Информация о клиенте</h3>
					            </div>
					
					            <!--Panel body-->
					            <div class="panel-body">
					
					                <!--Tabs content-->
					                <div class="tab-content">
					                    <div id="demo-tabs-box-1" class="tab-pane fade in active">
					                        <p class="text-main text-lg mar-no">Профиль</p>
					                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
					                    </div>
					                    <div id="demo-tabs-box-2" class="tab-pane fade">
					                        <p class="text-main text-lg mar-no">Отзывы</p>
					                        Duis autem vel eum iriure dolor in hendrerit in vulputate.
					                    </div>
                                        <div id="demo-tabs-box-1" class="tab-pane fade in active">
					                        <p class="text-main text-lg mar-no">Статистика покупок</p>
					                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
					                    </div>
					                </div>
					            </div>
					        </div>
					        <!--===================================================-->
					        <!--End of panel with tabs-->



     
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
{include file='aside.tpl'}
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



        <!-- FOOTER -->
        <!--===================================================-->
        <footer id="footer">

            <!-- Visible when footer positions are fixed -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="show-fixed pull-right">
                You have <a href="#" class="text-bold text-main"><span class="label label-danger">3</span> pending action.</a>
            </div>



            <!-- Visible when footer positions are static -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="hide-fixed pull-right pad-rgt">
                14GB of <strong>512GB</strong> Free.
            </div>



            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

            <p class="pad-lft">&#0169; 2016 Your Company</p>



        </footer>
        <!--===================================================-->
        <!-- END FOOTER -->


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}
