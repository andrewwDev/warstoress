﻿{include file='header.tpl'}

{if $login}
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Сотрудники</h1>

                    <!--Searchbox
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div> -->
                </div>
                {if !empty($current_shop)}
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                    <div class="input-group pad-all"  style="width:50%; margin-left:25%"  >
                        <form id="form_add_staff" action="">
                            <input id="staff_login" type="text" class="form-control" placeholder="Введите логин пльзователя чтобы добавить сотрудника"  autocomplete="off">
                            <span class="input-group-btn" style="display: inherit;">
			                    <button id="add_staff" type="button" class="btn btn-success"><i class="demo-pli-add"></i></button>
			                </span>
                        </form>
		            </div>

                <div id="after_none" class="input-group pad-all"  style="width:50%; margin-left:25%; display: none;"  >
                    <p style='text-align:center;'>Пользователь с таким логином не найден.</p>
                </div>

                <div id="after_find" class="input-group pad-all"  style="width:50%; margin-left:25%; display: none;"  >
                    <p style='text-align:center;'>Пользователь уже добавлен.</p>
                </div>

                <div id="after_yes" class="input-group pad-all"  style="width:50%; margin-left:25%; display: none;"  >
                    <p style='text-align:center;' '>Добавить пользователя <b id="user_login"></b> в список сотрудников ?</p><p style='text-align: center;'><button type='button' id='set_staff' class='finish btn btn-success'>Добавить</button></p>
                </div>

                <div id="after_add" class="input-group pad-all"  style="width:50%; margin-left:25%; display: none;"  >
                    <p style='text-align:center;'>Сотрудник добавлен</p>
                </div>


                {literal}
                <script>
                    $('#staff_login').keypress(function(event){

                        if (event.keyCode == 10 || event.keyCode == 13)
                            event.preventDefault();

                    });
                    var user_id = 0;
                    $( '#add_staff' ).click(function(event) {
                        event.preventDefault();

                        var user_login = {user_login: $('#staff_login').val()};

                        $.ajax({
                            url: 'backend/fn_staff.php',
                            data: user_login,
                            type: 'POST',
                            success: function (data) {
                                if (data == 0) {
                                    $("#after_none").show();
                                    $("#after_yes").hide();
                                    $("#after_find").hide();
                                } else if (data == -1) {
                                    $("#after_find").show();
                                    $("#after_none").hide();
                                    $("#after_yes").hide();
                                } else {
                                    $("#user_login").text($('#staff_login').val());
                                    user_id = data;
                                    $("#after_none").hide();
                                    $("#after_yes").show();
                                    $("#after_find").hide();
                                }
                            }
                        });
                    });

                    $( '#set_staff' ).click(function(event) {
                        event.preventDefault();
                        user_id = {user_id: user_id, shopid: {/literal}{$shop_id}{literal}};
                        $.ajax({
                            url: 'backend/fn_staff.php',
                            data: user_id,
                            type: 'POST',
                            success: function (data) {
                                console.log(data);
                                $("#after_yes").hide();
                                $("#after_add").show();
                                location.reload()
                            }
                        });
                    });


                </script>
                {/literal}

                                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    
					
					<div class="row">
					    <div class="col-sm-12">
					        <div class="panel">
					            <div class="panel-heading">
					                <h3 class="panel-title">Сотрудники</h3>
					            </div>
					
					            <!-- Foo Table - Row Toggler -->
					            <!--===================================================-->
					            <div class="panel-body">
									{if $managers}
										<div class="table-responsive">
											<table class="table table-striped">
												<thead>
												<tr>
													<th>Имя</th>
													<th>Фамилия</th>
													<th>Статус</th>
												</tr>
												</thead>
												<tbody>
												{foreach $managers as $value}
													<tr>
														<td><span class="text-muted"><i class="fa fa-clock-o"></i> {$value["name"]}</span></td>
														<td>{$value["surname"]}</td>
														<td>
															{if $value["active"] > 0}
																<span class="label label-table label-success">Работает</span>
															{else}
																<span class="label label-table label-danger">Уволен</span>
															{/if}
														</td>
													</tr>
												{/foreach}
												</tbody>
											</table>
										</div>
									{else}
										<p>В данной компании сотрудников нет</p>
									{/if}
					            </div>
					            </div>
					            <!--===================================================-->
					            <!-- End Foo Table - Row Toggler -->
					    </div>
					</div>
					</div>
                    {/if}






     
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
{include file='aside.tpl'}
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



       <!-- FOOTER -->
				<!--===================================================-->
					<nav id="footer2-container">
						<div id="footer2">
						{include file='footer2.tpl'}
						</div>
					</nav>
				<!--===================================================-->
				<!--END FOOTER -->	


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}
