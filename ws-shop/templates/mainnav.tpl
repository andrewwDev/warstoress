﻿            <!--MAIN NAVIGATION-->
            <!--===================================================-->
                    <!--Menu-->
                    <!--================================-->
                    <div id="mainnav-menu-wrap">
                        <div class="nano">
                            <div class="nano-content">

                                <!--Profile Widget-->
                                <!--================================-->
                                <div id="mainnav-profile" class="mainnav-profile">
                                    <div class="profile-wrap">
                                        <div class="pad-btm">
                                            {if $user_status == 1}
                                                <span class="label label-success pull-right">Обычный пользователь</span>
                                            {elseif $user_status == 2}
                                                <span class="label label-success pull-right">Магазин</span>
                                            {elseif $user_status == 3}
                                                <span class="label label-success pull-right">Администратор</span>
                                            {elseif $user_status == 4}
                                                <span class="label label-success pull-right">Модератор</span>
                                            {/if}
                                            {if $avatar}
                                                <img class="img-circle img-sm img-border" src="{$avatar}" alt="Profile Picture">
                                            {else}
                                                <img class="img-circle img-sm img-border" src="img/profile-photos/1.png" alt="Profile Picture">
                                            {/if}

                                        </div>
                                        <a href="/ws-shop/profile.php" class="box-block" /*data-toggle="collapse" aria-expanded="false"*/>

                                            <p class="mnp-name">{$owner_name}</p>
                                            <span class="mnp-desc"></span>
                                        </a>
                                    </div>

                                </div>


                                <!--Shortcut buttons-->
                                <!--================================-->
                                <div id="mainnav-shortcut">
                                    <ul class="list-unstyled">
                                        {*<li class="col-xs-3" data-content="Профиль">*}
                                            {*<a class="shortcut-grid" href="profile.php">*}
                                                {*<i class="demo-psi-male"></i>*}
                                            {*</a>*}
                                        {*</li>*}
                                        {*<li class="col-xs-3" data-content="Баланс">*}
                                            {*<a class="shortcut-grid" href="balance.php">*}
                                                {*<i class="demo-psi-speech-bubble-3"></i>*}
                                            {*</a>*}
                                        {*</li>*}
                                        {*<li class="col-xs-3" data-content="События">*}
                                            {*<a class="shortcut-grid" href="conews.php">*}
                                                {*<i class="demo-psi-thunder"></i>*}
                                            {*</a>*}
                                        {*</li>*} 
                                        <li class="col-xs-3 logout" data-content="Выйти">
                                            <a class="shortcut-grid" href="#">
                                                <i class="demo-psi-lock-2"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!--================================-->
                                <!--End shortcut buttons-->


                                <ul id="mainnav-menu" class="list-group">

						            <!--Category name-->
						            <li class="list-header">Мои компании</li>

                                    <li>
                                        <a href="index.php">
                                            <i class="demo-psi-bar-chart"></i>
                                            <span class="menu-title">
												<strong>Общая статистика</strong>
											</span>

                                        </a>


                                    </li>


                                    {foreach $shops_list as $shop}
                                    <!--Menu list item-->
                                        {if $shop.moderator_flag == 1 or $shop.moderator_flag == 3}
                                            <li class="active-link">
                                                <!-- <a href="?sid={$shop.id}"> -->
                                                <a href="#">
                                                    <i class="demo-psi-home"></i>
                                                    <span class="menu-title">
												<strong>{$shop.title}</strong>
											</span>
                                                    <i class="arrow"></i>
                                                </a>

                                                <!--Submenu-->
                                                <ul class="collapse">
                                                    <li><a href="overallstatistics.php?sid={$shop.id}"">Статистика</a></li>
                                                    <li><a href="conews.php?sid={$shop.id}">Новости</a></li>
                                                    <li><a href="buyers.php?sid={$shop.id}">Клиенты</a></li>
                                                    <li><a href="staff.php?sid={$shop.id}">Сотрудники</a></li>
                                                    <!-- <li class="list-divider"></li> -->
                                                    <!-- <li><a href="reviews.php?sid={$shop.id}">Отзывы</a></li> -->
                                                    <li><a href="cobalance.php?sid={$shop.id}">Баланс</a></li>
                                                    {*<li><a href="push.php?sid={$shop.id}">Push</a></li>*}
                                                    <li><a href="cosettings.php?sid={$shop.id}">Настройки</a></li>
                                                </ul>
                                            </li>
                                        {/if}
                                        {if $shop.moderator_flag == 2}
                                            <li class="active-link">
                                                <!-- <a href="?sid={$shop.id}"> -->
                                                <a href="#" style="background: darkred;">
                                                    <i class="demo-psi-home"></i>
                                                    <span class="menu-title">
												<strong>{$shop.title}</strong>
											</span>
                                                    <i class="arrow"></i>
                                                </a>

                                                <!--Submenu-->
                                                <ul class="collapse">
                                                    <li><a href="cosettings.php?sid={$shop.id}">Настройки</a></li>
                                                </ul>
                                            </li>
                                        {/if}
                                    {/foreach}

						            <!--Menu list item-->
						            <li>
						                <a href="coreg.php">
						                    <i class="demo-pli-plus"></i>
						                    <span class="menu-title">
												<strong>Добавить Компанию</strong>
											</span>

						                </a>


						            </li>


						            <li class="list-divider"></li>


						            <!--Menu list item-->
						            <li>
						                <a href="#">
						                    <i class="demo-psi-receipt-4"></i>
						                    <span class="menu-title">Документы</span>
											<i class="arrow"></i>
						                </a>

						                <!--Submenu-->
						                <ul class="collapse">
						                    <li><a href="http://warstores.net/doc/oferta.doc">Договор-оферты</a></li>
											{*<li><a href="https://152фз.рф/get_prv/9ab0ea60f8985bc6cdd95706a8e7905f">Политика конфедициальности</a></li>*}
                                            <li><a href="http://warstores.net/doc/privacy.pdf">Политика конфедициальности</a></li>
											<li><a href="https://152фз.рф/get_terms/9ab0ea60f8985bc6cdd95706a8e7905f">Общее пользовательское соглашение</a></li>

										</ul>
						            </li>

						            <!--Menu list item-->
						            <li>
						                <a href="#">
						                    <i class="demo-psi-inbox-full"></i>
						                    <span class="menu-title">Материалы</span>
											<i class="arrow"></i>
						                </a>

						                <!--Submenu-->
						                <ul class="collapse">
						                    <li><a href="reklama.php">Рекламные буклеты</a></li>
											<li><a href="commercial.php">Коммерческое предложение</a></li>
											<li><a href="sticker.php">Наклейки</a></li>
										
						                </ul>
						            </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <!--================================-->
                    <!--End menu-->


            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

