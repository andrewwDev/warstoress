﻿{include file='header.tpl'}

{if $login}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.1/css/star-rating.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.1/js/star-rating.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.1/js/star-rating_locale_ru.js"></script>
    <script type="text/javascript">
        var reviews = [];
        {foreach $reviews as $key => $value}
        reviews.push({{$value.rating}});
        {/foreach}
        console.log(reviews);
    </script>

    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Отзывы</h1>

                    <!--Searchbox-->
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                {if !empty($current_shop)}
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->
                  <div class="col-lg-6">
                       
                                                                 <!--Tile-->
                                            <!--===================================================-->
                                            <div class="panel panel-warning panel-colorful" >
                                                <div class="pad-all text-center">
                                                    <span class="text-3x text-thin">
                                                        {if $overall_rating}
                                                            {$overall_rating}
                                                        {else}
                                                            Нет
                                                        {/if}
                                                    </span>
                                                    <p>Средняя оценка</p>
                                                    <i class="demo-psi-mail icon-lg"></i>
                                                </div>
                                            </div>
                                            <!--===================================================-->

                                              <!--===================================================-->
                                               {*<div class="col-sm-6 col-lg-3" style="margin-left:35%; margin-top:-28%; width:60%;">*}
                                            {**}
                                                    {*<!--Sparkline Area Chart-->*}
                                                    {*<!--===================================================-->*}
                                                    {*<div class="panel panel-success panel-colorful" >*}
                                                        {*<div class="pad-all">*}
                                                            {*<p class="text-lg text-semibold">История оценок</p>*}
                                                        {*</div>*}
                                                        {*<div class="pad-all text-center">*}
                                                            {*<!--Placeholder-->*}
                                                            {*<div id="feedback-sparkline-area"></div>*}
                                                        {*</div>*}
                                                    {*</div>*}
                                                    {*<!--===================================================-->*}
                                                    {*<!--End Sparkline Area Chart-->*}
                                            {**}
                                                {*</div>*}

                                             <!--===================================================-->


                                            <div class="col-lg-5 col-lg-offset-2">
                                                    <!-- Timeline -->
                                                    <!--===================================================-->
                                                    <div class="timeline" style="margin-left:-25%;">
                                            
                                                        <!-- Timeline header -->
                                                        {foreach $reviews as $item}
                                                            <div class="timeline-entry">
                                                                <div class="timeline-stat">
                                                                    <div class="timeline-icon">
                                                                        {if $item["rating"] < 3}
                                                                            <i class="demo-pli-unlike icon-2x"></i>
                                                                        {else}
                                                                            <i class="demo-pli-like icon-2x"></i>
                                                                        {/if}
                                                                    </div>
                                                                    <div class="timeline-time">{$item["date"]}</div>
                                                                </div>
                                                                <div class="timeline-label" style="width:150%">
                                                                    <label for="input-{$item["id"]}" class="control-label">Оценка:</label>
                                                                    <input id="input-{$item["id"]}" name="input-{$item["id"]}" value="{$item["rating"]}" class="rating rating-loading" data-show-clear="false" data-show-caption="true">
                                                                    <script>
                                                                        $("#input-{$item["id"]}").rating({literal}{displayOnly: true, step: 1}{/literal});
                                                                    </script>
                                                                    <p class="mar-no pad-btm"> От <a href="#" class="text-semibold"><i>{$users[$item["uid"]][0]} {$users[$item["uid"]][1]}</i></a></p>
                                                                    <blockquote class="bq-sm bq-open mar-no">{$item["text"]}</blockquote>
                                                                    {*<blockquote class="bq-sm bq-open mar-no">*}
                                                                        {*<div class="form-group">*}
                                                                            {*<input type="text" class="form-control">*}
                                                                        {*</div>*}
                                                                        {*<button class="btn btn-success">Ответить</button>*}
                                                                    {*</blockquote>*}
                                                                </div>
                                                            </div>
                                                        {/foreach}
                                                    
                                                    <!--===================================================-->
                                                    <!-- End Timeline -->
                            
                                                    </div>
                                        </div>

                                
                  </div>


                               <!--Default Tabs (Right Aligned)-->
					        <!--===================================================-->
					        
					
					            <!--Nav tabs-->
					            <div class="nav nav-tabs tabs-right" style="border:0ch;" >
					                <!--Bordered Accordion-->
					        <!--===================================================-->
					        <div class="panel-group accordion" id="demo-acc-info-outline">


                                <div class="col-sm-6 col-lg-3">




                                    <!--Morris Charts -->
					<!--===================================================-->
					<div class="row">
					    <div class="col-lg-6" style="width:120%;">
					        <div class="panel" style="margin-right:-65%;">
					            <div class="panel-heading" >
                                    {*<div class="panel-control">*}
					                    {*<button id="demo-panel-network-refresh" data-toggle="panel-overlay" data-target="#demo-panel-network" class="btn"><i class="demo-pli-repeat-2 icon-lg"></i></button>*}
					                    {*<div class="btn-group">*}
					                        {*<button class="dropdown-toggle btn" data-toggle="dropdown" aria-expanded="false"><i class="demo-pli-gear icon-lg"></i></button>*}
					                        {*<ul class="dropdown-menu dropdown-menu-right">*}
					                            {*<li><a href="#">Неделя</a></li>*}
					                            {*<li><a href="#">Месяц</a></li>*}
					                            {*<li><a href="#">Год</a></li>*}
					                            {*<li class="divider"></li>*}
					                            {*<li><a href="#">За всё время</a></li>*}
					                        {*</ul>*}
					                    {*</div>*}
					                {*</div>*}
					                <h3 class="panel-title">&nbsp;&nbsp;Статистика отзывов.</h3>
					            </div>
					            <div class="panel-body">
					
					                <!--Morris Area Chart placeholder-->
					                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
					                <div id="demo-morris-area" style="height:212px"></div>
					                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
					
					            </div>
					        </div>
					    </div>
					</div>

					            <!--Panel with Footer-->
					            <!--===================================================-->
					            {*<div class="panel" style="margin-bottom:-20%; width:200%; margin-top:2%;">*}
					                {*<div class="panel-heading">*}
					                    {*<h3 class="panel-title">*}
					                        {*&nbsp;&nbsp;Вознаграждение за отзыв*}
					                    {*</h3>*}
					                {*</div>*}
					                {*<div class="panel-body">*}
					                    {*<div><input type="text" class="form-control" style="width:50px"> За отзыв</div>*}
                                        {*<div><input type="text" class="form-control" style="width:50px"> За отзыв на своей странице</div>*}
					                {*</div>*}
					                {*<div class="panel-footer">*}
                                        {*<button class="btn btn-success" style="float:right">Сохранить</button>*}
                                       {*<br />*}
                                       {*<br />*}

                                    {*</div>*}
					            {*</div>*}
					            <!--===================================================-->
					            <!--End Panel with Footer-->
					
					      

                            
					            
					        </div>
					            </div>
					            <!--===================================================-->
					            <!-- End Form wizard with Validation -->
					            
					
					            
					        
					        <!--===================================================-->
					        <!--End Default Tabs (Right Aligned)-->

                        </div>
                  {/if}
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
            <!-- FOOTER -->
            <!--===================================================-->
            <footer id="footer">

                <!-- Visible when footer positions are fixed -->
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <div class="show-fixed pull-right">
                    You have <a href="#" class="text-bold text-main"><span class="label label-danger">3</span> pending action.</a>
                </div>



                <!-- Visible when footer positions are static -->
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <div class="hide-fixed pull-right pad-rgt">
                    14GB of <strong>512GB</strong> Free.
                </div>



                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

                <p class="pad-lft">&#0169; 2016 Your Company</p>



            </footer>
            <!--===================================================-->
            <!-- END FOOTER -->
        </div>


            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
{include file='aside.tpl'} 
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>






        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
    
    <!--===================================================-->
    <!-- END SETTINGS -->
{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}
