﻿{include file='header.tpl'}

{if $login}
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Наклейки</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->
				
				<!--Page content-->
                <!--===================================================-->
                <div id="page-content">
				
							<div class="panel panel-primary">
					
					            <!--Panel heading-->
					            <div class="panel-heading">
					                <div class="panel-control">
					
					                    <!--Nav tabs-->
					                    <ul class="nav nav-tabs">
					                        <li class="active"><a data-toggle="tab" href="#demo-tabs-box-1">Входные</a></li>
					                        <li><a data-toggle="tab" href="#demo-tabs-box-2">Внутренние</a></li>
					                    </ul>
					
					                </div>
					                <h3 class="panel-title">Скачать доступные</h3>
					            </div>
					
					            <!--Panel body-->
					            <div class="panel-body">
					
					                <!--Tabs content-->
					                <div class="tab-content">
					                    <div id="demo-tabs-box-1" class="tab-pane fade in active">
					                        <p class="text-main text-lg mar-no">Входные наклейки</p>
											Данные наклейки рекомендуем клеить на входную дверь или рядом со входом. Размер наклейки может быть выбран вами по желанию, однако стандартные размер наших наклее составляет 13х13см.
											<br />											
											Для скачивания наклейки нажмите правую кнопку мыши и выберите "Сохранить изображение".
											<br />
											<br />
											<br />
											<a href="img/addoc/playblue.png" target="_blank"><img src="img/addoc/playblue.png" width="250px"></a> 
											<a href="img/addoc/scwhite.png" target="_blank"><img src="img/addoc/scwhite.png" width="250px"></a> 
											<a href="img/addoc/takebonus.png" target="_blank"><img src="img/addoc/takebonus.png" width="250px"></a>
					                    </div>
					                    <div id="demo-tabs-box-2" class="tab-pane fade">
					                        <p class="text-main text-lg mar-no">Внутренние наклейки</p>
											Данные наклейки рекомендуем клеить возле кассовой зоны. Размер наклейки может быть выбран вами по желанию, однако стандартные размер наших наклее составляет 86х54 мм.
											<br />											
											Для скачивания наклейки нажмите правую кнопку мыши и выберите "Сохранить изображение".
											<br />
											<br />
											<br />
											<a href="img/addoc/logocategory.jpg" target="_blank"><img src="img/addoc/logocategory.jpg" width="250px"></a> 
					                    </div>
					                </div>
					            </div>
					        </div>
				
				
				</div>
                     <!--===================================================-->
                <!--End page content-->
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
{include file='aside.tpl'} 
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



        <!-- FOOTER -->
        <!--===================================================-->
	<footer id="footer">

		<!-- Visible when footer positions are fixed -->
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<div class="show-fixed pull-right">
			You have <a href="#" class="text-bold text-main"><span class="label label-danger">3</span> pending action.</a>
		</div>



		<!-- Visible when footer positions are static -->
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<div class="hide-fixed pull-right pad-rgt">
			Плати только за <strong>настоящих</strong> клиентов.
		</div>



		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

		<p class="pad-lft">&#0169; 2016 AdPoints</p>



	</footer>
        <!--===================================================-->
        <!-- END FOOTER -->


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}
