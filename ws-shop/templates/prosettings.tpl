﻿{include file='header.tpl'}

{if $login}
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Редактирование профиля</h1>

                    <!--Searchbox-->
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                 <div class="col-lg-6">
					        <div class="panel">
					            <div class="panel-heading">
					                <h3 class="panel-title">Карточка магазина</h3>
					            </div>
					
					
					            <!-- BASIC FORM ELEMENTS -->
					            <!--===================================================-->
					            <form class="panel-body form-horizontal form-padding">
					                <!--First tab-->
					                            <div id="demo-bv-tab1" class="tab-pane">
					                                <div class="form-group">
					                                    <label class="col-lg-3 control-label">Имя:</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" class="form-control" name="shop_jtitle" id="shop_jtitle" placeholder="Имя" value="{$row.jtitle}">
					                                    </div>
					                                </div>
					                                <div class="form-group">
					                                    <label class="col-lg-3 control-label">Фамилия:</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" class="form-control" name="Фамилия" placeholder="ИНН">
					                                    </div>
					                                </div>
                                                     <div class="form-group">
					                                    <label class="col-lg-3 control-label">Номер телефона:</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" class="form-control" name="Номер телефона" placeholder="ОГРН">
					                                    </div>
					                                </div>
                                                     <div class="form-group">
					                                    <label class="col-lg-3 control-label">Дата рождения:</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" class="form-control" name="Дата рождения" placeholder="Телефон для связи">
					                                    </div>
					                                </div>
                                                    <div class="form-group">
					                                    <label class="col-lg-3 control-label">e-mail:</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" class="form-control" name="e-mail" placeholder="Юридический адрес">
					                                    </div>
					                                </div>
                                                    <div class="form-group">
					                                    <label class="col-lg-3 control-label">Город:</label>
					                                    <div class="col-lg-7">
					                                        <input type="text" class="form-control" name="Город" placeholder="id группы Вконтакте">
					                                    </div>
					                                </div>
					                            </div>

                                                <!--Second tab-->
                                                <div class="panel panel-trans">
                                                    <div class="panel-heading">
					                                    <h3 class="panel-title">Уведомления</h3>
					                                </div>
                                                    <div class="pad-ver">
                                                        <ul class="list-group bg-trans list-todo mar-no">
                                                             <li class="list-group-item">
					                                            <input id="demo-todolist-all" class="magic-checkbox" type="checkbox">
					                                            <label id="sd" for="demo-todolist-all"><span>ВСЕ</span></label>
					                                        </li>
                                                            <li class="list-group-item">
					                                            <input id="demo-todolist-1" class="magic-checkbox" type="checkbox">
					                                            <label for="demo-todolist-1"><span>Новости в регионе</span></label>
					                                        </li>
                                                            <li class="list-group-item">
					                                            <input id="demo-todolist-2" class="magic-checkbox" type="checkbox">
					                                            <label for="demo-todolist-2"><span>Пуш уведомления из посещённых мест</span></label>
					                                        </li>
                                                            <li class="list-group-item">
					                                            <input id="demo-todolist-3" class="magic-checkbox" type="checkbox">
					                                            <label for="demo-todolist-3"><span>Уведомления о новых компаниях</span></label>
					                                        </li>
                                                            <li class="list-group-item">
					                                            <input id="demo-todolist-4" class="magic-checkbox" type="checkbox">
					                                            <label for="demo-todolist-4"><span>Уведомление о награде за просмотр рекламы</span></label>
					                                        </li>
                                                            <li class="list-group-item">
					                                            <input id="demo-todolist-5" class="magic-checkbox" type="checkbox">
					                                            <label for="demo-todolist-5"><span>Уведомления о покупках</span></label>
					                                        </li>
                                                            <li class="list-group-item">
					                                            <input id="demo-todolist-6" class="magic-checkbox" type="checkbox">
					                                            <label for="demo-todolist-6"><span>Уведомления об изменении баланса</span></label>
					                                        </li>
                                                            <li class="list-group-item">
					                                            <input id="demo-todolist-7" class="magic-checkbox" type="checkbox">
					                                            <label for="demo-todolist-7"><span>Новости ваших заведений</span></label>
					                                        </li>
                                                            <li class="list-group-item">
					                                            <input id="demo-todolist-8" class="magic-checkbox" type="checkbox">
					                                            <label for="demo-todolist-8"><span>Новости друзей</span></label>
					                                        </li>
                                                         </ul>
                                                    </div>

                                                    <script>
                                                        $("#demo-todolist-all").change(function () {
                                                            if ($("#demo-todolist-all").prop("checked")) {
                                                                $("#demo-todolist-1").prop("checked", true);
                                                                $("#demo-todolist-2").prop("checked", true);
                                                                $("#demo-todolist-3").prop("checked", true);
                                                                $("#demo-todolist-4").prop("checked", true);
                                                                $("#demo-todolist-5").prop("checked", true);
                                                                $("#demo-todolist-6").prop("checked", true);
                                                                $("#demo-todolist-7").prop("checked", true);
                                                                $("#demo-todolist-8").prop("checked", true);
                                                            } else {
                                                                $("#demo-todolist-1").prop("checked", false);
                                                                $("#demo-todolist-2").prop("checked", false);
                                                                $("#demo-todolist-3").prop("checked", false);
                                                                $("#demo-todolist-4").prop("checked", false);
                                                                $("#demo-todolist-5").prop("checked", false);
                                                                $("#demo-todolist-6").prop("checked", false);
                                                                $("#demo-todolist-7").prop("checked", false);
                                                                $("#demo-todolist-8").prop("checked", false);
                                                            }
                                                        });
                                                    </script>
                                                </div>
					                                
					                              <!--Footer button-->
					                    
					                         <div class="text-right">
                                                 <div class="box-inline">
					                            <button type="button" class="finish btn btn-warning" disabled>Finish</button>
                                            </div>
					                    </div>
					                        <!-- FINISH FOOTER -->                                                    

					            </form>
					            <!--===================================================-->
					            <!-- END BASIC FORM ELEMENTS -->


					
					</div>
					
                </div>
                <!--===================================================-->
                <!--End page content-->

                 <!-- FOOTER -->
        <!--===================================================-->
        <footer id="footer">

            <!-- Visible when footer positions are fixed -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="show-fixed pull-right">
                You have <a href="#" class="text-bold text-main"><span class="label label-danger">3</span> pending action.</a>
            </div>



            <!-- Visible when footer positions are static -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="hide-fixed pull-right pad-rgt">
                14GB of <strong>512GB</strong> Free.
            </div>



            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

            <p class="pad-lft">&#0169; 2016 Your Company</p>



        </footer>
        <!--===================================================-->
        <!-- END FOOTER -->







                <!--Default Tabs (Right Aligned)-->
					        <!--===================================================-->
					        
					
					            <!--Nav tabs-->
					            <div class="nav nav-tabs tabs-right">
					                <!--Bordered Accordion-->
					        <!--===================================================-->
					        <div class="panel-group accordion" id="demo-acc-info-outline">
					            <div class="panel panel-bordered panel-info">
					
					                <!--Accordion title-->
					                <div class="panel-heading">
					                    <h4 class="panel-title">
					                        <a data-parent="#demo-acc-info-outline" data-toggle="collapse" href="#demo-acd-info-outline-1">Дополнительная информация</a>
					                    </h4>
					                </div>
					
					                <!--Accordion content-->
					                <div class="panel-collapse collapse in" id="demo-acd-info-outline-1">
					                    <div class="panel-body">
					                      В зависимости от выбранного вами процента вознаграждения пользователю добавляется реферальное вознаграждение.
                                          Тириф можно настроить исходя из  процента который вы хотите дарить вашему клиенту  или из общего процента затрат.
                                          Обратите внимание, весь остаток от партнёрской программы является платой  за привличенного к вам клиента 
					                    </div>
					                </div>
					            </div>
					        </div>
					            </div>
					            <!--===================================================-->
					            <!-- End Form wizard with Validation -->
					            </div>
					
					            
					        
					        <!--===================================================-->
					        <!--End Default Tabs (Right Aligned)-->






     
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
{include file='aside.tpl'} 
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



     


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->

         



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
    
    <!--===================================================-->
    <!-- END SETTINGS -->
{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}
