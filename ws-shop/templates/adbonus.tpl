﻿{include file='header.tpl'}

{if $login}
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed" style="
    position: absolute;
    display: block;
    height: 100%;
">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container" class="bonus">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Мобильная версия выдачи бонусов</h1>


                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
					<div class="col-md-12 text-center">
                        <div class="add-bonus" style="width:25%; ">
							<div id="after" style="display: none;">
								<p>Бонусы выданы {$user_name}</p>
							</div>
							<div id="before">
								<div class="form-group">
									<div class="col-lg-7">
												<input type="text" onkeyup="this.value = this.value.replace (/[^0-9+]/, '')" maxlength="12" class="form-control" name="phone" id="phone" placeholder="Номер телефона клиента">
												<button type="button" id="check_phone" class="btn btn-primary">Проверить номер</button>
											<div id="check_phone" style="display: none; color: red; font-weight: bold;">
												Такого пользователя нет
											</div>
									</div>
								</div>
								<!-- это доступно если номер телефона зарегистрирован в системе -->
								<div class="form-group">
									<div class="col-lg-7">
										Имя клиента: {$owner_name}
									</div>
									<div class="col-lg-7">
										На балансе клиента: {$user_balance}
									</div>
								</div>											
									<div class="form-group">
									<div class="col-lg-7">
										<input type="text" onkeyup="this.value = this.value.replace (/[^0-9+]/, '')" maxlength="12" class="form-control" name="phone" id="phone" placeholder="Снять с баланса клиента">
									</div>
								</div>	
															<!-- выдача бонусов происходит со счета компании доступной сотруднику -->

								<div class="form-group">
									<div class="col-lg-7">
									Всего к оплате: {$user_balance}
									<button type="submit" id="after" class="btn btn-warning">Выдать бонусы</button>
									</div>

								</div>
							</div>
						</div>
                    </div>
				</div>

                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
					{include file='aside.tpl'}
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



      <!-- FOOTER -->
		<!--===================================================-->
		<nav id="footer2-container">
			<div id="footer2">
				{include file='footer2.tpl'}
			</div>
		</nav>
		<!--===================================================-->
	<!--END FOOTER -->


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}
