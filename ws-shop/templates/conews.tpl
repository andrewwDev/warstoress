﻿{include file='header.tpl'}

{if $login}
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
{include file='navbar.tpl'}
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed" style="
    position: absolute;
    display: block;
    height: 100%;
">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container" class="news">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Новости</h1>

                    <!--Searchbox
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div> -->
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                <!--Page content-->
                <!--===================================================-->
                {if !empty($current_shop)}
                <div id="page-content">

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="add-news" style="width:25%; ">
                                <h4>Добавить новость</h4>
                                <hr>
                                <div id="after" style="display: none;">
                                    <p>Новость успешно добавлена</p>
                                </div>
                                <div id="before">
                                    {if $time_news >= 5}
                                        <form id="form-news" action="backend/set_news.php" class="form-horizontal" enctype="multipart/form-data"><input type="hidden" name="sid" value="{$shop_id}" readonly>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Заголовок</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control"  maxlength="25" type="text" name="title" id="news-title">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Текст новости</label>
                                                <div class="col-lg-9">
                                                    <textarea class="form-control" maxlength="50" name="description" id="news-desc"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Картинка</label>
                                                <div class="col-lg-9">
                                                    <div class="fileform news">
                                                        <div id="fileformlabel-news"></div>
                                                        <div class="selectbutton btn btn-success fileinput-button dz-clickable">Добавить</div>
                                                        <input id="upload-news" accept="image/jpeg,image/png" type="file" name="img_news" onchange="getName(this.value,'upload-news');"/>
                                                        <!-- На размерность изображения -->
                                                        <script type="text/javascript">
                                                        function validateSize(fileInput,size) {
                                                            var fileObj, oSize;
                                                            if ( typeof ActiveXObject == "function" ) { // IE
                                                                fileObj = (new ActiveXObject("Scripting.FileSystemObject")).getFile(fileInput.value);
                                                            }else {
                                                                fileObj = fileInput.files[0];
                                                            }
                                                        
                                                            oSize = fileObj.size; // Size returned in bytes.
                                                            if(oSize > size * 1024 * 1024){
                                                                return false
                                                            }
                                                            return true;
                                                        }
                                                        </script>
                                                        <!-- Конец размерность изображения -->
                                                    </div> 
                                                </div>
                                            </div>
                                            <button type="submit" id="news-finish" class="finish btn btn-success" style="display: inline-block;">Отправить</button>
                                        </form>
                                    {else}
                                        <p>Добавлять новости можно один раз в пять дней.</p>
                                    {/if}

                                </div>
                                <script>
                                    $( '#news-finish' ).click(function(event) {
                                        event.preventDefault();

                                        $("#before").hide();
                                        var formData  = new FormData($( '#form-news' )[0]);

                                        $.ajax({
                                            url: 'backend/set_news.php',
                                            data: formData,
                                            processData: false,
                                            contentType: false,
                                            type: 'POST',
                                            success: function (data) {
                                                console.log(data);
                                                $("#after").show();
                                            }
                                        });


//							$.post('backend/set_shop.php', $( '#shopRegister' ).serialize())
//								.done(function( data ) {
//									console.log(data);
//									$("#after-form").show();
//								});

                                    });
                                </script>
                            </div>
                        </div>
                    </div>

                {if $news}
                    <div class="timeline two-column" style="width:100%; ">
                        <!-- Timeline header -->
                        <div class="timeline-header">
                            <div class="timeline-header-title bg-success">{date("d.m.y",time())}</div>
                        </div>
                        {foreach $news as $item}
                            <div class="timeline-entry">
                                <div class="timeline-stat">
                                    <div class="timeline-icon"></div>
                                    <div class="timeline-time">{$item["ndate"]}</div>
                                </div>
                                <div class="timeline-label">
                                    <h4>{$item["title"]}</h4>
                                    {if $item['hasimg'] == 1}
                                        <img src="/ws_images/news/img{$item['nid']}_xh.jpg" alt="" class="news-img" style="width:50%;">
                                    {/if}
                                    {$item["description"]}
                                </div>
                            </div>
                        {/foreach}
                        <div class="clearfix"></div>
                    </div>
                {else}
                    <div class="timeline two-column">
                        <div class="timeline-header">
                            <div class="timeline-header-title bg-warning">Новостей нет</div>
                        </div>
                    </div>
                {/if}
                <!--===================================================-->
                <!--End page content-->


            </div>
            {/if}
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
					{include file='aside.tpl'}
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
{include file='mainnav.tpl'}
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



      <!-- FOOTER -->
		<!--===================================================-->
		<nav id="footer2-container">
			<div id="footer2">
				{include file='footer2.tpl'}
			</div>
		</nav>
		<!--===================================================-->
	<!--END FOOTER -->


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
{else}
{include file='login.tpl'}
{/if}
{include file='footer.tpl'}
