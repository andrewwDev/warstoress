
	<div id="container" class="cls-container login-page">

		<!-- LOGIN FORM -->
		<div class="cls-content">
		    <div class="cls-content-sm panel">
		        <div class="panel-body">
		            <div class="mar-ver pad-btm">
		                <p><h3 class="h4 mar-no">Вход для владельцев компаний</h3></p>
						<p><i class="demo-psi-heart-2 icon-3x"></i></p>
		                <p>Открытая бета версия</p>
		            </div>
		            <form id="loginForm" method="post" action="">
		                <div class="form-group">
		                    <input type="text" name="login" class="form-control" placeholder="Логин" autofocus>
		                </div>
		                <div class="form-group">
		                    <input type="password" id="password" name="password" class="form-control" placeholder="Пароль">
		                </div>
		                <div class="checkbox pad-btm text-left">
		                    <input id="demo-form-checkbox" class="magic-checkbox" type="checkbox">
		                    <label for="demo-form-checkbox">Запомнить меня</label>
		                </div>
		                <button class="btn btn-primary btn-lg btn-block" type="submit">Войти</button>
		            </form>
		        </div>

		         <div class="pad-all border-top">
		            <a href="forgot_pass.php" class="btn-link mar-rgt">Забыли пароль?</a>
		            <a href="pages-register.php" class="btn-link mar-lft">Cоздать новый аккаунт</a>

		            <div class="media pad-top bord-top">
		                <div class="pull-right">
		                    <a href="https://oauth.vk.com/authorize?client_id=6091025&scope=status,pages,wall,offline,groups,friends,&redirect_uri=http://warstores.net/ws-shop/backend/vk.php&response_type=code" id="vk_auth" class="pad-rgt"><img src="http://warstores.net/ws-shop/img/vk512.png" width="30"></a>
		                    <a href="https://accounts.google.com/o/oauth2/auth?redirect_uri=http://warstores.net/ws-shop/backend/oauth/google.php&response_type=code&client_id=779295172696-3k82h87p1e0j32bt8keabi3rats7fq14.apps.googleusercontent.com&scope=https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile" id="google_auth" class="pad-rgt"><img src="http://warstores.net/ws-shop/img/google512.svg" width="30"></a>
		                </div>
		                <div class="media-body text-left">
		                    Войти через
		                </div>
		            </div>
		        </div>
		    </div>
		</div>

	</div>
<script src="/ws-shop/js/md5.min.js"></script>
<script>
	$( 'button[type=submit]' ).click(function(event) {
		event.preventDefault();
		$("#password").val(md5($("#password").val()));

		$.post('backend/auth.php', $( '#loginForm' ).serialize())
		    .done(function( data ) {
		    	console.log(data);
				if (data == 'yes')
					location.reload();
				else alert(data);
		    });

  	});
</script>