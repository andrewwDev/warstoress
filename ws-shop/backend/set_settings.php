
<?php

session_start();

if (isset($_POST["courname"])) {
	require_once './../db.php';
    require_once '/var/www/u0413200/data/www/warstores.net/ws-panel/ajax/enj_submitfig.php';
	$method = 'POST';
    $id = $_POST["shop_id"];

    if ($_POST["moderator_flag"] == "on") {
        $moderator_flag = 3;
    } else {
        if ($_POST["moderator_flag"] == 1 or $_POST["moderator_flag"] == 3) {
            $moderator_flag = $_POST["moderator_flag"];
        } else {
            $moderator_flag = 0;
        }
    }

    $sql = "
	    UPDATE
	       wsq_shops
		SET
		   ur_title = '".($_POST["courname"] ? htmlspecialchars($_POST["courname"]) : '')."',
		   ur_inn = '".($_POST["INN"] ? htmlspecialchars($_POST["INN"]) : '')."',
		   ur_ogrn = '".($_POST["OGRN"] ? htmlspecialchars($_POST["OGRN"]) : '')."',
		   ur_phone = '".($_POST["cotel"] ? htmlspecialchars($_POST["cotel"]) : '')."',
		   ur_address = '".($_POST["couradress"] ? htmlspecialchars($_POST["couradress"]) : '')."',
		   title = '".($_POST["copubname"] ? htmlspecialchars($_POST["copubname"]) : '')."',
		   slogon = '".($_POST["copublessinfo"] ? htmlspecialchars($_POST["copublessinfo"]) : '')."',
		   description = '".($_POST["copubinfo"] ? htmlspecialchars($_POST["copubinfo"]) : '')."',
		   worktime = '".($_POST["copubworktime"] ? htmlspecialchars($_POST["copubworktime"]) : '')."',
		   address = '".($_POST["copubgeoadress"] ? htmlspecialchars($_POST["copubgeoadress"]) : '')."',
		   phone = '".($_POST["copubtel"] ? htmlspecialchars($_POST["copubtel"]) : '')."',
		   url = '".($_POST["copubsite"] ? htmlspecialchars($_POST["copubsite"]) : '')."',
		   payvalue = '".($_POST["percent_client"] ? htmlspecialchars($_POST["percent_client"]) : '')."',
		   paytomanbynewuser = '".($_POST["paytomanbynewuser"] ? htmlspecialchars($_POST["paytomanbynewuser"]) : '')."',
		   paytomanbyoper = '".($_POST["paytomanbyoper"] ? htmlspecialchars($_POST["paytomanbyoper"]) : '')."',
		   bonusborderpercent = '".($_POST["bonusborderpercent"] ? htmlspecialchars($_POST["bonusborderpercent"]) : '')."',
		   bonusbordervalue = '".($_POST["bonusbordervalue"] ? htmlspecialchars($_POST["bonusbordervalue"]) : '')."',
		   vk_id = '".($_POST["vk_id"] ? htmlspecialchars($_POST["vk_id"]) : '')."',
		   delivery = '".($_POST["delivery"] ? htmlspecialchars($_POST["delivery"]) : '')."',
		   YGeoLoc = '".($_POST["YGeoLoc"] ? htmlspecialchars($_POST["YGeoLoc"]) : '')."',
		   moderator_flag = '".$moderator_flag."'
		WHERE
		   sid = '".$id."';
		   ";

    $res = $mysqli->query($sql);

    if ($_FILES) {
        $id= $_POST["shop_id"];
        saveimageforcorp($id);
    }
	
	if ($res != false){
		$response = [
		  "status" => "success"
	    ];
	} else {
		$response = [
		  "status" => "bad error"
	    ];
	}

	echo json_encode($response);

    $sql = "DELETE FROM `wsq_shopsincat` WHERE `sid` = ".$id.";";
    $res = $mysqli->query($sql);


    foreach ($_POST["heading"] as $value) {
        $sql = "
	    INSERT INTO
	       `wsq_shopsincat`
		SET
		   
		   `sid` = '".$id."',
		   `cid` = '".htmlspecialchars($value)."';
		   ";
        $res = $mysqli->query($sql);
        if ($res != false){
            $response = "success";
        } else {
            $response = "error";
        }
        echo json_encode($response);
    }
}

?>