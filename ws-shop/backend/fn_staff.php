<?

session_start();

if (isset($_POST["user_login"])) {
    require_once './../db.php';
    $user_id = 0;
    if ($stmt = $mysqli->prepare("SELECT uid FROM `wsq_users` WHERE login = ?")) {
        $stmt->bind_param('s', $_POST["user_login"]);
        $stmt->execute();
        $stmt->bind_result($user_id);
        $stmt->fetch();
        $stmt->close();
    }

    if ($stmt = $mysqli->prepare("SELECT COUNT(*) FROM `wsq_managers` WHERE uid = ?")) {
        $stmt->bind_param('s', $user_id);
        $stmt->execute();
        $stmt->bind_result($row);
        $stmt->fetch();
        $stmt->close();
        if ($row > 0) {
            echo json_encode(-1);
        } else {
            echo json_encode($user_id);
        }
    }
}

if (isset($_POST["user_id"])) {
    require_once './../db.php';
    $regdate = date("Y-m-d", time());
    $sql = "
	    INSERT INTO
	       `wsq_managers`
		SET
		   `uid` = '".htmlspecialchars($_POST["user_id"])."',
		   `shopid` = '".htmlspecialchars($_POST["shopid"])."',
		   `regdate` = '".$regdate."';
		   ";
    $res = $mysqli->query($sql);
    if ($res != false){
        echo json_encode("success");
    } else {
        echo json_encode("error");
    }



}