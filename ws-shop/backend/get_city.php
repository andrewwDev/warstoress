<?

session_start();

if (isset($_POST["region"])) {
    require_once './../db.php';
    $region = $_POST["region"];

    $geo_city = [];
    if ($stmt = $mysqli->prepare("SELECT geo_city_id, name FROM `geo_city` WHERE geo_region_id = ?")) {
        $stmt->bind_param('s', $region);
        $stmt->execute();
        $stmt->bind_result($geo_city_id, $geo_city_name);
        while ($stmt->fetch()) $geo_city[] = array(
            'geo_city_id' => $geo_city_id,
            'geo_city_name' => $geo_city_name,
        );
        $stmt->close();
    }

    echo json_encode($geo_city, JSON_UNESCAPED_UNICODE);
}