<?
session_start();
if (isset($_POST["phone"])) {
    $phone = "#".htmlspecialchars($_POST["phone"]);
    require_once './../db.php';

    $count = 0;
    if ($stmt = $mysqli->prepare("SELECT COUNT(*) FROM `wsq_shops` WHERE phone = ?")) {
        $stmt->bind_param('s', $phone);
        $stmt->execute();
        $stmt->bind_result($count);
        $stmt->fetch();
        $stmt->close();
    }

    echo json_encode($count);
}
