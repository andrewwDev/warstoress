<?php
if ($stmt = $mysqli->prepare("SELECT uid, CONCAT_WS(' ', name, surname), status, vk_photo_100 AS name FROM `wsq_users` WHERE uid = ?")) {
	$stmt->bind_param('s', $uid);
	$stmt->execute();
	$stmt->bind_result($uid, $username, $user_status, $avatar);
	$stmt->fetch();
	$stmt->close();
}
//if ($stmt = $mysqli->prepare("SELECT uid, CONCAT_WS(' ', name, surname) AS name FROM `wsq_users` WHERE uid=?")) {
//    $stmt->bind_param('s', $uid);
//    $stmt->execute();
//    $stmt->bind_result($id, $username);
//    while ($stmt->fetch()) {
//        $uid = $id;
//    }
//    $stmt->close();
//}

$shops_list = [];

if ($stmt = $mysqli->prepare("SELECT sid, ur_title, shopnick, ur_address, ur_inn, ownerid, ur_ogrn, ur_phone, title, slogon, description, worktime, address, phone, url, paymenttype, YGeoLoc, vk_id, payvalue, bonusborderpercent, bonusbordervalue, paytomanbynewuser, paytomanbyoper, delivery, moderator_flag    FROM `wsq_shops` WHERE ownerid=?")) {
	$stmt->bind_param('s', $uid);
	$stmt->execute();
	$stmt->bind_result($shop_id, $ur_title, $shopnick, $ur_address, $ur_inn, $ownerid, $ur_ogrn, $ur_phone, $title, $slogon, $description, $worktime, $address, $phone, $url, $paymenttype, $YGeoLoc, $vk_id, $payvalue, $bonusborderpercent, $bonusbordervalue, $paytomanbynewuser, $paytomanbyoper, $delivery, $moderator_flag);
	while ($stmt->fetch()) $shops_list[] = array(
		'id' => $shop_id, 
		'title' => $title,
		'ur_title' => $ur_title,
		'shopnick' => str_replace("#","",$shopnick),
		'ur_address' => $ur_address,
		'ur_inn' => $ur_inn,
		'ownerid' => $ownerid,
		'ur_ogrn' => $ur_ogrn,
		'ur_phone' => $ur_phone,
		'slogon' => $slogon,
		'description' => $description,
		'worktime' => $worktime,
		'address' => $address,
		'phone' => $phone,
		'url' => $url,
		'paymenttype' => $paymenttype,
        'YGeoLoc' => $YGeoLoc,
        'vk_id' => $vk_id,
        'payvalue' => $payvalue,
        'bonusborderpercent' => $bonusborderpercent,
        'bonusbordervalue' => $bonusbordervalue,
        'paytomanbynewuser' => $paytomanbynewuser,
        'paytomanbyoper' => $paytomanbyoper,
        'delivery' => $delivery,
        'moderator_flag' => $moderator_flag
    );
	$stmt->close();
}




