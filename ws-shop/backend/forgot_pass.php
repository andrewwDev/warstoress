<?php
function generateCode($length = 15){
    $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
    $numChars = strlen($chars);
    $string = '';
    for ($i = 0; $i < $length; $i++) {
        $string .= substr($chars, rand(1, $numChars) - 1, 1);
    }
    return $string;
}

session_start();
if (isset($_POST["email"])) {
    require_once './../db.php';

    header("Content-Type: application/json");
    $email = htmlspecialchars($_POST["email"]);
    $is_vk = false;

    if ($stmt = $mysqli->prepare("SELECT email, vk_uid FROM `wsq_users` WHERE login=? OR email=? OR uid=?")) {
        $stmt->bind_param("sss", $email, $email, $email);
        $stmt->execute();
        $stmt->bind_result($email, $vk_uid);
        $stmt->fetch();
        $stmt->close();
    }
    if ($vk_uid !== 0) {
        $status = "Данный аккаунт был создан через OAuth VK или Google, обратитесь в тех.поддержку.";
    } elseif (strlen($email) > 0) {

        if ($stmt = $mysqli->prepare("SELECT count(*) FROM `wsq_forgot_pass` WHERE email=?")) {
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $stmt->bind_result($row);
            $stmt->fetch();
            $stmt->close();
        }

        if ($row == 0) {
            $code = generateCode(15);
            if($stmt = $mysqli->prepare("INSERT INTO wsq_forgot_pass (email, code) VALUES(?, ?)")) {
                $stmt->bind_param("ss", htmlspecialchars($email),$code);
                $stmt->execute();
                $stmt->close();
                $status = "Сообщение с инструкциями для восстановление было отправлено на вашу почту.";
            }else{
                $status = "Серверная ошибка, обратитесь в техподдержку сайта.";
            }
        } else {
            if($stmt = $mysqli->prepare("SELECT code FROM wsq_forgot_pass WHERE email=?")){
                $stmt->bind_param("s", $email);
                $stmt->execute();
                $stmt->bind_result($code);
                $stmt->fetch();
                $stmt->close();
            }

            if($code){
                $status = "Сообщение ранее уже было отправлено. Мы повторно выслали Вам сообщение с инструкциями для восстановления.";
            }else{
                $status = "Серверная ошибка, обратитесь в техподдержку сайта.";
            }
        }

        $subject = "WarStores: Восстановление пароля";
        $message = ' <p>Вы отправили запрос на восстановление пароля в системе WarStores. Для восстановления перейдите по <a href="http://warstores.net/ws-shop/forgot_pass.php?code='.$code.'">ссылке</a><p>Если вы не отправляли запрос на восстановление просто проигнорируйте это письмо.</p><p>С Уважением, <br> Команда WarStores';
        $headers  = "Content-type: text/html; charset=utf-8 \r\n";
        $headers .= "From: Warstores <noreply@warstores.net>\r\n";
        mail($email, $subject, $message, $headers);
    } else {
        $status = "Пользователь с такими данными не найден.";
    }

    echo json_encode(["status" => $status]);
}