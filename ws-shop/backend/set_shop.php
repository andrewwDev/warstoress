
<?php

session_start();
if (isset($_POST["courname"])) {
	/**
     * RESTful API SET
	 */
	require_once './../db.php';
	require_once '/var/www/u0413200/data/www/warstores.net/ws-panel/ajax/enj_submitfig.php';

	if (isset($_POST["shopnick"])) {
	    $shopnick = "#".htmlspecialchars($_POST["shopnick"]);
    }

    $sql = "
	    INSERT INTO
	       `wsq_shops`
		SET
		   `ur_title` = '".htmlspecialchars($_POST["courname"])."',
		   `ur_inn` = '".htmlspecialchars($_POST["INN"])."',
		   `ownerid` = '".htmlspecialchars($_POST["coowner"])."',
		   `ur_ogrn` = '".($_POST["OGRN"] ? htmlspecialchars($_POST["OGRN"]) : '')."',
		   `shopnick` = '".$shopnick."',
		   `ur_phone` = '".($_POST["cotel"] ? htmlspecialchars($_POST["cotel"]) : '')."',
		   `ur_address` = '".($_POST["couradress"] ? htmlspecialchars($_POST["couradress"]) : '')."',
		   `title` = '".($_POST["copubname"] ? htmlspecialchars($_POST["copubname"]) : '')."',
		   `slogon` = '".($_POST["copublessinfo"] ? htmlspecialchars($_POST["copublessinfo"]) : '')."',
		   `description` = '".($_POST["copubinfo"] ? htmlspecialchars($_POST["copubinfo"]) : '')."',
		   `worktime` = '".($_POST["copubworktime"] ? htmlspecialchars($_POST["copubworktime"]) : '')."',
		   `address` = '".($_POST["copubgeoadress"] ? htmlspecialchars($_POST["copubgeoadress"]) : '')."',
		   `phone` = '".($_POST["copubtel"] ? htmlspecialchars($_POST["copubtel"]) : '')."',
		   `url` = '".($_POST["copubsite"] ? htmlspecialchars($_POST["copubsite"]) : '')."',
		   `payvalue` = '".($_POST["percent_client"] ? htmlspecialchars($_POST["percent_client"]) : '')."',
		   `paytomanbynewuser` = '".($_POST["paytomanbynewuser"] ? htmlspecialchars($_POST["paytomanbynewuser"]) : '')."',
		   `paytomanbyoper` = '".($_POST["paytomanbyoper"] ? htmlspecialchars($_POST["paytomanbyoper"]) : '')."',
		   `bonusborderpercent` = '".($_POST["bonusborderpercent"] ? htmlspecialchars($_POST["bonusborderpercent"]) : '')."',
		   `bonusbordervalue` = '".($_POST["bonusbordervalue"] ? htmlspecialchars($_POST["bonusbordervalue"]) : '')."',
		   `vk_id` = '".($_POST["vk_id"] ? htmlspecialchars($_POST["vk_id"]) : '')."',
		   `delivery` = '".($_POST["delivery"] ? htmlspecialchars($_POST["delivery"]) : '')."',
		   `YGeoLoc` = '".($_POST["YGeoLoc"] ? htmlspecialchars($_POST["YGeoLoc"]) : '')."',
		   `paymenttype` = '1';
		   ";

	$res = $mysqli->query($sql);

    $id=mysqli_insert_id($mysqli);
    if ($_FILES) {
        saveimageforcorp($id);
    }

    if ($res != false){
        $response = "success";
    } else {
        $response = "error";
    }
    echo json_encode($response);

    if (isset($_POST["region"]) && isset($_POST["city"])) {
        $sql = "
	    INSERT INTO
	       `wsq_shopsinreg`
		SET
		   
		   `sid` = '".$id."',
		   `city_id` = '".htmlspecialchars($_POST["city"])."',
		   `reg_id` = '".htmlspecialchars($_POST["region"])."';
		   ";
        $res = $mysqli->query($sql);
        if ($res != false){
            $response = "success";
        } else {
            $response = "error";
        }
        echo json_encode($response);
    }

    foreach ($_POST["heading"] as $value) {
        $sql = "
	    INSERT INTO
	       `wsq_shopsincat`
		SET
		   
		   `sid` = '".$id."',
		   `cid` = '".htmlspecialchars($value)."';
		   ";
        $res = $mysqli->query($sql);
        if ($res != false){
            $response = "success";
        } else {
            $response = "error";
        }
        echo json_encode($response);
	}
}

?>