<?
session_start();
if (isset($_POST["shopnick"])) {
    $shopnick = "#".htmlspecialchars($_POST["shopnick"]);
    require_once './../db.php';

    $count = 0;
    if ($stmt = $mysqli->prepare("SELECT COUNT(*) FROM `wsq_shops` WHERE shopnick = ?")) {
        $stmt->bind_param('s', $shopnick);
        $stmt->execute();
        $stmt->bind_result($count);
        $stmt->fetch();
        $stmt->close();
    }

    echo json_encode($count);
}
