<?php
session_start();

if (isset($_POST['code']) and isset($_POST['pass'])) {
    require_once './../db.php';
    $code = htmlspecialchars($_POST["code"]);

    if ($stmt = $mysqli->prepare("SELECT email FROM `wsq_forgot_pass` WHERE code=?")) {
        $stmt->bind_param("s", $code);
        $stmt->execute();
        $stmt->bind_result($email);
        $stmt->fetch();
        $stmt->close();
    }
    if (strlen($email) > 0) {
        $sql = "DELETE FROM `wsq_forgot_pass` WHERE code = '".$code."';";
        $res = $mysqli->query($sql);
        $sql = "UPDATE `wsq_users` SET pwdhash = '".$_POST['pass']."' WHERE email = '".$email."';";
        $res = $mysqli->query($sql);
        echo json_encode($res);
        if ($res != false) {
            echo json_encode("success");
        }
    }
}
