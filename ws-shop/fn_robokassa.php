<?php
//Функция для работы с робокассой
function get_shp_item($uid,$packet_pid){//Сформулировать shp_item на основе пакета 
	$shp_item="0:0:0:0:0:0:";//"$stakes:$uid:$packet_pid";
	$res=mysql_query("SELECT * from stakepackets WHERE spid=$packet_pid");
	if ($res!=false){
		if (mysql_num_rows($res)>0){
			$row=mysql_fetch_array($res);
			$shp_item="".$row['stake'].":$uid:$packet_pid:".$row['torefer'].":".$row['operrub'].":".$row['torefrub'].":".$row['cost'].":";
		}
	}
	$rid=0;
	$res=mysql_query("SELECT refid from users WHERE uid=$uid");
	if ($res!=false){
		if (mysql_num_rows($res)>0){			
			$rid=0+mysql_result($res, 0);
		}
	}
	$shp_item=$shp_item.$rid;
	$shp_item=$shp_item.":1";//Тип покупки 1-покупка ставок, 2-покупка голосов
	return $shp_item;
}

function get_shp_item_sum($uid,$stakes){//Сформулировать shp_item без пакета на произвольную сумму (ставок)
	$cfgfromdb=getconfigfromdb();
	$packet_pid=0;
	$torefer=0;	
	$operrub=$stakes*($cfgfromdb['stakecost']-$cfgfromdb['golos']);
	$torefrub=0;
	$cost=$stakes*$cfgfromdb['stakecost']; //Сколько запросить с пользователя
	$rid=0; //ID реферала
	$shp_item="$stakes:$uid:$packet_pid:$torefer:$operrub:$torefrub:$cost:$rid:1";//"$stakes:$uid:$packet_pid";
	return array($shp_item,$cost);
}

function get_shp_item_sum_golos($uid,$golos){//Сформулировать shp_item без пакета на произвольную сумму (голосов)
	$cfgfromdb=getconfigfromdb();
	$packet_pid=0;
	$torefer=0;	
	$operrub=0;//$stakes*($cfgfromdb['stakecost']-$cfgfromdb['golos']);
	$torefrub=0;
	$cost=$golos*$cfgfromdb['goloscostdokup']; //Сколько запросить с пользователя
	$rid=0; //ID реферала
	$shp_item="$golos:$uid:$packet_pid:$torefer:$operrub:$torefrub:$cost:$rid:2";//"$stakes:$uid:$packet_pid";
	return array($shp_item,$cost);
}


function prepareform($packet_pid,$cost,$stakes,$user_email,$uid,$formid){
	$IsTest=0;
	include('/home/meguro/breakbox.ru/configs/robokassa.php'); //тут грузим mrh_pass1, mrh_login, IsTest
	$inv_id = 0;	//Номер заказа
	$inv_desc = "Пакет на $stakes ставок";// описание заказа
	$out_summ = $cost;// сумма заказа
	$shp_item = get_shp_item($uid,$packet_pid);
	$in_curr = "";// предлагаемая валюта платежа
	$culture = "ru";// язык
	$encoding = "utf-8";// кодировка
	$Email = $user_email;
	//$ExpirationDate = "2015-07-30T12:00";// Срок действия счёта
	$crc  = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item");// формирование подписи

// форма оплаты товара Можно щелкнуть jQuery
  $res="<form action='https://auth.robokassa.ru/Merchant/Index.aspx' method=POST id='$formid' style='display:none;'>".
   "<input type=hidden name=MrchLogin value=$mrh_login>".
   "<input type=hidden name=OutSum value=$out_summ>".
   "<input type=hidden name=InvId value=$inv_id>".
   "<input type=hidden name=Desc value='$inv_desc'>".
   "<input type=hidden name=SignatureValue value=$crc>".
   "<input type=hidden name=Shp_item value='$shp_item'>".
   "<input type=hidden name=IncCurrLabel value=$in_curr>".
   "<input type=hidden name=Culture value=$culture>";
   if ($IsTest==1){ $res= $res."<input type=hidden name=IsTest value=$IsTest>";}
   $res= $res."<input type=hidden name=Email value=$Email>".
   "<input type=submit value='Оплатить'>".
   "</form>";
   return $res;
}

function prepareform_anysum($stakes,$user_email,$uid,$formid){//Подготовить форму на произвольную сумму ставок
	if ($stakes==0){$stakes=1;}
	$IsTest=0;
	include('/home/meguro/breakbox.ru/configs/robokassa.php'); //тут грузим mrh_pass1, mrh_login, IsTest
	$inv_id = 0;	//Номер заказа
	$inv_desc = "Пакет на $stakes ставок";// описание заказа	
	$res1=get_shp_item_sum($uid,$stakes);
	$shp_item = $res1[0];
	$out_summ = (0+$res1[1]*0.001);// сумма заказа
	$in_curr = "";// предлагаемая валюта платежа
	$culture = "ru";// язык
	$encoding = "utf-8";// кодировка
	$Email = $user_email;
	//$ExpirationDate = "2015-07-30T12:00";// Срок действия счёта
	$crc  = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item");// формирование подписи

// форма оплаты товара Можно щелкнуть jQuery
  $res="<form action='https://auth.robokassa.ru/Merchant/Index.aspx' method=POST id='$formid' style='display:none;'>".
   "<input type=hidden name=MrchLogin value=$mrh_login>".
   "<input type=hidden name=OutSum value=$out_summ>".
   "<input type=hidden name=InvId value=$inv_id>".
   "<input type=hidden name=Desc value='$inv_desc'>".
   "<input type=hidden name=SignatureValue value=$crc>".
   "<input type=hidden name=Shp_item value='$shp_item'>".
   "<input type=hidden name=IncCurrLabel value=$in_curr>".
   "<input type=hidden name=Culture value=$culture>";
   if ($IsTest==1){ $res= $res."<input type=hidden name=IsTest value=$IsTest>";}
   $res= $res."<input type=hidden name=Email value=$Email>".
   "<input type=submit value='Оплатить'>".
   "</form>";
   return $res;
}

function prepareform_anysum_golos($golos,$user_email,$uid,$formid){//Подготовить форму на произвольную сумму голосов
	$cfgfromdb=getconfigfromdb();
	if ($golos==0){$golos=1;}
	$IsTest=0;
	include('/home/meguro/breakbox.ru/configs/robokassa.php'); //тут грузим mrh_pass1, mrh_login, IsTest
	$inv_id = 0;	//Номер заказа
	$inv_desc = "Пакет на $golos ".$cfgfromdb['currency5'];// описание заказа	
	$res1=get_shp_item_sum_golos($uid,0+$golos);
	$shp_item = $res1[0];
	$out_summ = (0+$res1[1]*0.001);// сумма заказа
	$in_curr = "";// предлагаемая валюта платежа
	$culture = "ru";// язык
	$encoding = "utf-8";// кодировка
	$Email = $user_email;
	//$ExpirationDate = "2015-07-30T12:00";// Срок действия счёта
	$crc  = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item");// формирование подписи

// форма оплаты товара Можно щелкнуть jQuery
  $res="<form action='https://auth.robokassa.ru/Merchant/Index.aspx' method=POST id='$formid' style='display:none;'>".
   "<input type=hidden name=MrchLogin value=$mrh_login>".
   "<input type=hidden name=OutSum value=$out_summ>".
   "<input type=hidden name=InvId value=$inv_id>".
   "<input type=hidden name=Desc value='$inv_desc'>".
   "<input type=hidden name=SignatureValue value=$crc>".
   "<input type=hidden name=Shp_item value='$shp_item'>".
   "<input type=hidden name=IncCurrLabel value=$in_curr>".
   "<input type=hidden name=Culture value=$culture>";
   if ($IsTest==1){ $res= $res."<input type=hidden name=IsTest value=$IsTest>";}
   $res= $res."<input type=hidden name=Email value=$Email>".
   "<input type=submit value='Оплатить'>".
   "</form>";
   return $res;
}


?>