<?php
error_reporting(-1);
require_once '../ws-panel/libs/Smarty.class.php';
$smarty = new Smarty();

session_start();
if (isset($_SESSION['uid'])) {
	require_once './db.php';

	$uid = $_SESSION['uid'];
	require_once './backend/user.php';

	$smarty->assign('owner_name', $username);
	$smarty->assign('owner_id', $uid);
    $smarty->assign('user_status', $user_status);
    $smarty->assign('avatar', $avatar);
	$smarty->assign('shops_list', $shops_list);

	if (isset($_GET['sid'])) {
		$shop_id = (int) $_GET['sid'];
		require_once './backend/shop.php';
	}

    $categories = [];
    if ($stmt = $mysqli->prepare("SELECT csid, title FROM `wsq_catshop`")) {
        $stmt->execute();
        $stmt->bind_result($csid, $title);
        while ($stmt->fetch()) $categories[] = array(
            'csid' => $csid,
            'title' => $title,
        );
        $stmt->close();
        $smarty->assign('categories', $categories);
    }

    $geo_regions = [];
    if ($stmt = $mysqli->prepare("SELECT geo_region_id, name FROM `geo_region`")) {
        $stmt->execute();
        $stmt->bind_result($geo_reg_id, $geo_reg_name);
        while ($stmt->fetch()) $geo_regions[] = array(
            'geo_reg_id' => $geo_reg_id,
            'geo_reg_name' => $geo_reg_name,
        );
        $stmt->close();
        $smarty->assign('geo_regions', $geo_regions);
    }



	$login = true;
} else {
	$login = false;
}








$smarty->assign('login', $login);
$smarty->display('coreg.tpl');
