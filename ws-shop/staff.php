<?php
error_reporting(-1);
require_once '../ws-panel/libs/Smarty.class.php';
$smarty = new Smarty();

session_start();
if (isset($_SESSION['uid'])) {
	require_once './db.php';

	$uid = $_SESSION['uid'];
	require_once './backend/user.php';

	$smarty->assign('owner_name', $username);
    $smarty->assign('user_status', $user_status);
    $smarty->assign('avatar', $avatar);
	$smarty->assign('shops_list', $shops_list);


	if (isset($_GET['sid'])) {
		$shop_id = (int)$_GET['sid'];
        $smarty->assign('shop_id', $shop_id);

        $smarty->assign('current_shop', '');

        foreach ($shops_list as $key => $value) {
            if ($value['id'] == $shop_id) {
                $smarty->assign('current_shop', $shops_list[$key]);

                require_once './backend/shop.php';

                $managers_id = [];
                $managers = [];
                if ($stmt = $mysqli->prepare("SELECT uid FROM `wsq_managers` WHERE shopid=?")) {
                    $stmt->bind_param('s', $shop_id);
                    $stmt->execute();
                    $stmt->bind_result($uid);
                    while ($stmt->fetch()) $managers_id[] = array(
                        'uid' => $uid
                    );
                    $stmt->close();
                    foreach ($managers_id as $key => $value) {
                        $stmt = $mysqli->prepare("SELECT name, surname, active FROM `wsq_users` WHERE uid=?");
                        $stmt->bind_param('s', $value["uid"]);
                        $stmt->execute();
                        $stmt->bind_result($name, $surname, $active);
                        while ($stmt->fetch()) $managers[$value["uid"]] = array(
                            'name' => $name,
                            'surname' => $surname,
                            'active' => $active,
                        );
                        $stmt->close();
                    }
                    $smarty->assign('managers', $managers);
                }
            }
        }



		



	}

	$login = true;
} else {
	$login = false;
}








$smarty->assign('login', $login);
$smarty->display('staff.tpl');
