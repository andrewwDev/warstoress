<?php
error_reporting(-1);
require_once '../ws-panel/libs/Smarty.class.php';
$smarty = new Smarty();

session_start();
if (isset($_SESSION['uid'])) {
	require_once './db.php';

	$uid = $_SESSION['uid'];
	require_once './backend/user.php';

	$smarty->assign('owner_name', $username);
    $smarty->assign('user_status', $user_status);
    $smarty->assign('avatar', $avatar);
	$smarty->assign('shops_list', $shops_list);


	if (isset($_GET['sid'])) {
		$shop_id = (int)$_GET['sid'];

        $smarty->assign('current_shop', '');

        foreach ($shops_list as $key => $value) {
            if ($value['id'] == $shop_id) {
                $smarty->assign('current_shop', $shops_list[$key]);

                require_once './backend/shop.php';
                $movement_funds = [];
                $user_arr = [];
                $manager_arr = [];

                if ($stmt = $mysqli->prepare("SELECT date, summ, troper, uid, mid, comment FROM `wsq_transaction_shop` WHERE sid=?")) {
                    $stmt->bind_param('s', $shop_id);
                    $stmt->execute();
                    $stmt->bind_result($date, $summ, $troper, $user_id, $mid, $comment);
                    while ($stmt->fetch()) $movement_funds[] = array(
                        'date' => $date,
                        'summ' => $summ,
                        'troper' => $troper,
                        'uid' => $user_id,
                        'mid' => $mid,
                        'comment' => $comment,
                    );
                    $stmt->close();
                    $move_bonus_down = 0;
                    $move_bonus_up = 0;
                    $move_bonus_manager = 0;
                    $move_bonus_clients = 0;
                    $move_bonus_up_clients = 0;
                    $move_bonus_up_eqvair = 0;
                    $move_bonus_up_ls = 0;
                    foreach ($movement_funds as $key => $value) {
                        if ($value["troper"] == 3) {
                            $move_bonus_up += $value["summ"];
                        } else if ($value["troper"] == 2) {
                            $move_bonus_up += $value["summ"];
                            $move_bonus_up_ls += $value["summ"];
                        } else if ($value["troper"] == 1) {
                            $move_bonus_up += $value["summ"];
                            $move_bonus_up_eqvair += $value["summ"];
                        } else if ($value["troper"] == 4) {
                            $move_bonus_up += $value["summ"];
                            $move_bonus_up_clients += $value["summ"];
                        } else if (($value["troper"] == 10) or ($value["troper"] == 11)) {
                            $move_bonus_manager += $value["summ"];
                        } else if ($value["troper"] == 5) {
                            $move_bonus_clients += $value["summ"];
                        }
                        else {
                            $move_bonus_down -= $value["summ"];
                        }
                        /* for robocassa transaction */
                        if ($value["uid"] == 0) {
                            $user_arr[0] = "Robocassa";
                        }
                        if ($value["mid"] == 0) {
                            $manager_arr[0] = "WarStores";
                        }
                        /* for robocassa transaction end */
                        if ($sql = $mysqli->prepare("SELECT name, surname FROM `wsq_users` WHERE uid=?")) {
                            $sql->bind_param('s', $value["uid"]);
                            $sql->execute();
                            $res = $sql->bind_result($name, $surname);
                            while ($sql->fetch()) {
                                $user_arr[$value["uid"]] = $name." ".$surname;
                            }
                        }
                        if ($sql = $mysqli->prepare("SELECT name, surname FROM `wsq_users` WHERE uid=?")) {
                            $sql->bind_param('s', $value["mid"]);
                            $sql->execute();
                            $res = $sql->bind_result($name, $surname);
                            while ($sql->fetch()) {
                                $manager_arr[$value["mid"]] = $name." ".$surname;
                            }
                        }
                        $sql->close();
                    }
                    if ($move_bonus_down > 0) {
                        $coefficient = 100 * $move_bonus_up_clients / $move_bonus_down;
                    } else {
                        $coefficient = 100;
                    }
                    if ($coefficient > 100) $coefficient = 100;
                    $smarty->assign('movement_funds', array_reverse($movement_funds));
                    $smarty->assign('user_arr', $user_arr);
                    $smarty->assign('manager_arr', $manager_arr);
                    $smarty->assign('move_bonus_up', $move_bonus_up);
                    $smarty->assign('move_bonus_down', $move_bonus_down);
                    $smarty->assign('move_bonus_manager', $move_bonus_manager);
                    $smarty->assign('move_bonus_clients', $move_bonus_clients);
                    $smarty->assign('move_bonus_up_clients', $move_bonus_up_clients);
                    $smarty->assign('move_bonus_up_eqvair', $move_bonus_up_eqvair);
                    $smarty->assign('move_bonus_up_ls', $move_bonus_up_ls);
                    $smarty->assign('coefficient', $coefficient);
                    $smarty->assign('shop_id', $shop_id);
                    $smarty->assign('uid', $uid);
                }

                $user_transactions = [];
                $user_balance = 0;
                if ($stmt = $mysqli->prepare("SELECT summ FROM `wsq_transaction` WHERE uid=?")) {
                    $stmt->bind_param('s', $uid);
                    $stmt->execute();
                    $stmt->bind_result($summ);
                    while ($stmt->fetch()) $user_transactions[] = $summ;
                    $stmt->close();
                    $user_balance = array_sum($user_transactions);
                    $smarty->assign('user_balance', $user_balance);
                    
                }

                $user_company = [];
                $user_company_balance = [];
                $all_company = [];
                if ($stmt = $mysqli->prepare("SELECT sid, title FROM `wsq_shops` WHERE ownerid=? AND moderator_flag=1 AND sid != ?")) {
                    $stmt->bind_param('ss', $uid, $shop_id);
                    $stmt->execute();
                    $stmt->bind_result($sid, $title);
                    while ($stmt->fetch()) $user_company[] = array(
                        'sid' => $sid,
                        'title' => $title
                    );
                    $stmt->close();
                    foreach ($user_company as $item) {
                        $stmt = $mysqli->prepare("SELECT summ FROM `wsq_transaction_shop` WHERE sid=?");
                        $stmt->bind_param('s', $item["sid"]);
                        $stmt->execute();
                        $stmt->bind_result($summ);
                        while ($stmt->fetch()) $all_company[] = $summ;
                        $stmt->close();
                        $user_company_balance[$item["sid"]] = array_sum($all_company);
                        $all_company = [];
                    }
                    $smarty->assign('user_company', $user_company);
                    $smarty->assign('user_company_balance', $user_company_balance);
                }


                // robocassa

                // robocassa

                $mrh_login = "Warstores";
                $mrh_pass1 = "VQUT4bC2rNJ031UMufKk";

                // номер заказа
                // number of order
                $inv_id = rand(1,2147483647);

                // описание заказа
                // order description
                $inv_desc = "Пополнение личного счета магазина ".$shop_id;

                // сумма заказа
                // sum of order
                $def_sum = "10";

                // тип товара
                // code of goods
                $shp_sid = $shop_id;

                // язык
                // language
                $culture = "ru";

                // кодировка
                // encoding
                $encoding = "utf-8";

                // формирование подписи
                // generate signature
                $crc  = md5("$mrh_login::$inv_id:$mrh_pass1:shp_sid=$shop_id");

                // HTML-страница с кассой
                // ROBOKASSA HTML-page
                $robocassa = "<html><script language=JavaScript ".
                    "src='https://auth.robokassa.ru/Merchant/PaymentForm/FormFLS.js?".
                    "MerchantLogin=$mrh_login&DefaultSum=$def_sum&InvId=$inv_id".
                    "&Description=$inv_desc&SignatureValue=$crc&shp_sid=$shop_id".
                    "&Culture=$culture&Encoding=$encoding'></script></html>";
                $smarty->assign('robocassa', $robocassa);
                    }
                }


		



	}

	$login = true;
} else {
	$login = false;
}








$smarty->assign('login', $login);
$smarty->display('cobalance.tpl');
