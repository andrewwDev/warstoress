<?php /* Smarty version 3.1.27, created on 2017-11-04 13:19:22
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/cobalance.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:120318493459fd942a029011_55594604%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '398647752c5b47a743c916f0dfe11d195fa84cde' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/cobalance.tpl',
      1 => 1509790759,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '120318493459fd942a029011_55594604',
  'variables' => 
  array (
    'login' => 0,
    'current_shop' => 0,
    'move_bonus_up' => 0,
    'move_bonus_down' => 0,
    'robocassa' => 0,
    'user_balance' => 0,
    'uid' => 0,
    'shop_id' => 0,
    'user_company' => 0,
    'item' => 0,
    'user_company_balance' => 0,
    'key' => 0,
    'value' => 0,
    'move_bonus_up_clients' => 0,
    'coefficient' => 0,
    'move_bonus_clients' => 0,
    'move_bonus_manager' => 0,
    'move_bonus_up_eqvair' => 0,
    'move_bonus_up_ls' => 0,
    'movement_funds' => 0,
    'user_arr' => 0,
    'manager_arr' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59fd942a07ea62_65015553',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59fd942a07ea62_65015553')) {
function content_59fd942a07ea62_65015553 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '120318493459fd942a029011_55594604';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<?php if ($_smarty_tpl->tpl_vars['login']->value) {?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"><?php echo '</script'; ?>
>
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Баланс компании</h1>

                    <!--Searchbox
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div> -->
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->
                <?php if (!empty($_smarty_tpl->tpl_vars['current_shop']->value)) {?>
                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    
					<div class="row">
					    <div class="col-lg-8">
					
					        <!--Network Line Chart-->
					        <!--===================================================-->
					        <div id="demo-panel-network" class="panel">
					            <div class="panel-heading">
					                <div class="panel-control">
					                    
					                    <div class="btn-group"><h5><?php echo (($_smarty_tpl->tpl_vars['move_bonus_up']->value-$_smarty_tpl->tpl_vars['move_bonus_down']->value)/100);?>
 баллов на балансе компании</h5>
					                        
					                    </div>
					                </div>
					                <h3 class="panel-title">Действия со счётом </h3>
					            </div>
					
					            <!--Chart information-->
					            <div class="panel-body">
					                <div class="row">
					                    <div class="col-lg-12">
                                           <h5>Пополнить баланс через робокассу</h5>
					                       <p><?php echo $_smarty_tpl->tpl_vars['robocassa']->value;?>
</p>
                                            <hr>
					                    </div>


					                    <div class="col-lg-12">
                                            <h5>Пополнить с личного счета:</h5>
                                            <div id="after_money_to_shop_from_user" style="display: none;">
                                                <p>Перевод выполнен успешно</p>
                                            </div>
                                            <form action="" id="money_to_shop_from_user">
                                                <p>Баланс личного счета: <?php echo ($_smarty_tpl->tpl_vars['user_balance']->value/100);?>
 балла</p>
                                                <div class="row">
                                                        <input type="hidden" name="type_fn" value="money_to_shop_from_user">
                                                        <input type="hidden" name="uid" value="<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
">
                                                        <input type="hidden" name="sid" value="<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
">
                                                        <div class="col-md-6"><input type="text" name="cash" class="form-control" placeholder="Количество баллов"></div>
                                                        <div class="col-md-6"><button id="sub_money_to_shop_from_user" class="btn btn-primary">Пополнить</button></div>
                                                </div>
                                            </form>
                                            <?php echo '<script'; ?>
>
                                                $( '#sub_money_to_shop_from_user' ).click(function(event) {
                                                    event.preventDefault();
                                                    var flag = confirm("Подтвердите перевод");
                                                    if (flag) {
                                                        var formData  = new FormData($( '#money_to_shop_from_user' )[0]);
                                                        $("#money_to_shop_from_user").hide();

                                                        $.ajax({
                                                            url: 'backend/fn_cobalance.php',
                                                            data: formData,
                                                            processData: false,
                                                            contentType: false,
                                                            type: 'POST',
                                                            success: function (data) {
                                                                console.log(data);
                                                                $("#after_money_to_shop_from_user").show();
                                                            }
                                                        });
                                                    }
                                                });
                                            <?php echo '</script'; ?>
>
                                            <h5>Перевести со счета другой компании</h5>
                                            <div id="after_money_to_shop_from_shop" style="display: none;">
                                                <p>Перевод выполнен успешно</p>
                                            </div>
                                            <form action="" id="money_to_shop_from_shop">
                                                <input type="hidden" name="type_fn" value="money_to_shop_from_shop">
                                                <input type="hidden" name="uid" value="<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
">
                                                <input type="hidden" name="sid_to" value="<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <select id="select_comp" name="sid_from" class="selectpicker title="Выберите компанию">
                                                    <?php
$_from = $_smarty_tpl->tpl_vars['user_company']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                                        <option value="<?php echo $_smarty_tpl->tpl_vars['item']->value["sid"];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value["title"];?>
</option>
                                                    <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                                                    </select>
                                                </div>
                                                <div class="col-md-8">
                                                    <p style="margin: 7px 0 15px 0;">Баланс компании: <span id="bal_comp"></span></p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="cash" class="form-control" placeholder="Количество баллов">
                                                </div>
                                                <div class="col-md-6">
                                                    <button id="sub_money_to_shop_from_shop" class="btn btn-primary">Пополнить</button>
                                                </div>
                                            </div>
                                            </form>
                                            <?php echo '<script'; ?>
>
                                                var user_company_balance = [];
                                                <?php
$_from = $_smarty_tpl->tpl_vars['user_company_balance']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
                                                user_company_balance[<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
] = <?php echo $_smarty_tpl->tpl_vars['value']->value;?>
;
                                                <?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
                                                $("#bal_comp").text(user_company_balance[$('#select_comp').val()]);
                                                $('#select_comp').on('changed.bs.select', function (event, clickedIndex, newValue, oldValue) {
                                                    $("#bal_comp").text(user_company_balance[$('#select_comp').val()]);
                                                });


                                                $( '#sub_money_to_shop_from_shop' ).click(function(event) {
                                                    event.preventDefault();
                                                    var flag = confirm("Подтвердите перевод");
                                                    if (flag) {
                                                        var formData  = new FormData($( '#money_to_shop_from_shop' )[0]);
                                                        $("#money_to_shop_from_shop").hide();

                                                        $.ajax({
                                                            url: 'backend/fn_cobalance.php',
                                                            data: formData,
                                                            processData: false,
                                                            contentType: false,
                                                            type: 'POST',
                                                            success: function (data) {
                                                                console.log(data);
                                                                $("#after_money_to_shop_from_shop").show();
                                                            }
                                                        });
                                                    }
                                                });
                                            <?php echo '</script'; ?>
>
                                            
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                                
                                            


					                    </div>
					                </div>
					            </div>
					
					
					        </div>
					        <!--===================================================-->
					        <!--End network line chart-->
					
					    </div>
                        <div class="col-lg-2">
                            <div class="panel panel-success panel-colorful cobalance">
                                <div class="pad-all">
                                    <p class="text-lg text-semibold"><i class="demo-pli-check icon-fw"></i> Состояние</p>
                                    <p class="mar-no">
                                        <span class="pull-right text-bold"><?php echo ($_smarty_tpl->tpl_vars['move_bonus_up_clients']->value-$_smarty_tpl->tpl_vars['move_bonus_down']->value)/100;?>
</span>
                                        Прибыль
                                    </p>
                                    <ul class="list-group list-unstyled">
                                        <li class="mar-btm">
                                            <span class="label label-info pull-right" style="font-size: 1.4em;
    margin-top: 6px;"><?php echo $_smarty_tpl->tpl_vars['coefficient']->value;?>
 %</span>
                                            <p>Коэффициент успешности</p>
                                            <div class="progress progress-md">
                                                <div style="width: <?php echo $_smarty_tpl->tpl_vars['coefficient']->value;?>
%;" class="progress-bar progress-bar-light">
                                                    <span class="sr-only"><?php echo $_smarty_tpl->tpl_vars['coefficient']->value;?>
%</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <hr>
                                    <p>Коэффициент успешности это соотношение ваших денежных затрат к бесплатным баллам которые вы вновь вкладываете в рекламный бюджет.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="panel panel-warning panel-colorful cobalance">
                                <div class="pad-all">
                                    <p class="text-lg text-semibold"><i class="demo-pli-check icon-fw"></i> Баланс</p>
                                    <p class="mar-no">
                                        <span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['move_bonus_down']->value/100;?>
</span>
                                        Расход
                                    <p class="mar-no">
                                        <span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['move_bonus_clients']->value/100;?>
</span>
                                        Клиентам
                                    </p>
                                    <p class="mar-no">
                                        <span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['move_bonus_manager']->value/100;?>
</span>
                                        Сотрудникам
                                    </p>
                                    <hr>
                                    <p class="mar-no">
                                        <span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['move_bonus_up']->value/100;?>
</span>
                                        Пополнение
                                    </p>
                                    <p class="mar-no">
                                        <span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['move_bonus_up_eqvair']->value/100;?>
</span>
                                        Денежные
                                    </p>
                                    <p class="mar-no">
                                        <span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['move_bonus_up_clients']->value/100;?>
</span>
                                        От клиентов
                                    </p>
                                    <p class="mar-no">
                                        <span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['move_bonus_up_ls']->value/100;?>
</span>
                                        Из личного счёта
                                    </p>
                                </div>
                            </div>
                        </div>
					</div>
					
				
					
					<div class="row">
					    <div class="col-xs-12">
					        <div class="panel">
					            <div class="panel-heading">
					                <h3 class="panel-title">Детализированный список</h3>
					            </div>
					
					            <!--Data Table-->
					            <!--===================================================-->
					            <div class="panel-body">
                                    <?php if ($_smarty_tpl->tpl_vars['movement_funds']->value) {?>
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Дата операции</th>
                                                    <th>Клиент</th>
                                                    <th>Сотрудник</th>
                                                    <th>Сумма</th>
                                                    <th>Комментарий</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
$_from = $_smarty_tpl->tpl_vars['movement_funds']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
                                                    <tr>
                                                        <td><?php echo $_smarty_tpl->tpl_vars['value']->value["date"];?>
</td>
                                                        <td><?php echo $_smarty_tpl->tpl_vars['user_arr']->value[$_smarty_tpl->tpl_vars['value']->value["uid"]];?>
</td>
                                                        <td><?php echo $_smarty_tpl->tpl_vars['manager_arr']->value[$_smarty_tpl->tpl_vars['value']->value["mid"]];?>
</td>
                                                        <td><?php echo $_smarty_tpl->tpl_vars['value']->value["summ"]/100;?>
</td>
                                                        <td><?php echo $_smarty_tpl->tpl_vars['value']->value["comment"];?>
</td>
                                                    </tr>
                                                <?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } else { ?>
                                        <p>Операции отсутствуют</p>
                                    <?php }?>

					                <hr>
					            </div>
					            <!--===================================================-->
					            <!--End Data Table-->
					
					        </div>
					    </div>
					</div>
					
					
					
                </div>
                <!--===================================================-->
                <!--End page content-->
                 






            <?php }?>
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
<?php echo $_smarty_tpl->getSubTemplate ('aside.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>
 
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
<?php echo $_smarty_tpl->getSubTemplate ('mainnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



       <!-- FOOTER -->
				<!--===================================================-->
					<nav id="footer2-container">
						<div id="footer2">
						<?php echo $_smarty_tpl->getSubTemplate ('footer2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

						</div>
					</nav>
				<!--===================================================-->
				<!--END FOOTER -->	


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--==================================================
   
    <!--===================================================-->
    <!-- END SETTINGS -->
<?php } else { ?>
<?php echo $_smarty_tpl->getSubTemplate ('login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }
}
?>