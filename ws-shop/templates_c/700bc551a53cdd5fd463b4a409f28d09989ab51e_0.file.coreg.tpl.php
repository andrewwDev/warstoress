<?php /* Smarty version 3.1.27, created on 2017-10-09 17:45:32
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/coreg.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:31108044759dba7ac81f112_58854123%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '700bc551a53cdd5fd463b4a409f28d09989ab51e' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/coreg.tpl',
      1 => 1507560566,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '31108044759dba7ac81f112_58854123',
  'variables' => 
  array (
    'login' => 0,
    'categories' => 0,
    'item' => 0,
    'geo_regions' => 0,
    'owner_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59dba7ac8a6e55_65818241',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59dba7ac8a6e55_65818241')) {
function content_59dba7ac8a6e55_65818241 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '31108044759dba7ac81f112_58854123';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<?php if ($_smarty_tpl->tpl_vars['login']->value) {?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/css/bootstrap-slider.min.css" />
<?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/bootstrap-slider.min.js"><?php echo '</script'; ?>
>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
<?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"><?php echo '</script'; ?>
>
<div id="container" class="effect aside-float aside-bright mainnav-lg">

	<!--NAVBAR-->
	<!--===================================================-->
	<header id="navbar">
		<div id="navbar-container" class="boxed">
			<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

		</div>

		<!--На ввод символов проверка-->
								<?php echo '<script'; ?>
 type="text/javascript"> 
									function proverka(input) { 
										var value = input.value; 
										var rep = /["><>'"`,#/\?@%]/; 
										if (rep.test(value)) { 
											value = value.replace(rep, ''); 
											input.value = value; 
										} 
									} 
									<?php echo '</script'; ?>
>
								<!-- /////////////////////////////// -->
	</header>
	<!--===================================================-->
	<!--END NAVBAR-->

	<div class="boxed">
		

		<!--CONTENT CONTAINER-->
		<!--===================================================-->
		<div id="content-container">

			<!--Page Title-->
			<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
			<div id="page-title">
				<h1 class="page-header text-overflow">Добавить компанию</h1>

				<!--Searchbox
				<div class="searchbox">
					<div class="input-group custom-search-form">
						<input type="text" class="form-control" placeholder="Search..">
						<span class="input-group-btn">
							<button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
						</span>
					</div>
				</div> -->
			</div>
			<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
			<!--End page title-->

			<div class="col-lg-8">
				<div class="panel" style="width:80%">
					<!-- Form wizard with Validation -->
					<!--===================================================-->
					<div id="demo-bv-wz">
						<div class="wz-heading pad-top">

							<!--Nav-->
							<ul class="row wz-step wz-icon-bw wz-nav-off mar-top">
								<li class="col-xs-3">
									<a data-toggle="tab" href="#demo-bv-tab1">
										<span class="text-danger"><i class="demo-pli-information icon-2x"></i></span>
										<p class="text-semibold mar-no">Юридическая информация</p>
									</a>
								</li>
								<li class="col-xs-3">
									<a data-toggle="tab" href="#demo-bv-tab2">
										<span class="text-warning"><i class="demo-pli-male icon-2x"></i></span>
										<p class="text-semibold mar-no">Публичная</p>
									</a>
								</li>
								<li class="col-xs-3">
									<a data-toggle="tab" href="#demo-bv-tab3">
										<span class="text-info"><i class="demo-pli-home icon-2x"></i></span>
										<p class="text-semibold mar-no">Тарификация</p>
									</a>
								</li>
								<li class="col-xs-3">
									<a data-toggle="tab" href="#demo-bv-tab4">
										<span class="text-success"><i class="demo-pli-medal-2 icon-2x"></i></span>
										<p class="text-semibold mar-no">Старт</p>
									</a>
								</li>
							</ul>
						</div>

						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<!--End page title-->

						<!--progress bar-->
						<div class="progress progress-xs">
							<div class="progress-bar progress-bar-primary"></div>
						</div>


						<!--Form-->
						<form id="shopRegister" action="backend/set_shop.php" name="shop_registration" method="POST" class="form-horizontal" enctype="multipart/form-data">
							<div class="panel-body">
								<div class="tab-content">


									<!--First tab-->
									<div id="demo-bv-tab1" class="tab-pane">
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Данное название используется для понимания кто вы :)">Юридическое название <span class="red">*</span></label>
											<div class="col-lg-7">
												<input type="text" class="form-control" id="courname" name="courname" placeholder="Юридическое название или если физлицо пишите что физлицо">
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Достаточно только ИНН">ИНН <span class="red">*</span></label>
											<div class="col-lg-7">
												<input type="text" class="form-control" id="INN" name="INN" placeholder="ИНН" required> 
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Для физлиц продублировать ИНН">ОГРН <span class="red">*</span></label>
											<div class="col-lg-7">
												<input type="text" class="form-control" id="OGRN" name="OGRN" required placeholder="ОГРН">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Данный телефон используется для связи с вами и не показывается для игроков">Телефон для связи <span class="red">*</span></label>
											<div class="col-lg-7">
												<input type="text" class="form-control" id="cotel" name="cotel" required placeholder="Пример 89204244847 указывайте только цифры">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Данный адрес используется в случае отправки документов от нас">Юридический адрес <span class="red">*</span></label>
											<div class="col-lg-7">
												<input type="text" class="form-control" id="couradress"  name="couradress" required placeholder="Юридический адрес">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Если вы имеете группу Вконтакте напиши нам, чтобы когда мы писали о вас в нашей группе сразу указали и вашу :)">У меня есть группа Вконтакте</label>
											<div class="col-lg-7">
												<input type="text" class="form-control" id="covk" name="vk_id" placeholder="id группы Вконтакте">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Есть доставка? Пишите по каким городам работаете :)">Доставка товаров по городу</label>
											<div class="col-lg-7">
												<input type="text" class="form-control" id="cotown" name="delivery" placeholder="Доставка товаров по городу">
											</div>
										</div>
										<p class="text-center">Поля отмеченные значком <span class="red">*</span>, являются обязательными.</p>
									</div>



									<!--Second tab-->
									<div id="demo-bv-tab2" class="tab-pane fade">
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Данное название отображается в приложении в списке">Название <span class="red">*</span></label>
											<div class="col-lg-7">
												<input type="text" placeholder="Название(Не более 20 символов)"  maxlength="20" id="copubname" name="copubname" required class="form-control">
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Используется для регистрации других игроков в вашу команду. Вписывайте только английские буквы, без цифр. Не более 12 символов.">Хэштэг компании<span class="red">*</span></label>
					                        <div class="col-lg-7">
					                            <input type="text" onkeyup="return proverka(this);" maxlength="12" class="form-control" name="shopnick" id="shopnick" placeholder="Хэштэг компании">
												<div id="check_shopnick" style="display: none; color: red; font-weight: bold;">
													Такой хэштег занят
												</div>
											</div>
					                    </div>
										
											<?php echo '<script'; ?>
>
                                                $( "#shopnick" ).blur(function() {
                                                    var shopnick = {shopnick: $('#shopnick').val()};
                                                    $.ajax({
                                                        url: 'backend/check_shopnick.php',
                                                        data: shopnick,
                                                        type: 'POST',
                                                        success: function (data) {
                                                            console.log(data);
                                                            if (data == 1) {
                                                                $("#check_shopnick").show();
                                                                $("#shopnick").addClass("errors");
                                                            } else {
                                                                $("#check_shopnick").hide();
                                                                $("#shopnick").removeClass("errors");
                                                            }
                                                        }
                                                    });
                                                });
											<?php echo '</script'; ?>
>
										
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Высвечивается под названием в приложении">Краткое описание. Максимум три слова</label>
											<div class="col-lg-7">
												<input type="text" maxlength="100" placeholder="Не более 100 символов" name="copublessinfo" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Вы можете выбрать две рубрики">Рубрика <span class="red">*</span></label>
											<div class="col-lg-7">
												<select id="select_comp" name="heading[]" class="selectpicker title="Выберите категорию" multiple data-max-options="2" data-live-search="true">
                                                <?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
													<option value="<?php echo $_smarty_tpl->tpl_vars['item']->value["csid"];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value["title"];?>
</option>
                                                <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Максимум 250 символов">Подробное описание</label>
											<div class="col-lg-7">
												<input type="text" maxlength="250" placeholder="Подробное описание" name="copubinfo" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Выберите оптимальное время работы. Мы дорабатываем систему работы по дням недели.">Время работы</label>
											<div class="col-lg-7">
												<b id="show_time">9:00 - 18:00</b> <br>
												<input id="slider_time" name="copubworktime" type="text" class="span2" value="" data-slider-min="0" data-slider-max="1440" data-slider-tooltip="hide" data-slider-step="30" data-slider-value="[540,1080]"/>
											</div>
										</div>
										<?php echo '<script'; ?>
>
                                            var slider_time = $('#slider_time').slider({
                                                formatter: function(value) {
                                                    return 'Время работы: ' + value + ' %';
                                                }
                                            });
                                            slider_time.change(function(e) {
                                                var hours1 = Math.floor(e.value.newValue["0"] / 60);
                                                var minutes1 = e.value.newValue["0"] - (hours1 * 60);

                                                if (hours1.length == 1) hours1 = '0' + hours1;
                                                if (minutes1.length == 1) minutes1 = '0' + minutes1;
                                                if (minutes1 == 0) minutes1 = '00';
                                                if (hours1 == 0) {
                                                    hours1 = 00;
                                                    minutes1 = minutes1;
                                                }

                                                var hours2 = Math.floor(e.value.newValue["1"] / 60);
                                                var minutes2 = e.value.newValue["1"] - (hours2 * 60);

                                                if (hours2.length == 1) hours2 = '0' + hours2;
                                                if (minutes2.length == 1) minutes2 = '0' + minutes2;
                                                if (minutes2 == 0) minutes2 = '00';
                                                if (hours2 == 24) {
                                                    hours2 = 00;
                                                    minutes2 = "00";
                                                }

                                                $('#show_time').html(hours1 + ':' + minutes1 + ' - ' + hours2 + ':' + minutes2);
                                            });
										<?php echo '</script'; ?>
>
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="В каком регионе России вы работаете">Регион</label>
											<div class="col-lg-7">
												<select id="select_region" name="region" class="selectpicker" data-live-search="true">
													<?php
$_from = $_smarty_tpl->tpl_vars['geo_regions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
														<option value="<?php echo $_smarty_tpl->tpl_vars['item']->value["geo_reg_id"];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value["geo_reg_name"];?>
</option>
													<?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
												</select>
											</div>
										</div>
										<div id="city" style="display: none;">
											<div class="form-group">
												<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="И в каком городе соответственно">Город</label>
												<div class="col-lg-7">
													<select id="select_city" name="city" class="selectpicker" data-live-search="true">
													</select>
												</div>
											</div>
										</div>
                                        
										<?php echo '<script'; ?>
>
                                            $('#select_region').on('changed.bs.select', function (e) {
                                                console.log($('#select_region').val());
                                                var region = {region: $('#select_region').val()};
                                                $.ajax({
                                                    url: 'backend/get_city.php',
                                                    data: region,
                                                    type: 'POST',
                                                    success: function (data) {
                                                        data = JSON.parse(data);
                                                        $('#select_city').html(' ');
                                                        var option = '';
                                                        for (var i=0;i<data.length;i++){
                                                            option += '<option value="'+ data[i].geo_city_id + '">' + data[i].geo_city_name + '</option>';
                                                        }
                                                        $('#select_city').append(option);
                                                        $('#select_city').selectpicker('refresh');
                                                        $("#city").show();
                                                    }
                                                });
                                            });
										<?php echo '</script'; ?>
>
                                        
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Данный адрес показывается для ваших клиентов">Фактический адрес</label>
											<div class="col-lg-7">
												<input id="realAddress" type="text" placeholder="Город, Улица, Дом" name="copubgeoadress" class="form-control">
												<i class="ion-location"></i>
											</div>
										</div>

										<!---==========-->

										<div class="panel">
											<div class="panel-heading">
												<h3 class="panel-title add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Просим внимательно указать геопозицию. Мы тестируем сервис и нам важно заполненное поле">Геопозиция</h3>
											</div>
											<div class="panel-body" style="padding: 0">
												<!-- Marker -->
												<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
											    <?php echo '<script'; ?>
 src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"><?php echo '</script'; ?>
>
												<div id="map" style="height:300px"></div>
												<?php echo '<script'; ?>
>
													ymaps.ready(init);

													function init() {
													    var myPlacemark,
													        myMap = new ymaps.Map('map', {
													            center: [55.753994, 37.622093],
													            zoom: 9,
													            controls: ['zoomControl', 'searchControl', 'fullscreenControl']
													        }, {
													            searchControlProvider: 'yandex#search'
													        });

													    // Слушаем клик на карте.
													    myMap.events.add('click', function (e) {
													        var coords = e.get('coords');
													        $("#YGeoLoc").val(coords);
													        

													        // Если метка уже создана – просто передвигаем ее.
													        if (myPlacemark) {
													            myPlacemark.geometry.setCoordinates(coords);
													        }
													        // Если нет – создаем.
													        else {
													            myPlacemark = createPlacemark(coords);
													            myMap.geoObjects.add(myPlacemark);
													            // Слушаем событие окончания перетаскивания на метке.
													            myPlacemark.events.add('dragend', function () {
													                getAddress(myPlacemark.geometry.getCoordinates());
													            });
													        }
													        var address = getAddress(coords);
													    });

													    // Создание метки.
													    function createPlacemark(coords) {
													        return new ymaps.Placemark(coords, {
													            iconCaption: 'поиск...'
													        }, {
													            preset: 'islands#violetDotIconWithCaption',
													            draggable: true
													        });
													    }

													    // Определяем адрес по координатам (обратное геокодирование).
													    function getAddress(coords) {
													        myPlacemark.properties.set('iconCaption', 'поиск...');
													        ymaps.geocode(coords).then(function (res) {
													            var firstGeoObject = res.geoObjects.get(0);
													            $("#realAddress").val(firstGeoObject.getAddressLine());

													            myPlacemark.properties
													                .set({
													                    // Формируем строку с данными об объекте.
													                    iconCaption: [
													                        // Название населенного пункта или вышестоящее административно-территориальное образование.
													                        firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
													                        // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
													                        firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
													                    ].filter(Boolean).join(', '),
													                    // В качестве контента балуна задаем строку с адресом объекта.
													                    balloonContent: firstGeoObject.getAddressLine()
													                });
													        });
													    }
													}
												<?php echo '</script'; ?>
>
												<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
											</div>
											<input type="hidden" id="YGeoLoc" name="YGeoLoc">
										</div>

										<!---==========-->


										<style>
											.empty-image {
												width: 64px;
											    height: 64px;
											    border-radius: 1px;
											    text-align: center;
											    border: solid 1px #c1c1c1;
											    color: #c1c1c1;
											    font-size: 28px;
											}

											.added-images:not(:last-child) {
												width: 69px;
												padding-right: 5px;
											}
										</style>

										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Данный телефон видят ваши клиенты">Телефон для связи</label>
											<div class="col-lg-7">
												<input type="text" placeholder="Телефон для связи" name="copubtel" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Убедительная просьба проверять размер фотографии">Большая фотография (размер 1280x960)</label>
											<div class="col-lg-7">
												<div class="fileform">
													<div id="fileformlabel-big"></div>
													<div class="selectbutton btn btn-success fileinput-button dz-clickable">Добавить</div>
													<input id="upload-big" accept="image/jpeg,image/png" type="file" name="img_big" onchange="getName(this.value,'upload-big');"/>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Выбирайте понятный логотип компании">Логотип (размер 96x96)</label>
											<div class="col-lg-7">
												<div class="fileform">
													<div id="fileformlabel-logo"></div>
													<div class="selectbutton btn btn-success fileinput-button dz-clickable">Добавить</div>
													<input id="upload-logo" accept="image/jpeg,image/png" type="file" name="img_logo" onchange="getName(this.value,'upload-logo');"/>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Действующий сайт вашей компании">Сайт</label>
											<div class="col-lg-7">
												<input type="text" placeholder="URL" name="copubsite" class="form-control">
											</div>
										</div>
										<p class="text-center">Поля отмеченные значком <span class="red">*</span>, являются обязательными.</p>



									</div>

									<!--Third tab-->

									<div id="demo-bv-tab3" class="tab-pane">
										<div class="row">
											<div class="col-md-12">
												<h4 class="text-main mar-btm">Клиенту</h4>

												<div class="form-group">
													<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Данное вознаграждение будет получать пользователь за покупку в вашей компании">
														<strong>Вознаграждение клиенту от чека: </strong>
													</label>
													<div class="col-lg-4">
														<input id="slideC" name="percent_client" data-slider-id='ex1Slider' type="text" data-slider-min="1" data-slider-max="100" data-slider-step="1" data-slider-value="7"/>
													</div>
													<label class="col-lg-3 control-label text-left">
														<span id="demo-range-def-val">7 %</span>
													</label>
												</div>
											</div>
										</div>
										
											
												
													
													
												
											
										
										
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Пользователь может оплатить часть покупки бонусами которые есть на его счету. Эти бонусы перечислятся на баланс вашей компании и вы сможете вновь выдать их другим клиентам">
														<strong>Процент от покупки, который можно оплатить бонусами: </strong>
													</label>
													<div class="col-lg-4">
														<input id="slidePerPay" name="bonusborderpercent" data-slider-id='ex2Slider' type="text" data-slider-min="50" data-slider-max="99" data-slider-step="1" data-slider-value="70"/>
													</div>
													<label class="col-lg-3 control-label text-left">
														<span id="val-slidePerPay">70 %</span>
													</label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Это итоговый процент который оплачивается из чека">
														<strong>Всего процент от чека: </strong>
													</label>
													<div class="col-lg-4">
														<input id="slideAll" name="percent_all" data-slider-id='ex2Slider' type="text" data-slider-min="3" data-slider-max="133" data-slider-step="1" data-slider-value="9"/>
													</div>
													<label class="col-lg-3 control-label text-left">
														<span id="demo-range-step-val">9 %</span>
													</label>
												</div>
												<br>
												<div class="form-group">
													<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Максимальное число бонусов, которые можно отдать за покупку, укажите пожалуйста в копейках, так как бонус=рублю">Не более</label>
													<div class="col-lg-7">
														<input id="realAddress" type="text" placeholder="в копейках" name="bonusbordervalue" class="form-control">
														<i class="ion-location"></i>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<h4 class="text-main mar-btm">Вознаграждение сотруднику</h4>
												<div class="form-group">
													<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Если ваш сотрудник рекомендует программу и по этой рекомендации ваш клиент регистрируется, то вы будете получать % с каждого бонуса вашего клиента. За это стоит поблагодарить сотрудника бонусами :) Вознаграждение списывается из баланса компании.">За подключение клиента</label>
													<div class="col-lg-7">
														<input id="realAddress" type="text" placeholder="Размер вознаграждения фиксированны" name="paytomanbynewuser" class="form-control">
														<i class="ion-location"></i>
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Укажите бонус который будет получать сотрудник за проведение каждой операции. Просто дополнительное вознаграждение. Вознаграждение списывается из баланса компании.">За выполнение операции (проводка чека)</label>
													<div class="col-lg-7">
														<input id="realAddress" type="text" placeholder="Размер вознаграждения фиксированны" name="paytomanbyoper" class="form-control">
														<i class="ion-location"></i>
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php echo '<script'; ?>
>
										var clientSlider = $('#slideC').slider({
											formatter: function(value) {
												return 'Процент от чека: ' + value + ' %';
											}
										});
										var perPaySlider = $('#slidePerPay').slider({
											formatter: function(value) {
												return 'Процент от покупки, который можно оплатить бонусами: ' + value + ' %';
											}
										});
										var allSlider = $('#slideAll').slider({
											formatter: function(value) {
												return 'Итого: ' + value + ' %';
											}
										});
										clientSlider.change(function(e) {
											allSlider.slider("setValue", Math.ceil(e.value.newValue * 1.338));
											$("#demo-range-def-val").text(e.value.newValue+" %");
											$("#demo-range-step-val").text(Math.ceil(e.value.newValue * 1.338) +" %");
										});
										perPaySlider.change(function(e) {
											$("#val-slidePerPay").text(e.value.newValue + " %");
										});
										allSlider.change(function(e) {
											clientSlider.slider("setValue", Math.ceil(e.value.newValue / 1.338));
											$("#demo-range-step-val").text(e.value.newValue+" %");
											$("#demo-range-def-val").text(Math.ceil(e.value.newValue / 1.338) +" %");
										});
									<?php echo '</script'; ?>
>



									<!--Fourth tab-->
									<div id="demo-bv-tab4" class="tab-pane  mar-btm text-center">
										<div id="before-form">
											<h4>Спасибо, за заполнение!</h4>
											<p class="text-muted">Если вы указали правильно нажмите «Подать на модерацию» и информация о вашей Компании поступит на модернизацию. С вами в ближайшее время свяжется наш сотрудник или компания одобрится автоматически. </p>
											<p>После прохождения проверки ваша Компания будет внесена в список авторизованных и клиенты начнут приходить к вам.</p>
											<!-- Accordion content -->



											<div class="col-lg-12">
												<div class="col-sm-12">
													<input id="agreement" class="magic-checkbox" type="checkbox" required>
													<label for="agreement">С условиями оферты в части участия как Партнёр сервиса ознакомлен и согласен.</label>
												</div>
											</div> 
										</div>
										<div id="after-form" style="display: none;">
											<h4>Ваша компания отправлена на модерацию!</h4>
										</div>
										  
									</div>
								</div>
							</div>


							<!--Footer button-->
							<div class="panel-footer text-right">
								<div class="box-inline">
									<input type="hidden" name="coowner" value="<?php echo $_smarty_tpl->tpl_vars['owner_id']->value;?>
">
									<button type="button" id="prev-btn" class="previous btn btn-primary">Назад</button>
									<button type="button" id="next-btn" class="next btn btn-primary" disabled>Дальше</button>
									<button type="submit" id="coreg-finish" class="finish btn btn-warning" disabled>Подать на модерацию</button>
								</div>
							</div>
						</form>
						<?php echo '<script'; ?>
>
						$('#shopRegister').keyup(function() {
							if ($('#demo-bv-tab1').hasClass("active")) {
								if ( $('#courname').val() == '' ) {
									return false;
								}
								
								if ( $('#INN').val() == '' ) {
									return false;
								}
								
								if ( $('#OGRN').val() == '' ) {
									return false;
								}
								
								if ( $('#cotel').val() == '' ) {
									return false;
								}
								
								if ( $('#couradress').val() == '' ) {
									return false;
								}
					
								if ( $('#couradress').val() == '' ) {
									return false;
								}
								$("#next-btn").attr("disabled", false);
							}
							if ($('#demo-bv-tab2').hasClass("active")) {
								if ( $('#copubname').val() == '' ) {
									return false;
								}
								$("#next-btn").attr("disabled", false);
							}
						});
						
						$("#next-btn").click(function(event) {
							if ($('#demo-bv-tab2').hasClass("active")) {
								$("#next-btn").attr("disabled", false);
							} else {
								$("#next-btn").attr("disabled", true);
							}
						});
						
						$( '#coreg-finish' ).click(function(event) {
							event.preventDefault();
							$("#coreg-finish").attr("disabled", true);
							$("#prev-btn").attr("disabled", true);
							$("#before-form").hide();

							var formData  = new FormData($( '#shopRegister' )[0]);

							$.ajax({
								url: 'backend/set_shop.php',
								data: formData,
								processData: false,
								contentType: false,
								type: 'POST',
								success: function (data) {
									console.log(data);
									$("#after-form").show();
								}
							});
						});
						<?php echo '</script'; ?>
>

						<!--Bordered Accordion-->
						<!--===================================================-->


					</div>


					<!--===================================================-->
					<!-- End Form wizard with Validation -->
					
				</div>


			</div>

			<!--Default Tabs (Right Aligned)-->
			<!--===================================================-->


			<!--Nav tabs-->
			<div class="nav nav-tabs tabs-right">


				<style type="text/css">
					.clear-contener {
						width: 5%;
						height: 10%
						
					
					}
				</style>
				<div class="clear-contener"></div>





				<div class="col-md-6 col-lg-4">
					
					<!--Bordered Accordion-->
					<!--===================================================-->
					<div class="panel-group accordion" id="demo-acc-info-outline" style="margin-right:30%; margin-left:-40%;">
						<div class="panel panel-bordered panel-info"  >

							<!--Accordion title-->
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-parent="#demo-acc-info-outline" data-toggle="collapse" href="#demo-acd-info-outline-1">О юридической информации</a>
								</h4>
							</div>

							<!--Accordion content-->
							<div class="panel-collapse collapse in" id="demo-acd-info-outline-1">
								<div class="panel-body">
									Юридическая информация создана для подтверждения прав на осуществление коммерческой деятельности вашей компании или оказания рекламных услуг для вас как для физического лица.
									Данная информация заполняется в соответствии с договором оферты для осуществления рекламных услуг в рамках данного договора.<br><br>
									Обращаем внимание что после отправки на модерацию созданной компании Вам требуется предоставить нам в течение 14 дней следующие документы:<br>
									Подписанный оригинал заявки на участие в Сервисе в качестве Партнера.<br>
									подписанный оригинал согласия на обработку персональных данных.<br>
									Для физического лица: копия паспорта.<br>
									Для индивидуального предпринимателя: копия свидетельства (или листа записи) о регистрации в качестве индивидуального предпринимателя, копия паспорта индивидуального предпринимателя.<br>
									Для юридического лица: копия свидетельства (или листа записи) о регистрации в качестве юридического лица, а также полномочия, в силу которых представитель юридического лица наделен правом подписания заявки и других необходимых для участия в Сервисе документов (решение / протокол о назначении руководителя, устав, доверенность и т.д.), копию паспорта представителя юридического лица.<br>
									<br>Передаваемые данные являются подтверждением о ваших намерениях на получение рекламных услуг нашего Сервиса.<br>
									Данные в праве передать лично Представителю Администратора (если таковой имеется в вашем городе) или отправить заказным письмом (если в вашем городе не имеется официального представителя Администратора Сервиса) на адрес Сервиса.
								</div>
							</div>
						</div>
						<div class="panel panel-bordered panel-info" ">

							<!--Accordion title-->
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-parent="#demo-acc-info-outline" data-toggle="collapse" href="#demo-acd-info-outline-2">О публичной информации</a>
								</h4>
							</div>

							<!--Accordion content-->
							<div class="panel-collapse collapse" id="demo-acd-info-outline-2">
								<div class="panel-body">
									Публичная информация отображается для всех пользователей. При заполнении будьте внимательны, ведь каждое слово написанное вами будет видеть ваш клиент. Обратите внимание что вам следует обязательно правильно отметить вашу компанию на карте. Делается это для игрового процесса захвата компаний и зданий.
								</div>
							</div>
						</div>
						<div class="panel panel-bordered panel-info" ">

							<!--Accordion title-->
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-parent="#demo-acc-info-outline" data-toggle="collapse" href="#demo-acd-info-outline-3">О тарифах</a>
								</h4>
							</div>

							<!--Accordion content-->
							<div class="panel-collapse collapse" id="demo-acd-info-outline-3">
								<div class="panel-body">
									Тариф всегда один. Вы можете выбрать каким процентом от суммы чека вознаградить ваших клиентов в таком случае мы добавляем процент на бонусные вознаграждения всем другим пользователям, либо вы указываете сколько готовы оплачивать всего процентов от суммы чека, а мы уже делим на сумму для клиента и бонусные вознаграждения. В зависимости от выбранного вами процента вознаграждения пользователю 
									добавляется реферальное вознаграждение.
									Тариф можно настроить исходя из процента который вы хотите дарить вашему клиенту
									или из общего процента затрат.
									Обратите внимание, весь остаток от партнерской программы является оплатой за
									привлеченного к вам клиента..
								</div>
							</div>
						</div>
					</div>
					<!--===================================================-->
					<!--End Bordered Accordion-->
					
				</div>





			</div>
			<!--===================================================-->

		</div>
		<!--===================================================-->
		<!--END CONTENT CONTAINER-->



		<!--ASIDE-->
		<!--===================================================-->
		<aside id="aside-container">
			<div id="aside">
				<?php echo $_smarty_tpl->getSubTemplate ('aside.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

			</div>
		</aside>
		<!--===================================================-->
		<!--END ASIDE-->


		<!--MAIN NAVIGATION-->
		<!--===================================================-->
		<nav id="mainnav-container">
			<div id="mainnav">
				<?php echo $_smarty_tpl->getSubTemplate ('mainnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

			</div>
		</nav>
		<!--===================================================-->
		<!--END MAIN NAVIGATION-->

	</div>
	
	
	
<!-- FOOTER -->
		<!--===================================================-->
		<nav id="footer2-container">
			<div id="footer2">
				<?php echo $_smarty_tpl->getSubTemplate ('footer2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

			</div>
		</nav>
		<!--===================================================-->
	<!--END FOOTER -->
	

	<!-- SCROLL PAGE BUTTON -->
	<!--===================================================-->
	<button class="scroll-top btn">
		<i class="pci-chevron chevron-up"></i>
	</button>
	<!--===================================================-->



</div>
<!--===================================================-->
<!-- END OF CONTAINER -->



<!-- SETTINGS - DEMO PURPOSE ONLY -->
<!--===================================================-->

<!--===================================================-->
<!-- END SETTINGS -->
<?php } else { ?>
<?php echo $_smarty_tpl->getSubTemplate ('login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }
}
?>