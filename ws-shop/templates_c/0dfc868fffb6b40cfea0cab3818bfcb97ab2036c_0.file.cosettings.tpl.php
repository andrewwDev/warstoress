<?php /* Smarty version 3.1.27, created on 2018-07-26 22:06:24
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/cosettings.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:10311345475b5a1bb00e1794_97737964%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0dfc868fffb6b40cfea0cab3818bfcb97ab2036c' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/cosettings.tpl',
      1 => 1532631980,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10311345475b5a1bb00e1794_97737964',
  'variables' => 
  array (
    'login' => 0,
    'current_shop' => 0,
    'shop_id' => 0,
    'categories' => 0,
    'item' => 0,
    'select_cats' => 0,
    'current_work_time' => 0,
    'geo_regions' => 0,
    'geo_city' => 0,
    'reg_and_city' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5b5a1bb0177d50_11201070',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5b5a1bb0177d50_11201070')) {
function content_5b5a1bb0177d50_11201070 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '10311345475b5a1bb00e1794_97737964';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<?php if ($_smarty_tpl->tpl_vars['login']->value) {?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/css/bootstrap-slider.min.css" />
<?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/bootstrap-slider.min.js"><?php echo '</script'; ?>
>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
<?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"><?php echo '</script'; ?>
>
<div id="container" class="effect aside-float aside-bright mainnav-lg">

	<!--NAVBAR-->
	<!--===================================================-->
	<header id="navbar">
		<div id="navbar-container" class="boxed">
			<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

		</div>
	</header>
	<!--===================================================-->
	<!--END NAVBAR-->

	<div class="boxed">

		<!--CONTENT CONTAINER-->
		<!--===================================================-->
		<div id="content-container">

			<!--Page Title-->
			<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
			<div id="page-title">
				<h1 class="page-header text-overflow">Настройки</h1>


			</div>
			<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
			<!--End page title-->

			<div class="col-lg-6">
				<div class="panel">
					<div class="panel-heading">
						<h3 class="panel-title">Карточка магазина</h3>
					</div>


					<!-- BASIC FORM ELEMENTS -->
					<!--===================================================-->
					<div id="after" class="panel-body form-horizontal form-padding" style="display: none;">
						<h5>Данные успешно изменены</h5>
					</div>
					<div id="before">
					<form action="backend/set_settings.php" method="post" id="shop_settings" class="panel-body form-horizontal form-padding" enctype="multipart/form-data">
						<!--First tab-->

						<?php if (!empty($_smarty_tpl->tpl_vars['current_shop']->value)) {?>
						<div id="demo-bv-tab1" class="tab-pane">
							<?php if ($_smarty_tpl->tpl_vars['current_shop']->value['moderator_flag'] == 2) {?>
								<p style="background: darkred; color: #fff; text-align: center;">Компания не прошла модерацию. Проверьте правильность заполнения данных.</p>
							<?php }?>
							<input type="hidden" name="shop_id" value="<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
" readonly>
							<div class="form-group">
								<label class="col-lg-3 control-label">Юридическое название</label>
								<div class="col-lg-7">
									<input type="text" readonly class="form-control" name="courname" id="current_shop.title" placeholder="Юридическое название компании" value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['ur_title'];?>
">
								</div>
							</div>

							<div class="form-group">
								<label class="col-lg-3 control-label">Хэштэг компании</label>
								<div class="col-lg-7">
									<input type="text" onkeyup="return proverka(this);" maxlength="12" class="form-control" name="shopnick" id="shopnick" placeholder="Хэштэг компании" value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['shopnick'];?>
">
									<div id="check_shopnick" style="display: none; color: red; font-weight: bold;">
										Такой хэштег занят
									</div>
								</div>
							</div>
                            
								<?php echo '<script'; ?>
>
                                    $( "#shopnick" ).blur(function() {
                                        var shopnick = {shopnick: $('#shopnick').val()};
                                        $.ajax({
                                            url: 'backend/check_shopnick.php',
                                            data: shopnick,
                                            type: 'POST',
                                            success: function (data) {
                                                console.log(data);
                                                if (data == 1) {
                                                    $("#check_shopnick").show();
                                                    $("#shopnick").addClass("errors");
                                                } else {
                                                    $("#check_shopnick").hide();
                                                    $("#shopnick").removeClass("errors");
                                                }
                                            }
                                        });
                                    });
								<?php echo '</script'; ?>
>
                            

							<!--На ввод символов проверка-->
								<?php echo '<script'; ?>
 type="text/javascript">
									function proverka(input) {
										var value = input.value;
										var rep = /["><>'"`,/\?@%]/;
										if (rep.test(value)) {
											value = value.replace(rep, '');
											input.value = value;
										}
									}
									<?php echo '</script'; ?>
>
								<!-- /////////////////////////////// -->

							<div class="form-group">
								<label class="col-lg-3 control-label">ИНН</label>
								<div class="col-lg-7">
									<input type="text" readonly class="form-control" name="INN" placeholder="ИНН" Value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['ur_inn'];?>
">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">ОГРН</label>
								<div class="col-lg-7">
									<input type="text" onkeyup="return proverka(this);" class="form-control" name="OGRN" placeholder="ОГРН" Value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['ur_ogrn'];?>
">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Телефон</label>
								<div class="col-lg-7">
									<input type="text" oninput="var v = this.value;this.value=v.replace(/[.,]\d+/,'.5')" class="form-control" name="cotel" placeholder="Телефон для связи" Value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['ur_phone'];?>
">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Юридический адрес</label>
								<div class="col-lg-7">
									<input type="text" readonly class="form-control" name="couradress" placeholder="Юридический адрес" Value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['ur_address'];?>
">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">У меня есть группа Вконтакте</label>
								<div class="col-lg-7">
									<input type="text" oninput="var v = this.value;this.value=v.replace(/[.,]\d+/,'.5')" class="form-control" name="vk_id" placeholder="id группы Вконтакте" Value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['vk_id'];?>
">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Доставка товаров по городу</label>
								<div class="col-lg-7">
									<input type="text" onkeyup="return proverka(this);" class="form-control" name="delivery" placeholder="Доставка товаров по городу" Value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['delivery'];?>
">
								</div>
							</div>
						</div>
						<!--Second tab-->

						<div class="form-group">
							<label class="col-lg-3 control-label">Название</label>
							<div class="col-lg-7">
								<input type="text" onkeyup="return proverka(this);" class="form-control" placeholder="Не более 25 символов" maxlength="25"  name="copubname" value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['title'];?>
" id="shop_title">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">Краткое описание</label>
							<div class="col-lg-7">
								<input type="text" onkeyup="return proverka(this);" placeholder="Не более 200 символов" maxlength="200" name="copublessinfo" class="form-control" Value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['slogon'];?>
">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label" data-live-search="true">Рубрика</label>
							<div class="col-lg-7">
								<select id="select_comp" name="heading[]" class="selectpicker title="Выберите категорию" multiple data-max-options="2" data-live-search="true">
                                <?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['item']->value["csid"];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value["title"];?>
</option>
                                <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
								</select>
							</div>
						</div>
							<?php echo '<script'; ?>
>
								<?php if (count($_smarty_tpl->tpl_vars['select_cats']->value) > 1) {?>
                               		$('#select_comp').selectpicker('val', ['<?php echo $_smarty_tpl->tpl_vars['select_cats']->value[0][0];?>
','<?php echo $_smarty_tpl->tpl_vars['select_cats']->value[1][0];?>
']);
								<?php } else { ?>
                                	$('#select_comp').selectpicker('val', <?php echo $_smarty_tpl->tpl_vars['select_cats']->value[0][0];?>
);
								<?php }?>
							<?php echo '</script'; ?>
>
						<div class="form-group">
							<label class="col-lg-3 control-label">Подробное описание</label>
							<div class="col-lg-7">
								<input type="text" placeholder="Подробное описание" name="copubinfo" class="form-control" Value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['description'];?>
">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">Время работы</label>
							<div class="col-lg-7">
								<b id="show_time">
									<?php if (strlen($_smarty_tpl->tpl_vars['current_shop']->value['worktime']) > 0) {?>
										<?php echo $_smarty_tpl->tpl_vars['current_work_time']->value;?>

									<?php } else { ?>
										9:00 - 18:00
									<?php }?>
								</b> <br>
								<input id="slider_time" name="copubworktime" type="text" class="span2" value="" data-slider-min="0" data-slider-max="1440" data-slider-tooltip="hide" data-slider-step="30" data-slider-value="[<?php if (strlen($_smarty_tpl->tpl_vars['current_shop']->value['worktime']) > 0) {
echo $_smarty_tpl->tpl_vars['current_shop']->value['worktime'];
} else { ?>540,1080<?php }?>]"/>
							</div>
						</div>
							<?php echo '<script'; ?>
>
                                var slider_time = $('#slider_time').slider({
                                    formatter: function(value) {
                                        return 'Время работы: ' + value + ' %';
                                    }
                                });
                                slider_time.change(function(e) {
                                    var hours1 = Math.floor(e.value.newValue["0"] / 60);
                                    var minutes1 = e.value.newValue["0"] - (hours1 * 60);

                                    if (hours1.length == 1) hours1 = '0' + hours1;
                                    if (minutes1.length == 1) minutes1 = '0' + minutes1;
                                    if (minutes1 == 0) minutes1 = '00';
                                    if (hours1 == 0) {
                                        hours1 = 00;
                                        minutes1 = minutes1;
                                    }

                                    var hours2 = Math.floor(e.value.newValue["1"] / 60);
                                    var minutes2 = e.value.newValue["1"] - (hours2 * 60);

                                    if (hours2.length == 1) hours2 = '0' + hours2;
                                    if (minutes2.length == 1) minutes2 = '0' + minutes2;
                                    if (minutes2 == 0) minutes2 = '00';
                                    if (hours2 == 24) {
                                        hours2 = 00;
                                        minutes2 = "00";
                                    }

                                    $('#show_time').html(hours1 + ':' + minutes1 + ' - ' + hours2 + ':' + minutes2);
                                });
							<?php echo '</script'; ?>
>
							<div class="form-group">
								<label class="col-lg-3 control-label">Регион</label>
								<div class="col-lg-7">
									<select id="select_region" name="region" class="selectpicker" data-live-search="true">
                                        <?php
$_from = $_smarty_tpl->tpl_vars['geo_regions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
											<option value="<?php echo $_smarty_tpl->tpl_vars['item']->value["geo_reg_id"];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value["geo_reg_name"];?>
</option>
                                        <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
									</select>
								</div>
							</div>
							<div id="city"">
								<div class="form-group">
									<label class="col-lg-3 control-label">Город</label>
									<div class="col-lg-7">
										<select id="select_city" name="city" class="selectpicker" data-live-search="true">
                                            <?php
$_from = $_smarty_tpl->tpl_vars['geo_city']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
												<option value="<?php echo $_smarty_tpl->tpl_vars['item']->value["geo_city_id"];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value["geo_city_name"];?>
</option>
                                            <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
										</select>
									</div>
								</div>
							</div>
							<?php echo '<script'; ?>
>
                                $('#select_region').selectpicker('val', <?php echo $_smarty_tpl->tpl_vars['reg_and_city']->value[0]["geo_reg_id"];?>
);
                                $('#select_city').selectpicker('val', <?php echo $_smarty_tpl->tpl_vars['reg_and_city']->value[0]["geo_city_id"];?>
);

                                
                                $('#select_region').on('changed.bs.select', function (e) {
                                    console.log($('#select_region').val());
                                    var region = {region: $('#select_region').val()};
                                    $.ajax({
                                        url: 'backend/get_city.php',
                                        data: region,
                                        type: 'POST',
                                        success: function (data) {
                                            data = JSON.parse(data);
                                            $('#select_city').html(' ');
                                            var option = '';
                                            for (var i=0;i<data.length;i++){
                                                option += '<option value="'+ data[i].geo_city_id + '">' + data[i].geo_city_name + '</option>';
                                            }
                                            $('#select_city').append(option);
                                            $('#select_city').selectpicker('refresh');
                                        }
                                    });
                                });
                                
							<?php echo '</script'; ?>
>
							<div class="form-group">
								<label class="col-lg-3 control-label">Фактический адрес</label>
								<div class="col-lg-7">
									<input id="realAddress" onkeyup="return proverka(this);" type="text" placeholder="Город, Улица, Дом" name="copubgeoadress" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['address'];?>
">
									<i class="ion-location"></i>
								</div>
							</div>

							<!---==========-->

							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Геопозиция</h3>
								</div>
								<div class="panel-body" style="padding: 0">
									<!-- Marker -->
									<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
									<?php echo '<script'; ?>
 src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"><?php echo '</script'; ?>
>
									<div id="map" style="height:300px"></div>
									<?php echo '<script'; ?>
>
										ymaps.ready(init);

										function init() {
											var myPlacemark,
													myMap = new ymaps.Map('map', {
														center: [<?php if (strlen($_smarty_tpl->tpl_vars['current_shop']->value['YGeoLoc']) > 0) {
echo $_smarty_tpl->tpl_vars['current_shop']->value['YGeoLoc'];
} else { ?>55.753994, 37.622093<?php }?>],
														zoom: 9,
														controls: ['zoomControl', 'searchControl', 'fullscreenControl']
													}, {
														searchControlProvider: 'yandex#search'
													});



                                            <?php if (strlen($_smarty_tpl->tpl_vars['current_shop']->value['YGeoLoc']) > 0) {?>
												myPlacemark = createPlacemark([<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['YGeoLoc'];?>
], true);
												myMap.geoObjects.add(myPlacemark);
											<?php }?>

											// Слушаем клик на карте.
											myMap.events.add('click', function (e) {
												var coords = e.get('coords');
												$("#YGeoLoc").val(coords);


												// Если метка уже создана – просто передвигаем ее.
												if (myPlacemark) {
													myPlacemark.geometry.setCoordinates(coords);
												}
												// Если нет – создаем.
												else {
													myPlacemark = createPlacemark(coords);
													myMap.geoObjects.add(myPlacemark);
													// Слушаем событие окончания перетаскивания на метке.
													myPlacemark.events.add('dragend', function () {
														getAddress(myPlacemark.geometry.getCoordinates());
													});
												}
												var address = getAddress(coords);
											});

											// Создание метки.
											function createPlacemark(coords, default_point) {
												if (default_point) {
													return new ymaps.Placemark(coords, {
														iconCaption: '<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['address'];?>
'
													}, {
														preset: 'islands#violetDotIconWithCaption',
														draggable: true
													});
												} else {
													return new ymaps.Placemark(coords, {
														iconCaption: 'поиск...'
													}, {
														preset: 'islands#violetDotIconWithCaption',
														draggable: true
													});
												}

											}

											// Определяем адрес по координатам (обратное геокодирование).
											function getAddress(coords) {
												myPlacemark.properties.set('iconCaption', 'поиск...');
												ymaps.geocode(coords).then(function (res) {
													var firstGeoObject = res.geoObjects.get(0);
													$("#realAddress").val(firstGeoObject.getAddressLine());

													myPlacemark.properties
															.set({
																// Формируем строку с данными об объекте.
																iconCaption: [
																	// Название населенного пункта или вышестоящее административно-территориальное образование.
																	firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
																	// Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
																	firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
																].filter(Boolean).join(', '),
																// В качестве контента балуна задаем строку с адресом объекта.
																balloonContent: firstGeoObject.getAddressLine()
															});
												});
											}
										}
									<?php echo '</script'; ?>
>
									<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
								</div>
								<input type="hidden" id="YGeoLoc" name="YGeoLoc" value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['YGeoLoc'];?>
">
							</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">Юридический адрес</label>
							<div class="col-lg-7">
								<input type="text" readonly placeholder="Город,Улица,Дом" name="couradress" class="form-control" Value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['ur_address'];?>
">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">Телефон для связи</label>
							<div class="col-lg-7">
								<input type="text" onkeyup="return proverka(this);" placeholder="Телефон для связи" name="copubtel" class="form-control" Value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['phone'];?>
">
							</div>
						</div>
						<!--<div class="form-group">
							<label class="col-lg-3 control-label">Фотографии</label>
							<div class="col-lg-7">
								<td><img class="img-md" src="img/profile-photos/1.png" alt="Sample Image"></td>
								<td><img class="img-md" src="img/profile-photos/1.png" alt="Sample Image"></td>
								<div class="bord-top pad-ver">
									<span class="btn btn-success fileinput-button dz-clickable">
										<i class="fa fa-plus"></i>
										<span>Add files...</span>
									</span>
								</div>
							</div>
						</div>-->
						<div class="form-group">
							<label class="col-lg-3 control-label">Сайт</label>
							<div class="col-lg-7">
								<input type="text"  onkeyup="return proverka(this);" placeholder="Сайт" name="copubsite" class="form-control" Value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['url'];?>
">
							</div>
						</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Большая фотография (размер 1280x960)</label>
								<div class="col-lg-7">
									<div class="fileform">
										<div id="fileformlabel-big"></div>
										<div class="selectbutton btn btn-success fileinput-button dz-clickable">Добавить</div>
										<input id="upload-big" accept="image/jpeg,image/png" type="file" name="img_big" onchange="getName(this.value,'upload-big');"/>
									<?php echo '<?php
									';?>$img_big = "#upload-big";
									imagepng(
										imagecreatefromstring(
											file_get_contents($img_big)
										),
										"output.jpeg"
									);
									<?php echo '?>';?>
									</div>
									<div class="checkbox"> 
										<input id="demo-checkbox-1" class="magic-checkbox" type="checkbox" name="acceptTerms" data-bv-field="acceptTerms"> 
										<label for="demo-checkbox-1">Фотография загружена</label> 
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Логотип (размер 96x96)</label>
								<div class="col-lg-7">
									<div class="fileform">
										<div id="fileformlabel-logo"></div>
										<div class="selectbutton btn btn-success fileinput-button dz-clickable">Добавить</div>
										<input id="upload-logo" accept="image/jpeg,image/png" type="file" name="img_logo" onchange="getName(this.value,'upload-logo');"/>
									<?php echo '<?php
									';?>$upload-logo = "#upload-logo";
									imagepng(
										imagecreatefromstring(
											file_get_contents($upload-logo)
										),
										"output.png"
									);
									<?php echo '?>';?>
									</div>
									<div class="checkbox"> 
										<input id="demo-checkbox-1" class="magic-checkbox" type="checkbox" name="acceptTerms" data-bv-field="acceptTerms"> 
										<label for="demo-checkbox-1">Логотип загружен</label> 
									</div>
								</div>
							</div>



						<h4 class="text-main mar-btm">Клиенту</h4>

						<div class="form-group">
								<h4 class="text-main mar-btm" style="margin-left:35px;">Cashback</h4>
							<label class="col-lg-3 control-label">
								<strong>Вознаграждение клиенту от чека: </strong>
							</label>
							<div class="col-lg-5">
								<input id="slideC" name="percent_client" data-slider-id='ex1Slider' type="text" data-slider-min="1" data-slider-max="100" data-slider-step="1" data-slider-value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['payvalue'];?>
"/>
							</div>
							<label class="col-lg-3 control-label text-left">
								<span id="demo-range-def-val"><?php echo $_smarty_tpl->tpl_vars['current_shop']->value['payvalue'];?>
 %</span>
							</label>
						</div>

						<div class="form-group">
							<label class="col-lg-3 control-label">
								<strong>Всего процент от чека: </strong>
							</label>
							<div class="col-lg-5">
								<input id="slideAll" name="percent_all" data-slider-id='ex2Slider' type="text" data-slider-min="3" data-slider-max="133" data-slider-step="1" data-slider-value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['payvalue']*1.338;?>
"/>
							</div>
							<label class="col-lg-3 control-label text-left">
								<span id="demo-range-step-val"><?php echo $_smarty_tpl->tpl_vars['current_shop']->value['payvalue']*1.338;?>
 %</span>
							</label>
						</div>

						<div class="form-group">
								<h4 class="text-main mar-btm" style="margin-left:35px;">Бонусы</h4>
							<label class="col-lg-3 control-label">
								<strong>Процент от покупки, который можно оплатить бонусами: </strong>
							</label>
							<div class="col-lg-5">
								<input id="slidePerPay" name="bonusborderpercent" data-slider-id='ex2Slider' type="text" data-slider-min="50" data-slider-max="99" data-slider-step="1" data-slider-value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['bonusborderpercent'];?>
"/>
							</div>
							<label class="col-lg-3 control-label text-left">
								<span id="val-slidePerPay"><?php echo $_smarty_tpl->tpl_vars['current_shop']->value['bonusborderpercent'];?>
 %</span>
							</label>
						</div>
						<br>
						<div class="form-group">
							<label class="col-lg-3 control-label">Максимальное число бонусов, которые можно отдать за покупку</label>
							<div class="col-lg-7">
								<input id="realAddress"  oninput="var v = this.value;this.value=v.replace(/[.,]\d+/,'.5')" type="number" placeholder="в копейках" name="bonusbordervalue" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['bonusbordervalue'];?>
">
								<i class="ion-location"></i>
							</div>
						</div>
						<br>

						<div class="form-group">
								<h4 class="text-main mar-btm" style="margin-left:35px;">Купоны</h4>
							<label class="col-lg-3 control-label">
								<strong>Стоимость одного купона в бонусах: </strong>
							</label>
							<div class="col-lg-7">
								<input type="text" id="one" oninput="var v = this.value;this.value=v.replace(/[.,]\d+/,'.5')" class="form-control" name="cotel" placeholder="" Value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">
								<strong>Максимальная сумма продажи скидки: </strong>
							</label>
							<div class="col-lg-7">
								<input type="text" oninput="var v = this.value;this.value=v.replace(/[.,]\d+/,'.5')" class="form-control" name="cotel" placeholder="" Value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">Срок действия:</label>
							<div class="col-lg-7">
								<input type="text" oninput="var v = this.value;this.value=v.replace(/[.,]\d+/,'.5')" class="form-control" name="cotel" placeholder="" Value="">
							</div>
						</div>
					<br />

						<h4 class="text-main mar-btm">Вознаграждение сотруднику</h4>
						<div class="form-group">
							<label class="col-lg-3 control-label">За подключение клиента</label>
							<div class="col-lg-7">
								<input id="realAddress" type="number"  oninput="var v = this.value;this.value=v.replace(/[.,]\d+/,'.5')" placeholder="Размер вознаграждения фиксированны" name="paytomanbynewuser" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['paytomanbynewuser'];?>
">
								<i class="ion-location"></i>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">За выполнение операции (проводка чека)</label>
							<div class="col-lg-7">
								<input id="realAddress" type="number"  oninput="var v = this.value;this.value=v.replace(/[.,]\d+/,'.5')" placeholder="Размер вознаграждения фиксированны" name="paytomanbyoper" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['current_shop']->value['paytomanbyoper'];?>
">
								<i class="ion-location"></i>
							</div>
						</div>
									<?php echo '<script'; ?>
>
										var clientSlider = $('#slideC').slider({
											formatter: function(value) {
												return 'Процент от чека: ' + value + ' %';
											}
										});
										var perPaySlider = $('#slidePerPay').slider({
											formatter: function(value) {
												return 'Процент от покупки, который можно оплатить бонусами: ' + value + ' %';
											}
										});
										var allSlider = $('#slideAll').slider({
											formatter: function(value) {
												return 'Итого: ' + value + ' %';
											}
										});
										clientSlider.change(function(e) {
											allSlider.slider("setValue", Math.ceil(e.value.newValue * 1.338));
											$("#demo-range-def-val").text(e.value.newValue+" %");
											$("#demo-range-step-val").text(Math.ceil(e.value.newValue * 1.338) +" %");
										});
										perPaySlider.change(function(e) {
											$("#val-slidePerPay").text(e.value.newValue + " %");
										});
										allSlider.change(function(e) {
											clientSlider.slider("setValue", Math.ceil(e.value.newValue / 1.338));
											$("#demo-range-step-val").text(e.value.newValue+" %");
											$("#demo-range-def-val").text(Math.ceil(e.value.newValue / 1.338) +" %");
										});
									<?php echo '</script'; ?>
>

							<h4 class="text-main mar-btm">Статус компании</h4>
							<div class="form-horizontal">
								<div class="form-group">
                                    <?php if ($_smarty_tpl->tpl_vars['current_shop']->value['moderator_flag'] == 2) {?>
										<input type="checkbox" name="moderator_flag"> В архив
									<?php } else { ?>
										<input type="radio" name="moderator_flag" value="1" <?php if ($_smarty_tpl->tpl_vars['current_shop']->value['moderator_flag'] == 1) {?>checked<?php }?>> Активна<br>
										<input type="radio" name="moderator_flag" value="3" <?php if ($_smarty_tpl->tpl_vars['current_shop']->value['moderator_flag'] == 3) {?>checked<?php }?>> В архиве<br>
									<?php }?>
								</div>
							</div>





						<!--Footer button-->

						<div class="text-right">
							<div class="box-inline">
								<button type="button" id="set-shop" class="finish btn btn-warning">Применить</button>
							</div>
						</div>
						<!-- FINISH FOOTER -->
						<?php }?>
					</form>
					</div>

					<?php echo '<script'; ?>
>
						$( '#set-shop' ).click(function(event) {
							event.preventDefault();

							var formData = new FormData($('#shop_settings')[0]);
							$("#before").hide();

							$.ajax({
								url: 'backend/set_settings.php',
								data: formData,
								processData: false,
								contentType: false,
								type: 'POST',
								success: function (data) {
									$("#after").show();
								}
							});
						});
					<?php echo '</script'; ?>
>
					<!--===================================================-->
					<!-- END BASIC FORM ELEMENTS -->



				</div>

			</div>
			<!--===================================================-->
			<!--End page content-->









			<!--Default Tabs (Right Aligned)-->
			<!--===================================================-->


			<!--Nav tabs-->
			<div class="nav nav-tabs tabs-right">
				<!--Bordered Accordion-->
				<!--===================================================-->
				<div class="panel-group accordion" id="demo-acc-info-outline">
					<div class="panel panel-bordered panel-info">

						<!--Accordion title-->
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-parent="#demo-acc-info-outline" data-toggle="collapse" href="#demo-acd-info-outline-1">Дополнительная информация</a>
							</h4>
						</div>

						<!--Accordion content-->
						<div class="panel-collapse collapse in" id="demo-acd-info-outline-1">
							<div class="panel-body">
								В зависимости от выбранного вами процента вознаграждения пользователю добавляется реферальное вознаграждение.
								Тириф можно настроить исходя из  процента который вы хотите дарить вашему клиенту  или из общего процента затрат.
								Обратите внимание, весь остаток от партнёрской программы является платой  за привличенного к вам клиента
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--===================================================-->
			<!-- End Form wizard with Validation -->
		</div>



		<!--===================================================-->
		<!--End Default Tabs (Right Aligned)-->







	</div>
	<!--===================================================-->
	<!--END CONTENT CONTAINER-->

	<!-- FOOTER -->
		<!--===================================================-->
		<nav id="footer2-container">
			<div id="footer2">
				<?php echo $_smarty_tpl->getSubTemplate ('footer2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

			</div>
		</nav>
		<!--===================================================-->
	<!--END FOOTER -->



	<!--ASIDE-->
	<!--===================================================-->
	<aside id="aside-container">
		<div id="aside">
			<?php echo $_smarty_tpl->getSubTemplate ('aside.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

		</div>
	</aside>
	<!--===================================================-->
	<!--END ASIDE-->


	<!--MAIN NAVIGATION-->
	<!--===================================================-->
	<nav id="mainnav-container">
		<div id="mainnav">
			<?php echo $_smarty_tpl->getSubTemplate ('mainnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

		</div>
	</nav>
	<!--===================================================-->
	<!--END MAIN NAVIGATION-->

</div>




<!-- SCROLL PAGE BUTTON -->
<!--===================================================-->
<button class="scroll-top btn">
	<i class="pci-chevron chevron-up"></i>
</button>
<!--===================================================-->





</div>
<!--===================================================-->
<!-- END OF CONTAINER -->



<!-- SETTINGS - DEMO PURPOSE ONLY -->
<!--===================================================-->

<!--===================================================-->
<!-- END SETTINGS -->
<?php } else { ?>
<?php echo $_smarty_tpl->getSubTemplate ('login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }
}
?>