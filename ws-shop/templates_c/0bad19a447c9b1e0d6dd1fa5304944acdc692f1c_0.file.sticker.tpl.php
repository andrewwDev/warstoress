<?php /* Smarty version 3.1.27, created on 2017-10-23 15:10:55
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/sticker.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:113209097659eddc4f87fd87_41413810%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0bad19a447c9b1e0d6dd1fa5304944acdc692f1c' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/sticker.tpl',
      1 => 1508674455,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '113209097659eddc4f87fd87_41413810',
  'variables' => 
  array (
    'login' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59eddc4f898372_40004016',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59eddc4f898372_40004016')) {
function content_59eddc4f898372_40004016 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '113209097659eddc4f87fd87_41413810';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<?php if ($_smarty_tpl->tpl_vars['login']->value) {?>
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Наклейки</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->
				
				<!--Page content-->
                <!--===================================================-->
                <div id="page-content">
				
							<div class="panel panel-primary">
					
					            <!--Panel heading-->
					            <div class="panel-heading">
					                <div class="panel-control">
					
					                    <!--Nav tabs-->
					                    <ul class="nav nav-tabs">
					                        <li class="active"><a data-toggle="tab" href="#demo-tabs-box-1">Входные</a></li>
					                        <li><a data-toggle="tab" href="#demo-tabs-box-2">Внутренние</a></li>
					                    </ul>
					
					                </div>
					                <h3 class="panel-title">Скачать доступные</h3>
					            </div>
					
					            <!--Panel body-->
					            <div class="panel-body">
					
					                <!--Tabs content-->
					                <div class="tab-content">
					                    <div id="demo-tabs-box-1" class="tab-pane fade in active">
					                        <p class="text-main text-lg mar-no">Входные наклейки</p>
											Данные наклейки рекомендуем клеить на входную дверь или рядом со входом. Размер наклейки может быть выбран вами по желанию, однако стандартные размер наших наклее составляет 13х13см.
											<br />											
											Для скачивания наклейки нажмите правую кнопку мыши и выберите "Сохранить изображение".
											<br />
											<br />
											<br />
											<a href="img/addoc/playblue.png" target="_blank"><img src="img/addoc/playblue.png" width="250px"></a> 
											<a href="img/addoc/scwhite.png" target="_blank"><img src="img/addoc/scwhite.png" width="250px"></a> 
											<a href="img/addoc/takebonus.png" target="_blank"><img src="img/addoc/takebonus.png" width="250px"></a>
					                    </div>
					                    <div id="demo-tabs-box-2" class="tab-pane fade">
					                        <p class="text-main text-lg mar-no">Внутренние наклейки</p>
											Данные наклейки рекомендуем клеить возле кассовой зоны. Размер наклейки может быть выбран вами по желанию, однако стандартные размер наших наклее составляет 86х54 мм.
											<br />											
											Для скачивания наклейки нажмите правую кнопку мыши и выберите "Сохранить изображение".
											<br />
											<br />
											<br />
											<a href="img/addoc/logocategory.jpg" target="_blank"><img src="img/addoc/logocategory.jpg" width="250px"></a> 
					                    </div>
					                </div>
					            </div>
					        </div>
				
				
				</div>
                     <!--===================================================-->
                <!--End page content-->
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
<?php echo $_smarty_tpl->getSubTemplate ('aside.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>
 
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
<?php echo $_smarty_tpl->getSubTemplate ('mainnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



        <!-- FOOTER -->
        <!--===================================================-->
	<footer id="footer">

		<!-- Visible when footer positions are fixed -->
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<div class="show-fixed pull-right">
			You have <a href="#" class="text-bold text-main"><span class="label label-danger">3</span> pending action.</a>
		</div>



		<!-- Visible when footer positions are static -->
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<div class="hide-fixed pull-right pad-rgt">
			Плати только за <strong>настоящих</strong> клиентов.
		</div>



		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

		<p class="pad-lft">&#0169; 2016 AdPoints</p>



	</footer>
        <!--===================================================-->
        <!-- END FOOTER -->


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
<?php } else { ?>
<?php echo $_smarty_tpl->getSubTemplate ('login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }
}
?>