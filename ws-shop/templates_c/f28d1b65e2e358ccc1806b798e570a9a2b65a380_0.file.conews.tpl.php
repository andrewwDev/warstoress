<?php /* Smarty version 3.1.27, created on 2017-10-31 12:50:05
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/conews.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:100654854659f8474dde17d8_60768482%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f28d1b65e2e358ccc1806b798e570a9a2b65a380' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/conews.tpl',
      1 => 1509443402,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '100654854659f8474dde17d8_60768482',
  'variables' => 
  array (
    'login' => 0,
    'current_shop' => 0,
    'time_news' => 0,
    'shop_id' => 0,
    'news' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59f8474de11651_22353864',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59f8474de11651_22353864')) {
function content_59f8474de11651_22353864 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '100654854659f8474dde17d8_60768482';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<?php if ($_smarty_tpl->tpl_vars['login']->value) {?>
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed" style="
    position: absolute;
    display: block;
    height: 100%;
">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container" class="news">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Новости</h1>

                    <!--Searchbox
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div> -->
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                <!--Page content-->
                <!--===================================================-->
                <?php if (!empty($_smarty_tpl->tpl_vars['current_shop']->value)) {?>
                <div id="page-content">

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="add-news" style="width:25%; ">
                                <h4>Добавить новость</h4>
                                <hr>
                                <div id="after" style="display: none;">
                                    <p>Новость успешно добавлена</p>
                                </div>
                                <div id="before">
                                    <?php if ($_smarty_tpl->tpl_vars['time_news']->value >= 5) {?>
                                        <form id="form-news" action="backend/set_news.php" class="form-horizontal" enctype="multipart/form-data"><input type="hidden" name="sid" value="<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
" readonly>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Заголовок</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control"  maxlength="25" type="text" name="title" id="news-title">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Текст новости</label>
                                                <div class="col-lg-9">
                                                    <textarea class="form-control" maxlength="50" name="description" id="news-desc"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Картинка</label>
                                                <div class="col-lg-9">
                                                    <div class="fileform news">
                                                        <div id="fileformlabel-news"></div>
                                                        <div class="selectbutton btn btn-success fileinput-button dz-clickable">Добавить</div>
                                                        <input id="upload-news" accept="image/jpeg,image/png" type="file" name="img_news" onchange="getName(this.value,'upload-news');"/>
                                                        <!-- На размерность изображения -->
                                                        <?php echo '<script'; ?>
 type="text/javascript">
                                                        function validateSize(fileInput,size) {
                                                            var fileObj, oSize;
                                                            if ( typeof ActiveXObject == "function" ) { // IE
                                                                fileObj = (new ActiveXObject("Scripting.FileSystemObject")).getFile(fileInput.value);
                                                            }else {
                                                                fileObj = fileInput.files[0];
                                                            }
                                                        
                                                            oSize = fileObj.size; // Size returned in bytes.
                                                            if(oSize > size * 1024 * 1024){
                                                                return false
                                                            }
                                                            return true;
                                                        }
                                                        <?php echo '</script'; ?>
>
                                                        <!-- Конец размерность изображения -->
                                                    </div> 
                                                </div>
                                            </div>
                                            <button type="submit" id="news-finish" class="finish btn btn-success" style="display: inline-block;">Отправить</button>
                                        </form>
                                    <?php } else { ?>
                                        <p>Добавлять новости можно один раз в пять дней.</p>
                                    <?php }?>

                                </div>
                                <?php echo '<script'; ?>
>
                                    $( '#news-finish' ).click(function(event) {
                                        event.preventDefault();

                                        $("#before").hide();
                                        var formData  = new FormData($( '#form-news' )[0]);

                                        $.ajax({
                                            url: 'backend/set_news.php',
                                            data: formData,
                                            processData: false,
                                            contentType: false,
                                            type: 'POST',
                                            success: function (data) {
                                                console.log(data);
                                                $("#after").show();
                                            }
                                        });


//							$.post('backend/set_shop.php', $( '#shopRegister' ).serialize())
//								.done(function( data ) {
//									console.log(data);
//									$("#after-form").show();
//								});

                                    });
                                <?php echo '</script'; ?>
>
                            </div>
                        </div>
                    </div>

                <?php if ($_smarty_tpl->tpl_vars['news']->value) {?>
                    <div class="timeline two-column" style="width:100%; ">
                        <!-- Timeline header -->
                        <div class="timeline-header">
                            <div class="timeline-header-title bg-success"><?php echo date("d.m.y",time());?>
</div>
                        </div>
                        <?php
$_from = $_smarty_tpl->tpl_vars['news']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                            <div class="timeline-entry">
                                <div class="timeline-stat">
                                    <div class="timeline-icon"></div>
                                    <div class="timeline-time"><?php echo $_smarty_tpl->tpl_vars['item']->value["ndate"];?>
</div>
                                </div>
                                <div class="timeline-label">
                                    <h4><?php echo $_smarty_tpl->tpl_vars['item']->value["title"];?>
</h4>
                                    <?php if ($_smarty_tpl->tpl_vars['item']->value['hasimg'] == 1) {?>
                                        <img src="/ws_images/news/img<?php echo $_smarty_tpl->tpl_vars['item']->value['nid'];?>
_xh.jpg" alt="" class="news-img" style="width:50%;">
                                    <?php }?>
                                    <?php echo $_smarty_tpl->tpl_vars['item']->value["description"];?>

                                </div>
                            </div>
                        <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                        <div class="clearfix"></div>
                    </div>
                <?php } else { ?>
                    <div class="timeline two-column">
                        <div class="timeline-header">
                            <div class="timeline-header-title bg-warning">Новостей нет</div>
                        </div>
                    </div>
                <?php }?>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <?php }?>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
					<?php echo $_smarty_tpl->getSubTemplate ('aside.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
<?php echo $_smarty_tpl->getSubTemplate ('mainnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



      <!-- FOOTER -->
		<!--===================================================-->
		<nav id="footer2-container">
			<div id="footer2">
				<?php echo $_smarty_tpl->getSubTemplate ('footer2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

			</div>
		</nav>
		<!--===================================================-->
	<!--END FOOTER -->


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
<?php } else { ?>
<?php echo $_smarty_tpl->getSubTemplate ('login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }
}
?>