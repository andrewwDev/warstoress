<?php /* Smarty version 3.1.27, created on 2018-08-01 23:23:55
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/index.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:13213931395b6216db652964_89892116%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a37edd48184a0b21e68b701af4f33d25454ab971' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/index.tpl',
      1 => 1533155032,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13213931395b6216db652964_89892116',
  'variables' => 
  array (
    'login' => 0,
    'shops_list' => 0,
    'shops_data' => 0,
    'value' => 0,
    'count_sales' => 0,
    'count_bonus' => 0,
    'count_cashback' => 0,
    'movement_funds' => 0,
    'move_bonus_down' => 0,
    'move_bonus_up' => 0,
    'profit_from_team' => 0,
    'all_sales' => 0,
    'num_sales' => 0,
    'sales' => 0,
    'user_arr' => 0,
    'manager_arr' => 0,
    'num_pages' => 0,
    'page' => 0,
    'active_page' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5b6216db6ab0b9_27648087',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5b6216db6ab0b9_27648087')) {
function content_5b6216db6ab0b9_27648087 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '13213931395b6216db652964_89892116';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<?php if ($_smarty_tpl->tpl_vars['login']->value) {?>
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </div>
        </header>


        <!--===================================================-->
        <!--END NAVBAR-->
		<?php if (count($_smarty_tpl->tpl_vars['shops_list']->value) == 0) {?>
			<div class="boxed">
				<div id="content-container">

					<!--Page Title-->
					<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					<div id="page-title">
						<h1 class="page-header text-overflow">У Вас нет компаний</h1>
					</div>
					<div id="page-content">
						<a href="http://warstores.net/ws-shop/coreg.php">Регистрация новой компании</a>
					</div>
				</div>
				<!-- FOOTER -->
				<!--===================================================-->
	<footer id="footer">

		<!-- Visible when footer positions are fixed -->
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<div class="show-fixed pull-right">
			You have <a href="#" class="text-bold text-main"><span class="label label-danger">3</span> pending action.</a>
		</div>



		<!-- Visible when footer positions are static -->
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<div class="hide-fixed pull-right pad-rgt">
			Плати только за <strong>настоящих</strong> клиентов.
		</div>



		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

		<p class="pad-lft">&#0169; 2016 AdPoints</p>



	</footer>
				<!--===================================================-->
				<!-- END FOOTER -->



				<!--ASIDE-->
				<!--===================================================-->
				<aside id="aside-container">
					<div id="aside">
						<?php echo $_smarty_tpl->getSubTemplate ('aside.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

					</div>
				</aside>
				<!--===================================================-->
				<!--END ASIDE-->


				<!--MAIN NAVIGATION-->
				<!--===================================================-->

				<nav id="mainnav-container">
					<div id="mainnav">
						<?php echo $_smarty_tpl->getSubTemplate ('mainnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

					</div>
				</nav>
				<!--===================================================-->
				<!--END MAIN NAVIGATION-->
			</div>
		<?php } else { ?>
			<div class="boxed">

				<!--CONTENT CONTAINER-->
				<!--===================================================-->
				<div id="content-container">

					<!--Page Title-->
					<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					<div id="page-title">
						<h1 class="page-header text-overflow">Общая статистика компаний</h1>

						<!--Searchbox
						<div class="searchbox">
							<div class="input-group custom-search-form">
								<input type="text" class="form-control" placeholder="Search..">
								<span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
							</div>
						</div> -->
					</div>
					<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					<!--End page title-->

					<!--Page content-->
					<!--===================================================-->
					<div id="page-content">

						<div class="row">
							<div class="col-lg-7">

								<!--Network Line Chart-->
								<!--===================================================-->
								<div id="demo-panel-network" class="panel">
									<div class="panel-heading">
										
										
										
										
										
										
										
										
										
										
										
										
										
										<h3 class="panel-title">График привлечения клиентов и показатели</h3>
									</div>

									<!--Morris line chart placeholder-->
									<?php echo '<script'; ?>
 type="text/javascript">
										var shops_data = [];
										<?php
$_from = $_smarty_tpl->tpl_vars['shops_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
										shops_data.push({
											"id": "<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
",
											"day": "<?php echo $_smarty_tpl->tpl_vars['value']->value['day'];?>
",
											"clients": "<?php echo $_smarty_tpl->tpl_vars['value']->value['clients'];?>
",
											"newclients": "<?php echo $_smarty_tpl->tpl_vars['value']->value['newclients'];?>
",
											"spend_total": "<?php echo $_smarty_tpl->tpl_vars['value']->value['spend_total'];?>
",
											"spend_money": "<?php echo $_smarty_tpl->tpl_vars['value']->value['spend_money'];?>
",
											"received": "<?php echo $_smarty_tpl->tpl_vars['value']->value['received'];?>
",
											"responses": "<?php echo $_smarty_tpl->tpl_vars['value']->value['responses'];?>
"
										});
										<?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
									<?php echo '</script'; ?>
>
									<div id="morris-chart-network" class="morris-full-content"
										 style="height: 170px;">
										<div style="display: table; height: 100%; width: 100%;">
											<span style="display: table-cell; vertical-align: middle; text-align: center;">Слишком мало данных</span>
										</div>
									</div>

									<!--Chart information-->
									<div class="panel-body">
										<div class="row pad-top">
											<div class="col-lg-12">
												<div class="media">
													<small> <p class="text-semibold text-main">Показатели</p></small>
													<div class="media-body pad-lft">
														<div class="media">
															<div class="media-left pad-no">
																<small><span class="text-2x text-semibold text-nowrap text-main"><?php echo $_smarty_tpl->tpl_vars['count_sales']->value/10;?>
</span></small>
															</div>
															<div class="media-body">
																<small><p class="mar-no">Рублей от клиентов </p></small>
															</div>
															<div class="media-left pad-no">
																<small><span class="text-2x text-semibold text-nowrap text-main">
					                                             <?php echo $_smarty_tpl->tpl_vars['count_bonus']->value/100;?>

					                                        </span></small>
															</div>
															<div class="media-body">
																<small><p class="mar-no">Баллов от клиентов</p></small>
															</div>
															<div class="media-left pad-no">
																<small><span class="text-2x text-semibold text-nowrap text-main"><?php echo $_smarty_tpl->tpl_vars['count_cashback']->value/100;?>
</span></small>
															</div>
															<div class="media-body">
																<small><p class="mar-no">CashBack баллов</p></small>
															</div>
															
															
															
															
															
															
														</div>
													</div>
												</div>
											</div>

											
											
											
											
											
											
											
											
											
											
											
											
											
											
											
											
											
											
											
											
											
											
											
										</div>
									</div>


								</div>
								<!--===================================================-->
								<!--End network line chart-->

							</div>
							<div class="col-lg-5">
								<div class="row">
									<div class="col-sm-6 col-lg-6">
										<?php echo '<script'; ?>
 type="text/javascript">
											var movement_funds = [];
											<?php
$_from = $_smarty_tpl->tpl_vars['movement_funds']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
											movement_funds.push(<?php ob_start();
echo $_smarty_tpl->tpl_vars['value']->value['summ'];
$_tmp1=ob_get_clean();
echo $_tmp1;?>
);
											<?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
										<?php echo '</script'; ?>
>

										<!--Sparkline Area Chart-->
										<div class="panel panel-success panel-colorful">
											<div class="pad-all">
												<p class="text-lg text-semibold"><i class="demo-pli-data-storage icon-fw"></i>Движение баллов</p>
												<p class="mar-no">
													<span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['move_bonus_down']->value/100;?>
</span>
													Потраченные
												</p>
												<p class="mar-no">
													<span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['move_bonus_up']->value/100;?>
</span>
													Полученные
												</p>
											</div>
											<div class="pad-all text-center">
												<!--Placeholder-->
												<div id="demo-sparkline-area"></div>
											</div>
										</div>
									</div>
									<div class="col-sm-6 col-lg-6">

										<!--Sparkline Line Chart-->
										<div class="panel panel-info panel-colorful">
											<div class="pad-all">
												<p class="text-lg text-semibold"><i class="demo-pli-wallet-2 icon-fw"></i>Прибыль</p>
												<p class="mar-no">
													<span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['count_sales']->value/100;?>
</span>
													От компании
												</p>
												<p class="mar-no">
													<span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['profit_from_team']->value/100;?>
</span>
													От команды
												</p>
											</div>
											
											
											
											
											
											
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6 col-lg-6">
										<?php echo '<script'; ?>
 type="text/javascript">
											var sales = [];
											<?php
$_from = $_smarty_tpl->tpl_vars['all_sales']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
											sales.push(<?php ob_start();
echo $_smarty_tpl->tpl_vars['value']->value['summ'];
$_tmp2=ob_get_clean();
echo $_tmp2;?>
);
											<?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
										<?php echo '</script'; ?>
>

										<!--Sparkline bar chart -->
										<div class="panel panel-purple panel-colorful">
											<div class="pad-all">
												<p class="text-lg text-semibold"><i class="demo-pli-bag-coins icon-fw"></i>Продажи</p>
												<p class="mar-no">
													<span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['num_sales']->value;?>
</span>
													Кол-во продаж
												</p>
											</div>
											<div class="pad-all text-center">

												<!--Placeholder-->
												<div id="demo-sparkline-bar" class="box-inline"></div>

											</div>
										</div>
									</div>
									<div class="col-sm-6 col-lg-6">

										<!--Sparkline pie chart -->
										<div class="panel panel-warning panel-colorful">
											<div class="pad-all">
												<p class="text-lg text-semibold"><i class="demo-pli-check icon-fw"></i>Баланс</p>
												<p class="mar-no">
													<span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['move_bonus_down']->value/100;?>
</span>
													Всего потрачено
												</p>
												<p class="mar-no">
													<span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['move_bonus_up']->value/100;?>
</span>
													Всего получено
												</p>
												<p class="mar-no">
													<span class="pull-right text-bold"><?php echo ($_smarty_tpl->tpl_vars['move_bonus_up']->value-$_smarty_tpl->tpl_vars['move_bonus_down']->value)/100;?>
</span>
													Остаток
												</p>
											</div>
											<div class="pad-all">
												
												
												
												
												
												
												
												
												
												
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">


						</div>

						<div class="row">
							<div class="col-xs-12">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">Детализированный список</h3>
									</div>

									<!--Data Table-->
									<!--===================================================-->


									<div class="panel-body">
										<?php if ($_smarty_tpl->tpl_vars['sales']->value) {?>
											<div class="pad-btm form-inline">
												<div class="row">
													<div class="col-sm-6 table-toolbar-left">
														<!--<button class="btn btn-purple"><i class="demo-pli-add icon-fw"></i>Add</button>-->
														<button class="btn btn-default"><i class="demo-pli-printer"></i></button>
														<!--<div class="btn-group">
                                                            <button class="btn btn-default"><i class="demo-pli-information"></i></button>
                                                            <button class="btn btn-default"><i class="demo-pli-recycling"></i></button>
                                                        </div>-->
													</div>
												</div>
											</div>
											<div class="table-responsive">
												<table class="table table-striped">
													<thead>
													<tr>
														<th>Дата продажи</th>
														<th>Покупатель</th>
														<th>Сотрудник</th>
														<th>Cashback</th>
														<th class="text-center">Получено рублей</th>
														<th class="text-center">Получено баллов</th>
														<th class="text-center">Отзыв</th>
														<th class="text-center">Возврат</th>
													</tr>
													</thead>
													<tbody>
													<?php
$_from = $_smarty_tpl->tpl_vars['sales']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
														<tr>
															<td><span class="text-muted"><i class="fa fa-clock-o"></i> <?php echo $_smarty_tpl->tpl_vars['value']->value["date"];?>
</span></td>
															<td><?php echo $_smarty_tpl->tpl_vars['user_arr']->value[$_smarty_tpl->tpl_vars['value']->value["user_id"]];?>
</td>
															<td><?php echo $_smarty_tpl->tpl_vars['manager_arr']->value[$_smarty_tpl->tpl_vars['value']->value["manager_id"]];?>
</td>
															<td><?php echo $_smarty_tpl->tpl_vars['value']->value["cashback"];?>
</td>
															<td class="text-center">
																<?php echo $_smarty_tpl->tpl_vars['value']->value["cash"];?>

															</td>
															<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['value']->value["summ"]-$_smarty_tpl->tpl_vars['value']->value["cash"]*100;?>
</td>
															<td class="text-center">-</td>
															<td class="text-center">-</td>
														</tr>
													<?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
													</tbody>
												</table>
											</div>
											<hr>
											<?php if ($_smarty_tpl->tpl_vars['num_pages']->value > 0) {?>
												<div class="pull-right">
													<ul class="pagination text-nowrap mar-no">
														<?php $_smarty_tpl->tpl_vars['page'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['page']->step = 1;$_smarty_tpl->tpl_vars['page']->total = (int) ceil(($_smarty_tpl->tpl_vars['page']->step > 0 ? $_smarty_tpl->tpl_vars['num_pages']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['num_pages']->value)+1)/abs($_smarty_tpl->tpl_vars['page']->step));
if ($_smarty_tpl->tpl_vars['page']->total > 0) {
for ($_smarty_tpl->tpl_vars['page']->value = 1, $_smarty_tpl->tpl_vars['page']->iteration = 1;$_smarty_tpl->tpl_vars['page']->iteration <= $_smarty_tpl->tpl_vars['page']->total;$_smarty_tpl->tpl_vars['page']->value += $_smarty_tpl->tpl_vars['page']->step, $_smarty_tpl->tpl_vars['page']->iteration++) {
$_smarty_tpl->tpl_vars['page']->first = $_smarty_tpl->tpl_vars['page']->iteration == 1;$_smarty_tpl->tpl_vars['page']->last = $_smarty_tpl->tpl_vars['page']->iteration == $_smarty_tpl->tpl_vars['page']->total;?>
															<li class="page-number <?php if ($_smarty_tpl->tpl_vars['page']->value == $_smarty_tpl->tpl_vars['active_page']->value) {?>active<?php }?>">
																<a href="./overallstatistics.php?sid=2&page=<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['page']->value;?>
</a>
															</li>
														<?php }} ?>
													</ul>
												</div>
											<?php }?>
										<?php } else { ?>
											<div class="row">
												<div class="col-md-12">
													<p>Операций по данной компании нет</p>
												</div>
											</div>
										<?php }?>
									</div>
									<!--===================================================-->
									<!--End Data Table-->

								</div>
							</div>
						</div>

					</div>





				</div>
				<!--===================================================-->
				<!--End page content-->

				<!-- FOOTER -->
				<!--===================================================-->
					<nav id="footer2-container">
						<div id="footer2">
						<?php echo $_smarty_tpl->getSubTemplate ('footer2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

						</div>
					</nav>
				<!--===================================================-->
				<!--END FOOTER -->		



				<!--ASIDE-->
				<!--===================================================-->
				<aside id="aside-container">
					<div id="aside">
						<?php echo $_smarty_tpl->getSubTemplate ('aside.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

					</div>
				</aside>
				<!--===================================================-->
				<!--END ASIDE-->


				<!--MAIN NAVIGATION-->
				<!--===================================================-->

				<nav id="mainnav-container">
					<div id="mainnav">
						<?php echo $_smarty_tpl->getSubTemplate ('mainnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

					</div>
				</nav>
				<!--===================================================-->
				<!--END MAIN NAVIGATION-->
			</div>
		<?php }?>










        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
    
    <!--===================================================-->
    <!-- END SETTINGS -->
<?php } else { ?>
<?php echo $_smarty_tpl->getSubTemplate ('login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }
}
?>