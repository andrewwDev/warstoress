<?php /* Smarty version 3.1.27, created on 2017-07-26 07:18:29
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/aside.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1585366365597834350f1bc1_72146849%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fa1b61999a02ccb493339cd49d635986cadffae1' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/aside.tpl',
      1 => 1501049903,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1585366365597834350f1bc1_72146849',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59783435114366_36740600',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59783435114366_36740600')) {
function content_59783435114366_36740600 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1585366365597834350f1bc1_72146849';
?>
                    <div class="nano">
                        <div class="nano-content">

                            <!--Nav tabs-->
                            <!--================================-->
                            <ul class="nav nav-tabs nav-justified">

                                <li>
                                    <a href="#demo-asd-tab-2" data-toggle="tab">
                                        <i class="demo-pli-information icon-fw"></i> Техническая поддержка
                                    </a>
                                </li>

                            </ul>
                            <!--================================-->
                            <!--End nav tabs-->



                            <!-- Tabs Content -->
                            <!--================================-->
                            <div class="tab-content">



                                <!--Second tab (Custom layout)-->
                                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                                <div class="tab-pane fade in active" id="demo-asd-tab-2">

                                    <!--Monthly billing-->
                                    <div class="pad-all">
                                        <p class="text-semibold text-main">Телефон службы поддержки</p>
                                        <p class="text-muted text-sm"> +7 (473) 229-2-339</p>
                                    </div>
                                    <hr class="new-section-xs">
                                    <div class="pad-all">
                                        <span class="text-semibold text-main">По вопросам покупок</span>
                                        <p class="text-muted text-sm">orderq@warstores.net</p>
										<span class="text-semibold text-main">По вопросам сотрудничества</span>
                                        <p class="text-muted text-sm">pr@warstores.net</p>
                                    </div>


                                    <hr>

                                    <p class="pad-hor text-semibold text-main">Online помощь</p>

                                    <!--Simple Menu-->
                                    <div class="list-group bg-trans">
                                        <a href="http://warstores.net/wiki-faq" class="list-group-item"><i class="demo-pli-information icon-lg icon-fw"></i> Wiki страница</a>
                                        <a href="https://vk.com/warstores" class="list-group-item"><i class="demo-pli-mine icon-lg icon-fw"></i> Группа Вконтакте</a>
                                        <a href="https://vk.com/cowarstores" class="list-group-item"><span class="label label-info pull-right">Новое</span><i class="demo-pli-credit-card-2 icon-lg icon-fw"></i> Закрытая группа</a>
                                        <a href="https://vk.com/im?sel=-143957474" class="list-group-item"><i class="demo-pli-support icon-lg icon-fw"></i> Чат со специалистом</a>
                                    </div>


                                    <hr>

                                    <div class="text-center">
                                        <div><i class="demo-pli-old-telephone icon-3x"></i></div>
                                        Еще вопросы?
                                        <p class="text-lg text-semibold text-main"> +7 (473) 229-2-339 </p>
										<br>
										<a href="https://t.me/warstores">
										<i class="demo-pli-paper-plane icon-3x"></i>
										<p class="text-lg text-semibold text-main"> @warstores </p>
										</a>
                                        <small><em>С 8 до 22 по мск</em></small>
                                    </div>
                                </div>
                                <!--End second tab (Custom layout)-->
                                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

                            </div>
                        </div>
                    </div><?php }
}
?>