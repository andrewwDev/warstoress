<?php /* Smarty version 3.1.27, created on 2019-06-03 22:15:48
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/mainnav.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:13899019675cf571e4791d15_76148322%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9bb650eae5c9ba0e6413b198deefd9eccdc493e8' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/mainnav.tpl',
      1 => 1559589340,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13899019675cf571e4791d15_76148322',
  'variables' => 
  array (
    'user_status' => 0,
    'avatar' => 0,
    'owner_name' => 0,
    'shops_list' => 0,
    'shop' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5cf571e47b81a9_26558436',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5cf571e47b81a9_26558436')) {
function content_5cf571e47b81a9_26558436 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '13899019675cf571e4791d15_76148322';
?>
            <!--MAIN NAVIGATION-->
            <!--===================================================-->
                    <!--Menu-->
                    <!--================================-->
                    <div id="mainnav-menu-wrap">
                        <div class="nano">
                            <div class="nano-content">

                                <!--Profile Widget-->
                                <!--================================-->
                                <div id="mainnav-profile" class="mainnav-profile">
                                    <div class="profile-wrap">
                                        <div class="pad-btm">
                                            <?php if ($_smarty_tpl->tpl_vars['user_status']->value == 1) {?>
                                                <span class="label label-success pull-right">Обычный пользователь</span>
                                            <?php } elseif ($_smarty_tpl->tpl_vars['user_status']->value == 2) {?>
                                                <span class="label label-success pull-right">Магазин</span>
                                            <?php } elseif ($_smarty_tpl->tpl_vars['user_status']->value == 3) {?>
                                                <span class="label label-success pull-right">Администратор</span>
                                            <?php } elseif ($_smarty_tpl->tpl_vars['user_status']->value == 4) {?>
                                                <span class="label label-success pull-right">Модератор</span>
                                            <?php }?>
                                            <?php if ($_smarty_tpl->tpl_vars['avatar']->value) {?>
                                                <img class="img-circle img-sm img-border" src="<?php echo $_smarty_tpl->tpl_vars['avatar']->value;?>
" alt="Profile Picture">
                                            <?php } else { ?>
                                                <img class="img-circle img-sm img-border" src="img/profile-photos/1.png" alt="Profile Picture">
                                            <?php }?>

                                        </div>
                                        <a href="/ws-shop/profile.php" class="box-block" /*data-toggle="collapse" aria-expanded="false"*/>

                                            <p class="mnp-name"><?php echo $_smarty_tpl->tpl_vars['owner_name']->value;?>
</p>
                                            <span class="mnp-desc"></span>
                                        </a>
                                    </div>

                                </div>


                                <!--Shortcut buttons-->
                                <!--================================-->
                                <div id="mainnav-shortcut">
                                    <ul class="list-unstyled">
                                        
                                            
                                                
                                            
                                        
                                        
                                            
                                                
                                            
                                        
                                        
                                            
                                                
                                            
                                         
                                        <li class="col-xs-3 logout" data-content="Выйти">
                                            <a class="shortcut-grid" href="#">
                                                <i class="demo-psi-lock-2"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!--================================-->
                                <!--End shortcut buttons-->


                                <ul id="mainnav-menu" class="list-group">

						            <!--Category name-->
						            <li class="list-header">Мои компании</li>

                                    <li>
                                        <a href="index.php">
                                            <i class="demo-psi-bar-chart"></i>
                                            <span class="menu-title">
												<strong>Общая статистика</strong>
											</span>

                                        </a>


                                    </li>


                                    <?php
$_from = $_smarty_tpl->tpl_vars['shops_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['shop'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['shop']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['shop']->value) {
$_smarty_tpl->tpl_vars['shop']->_loop = true;
$foreach_shop_Sav = $_smarty_tpl->tpl_vars['shop'];
?>
                                    <!--Menu list item-->
                                        <?php if ($_smarty_tpl->tpl_vars['shop']->value['moderator_flag'] == 1 || $_smarty_tpl->tpl_vars['shop']->value['moderator_flag'] == 3) {?>
                                            <li class="active-link">
                                                <!-- <a href="?sid=<?php echo $_smarty_tpl->tpl_vars['shop']->value['id'];?>
"> -->
                                                <a href="#">
                                                    <i class="demo-psi-home"></i>
                                                    <span class="menu-title">
												<strong><?php echo $_smarty_tpl->tpl_vars['shop']->value['title'];?>
</strong>
											</span>
                                                    <i class="arrow"></i>
                                                </a>

                                                <!--Submenu-->
                                                <ul class="collapse">
                                                    <li><a href="overallstatistics.php?sid=<?php echo $_smarty_tpl->tpl_vars['shop']->value['id'];?>
"">Статистика</a></li>
                                                    <li><a href="conews.php?sid=<?php echo $_smarty_tpl->tpl_vars['shop']->value['id'];?>
">Новости</a></li>
                                                    <li><a href="buyers.php?sid=<?php echo $_smarty_tpl->tpl_vars['shop']->value['id'];?>
">Клиенты</a></li>
                                                    <li><a href="staff.php?sid=<?php echo $_smarty_tpl->tpl_vars['shop']->value['id'];?>
">Сотрудники</a></li>
                                                    <!-- <li class="list-divider"></li> -->
                                                    <!-- <li><a href="reviews.php?sid=<?php echo $_smarty_tpl->tpl_vars['shop']->value['id'];?>
">Отзывы</a></li> -->
                                                    <li><a href="cobalance.php?sid=<?php echo $_smarty_tpl->tpl_vars['shop']->value['id'];?>
">Баланс</a></li>
                                                    
                                                    <li><a href="cosettings.php?sid=<?php echo $_smarty_tpl->tpl_vars['shop']->value['id'];?>
">Настройки</a></li>
                                                </ul>
                                            </li>
                                        <?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['shop']->value['moderator_flag'] == 2) {?>
                                            <li class="active-link">
                                                <!-- <a href="?sid=<?php echo $_smarty_tpl->tpl_vars['shop']->value['id'];?>
"> -->
                                                <a href="#" style="background: darkred;">
                                                    <i class="demo-psi-home"></i>
                                                    <span class="menu-title">
												<strong><?php echo $_smarty_tpl->tpl_vars['shop']->value['title'];?>
</strong>
											</span>
                                                    <i class="arrow"></i>
                                                </a>

                                                <!--Submenu-->
                                                <ul class="collapse">
                                                    <li><a href="cosettings.php?sid=<?php echo $_smarty_tpl->tpl_vars['shop']->value['id'];?>
">Настройки</a></li>
                                                </ul>
                                            </li>
                                        <?php }?>
                                    <?php
$_smarty_tpl->tpl_vars['shop'] = $foreach_shop_Sav;
}
?>

						            <!--Menu list item-->
						            <li>
						                <a href="coreg.php">
						                    <i class="demo-pli-plus"></i>
						                    <span class="menu-title">
												<strong>Добавить Компанию</strong>
											</span>

						                </a>


						            </li>


						            <li class="list-divider"></li>


						            <!--Menu list item-->
						            <li>
						                <a href="#">
						                    <i class="demo-psi-receipt-4"></i>
						                    <span class="menu-title">Документы</span>
											<i class="arrow"></i>
						                </a>

						                <!--Submenu-->
						                <ul class="collapse">
						                    <li><a href="http://warstores.net/doc/oferta.doc">Договор-оферты</a></li>
											
                                            <li><a href="http://warstores.net/doc/privacy.pdf">Политика конфедициальности</a></li>
											<li><a href="https://152фз.рф/get_terms/9ab0ea60f8985bc6cdd95706a8e7905f">Общее пользовательское соглашение</a></li>

										</ul>
						            </li>

						            <!--Menu list item-->
						            <li>
						                <a href="#">
						                    <i class="demo-psi-inbox-full"></i>
						                    <span class="menu-title">Материалы</span>
											<i class="arrow"></i>
						                </a>

						                <!--Submenu-->
						                <ul class="collapse">
						                    <li><a href="reklama.php">Рекламные буклеты</a></li>
											<li><a href="commercial.php">Коммерческое предложение</a></li>
											<li><a href="sticker.php">Наклейки</a></li>
										
						                </ul>
						            </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <!--================================-->
                    <!--End menu-->


            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

<?php }
}
?>