<?php /* Smarty version 3.1.27, created on 2017-10-31 13:04:51
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/reviews.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:8117131159f84ac3035ab9_56338619%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f7bfae8cef6b121cb671b102aa7a815788a6e07c' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/reviews.tpl',
      1 => 1509444286,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8117131159f84ac3035ab9_56338619',
  'variables' => 
  array (
    'login' => 0,
    'reviews' => 0,
    'value' => 0,
    'current_shop' => 0,
    'overall_rating' => 0,
    'item' => 0,
    'users' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59f84ac30916a9_31345168',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59f84ac30916a9_31345168')) {
function content_59f84ac30916a9_31345168 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '8117131159f84ac3035ab9_56338619';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<?php if ($_smarty_tpl->tpl_vars['login']->value) {?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.1/css/star-rating.min.css" />
    <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.1/js/star-rating.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.1/js/star-rating_locale_ru.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript">
        var reviews = [];
        <?php
$_from = $_smarty_tpl->tpl_vars['reviews']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
        reviews.push(<?php ob_start();
echo $_smarty_tpl->tpl_vars['value']->value['rating'];
$_tmp1=ob_get_clean();
echo $_tmp1;?>
);
        <?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
        console.log(reviews);
    <?php echo '</script'; ?>
>

    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Отзывы</h1>

                    <!--Searchbox-->
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <?php if (!empty($_smarty_tpl->tpl_vars['current_shop']->value)) {?>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->
                  <div class="col-lg-6">
                       
                                                                 <!--Tile-->
                                            <!--===================================================-->
                                            <div class="panel panel-warning panel-colorful" >
                                                <div class="pad-all text-center">
                                                    <span class="text-3x text-thin">
                                                        <?php if ($_smarty_tpl->tpl_vars['overall_rating']->value) {?>
                                                            <?php echo $_smarty_tpl->tpl_vars['overall_rating']->value;?>

                                                        <?php } else { ?>
                                                            Нет
                                                        <?php }?>
                                                    </span>
                                                    <p>Средняя оценка</p>
                                                    <i class="demo-psi-mail icon-lg"></i>
                                                </div>
                                            </div>
                                            <!--===================================================-->

                                              <!--===================================================-->
                                               
                                            
                                                    
                                                    
                                                    
                                                        
                                                            
                                                        
                                                        
                                                            
                                                            
                                                        
                                                    
                                                    
                                                    
                                            
                                                

                                             <!--===================================================-->


                                            <div class="col-lg-5 col-lg-offset-2">
                                                    <!-- Timeline -->
                                                    <!--===================================================-->
                                                    <div class="timeline" style="margin-left:-25%;">
                                            
                                                        <!-- Timeline header -->
                                                        <?php
$_from = $_smarty_tpl->tpl_vars['reviews']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                                            <div class="timeline-entry">
                                                                <div class="timeline-stat">
                                                                    <div class="timeline-icon">
                                                                        <?php if ($_smarty_tpl->tpl_vars['item']->value["rating"] < 3) {?>
                                                                            <i class="demo-pli-unlike icon-2x"></i>
                                                                        <?php } else { ?>
                                                                            <i class="demo-pli-like icon-2x"></i>
                                                                        <?php }?>
                                                                    </div>
                                                                    <div class="timeline-time"><?php echo $_smarty_tpl->tpl_vars['item']->value["date"];?>
</div>
                                                                </div>
                                                                <div class="timeline-label" style="width:150%">
                                                                    <label for="input-<?php echo $_smarty_tpl->tpl_vars['item']->value["id"];?>
" class="control-label">Оценка:</label>
                                                                    <input id="input-<?php echo $_smarty_tpl->tpl_vars['item']->value["id"];?>
" name="input-<?php echo $_smarty_tpl->tpl_vars['item']->value["id"];?>
" value="<?php echo $_smarty_tpl->tpl_vars['item']->value["rating"];?>
" class="rating rating-loading" data-show-clear="false" data-show-caption="true">
                                                                    <?php echo '<script'; ?>
>
                                                                        $("#input-<?php echo $_smarty_tpl->tpl_vars['item']->value["id"];?>
").rating({displayOnly: true, step: 1});
                                                                    <?php echo '</script'; ?>
>
                                                                    <p class="mar-no pad-btm"> От <a href="#" class="text-semibold"><i><?php echo $_smarty_tpl->tpl_vars['users']->value[$_smarty_tpl->tpl_vars['item']->value["uid"]][0];?>
 <?php echo $_smarty_tpl->tpl_vars['users']->value[$_smarty_tpl->tpl_vars['item']->value["uid"]][1];?>
</i></a></p>
                                                                    <blockquote class="bq-sm bq-open mar-no"><?php echo $_smarty_tpl->tpl_vars['item']->value["text"];?>
</blockquote>
                                                                    
                                                                        
                                                                            
                                                                        
                                                                        
                                                                    
                                                                </div>
                                                            </div>
                                                        <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                                                    
                                                    <!--===================================================-->
                                                    <!-- End Timeline -->
                            
                                                    </div>
                                        </div>

                                
                  </div>


                               <!--Default Tabs (Right Aligned)-->
					        <!--===================================================-->
					        
					
					            <!--Nav tabs-->
					            <div class="nav nav-tabs tabs-right" style="border:0ch;" >
					                <!--Bordered Accordion-->
					        <!--===================================================-->
					        <div class="panel-group accordion" id="demo-acc-info-outline">


                                <div class="col-sm-6 col-lg-3">




                                    <!--Morris Charts -->
					<!--===================================================-->
					<div class="row">
					    <div class="col-lg-6" style="width:120%;">
					        <div class="panel" style="margin-right:-65%;">
					            <div class="panel-heading" >
                                    
					                    
					                    
					                        
					                        
					                            
					                            
					                            
					                            
					                            
					                        
					                    
					                
					                <h3 class="panel-title">&nbsp;&nbsp;Статистика отзывов.</h3>
					            </div>
					            <div class="panel-body">
					
					                <!--Morris Area Chart placeholder-->
					                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
					                <div id="demo-morris-area" style="height:212px"></div>
					                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
					
					            </div>
					        </div>
					    </div>
					</div>

					            <!--Panel with Footer-->
					            <!--===================================================-->
					            
					                
					                    
					                        
					                    
					                
					                
					                    
                                        
					                
					                
                                        
                                       
                                       

                                    
					            
					            <!--===================================================-->
					            <!--End Panel with Footer-->
					
					      

                            
					            
					        </div>
					            </div>
					            <!--===================================================-->
					            <!-- End Form wizard with Validation -->
					            
					
					            
					        
					        <!--===================================================-->
					        <!--End Default Tabs (Right Aligned)-->

                        </div>
                  <?php }?>
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
            <!-- FOOTER -->
            <!--===================================================-->
            <footer id="footer">

                <!-- Visible when footer positions are fixed -->
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <div class="show-fixed pull-right">
                    You have <a href="#" class="text-bold text-main"><span class="label label-danger">3</span> pending action.</a>
                </div>



                <!-- Visible when footer positions are static -->
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <div class="hide-fixed pull-right pad-rgt">
                    14GB of <strong>512GB</strong> Free.
                </div>



                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

                <p class="pad-lft">&#0169; 2016 Your Company</p>



            </footer>
            <!--===================================================-->
            <!-- END FOOTER -->
        </div>


            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
<?php echo $_smarty_tpl->getSubTemplate ('aside.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>
 
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
<?php echo $_smarty_tpl->getSubTemplate ('mainnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>






        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
    
    <!--===================================================-->
    <!-- END SETTINGS -->
<?php } else { ?>
<?php echo $_smarty_tpl->getSubTemplate ('login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }
}
?>