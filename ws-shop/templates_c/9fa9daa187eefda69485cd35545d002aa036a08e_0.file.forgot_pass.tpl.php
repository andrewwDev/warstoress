<?php /* Smarty version 3.1.27, created on 2020-05-19 10:55:19
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/forgot_pass.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:3301511595ec390e7ef38f0_72626523%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9fa9daa187eefda69485cd35545d002aa036a08e' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/forgot_pass.tpl',
      1 => 1560019642,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3301511595ec390e7ef38f0_72626523',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5ec390e7f1cc98_77818244',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5ec390e7f1cc98_77818244')) {
function content_5ec390e7f1cc98_77818244 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '3301511595ec390e7ef38f0_72626523';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<div id="container" class="cls-container">


    <!-- BACKGROUND IMAGE -->
    <!--===================================================-->
    <div id="bg-overlay"></div>


    <!-- REGISTRATION FORM -->
    <!--===================================================-->
    <div class="cls-content">
        <div class="cls-content-lg panel">
            <div class="panel-body">
                <div id="before" class="mar-ver pad-btm">
                    <h3 class="h4 mar-no">Восстановление пароля</h3>
                    <p class="text-muted">На email адрес связанный с аккаунтом будет отправлена ссылка для восстановления пароля.</p>
                </div>
                <div id="after" style="display: none;">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4></h4>
                        </div>
                    </div>
                </div>
                <form id="reg_form" action="">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Uid или Логин или Email" name="email" id="email" required>
                            </div>
                        </div>
                    </div>
                    <button id="reg_form_finish" class="btn btn-primary btn-block" type="submit">Восстановить</button>
                </form>
            </div>
            <div class="pad-all">
                <a href="/ws-shop/" class="btn-link mar-rgt">Войти</a>
            </div>
        </div>
    </div>
    <!--===================================================-->


</div>
<!--===================================================-->
<!-- END OF CONTAINER -->
<?php echo '<script'; ?>
>
    $( '#reg_form_finish' ).click(function(event) {
        event.preventDefault();
        var before = $("#before");
        var after = $("#after");
        var email = $("#email");
        if (email.val() == "") {
            after.find("h4").text("Поле не может быть пустым.");
            after.show();
            return false;
        }

        $("#reg_form").hide();

        var formData  = new FormData($( '#reg_form' )[0]);

        $.ajax({
            url: 'backend/forgot_pass.php',
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                after.find("h4").text(data["status"]);
                after.show();
                before.hide();
            }
        });
    });
<?php echo '</script'; ?>
>


</body>
</html>
<?php }
}
?>