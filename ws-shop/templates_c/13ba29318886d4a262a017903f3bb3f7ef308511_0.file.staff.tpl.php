<?php /* Smarty version 3.1.27, created on 2017-10-31 12:58:29
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/staff.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:5222037659f84945e606b7_09754485%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '13ba29318886d4a262a017903f3bb3f7ef308511' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/staff.tpl',
      1 => 1509443905,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5222037659f84945e606b7_09754485',
  'variables' => 
  array (
    'login' => 0,
    'current_shop' => 0,
    'shop_id' => 0,
    'managers' => 0,
    'value' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59f84945e84a98_91390103',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59f84945e84a98_91390103')) {
function content_59f84945e84a98_91390103 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '5222037659f84945e606b7_09754485';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<?php if ($_smarty_tpl->tpl_vars['login']->value) {?>
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Сотрудники</h1>

                    <!--Searchbox
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div> -->
                </div>
                <?php if (!empty($_smarty_tpl->tpl_vars['current_shop']->value)) {?>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                    <div class="input-group pad-all"  style="width:50%; margin-left:25%"  >
                        <form id="form_add_staff" action="">
                            <input id="staff_login" type="text" class="form-control" placeholder="Введите логин пльзователя чтобы добавить сотрудника"  autocomplete="off">
                            <span class="input-group-btn" style="display: inherit;">
			                    <button id="add_staff" type="button" class="btn btn-success"><i class="demo-pli-add"></i></button>
			                </span>
                        </form>
		            </div>

                <div id="after_none" class="input-group pad-all"  style="width:50%; margin-left:25%; display: none;"  >
                    <p style='text-align:center;'>Пользователь с таким логином не найден.</p>
                </div>

                <div id="after_find" class="input-group pad-all"  style="width:50%; margin-left:25%; display: none;"  >
                    <p style='text-align:center;'>Пользователь уже добавлен.</p>
                </div>

                <div id="after_yes" class="input-group pad-all"  style="width:50%; margin-left:25%; display: none;"  >
                    <p style='text-align:center;' '>Добавить пользователя <b id="user_login"></b> в список сотрудников ?</p><p style='text-align: center;'><button type='button' id='set_staff' class='finish btn btn-success'>Добавить</button></p>
                </div>

                <div id="after_add" class="input-group pad-all"  style="width:50%; margin-left:25%; display: none;"  >
                    <p style='text-align:center;'>Сотрудник добавлен</p>
                </div>


                
                <?php echo '<script'; ?>
>
                    $('#staff_login').keypress(function(event){

                        if (event.keyCode == 10 || event.keyCode == 13)
                            event.preventDefault();

                    });
                    var user_id = 0;
                    $( '#add_staff' ).click(function(event) {
                        event.preventDefault();

                        var user_login = {user_login: $('#staff_login').val()};

                        $.ajax({
                            url: 'backend/fn_staff.php',
                            data: user_login,
                            type: 'POST',
                            success: function (data) {
                                if (data == 0) {
                                    $("#after_none").show();
                                    $("#after_yes").hide();
                                    $("#after_find").hide();
                                } else if (data == -1) {
                                    $("#after_find").show();
                                    $("#after_none").hide();
                                    $("#after_yes").hide();
                                } else {
                                    $("#user_login").text($('#staff_login').val());
                                    user_id = data;
                                    $("#after_none").hide();
                                    $("#after_yes").show();
                                    $("#after_find").hide();
                                }
                            }
                        });
                    });

                    $( '#set_staff' ).click(function(event) {
                        event.preventDefault();
                        user_id = {user_id: user_id, shopid: <?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
};
                        $.ajax({
                            url: 'backend/fn_staff.php',
                            data: user_id,
                            type: 'POST',
                            success: function (data) {
                                console.log(data);
                                $("#after_yes").hide();
                                $("#after_add").show();
                                location.reload()
                            }
                        });
                    });


                <?php echo '</script'; ?>
>
                

                                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    
					
					<div class="row">
					    <div class="col-sm-12">
					        <div class="panel">
					            <div class="panel-heading">
					                <h3 class="panel-title">Сотрудники</h3>
					            </div>
					
					            <!-- Foo Table - Row Toggler -->
					            <!--===================================================-->
					            <div class="panel-body">
									<?php if ($_smarty_tpl->tpl_vars['managers']->value) {?>
										<div class="table-responsive">
											<table class="table table-striped">
												<thead>
												<tr>
													<th>Имя</th>
													<th>Фамилия</th>
													<th>Статус</th>
												</tr>
												</thead>
												<tbody>
												<?php
$_from = $_smarty_tpl->tpl_vars['managers']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
													<tr>
														<td><span class="text-muted"><i class="fa fa-clock-o"></i> <?php echo $_smarty_tpl->tpl_vars['value']->value["name"];?>
</span></td>
														<td><?php echo $_smarty_tpl->tpl_vars['value']->value["surname"];?>
</td>
														<td>
															<?php if ($_smarty_tpl->tpl_vars['value']->value["active"] > 0) {?>
																<span class="label label-table label-success">Работает</span>
															<?php } else { ?>
																<span class="label label-table label-danger">Уволен</span>
															<?php }?>
														</td>
													</tr>
												<?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
												</tbody>
											</table>
										</div>
									<?php } else { ?>
										<p>В данной компании сотрудников нет</p>
									<?php }?>
					            </div>
					            </div>
					            <!--===================================================-->
					            <!-- End Foo Table - Row Toggler -->
					    </div>
					</div>
					</div>
                    <?php }?>






     
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
<?php echo $_smarty_tpl->getSubTemplate ('aside.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
<?php echo $_smarty_tpl->getSubTemplate ('mainnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



       <!-- FOOTER -->
				<!--===================================================-->
					<nav id="footer2-container">
						<div id="footer2">
						<?php echo $_smarty_tpl->getSubTemplate ('footer2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

						</div>
					</nav>
				<!--===================================================-->
				<!--END FOOTER -->	


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
<?php } else { ?>
<?php echo $_smarty_tpl->getSubTemplate ('login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }
}
?>