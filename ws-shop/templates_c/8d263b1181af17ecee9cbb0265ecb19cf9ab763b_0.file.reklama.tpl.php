<?php /* Smarty version 3.1.27, created on 2017-10-23 15:10:38
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/reklama.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:166808262159eddc3e4d4a20_38466667%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8d263b1181af17ecee9cbb0265ecb19cf9ab763b' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/reklama.tpl',
      1 => 1508674455,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '166808262159eddc3e4d4a20_38466667',
  'variables' => 
  array (
    'login' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59eddc3e4ebb94_30167245',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59eddc3e4ebb94_30167245')) {
function content_59eddc3e4ebb94_30167245 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '166808262159eddc3e4d4a20_38466667';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<?php if ($_smarty_tpl->tpl_vars['login']->value) {?>
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed" style="
    position: absolute;
    display: block;
    height: 100%;
">


            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Рекламные буклеты</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
				
							<div class="panel panel-primary">
					
					            <!--Panel heading-->
					            <div class="panel-heading">
					                <div class="panel-control">
					
					                    <!--Nav tabs-->
					                    <ul class="nav nav-tabs">
					                        <li class="active"><a data-toggle="tab" href="#demo-tabs-box-1">Брошюра</a></li>
					                        <li><a data-toggle="tab" href="#demo-tabs-box-2">Тейблтент</a></li>
					                    </ul>
					
					                </div>
					                <h3 class="panel-title">Скачивание брошюр</h3>
					            </div>
					
					            <!--Panel body-->
					            <div class="panel-body">
					
					                <!--Tabs content-->
					                <div class="tab-content">
					                    <div id="demo-tabs-box-1" class="tab-pane fade in active">
					                        <p class="text-main text-lg mar-no">Брошюра</p>
											Брошюру рекомендуем положить рядом с кассовой зоной или распечатав с двух сторон использовать в тейблтент. Размер может быть выбран вами по желанию, однако стандартный размер А5.
											<br />											
											Для скачивания нажмите правую кнопку мыши и выберите "Сохранить изображение".
											<br />
											<br />
											<br />
											<a href="img/addoc/A5-1.jpg" target="_blank"><img src="img/addoc/A5-1.jpg" width="350px"></a> 
											<a href="img/addoc/A5-2.jpg" target="_blank"><img src="img/addoc/A5-2.jpg" width="350px"></a> 
					                    </div>
					                    <div id="demo-tabs-box-2" class="tab-pane fade">
					                        <p class="text-main text-lg mar-no">Тейблтент</p>
											Брошюру рекомендуем положить рядом с кассовой зоной или распечатав с двух сторон использовать в тейблтент. Размер может быть выбран вами по желанию, однако стандартный размер А5.
											<br />											
											Для скачивания нажмите правую кнопку мыши и выберите "Сохранить изображение".
											<br />
											<br />
											<br />
											<a href="img/addoc/tablet.jpg" target="_blank"><img src="img/addoc/tablet.jpg" width="350px"></a> 
					                    </div>
					                </div>
					            </div>
					        </div>
				
				
				</div>
                     <!--===================================================-->
                <!--End page content-->
            

     
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
<?php echo $_smarty_tpl->getSubTemplate ('aside.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
<?php echo $_smarty_tpl->getSubTemplate ('mainnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



      <!-- FOOTER -->
		<!--===================================================-->
		<nav id="footer2-container">
			<div id="footer2">
				<?php echo $_smarty_tpl->getSubTemplate ('footer2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

			</div>
		</nav>
		<!--===================================================-->
	<!--END FOOTER -->


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
<?php } else { ?>
<?php echo $_smarty_tpl->getSubTemplate ('login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>