<?php /* Smarty version 3.1.27, created on 2017-06-20 19:31:54
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/help.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:207631579759496a1a6c1d46_38408675%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1102be548d45825eae7cf57b8be9a31f78ebfc80' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/help.tpl',
      1 => 1497983511,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '207631579759496a1a6c1d46_38408675',
  'variables' => 
  array (
    'login' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59496a1a754ea0_26735592',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59496a1a754ea0_26735592')) {
function content_59496a1a754ea0_26735592 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '207631579759496a1a6c1d46_38408675';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<?php if ($_smarty_tpl->tpl_vars['login']->value) {?>
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Помощь</h1>

                    <!--Searchbox-->
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    <div class="fixed-fluid">
                        <p>Найти ответы на все интересующие Вас вопросы можно в нашей группе: <a href="https://vk.com/warstores" class="pad-rgt" target="_blank" style="text-decoration: underline;">WAR STORES</a></p>
					    
					        
					            
					                
					            
					
					            
					            
					                
					                    
					                    
					                
					                
					                    
					                    
					                
					                
					                    
					                    
					                
					                
					                    
					                    
					                
					                
					                    
					                    
					                
					            
					
					            
					                
					                
					            
					
					            
					            
					                
					
					                
					                
					                    
					                    
					                
					                
					                    
					                    
					                
					                
					                    
					                    
					                
					                
					                    
					                    
					                
					                
					                    
					                    
					                
					            
					
					            
					            
					                
					                    
					                
					                
					                    
					                
					                
					                    
					                
					                
					                    
					                
					                
					                    
					                
					                
					                    
					                
					            
					        
					    
					    
					        
					        
					        
					            
					                
					                    
					                    
					                        
					                            
					                            
					                        
					                    
					
					                    
					                        
					                    
					                
					
					
					
					                
					                
					                    
					                        
					                        
					                            
					                        
					                    
					                    
					                        
					                        
					                            
					                        
					                    
					                    
					                        
					                        
					                            
					                        
					                    
					                    
					                        
					                        
					                            
					                        
					                    
					                
					
					
					                
					                
					                    
					                        
					                            
					                        
					                    
					                    
					                
					
					
					                
					                
					
					                
					
					                    
					                    
					                        
					                    
					
					                    
					                    
					                        
					                    
					
					                    
					                    
					                        
					                    
					                
					            
					        
					        
					        
					    
					</div>

				
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
<?php echo $_smarty_tpl->getSubTemplate ('aside.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->

			<nav id="mainnav-container">
                <div id="mainnav">
<?php echo $_smarty_tpl->getSubTemplate ('mainnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->
        </div>



        <!-- FOOTER -->
        <!--===================================================-->
        <footer id="footer">

            <!-- Visible when footer positions are fixed -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="show-fixed pull-right">
                You have <a href="#" class="text-bold text-main"><span class="label label-danger">3</span> pending action.</a>
            </div>



            <!-- Visible when footer positions are static -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="hide-fixed pull-right pad-rgt">
                14GB of <strong>512GB</strong> Free.
            </div>



            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

            <p class="pad-lft">&#0169; 2016 Your Company</p>



        </footer>
        <!--===================================================-->
        <!-- END FOOTER -->


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
<?php } else { ?>
<?php echo $_smarty_tpl->getSubTemplate ('login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }
}
?>