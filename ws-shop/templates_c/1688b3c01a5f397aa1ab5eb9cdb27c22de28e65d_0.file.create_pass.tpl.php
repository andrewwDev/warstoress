<?php /* Smarty version 3.1.27, created on 2018-07-27 00:14:06
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/create_pass.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:3080201355b5a399e454d41_19627793%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1688b3c01a5f397aa1ab5eb9cdb27c22de28e65d' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/create_pass.tpl',
      1 => 1508674455,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3080201355b5a399e454d41_19627793',
  'variables' => 
  array (
    'code' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5b5a399e4a5ab3_30876775',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5b5a399e4a5ab3_30876775')) {
function content_5b5a399e4a5ab3_30876775 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '3080201355b5a399e454d41_19627793';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<div id="container" class="cls-container">


    <!-- BACKGROUND IMAGE -->
    <!--===================================================-->
    <div id="bg-overlay"></div>


    <!-- REGISTRATION FORM -->
    <!--===================================================-->
    <div class="cls-content">
        <div class="cls-content-lg panel">
            <div class="panel-body">
                <div class="mar-ver pad-btm">
                    <h3 class="h4 mar-no">Восстановление пароля</h3>
                    <p class="text-muted">Задайте новый пароль.</p>
                </div>
                <div id="after" style="display: none;">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Новый пароль создан</h4>
                        </div>
                    </div>
                </div>
                <form id="reg_form" action="">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Введите новый пароль" name="pass" id="pass" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Подтвердите новый пароль" name="p_pass" id="p_pass" required>
                            </div>
                        </div>
                    </div>
                    <button id="reg_form_finish" class="btn btn-primary btn-block" type="submit">Применить</button>
                </form>
            </div>
            <div class="pad-all">
                <a href="/ws-shop/" class="btn-link mar-rgt">Войти</a>
            </div>
        </div>
    </div>
    <!--===================================================-->


</div>
<!--===================================================-->
<!-- END OF CONTAINER -->
<?php echo '<script'; ?>
 src="/ws-shop/js/md5.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
    $( '#reg_form_finish' ).click(function(event) {
        event.preventDefault();
        if ($("#pass").val() == "") {
            alert("Введите пароль!");
            return false;
        }
        if ($("#p_pass").val() == "") {
            alert("Подтвердите пароль!");
            return false;
        }
        if ($("#pass").val() != $("#p_pass").val()) {
            alert("Введенные пароли не совпадают!");
            return false;
        }

        var post = {code: "<?php echo $_smarty_tpl->tpl_vars['code']->value;?>
", pass: md5($("#pass").val())};

        $("#reg_form").hide();

        $.ajax({
            url: 'backend/create_pass.php',
            data: post,
            type: 'POST',
            success: function (data) {
                //console.log(data);
                $("#after").show();
            }
        });
    });
<?php echo '</script'; ?>
>


</body>
</html>
<?php }
}
?>