<?php /* Smarty version 3.1.27, created on 2017-11-04 13:12:48
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/profile.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:193678944459fd92a0c0bb81_29634952%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '598618632bbf9f8de398f2054a07da639ff55bcd' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/profile.tpl',
      1 => 1509790366,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '193678944459fd92a0c0bb81_29634952',
  'variables' => 
  array (
    'login' => 0,
    'avatar' => 0,
    'owner_name' => 0,
    'user_buildings_count' => 0,
    'user_sectors_count' => 0,
    'user_friends_count' => 0,
    'user_balance' => 0,
    'user_transaction_out' => 0,
    'user_transaction_up' => 0,
    'user_transaction_ref' => 0,
    'user_transaction_agent' => 0,
    'user_transaction_bonus' => 0,
    'user_transaction_vyvod' => 0,
    'user_transaction' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59fd92a0c7ae39_86461160',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59fd92a0c7ae39_86461160')) {
function content_59fd92a0c7ae39_86461160 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '193678944459fd92a0c0bb81_29634952';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<?php if ($_smarty_tpl->tpl_vars['login']->value) {?>
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Профиль</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    
					<div class="row">
					    <div class="col-lg-2">


                           <div class="fixed-fluid">
					    <div class="fixed-sm-160 fixed-lg-200 pull-sm-left" style="width:100%;">
					        <div class="panel">
					            <!-- Simple profile -->
					            <div class="text-center pad-all bord-btm">
					                <div class="pad-ver">
                                        <?php if ($_smarty_tpl->tpl_vars['avatar']->value) {?>
											<img src="<?php echo $_smarty_tpl->tpl_vars['avatar']->value;?>
" class="img-lg img-border img-circle" alt="Profile Picture">
                                        <?php } else { ?>
											<img src="img/profile-photos/1.png" class="img-lg img-border img-circle" alt="Profile Picture">
                                        <?php }?>
					                </div>
					                <h4 class="text-lg text-overflow mar-no"><?php echo $_smarty_tpl->tpl_vars['owner_name']->value;?>
</h4>
					
					                <!-- <div class="pad-ver btn-groups">
					                    <a href="#" class="btn btn-hover-primary demo-pli-facebook icon-lg add-tooltip" data-original-title="Facebook" data-container="body"></a>
					                    <a href="#" class="btn btn-hover-info demo-pli-twitter icon-lg add-tooltip" data-original-title="Twitter" data-container="body"></a>
					                    <a href="#" class="btn btn-hover-danger demo-pli-google-plus icon-lg add-tooltip" data-original-title="Google+" data-container="body"></a>
					                    <a href="#" class="btn btn-hover-purple demo-pli-instagram icon-lg add-tooltip" data-original-title="Instagram" data-container="body"></a>
					                </div> -->
					            </div>
					            <div class="text-semibold text-main pad-all mar-no">Об игроке</div>
                                
                                
                                <div class="pad-all">
                                 <p class="mar-no">
					                            <span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['user_buildings_count']->value;?>
</span>
					                           Захвачено зданий
					                        </p> 
                                <p class="mar-no">
					                            <span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['user_sectors_count']->value;?>
</span>
					                          Захвачено территорий
					                        </p>
                                 <p class="mar-no">
					                            <span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['user_friends_count']->value;?>
</span>
					                         Друзей
					                        </p>            
                                </div>          
					        </div>
					    </div>
					    
                           </div>
					
					    </div>
						<div class="col-lg-5">
							<!--Network Line Chart-->
							<!--===================================================-->
							<div id="demo-panel-network" class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Личный счет</h3>
								</div>

								<!--Morris line chart placeholder-->
								<div class="row"
									 style="padding: 10px; text-align: center;">
									<div class="col-sm-12">
										На вашем счете: <?php echo $_smarty_tpl->tpl_vars['user_balance']->value/100;?>

									</div>
								</div>


							</div>
							<!--===================================================-->
							<!--End network line chart-->
						</div>
					    <div class="col-lg-5">
							<div class="panel panel-warning panel-colorful cobalance">
								<div class="pad-all">
									<p class="text-lg text-semibold"><i class="demo-pli-check icon-fw"></i> Баланс</p>
									<p class="mar-no">
										<span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['user_transaction_out']->value/100;?>
</span>
										Расход
									</p>
									<hr>
									<p class="mar-no" style="padding-left: 25px;">
										<span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['user_transaction_up']->value/100;?>
</span>
										Пополнение
									</p>
									<p class="mar-no" style="padding-left: 25px;">
										<span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['user_transaction_ref']->value/100;?>
</span>
										Реферальные
									</p>
									<p class="mar-no" style="padding-left: 25px;">
										<span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['user_transaction_agent']->value/100;?>
</span>
										Агентские
									</p>
									<p class="mar-no" style="padding-left: 25px;">
										<span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['user_transaction_bonus']->value/100;?>
</span>
										Бонусные
									</p>
									<hr>
									<p class="mar-no">
										<span class="pull-right text-bold"><?php echo $_smarty_tpl->tpl_vars['user_transaction_vyvod']->value/100;?>
</span>
										Выведено
									</p>
								</div>
							</div>
					    </div>
					</div>
					

					    
					</div>
					
					<div class="row">
					    <div class="col-xs-12" style="width:100%;">
					        <div class="panel">
					            <div class="panel-heading">
					                <h3 class="panel-title">Детализация</h3>
					            </div>
					
					            <!--Data Table-->
					            <!--===================================================-->
					            <div class="panel-body">
					                <div class="table-responsive">
										<?php if ($_smarty_tpl->tpl_vars['user_transaction']->value != 0) {?>
											<table class="table table-striped">
												<thead>
												<tr>
													<th>Дата операции</th>
													<th>Операция</th>
													<th>Магазин</th>
													<th>Баллы (руб.)</th>
												</tr>
												</thead>
												<tbody>
                                                <?php
$_from = $_smarty_tpl->tpl_vars['user_transaction']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
													<tr>

														<td><span class="text-muted"><?php echo $_smarty_tpl->tpl_vars['item']->value["tdate"];?>
</span></td>
														<td><?php echo $_smarty_tpl->tpl_vars['item']->value["tcomment"];?>
</td>
														<td><?php echo $_smarty_tpl->tpl_vars['item']->value["tsid"];?>
</td>
														<td><?php echo $_smarty_tpl->tpl_vars['item']->value["tsumm"]/100;?>
</td>
													</tr>
                                                <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
												</tbody>
											</table>
										<?php } else { ?>
											Операции отсутствуют
										<?php }?>

					                </div>
					            </div>
					            <!--===================================================-->
					            <!--End Data Table-->
					
					        </div>
					    </div>
					</div>
					
					
					
                </div>
                <!--===================================================-->
                <!--End page content-->

              





                <!-- FOOTER -->
				<!--===================================================-->
					<nav id="footer2-container">
						<div id="footer2">
						<?php echo $_smarty_tpl->getSubTemplate ('footer2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

						</div>
					</nav>
				<!--===================================================-->
				<!--END FOOTER -->	




     
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
<?php echo $_smarty_tpl->getSubTemplate ('aside.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>
 
                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
<?php echo $_smarty_tpl->getSubTemplate ('mainnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



       


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->


<?php } else { ?>
<?php echo $_smarty_tpl->getSubTemplate ('login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }
}
?>