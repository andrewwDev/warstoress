<?php /* Smarty version 3.1.27, created on 2017-07-09 19:22:00
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/forgot_pass.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:13044209959627448d63f34_20950162%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9551d632f36294715243d0591e291eaa24074a0c' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/forgot_pass.tpl',
      1 => 1499624488,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13044209959627448d63f34_20950162',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59627448d89469_65795268',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59627448d89469_65795268')) {
function content_59627448d89469_65795268 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '13044209959627448d63f34_20950162';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<div id="container" class="cls-container">


    <!-- BACKGROUND IMAGE -->
    <!--===================================================-->
    <div id="bg-overlay"></div>


    <!-- REGISTRATION FORM -->
    <!--===================================================-->
    <div class="cls-content">
        <div class="cls-content-lg panel">
            <div class="panel-body">
                <div class="mar-ver pad-btm">
                    <h3 class="h4 mar-no">Восстановление пароля</h3>
                    <p class="text-muted">На указаный email адрес будет отправлена ссылка для восстановления пароля.</p>
                </div>
                <div id="after" style="display: none;">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Ссылка для восстановления отправлена</h4>
                            Проверьте указанный почтовый ящик.
                        </div>
                    </div>
                </div>
                <form id="reg_form" action="">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email" name="email" id="email" required>
                            </div>
                        </div>
                    </div>
                    <button id="reg_form_finish" class="btn btn-primary btn-block" type="submit">Восстановить</button>
                </form>
            </div>
            <div class="pad-all">
                <a href="/ws-shop/" class="btn-link mar-rgt">Войти</a>
            </div>
        </div>
    </div>
    <!--===================================================-->


</div>
<!--===================================================-->
<!-- END OF CONTAINER -->
<?php echo '<script'; ?>
>
    $( '#reg_form_finish' ).click(function(event) {
        event.preventDefault();
        if ($("#email").val() == "") {
            alert("Введенные email адрес!");
            return false;
        }
        $("#reg_form").hide();

        var formData  = new FormData($( '#reg_form' )[0]);

        $.ajax({
            url: 'backend/forgot_pass.php',
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                //console.log(data);
                $("#after").show();
            }
        });
    });
<?php echo '</script'; ?>
>


</body>
</html>
<?php }
}
?>