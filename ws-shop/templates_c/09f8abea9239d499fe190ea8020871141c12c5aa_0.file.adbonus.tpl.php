<?php /* Smarty version 3.1.27, created on 2017-08-04 12:40:57
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/adbonus.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:94180237259845d496db541_14328431%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '09f8abea9239d499fe190ea8020871141c12c5aa' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/adbonus.tpl',
      1 => 1501846853,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '94180237259845d496db541_14328431',
  'variables' => 
  array (
    'login' => 0,
    'user_name' => 0,
    'owner_name' => 0,
    'user_balance' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59845d49711b44_89354267',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59845d49711b44_89354267')) {
function content_59845d49711b44_89354267 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '94180237259845d496db541_14328431';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<?php if ($_smarty_tpl->tpl_vars['login']->value) {?>
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed" style="
    position: absolute;
    display: block;
    height: 100%;
">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container" class="bonus">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Мобильная версия выдачи бонусов</h1>


                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
					<div class="col-md-12 text-center">
                        <div class="add-bonus" style="width:25%; ">
							<div id="after" style="display: none;">
								<p>Бонусы выданы <?php echo $_smarty_tpl->tpl_vars['user_name']->value;?>
</p>
							</div>
							<div id="before">
								<div class="form-group">
									<div class="col-lg-7">
												<input type="text" onkeyup="this.value = this.value.replace (/[^0-9+]/, '')" maxlength="12" class="form-control" name="phone" id="phone" placeholder="Номер телефона клиента">
												<button type="button" id="check_phone" class="btn btn-primary">Проверить номер</button>
											<div id="check_phone" style="display: none; color: red; font-weight: bold;">
												Такого пользователя нет
											</div>
									</div>
								</div>
								<!-- это доступно если номер телефона зарегистрирован в системе -->
								<div class="form-group">
									<div class="col-lg-7">
										Имя клиента: <?php echo $_smarty_tpl->tpl_vars['owner_name']->value;?>

									</div>
									<div class="col-lg-7">
										На балансе клиента: <?php echo $_smarty_tpl->tpl_vars['user_balance']->value;?>

									</div>
								</div>											
									<div class="form-group">
									<div class="col-lg-7">
										<input type="text" onkeyup="this.value = this.value.replace (/[^0-9+]/, '')" maxlength="12" class="form-control" name="phone" id="phone" placeholder="Снять с баланса клиента">
									</div>
								</div>	
															<!-- выдача бонусов происходит со счета компании доступной сотруднику -->

								<div class="form-group">
									<div class="col-lg-7">
									Всего к оплате: <?php echo $_smarty_tpl->tpl_vars['user_balance']->value;?>

									<button type="submit" id="after" class="btn btn-warning">Выдать бонусы</button>
									</div>

								</div>
							</div>
						</div>
                    </div>
				</div>

                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
					<?php echo $_smarty_tpl->getSubTemplate ('aside.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
<?php echo $_smarty_tpl->getSubTemplate ('mainnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>



      <!-- FOOTER -->
		<!--===================================================-->
		<nav id="footer2-container">
			<div id="footer2">
				<?php echo $_smarty_tpl->getSubTemplate ('footer2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

			</div>
		</nav>
		<!--===================================================-->
	<!--END FOOTER -->


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
<?php } else { ?>
<?php echo $_smarty_tpl->getSubTemplate ('login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }
}
?>