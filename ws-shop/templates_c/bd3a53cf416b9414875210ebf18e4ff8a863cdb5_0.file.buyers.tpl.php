<?php /* Smarty version 3.1.27, created on 2017-10-31 12:56:10
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-shop/templates/buyers.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:43822658259f848ba2ce3d7_11364335%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bd3a53cf416b9414875210ebf18e4ff8a863cdb5' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-shop/templates/buyers.tpl',
      1 => 1509443766,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '43822658259f848ba2ce3d7_11364335',
  'variables' => 
  array (
    'login' => 0,
    'current_shop' => 0,
    'sales' => 0,
    'value' => 0,
    'user_arr' => 0,
    'num_pages' => 0,
    'page' => 0,
    'active_page' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59f848ba317de8_56634132',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59f848ba317de8_56634132')) {
function content_59f848ba317de8_56634132 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '43822658259f848ba2ce3d7_11364335';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<?php if ($_smarty_tpl->tpl_vars['login']->value) {?>
    <div id="container" class="effect aside-float aside-bright mainnav-lg">

        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
<?php echo $_smarty_tpl->getSubTemplate ('navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Клиенты</h1>

                    <!--Searchbox-->
                    <!--<div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..">
                            <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                        </div>
                    </div>-->
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                    
                                <!--Page content-->
                <!--===================================================-->
                <?php if (!empty($_smarty_tpl->tpl_vars['current_shop']->value)) {?>
               <div id="page-content">
                   <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
                             <div class="panel-heading">
                                <h3 class="panel-title">Клиенты</h3>
                            </div>

                            <!--Data Table-->
                            <!--===================================================-->
                            <div class="panel-body">
                                <!--<div class="pad-btm form-inline">
                                    <div class="row">
                                        <div class="col-sm-6 table-toolbar-left">
                                            <button class="btn btn-purple"><i class="demo-pli-add icon-fw"></i>Add</button>
                                            <button class="btn btn-default"><i class="demo-pli-printer"></i></button>
                                            <div class="btn-group">
                                                <button class="btn btn-default"><i class="demo-pli-information"></i></button>
                                                <button class="btn btn-default"><i class="demo-pli-recycling"></i></button>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 table-toolbar-right">
                                            <div class="form-group">
                                                <input type="text" autocomplete="off" class="form-control" placeholder="Search" id="demo-input-search2">
                                            </div>
                                            <div class="btn-group">
                                                <button class="btn btn-default"><i class="demo-pli-download-from-cloud"></i></button>
                                                <div class="btn-group">
                                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="demo-pli-gear"></i>
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                                        <li><a href="#">Action</a></li>
                                                        <li><a href="#">Another action</a></li>
                                                        <li><a href="#">Something else here</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#">Separated link</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                                <?php if ($_smarty_tpl->tpl_vars['sales']->value) {?>
                                    <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Последняя дата операции</th>
                                            <th>Клиент</th>
                                            <th>Cashback</th>
                                            <th class="text-center">Оплачено рублей</th>
                                            <th class="text-center">Оплачено бонусов</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['sales']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
                                            <tr>
                                                <td><span class="text-muted"><i class="fa fa-clock-o"></i> <?php echo $_smarty_tpl->tpl_vars['value']->value["date"];?>
</span></td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['user_arr']->value[$_smarty_tpl->tpl_vars['value']->value["user_id"]];?>
</td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['value']->value["cashback"];?>
</td>
                                                <td class="text-center">
                                                    <?php echo $_smarty_tpl->tpl_vars['value']->value["cash"];?>

                                                </td>
                                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['value']->value["summ"]-$_smarty_tpl->tpl_vars['value']->value["cash"]*100;?>
</td>
                                            </tr>
                                        <?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
                                        </tbody>
                                    </table>
                                </div>
                                <hr>
                                <?php if ($_smarty_tpl->tpl_vars['num_pages']->value > 0) {?>
                                    <div class="pull-right">
                                        <ul class="pagination text-nowrap mar-no">
                                            <?php $_smarty_tpl->tpl_vars['page'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['page']->step = 1;$_smarty_tpl->tpl_vars['page']->total = (int) ceil(($_smarty_tpl->tpl_vars['page']->step > 0 ? $_smarty_tpl->tpl_vars['num_pages']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['num_pages']->value)+1)/abs($_smarty_tpl->tpl_vars['page']->step));
if ($_smarty_tpl->tpl_vars['page']->total > 0) {
for ($_smarty_tpl->tpl_vars['page']->value = 1, $_smarty_tpl->tpl_vars['page']->iteration = 1;$_smarty_tpl->tpl_vars['page']->iteration <= $_smarty_tpl->tpl_vars['page']->total;$_smarty_tpl->tpl_vars['page']->value += $_smarty_tpl->tpl_vars['page']->step, $_smarty_tpl->tpl_vars['page']->iteration++) {
$_smarty_tpl->tpl_vars['page']->first = $_smarty_tpl->tpl_vars['page']->iteration == 1;$_smarty_tpl->tpl_vars['page']->last = $_smarty_tpl->tpl_vars['page']->iteration == $_smarty_tpl->tpl_vars['page']->total;?>
                                                <li class="page-number <?php if ($_smarty_tpl->tpl_vars['page']->value == $_smarty_tpl->tpl_vars['active_page']->value) {?>active<?php }?>">
                                                    <a href="./buyers.php?sid=2&page=<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['page']->value;?>
</a>
                                                </li>
                                            <?php }} ?>
                                        </ul>
                                    </div>
                                <?php }?>
                                <?php } else { ?>
                                        <p>Операций по данной компании нет</p>
                                <?php }?>
                            </div>
                            </div>
                        </div>
                        
                           
                           
                           
                           

                               
                               
                               

                                   
                                   
                                       

                                           
                                           
                                               
                                               
                                               
                                           

                                       
                                       
                                   

                                   
                                   

                                       
                                       
                                           
                                               
                                               
                                           
                                           
                                               
                                               
                                                   
                                                   
                                               
                                           
                                           
                                               
                                               
                                               
                                                   
                                                       
                                                           
                                                               
                                                               
                                                               
                                                                   
                                                                   
                                                               
                                                           
                                                           
                                                               
                                                                   
                                                               
                                                               
                                                                   
                                                                   
                                                                       
                                                                           
                                                                           
                                                                       
                                                                       
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                       
                                                                   
                                                               
                                                           
                                                       
                                                   
                                                   
                                                       
                                                           
                                                           
                                                               
                                                               
                                                               
                                                               
                                                           
                                                           
                                                           
                                                           
                                                               
                                                               
                                                               
                                                               

                                                           
                                                           
                                                               
                                                               
                                                               
                                                               


                                                           
                                                           
                                                               
                                                               
                                                               
                                                               

                                                           
                                                           
                                                               
                                                               
                                                               
                                                               

                                                           
                                                           
                                                               
                                                               
                                                               
                                                               

                                                           
                                                           
                                                       
                                                   
                                                   
                                                   
                                                       
                                                           
                                                               
                                                           
                                                           
                                                               
                                                           
                                                           
                                                               
                                                           
                                                           
                                                               
                                                           
                                                           
                                                               
                                                           
                                                           
                                                               
                                                           
                                                           
                                                               
                                                           
                                                       
                                                   
                                               
                                               
                                           
                                       
                                   
                               
                           
                           
                           
                       
                   </div>

               
                <!--End-Page content-->
                <!--===================================================-->


              
               </div>
               </div>
               <?php }?>

     
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!--ASIDE-->
            <!--===================================================-->
            <aside id="aside-container">
                <div id="aside">
<?php echo $_smarty_tpl->getSubTemplate ('aside.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </aside>
            <!--===================================================-->
            <!--END ASIDE-->


            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
<?php echo $_smarty_tpl->getSubTemplate ('mainnav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>
		
                  <!-- FOOTER -->
				<!--===================================================-->
					<nav id="footer2-container">
						<div id="footer2">
						<?php echo $_smarty_tpl->getSubTemplate ('footer2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

						</div>
					</nav>
				<!--===================================================-->
				<!--END FOOTER -->	


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->



        <!-- SETTINGS - DEMO PURPOSE ONLY -->
    <!--===================================================-->
   
    <!--===================================================-->
    <!-- END SETTINGS -->
<?php } else { ?>
<?php echo $_smarty_tpl->getSubTemplate ('login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }
}
?>