<?php
error_reporting(-1);
require_once '../ws-panel/libs/Smarty.class.php';
$smarty = new Smarty();

session_start();
if (isset($_SESSION['uid'])) {
	require_once './db.php';

	$uid = $_SESSION['uid'];
	require_once './backend/user.php';

	$smarty->assign('owner_name', $username);
    $smarty->assign('user_status', $user_status);
    $smarty->assign('avatar', $avatar);
	$smarty->assign('shops_list', $shops_list);

    /* баланс игрока */
    $user_balance = 0;
    $user_transactions = [];
    if ($stmt = $mysqli->prepare("SELECT summ FROM `wsq_transaction` WHERE uid=?")) {
        $stmt->bind_param('s', $uid);
        $stmt->execute();
        $stmt->bind_result($summ);
        while ($stmt->fetch()) $user_transactions[] = $summ;
        $stmt->close();
        $user_balance = array_sum($user_transactions);
        if ($user_balance == NULL) {
            $smarty->assign('user_balance', 0);
        } else {
            $smarty->assign('user_balance', $user_balance);
        }
    }

    // захват зданий
    $user_buildings_count = 0;
    if ($stmt = $mysqli->prepare("SELECT COUNT(*) FROM `wsq_buildings` WHERE uid=?")) {
        $stmt->bind_param('s', $uid);
        $stmt->execute();
        $stmt->bind_result($user_buildings_count);
        $stmt->fetch();
        $stmt->close();
        if ($user_buildings_count == NULL) {
            $smarty->assign('user_buildings_count', 0);
        } else {
            $smarty->assign('user_buildings_count', $user_buildings_count);
        }
    }

    // захват территорий
    $user_sectors_count = 0;
    if ($stmt = $mysqli->prepare("SELECT COUNT(*) FROM `wsq_ter_sectors` WHERE uid=?")) {
        $stmt->bind_param('s', $uid);
        $stmt->execute();
        $stmt->bind_result($user_sectors_count);
        $stmt->fetch();
        $stmt->close();
        if ($user_sectors_count == NULL) {
            $smarty->assign('user_sectors_count', 0);
        } else {
            $smarty->assign('user_sectors_count', $user_sectors_count);
        }
    }

    // друзья
    $user_friends_count = 0;
    if ($stmt = $mysqli->prepare("SELECT COUNT(*) FROM `wsq_friends` WHERE uid=?")) {
        $stmt->bind_param('s', $uid);
        $stmt->execute();
        $stmt->bind_result($user_friends_count);
        $stmt->fetch();
        $stmt->close();
        if ($user_friends_count == NULL) {
            $smarty->assign('user_friends_count', 0);
        } else {
            $smarty->assign('user_friends_count', $user_friends_count);
        }
    }

    // транзакции
    $user_transaction = [];
    $user_transaction_out = 0;
    $user_transaction_up = 0;
    $user_transaction_ref = 0;
    $user_transaction_vyvod = 0;
    $user_transaction_agent = 0;
    $user_transaction_bonus = 0;
    if ($stmt = $mysqli->prepare("SELECT date, troper, comment, (SELECT title FROM `wsq_shops` WHERE wsq_transaction.sid = wsq_shops.sid) AS sid, mid, summ FROM `wsq_transaction` WHERE uid=?")) {
        $stmt->bind_param('s', $uid);
        $stmt->execute();
        $stmt->bind_result($tdate, $ttroper, $tcomment, $tsid, $tmid, $tsumm);
        while ($stmt->fetch()) $user_transaction[] = array(
            'tdate' => $tdate,
            'ttroper' => $ttroper,
            'tcomment' => $tcomment,
            'tsid' => $tsid,
            'tmid' => $tmid,
            'tsumm' => $tsumm,
        );
        $stmt->close();
        foreach ($user_transaction as $item) {
            if ($item["ttroper"] == 54 or $item["ttroper"] == 52) {
                $user_transaction_out += $item["tsumm"];
            } elseif ($item["ttroper"] == 56) {
                $user_transaction_ref += $item["tsumm"];
                $user_transaction_up += $item["tsumm"];
            } elseif ($item["ttroper"] == 64) {
                $user_transaction_agent += $item["tsumm"];
                $user_transaction_up += $item["tsumm"];
            } elseif ($item["ttroper"] == 55 or $item["ttroper"] == 60 or $item["ttroper"] == 61 or $item["ttroper"] == 62 or $item["ttroper"] == 63) {
                $user_transaction_bonus += $item["tsumm"];
                $user_transaction_up += $item["tsumm"];
            } elseif ($item["ttroper"] == 50) {
                $user_transaction_vyvod += $item["tsumm"];
            } else {
                $user_transaction_up += $item["tsumm"];
            }
        }
        if ($user_transaction == NULL) {
            $smarty->assign('user_transaction', 0);
            $smarty->assign('user_transaction_up', 0);
            $smarty->assign('user_transaction_ref', 0);
            $smarty->assign('user_transaction_agent', 0);
            $smarty->assign('user_transaction_vyvod', 0);
            $smarty->assign('user_transaction_bonus', 0);
            $smarty->assign('user_transaction_out', 0);
        } else {
            $smarty->assign('user_transaction', $user_transaction);
            $smarty->assign('user_transaction_up', $user_transaction_up);
            $smarty->assign('user_transaction_ref', $user_transaction_ref);
            $smarty->assign('user_transaction_agent', $user_transaction_agent);
            $smarty->assign('user_transaction_vyvod', $user_transaction_vyvod);
            $smarty->assign('user_transaction_bonus', $user_transaction_bonus);
            $smarty->assign('user_transaction_out', $user_transaction_out * -1);
        }
    }








	if (isset($_GET['sid'])) {
		$shop_id = (int)$_GET['sid'];
		require_once './backend/shop.php';
	}

	$login = true;
} else {
	$login = false;
}








$smarty->assign('login', $login);
$smarty->display('profile.tpl');
