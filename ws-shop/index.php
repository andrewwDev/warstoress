<?php
error_reporting(-1);
require_once '../ws-panel/libs/Smarty.class.php';
$smarty = new Smarty();

session_start();
if (isset($_SESSION['uid'])) {
	require_once './db.php';

    $username = "";
	$userrank = "";
	$uid = $_SESSION['uid'];
	require_once './backend/user.php';

    $smarty->assign('uid', $uid);
    $smarty->assign('owner_name', $username);
    $smarty->assign('user_status', $user_status);
    $smarty->assign('avatar', $avatar);
    $smarty->assign('shops_list', $shops_list);

    if (count($shops_list) > 0) {
        $shops_data = [];
        $profit_from_team = 0;
        $movement_funds = [];
        $move_bonus_up = 0;
        $move_bonus_down = 0;
        $sales = [];
        $all_sales = [];
        $count_sales = 0;
        $num_sales = 0;
        $count_bonus = 0;
        $count_cashback = 0;
        $balance_down = 0;
        $sales = [];
        $user_arr = [];
        $manager_arr = [];
        $active_page = 0;
        $num_pages = 0;
        foreach ($shops_list as $shop) {
            $shop_id = $shop["id"];
            require_once './backend/shop.php';
            // get statistic
            // $shop_id = 2;
            if ($stmt = $mysqli->prepare("SELECT sid, day, clients, newclients, spend_total, spend_money, received, responses FROM `wsq_shop_stat` WHERE sid=?")) {
                $stmt->bind_param('s', $shop_id);
                $stmt->execute();
                $stmt->bind_result($shop_id, $day, $clients, $newclients, $spend_total, $spend_money, $received, $responses);
                while ($stmt->fetch()) $shops_data[] = array(
                    'id' => $shop_id,
                    'day' => $day,
                    'clients' => $clients,
                    'newclients' => $newclients,
                    'spend_total' => $spend_total,
                    'spend_money' => $spend_money,
                    'received' => $received,
                    'responses' => $responses
                );
                $stmt->close();
                $profit_from_team = 0;
                foreach ($shops_data as $key => $value) {
                    $profit_from_team += $value["spend_money"];
                }
                $profit_from_team /= 100;
                $smarty->assign('shops_data', $shops_data);
                $smarty->assign('profit_from_team', $profit_from_team);
            } else {
                $smarty->assign('shops_data', []);
                $smarty->assign('profit_from_team', 0);
            }
            // получение данных о движении бонусов
            if ($stmt = $mysqli->prepare("SELECT date, summ, troper FROM `wsq_transaction_shop` WHERE sid=? AND btype=0")) {
                $stmt->bind_param('s', $shop_id);
                $stmt->execute();
                $stmt->bind_result($date, $summ, $troper);
                while ($stmt->fetch()) $movement_funds[] = array(
                    'date' => $date,
                    'summ' => $summ,
                    'troper' => $troper
                );
                $stmt->close();
                $move_bonus_down = 0;
                $move_bonus_up = 0;
                foreach ($movement_funds as $key => $value) {
                    if (($value["troper"] == 1) or ($value["troper"] == 2) or ($value["troper"] == 3) or ($value["troper"] == 4)) {
                        $move_bonus_up += $value["summ"];
                    } else {
                        $move_bonus_down -= $value["summ"];
                    }
                }
                $smarty->assign('movement_funds', $movement_funds);
                $smarty->assign('move_bonus_up', $move_bonus_up);
                $smarty->assign('move_bonus_down', $move_bonus_down);
            } else {
                $smarty->assign('movement_funds', []);
                $smarty->assign('move_bonus_up', 0);
                $smarty->assign('move_bonus_down', 0);
            }

            // Продажи

            if ($stmt = $mysqli->prepare("SELECT date, uid, mid, cashback, cash,  summ FROM `wsq_receipt` WHERE sid=?")) {
                $stmt->bind_param('s', $shop_id);
                $stmt->execute();
                $rows = $stmt->bind_result($date, $user_id, $manager_id, $cashback, $cash, $summ);
                while ($stmt->fetch()) {
                    $all_sales[] = array(
                        "date" => $date,
                        "user_id" => $user_id,
                        "manager_id" => $manager_id,
                        "cashback" => $cashback,
                        "cash" => $cash,
                        'summ' => $summ
                    );
                }
                $stmt->close();
                $count_sales = 0;
                $count_bonus = 0;
                $count_cashback = 0;
                $num_sales = 0;
                foreach ($all_sales as $key => $value) {
                    $num_sales++;
                    $count_sales += $value["summ"];
                    $count_bonus += $value["summ"] - $value["cash"];
                    $count_cashback += $value["cashback"];
                }
                $count_sales = $count_sales / 100;
                $smarty->assign('count_sales', $count_sales);
                $smarty->assign('num_sales', $num_sales);
                $smarty->assign('all_sales', $all_sales);
                $smarty->assign('count_bonus', $count_bonus);
                $smarty->assign('count_cashback', $count_cashback);
            } else {
                $smarty->assign('count_sales', 0);
                $smarty->assign('all_sales', []);
            }

            // блок баланс

            if ($stmt = $mysqli->prepare("SELECT spend_money FROM `wsq_shop_stat` WHERE sid=?")) {
                $stmt->bind_param('s', $shop_id);
                $stmt->execute();
                $rows = $stmt->bind_result($spend_money);
                while($stmt->fetch()) {
                    $balance_down += $spend_money;
                }
                $stmt->close();
                $balance_down = $balance_down / 100;
                $smarty->assign('balance_down', $balance_down);
            } else {
                $smarty->assign('balance_down', 0);
            }

            // пагинация
            if ($stmt = $mysqli->prepare("SELECT  COUNT(*) FROM `wsq_receipt` WHERE sid=?")) {
                $stmt->bind_param('s', $shop_id);
                $stmt->execute();
                $rows = $stmt->bind_result($count);
                while($stmt->fetch()) $total = $count;
                $per_page = 10;
                $num_pages = ceil($total / $per_page);
                if (isset($_GET["page"])) {
                    $page = $_GET['page']-1;
                    $active_page = $_GET['page'];
                } else {
                    $page = 0;
                    $active_page = 1;
                }
                $start=abs($page*$per_page);
                if ($stmt = $mysqli->prepare("SELECT date, uid, mid, cashback, cash,  summ FROM `wsq_receipt` WHERE sid=? LIMIT ?,?")) {
                    $stmt->bind_param('sss', $shop_id, $start, $per_page);
                    $stmt->execute();
                    $rows = $stmt->bind_result($date, $user_id, $manager_id, $cashback, $cash, $summ);
                    while ($stmt->fetch()) $sales[] = array(
                        "date" => $date,
                        "user_id" => $user_id,
                        "manager_id" => $manager_id,
                        "cashback" => $cashback,
                        "cash" => $cash/100,
                        'summ' => $summ
                    );
                    $stmt->close();
                    foreach ($sales as $key => $value) {
                        if ($sql = $mysqli->prepare("SELECT name, surname FROM `wsq_users` WHERE uid=?")) {
                            $sql->bind_param('s', $value["user_id"]);
                            $sql->execute();
                            $res = $sql->bind_result($name, $surname);
                            while ($sql->fetch()) {
                                $user_arr[$value["user_id"]] = $name." ".$surname;
                            }
                        }
                        if ($sql = $mysqli->prepare("SELECT name, surname FROM `wsq_users` WHERE uid=?")) {
                            $sql->bind_param('s', $value["manager_id"]);
                            $sql->execute();
                            $res = $sql->bind_result($name, $surname);
                            while ($sql->fetch()) {
                                $manager_arr[$value["manager_id"]] = $name." ".$surname;
                            }
                        }
                        $sql->close();
                    }
                }

                $smarty->assign('sales', $sales);
                $smarty->assign('user_arr', $user_arr);
                $smarty->assign('manager_arr', $manager_arr);
                $smarty->assign('active_page', $active_page);
                $smarty->assign('num_pages', $num_pages);

            }
        }
    }

	$login = true;
} else {
	$login = false;
}








$smarty->assign('login', $login);
$smarty->display('index.tpl');
