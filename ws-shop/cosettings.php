<?php
error_reporting(-1);
require_once '../ws-panel/libs/Smarty.class.php';
$smarty = new Smarty();

session_start();
if (isset($_SESSION['uid'])) {
	require_once './db.php';

	$uid = $_SESSION['uid'];
	require_once './backend/user.php';

	$smarty->assign('owner_name', $username);
    $smarty->assign('user_status', $user_status);
    $smarty->assign('avatar', $avatar);
	$smarty->assign('shops_list', $shops_list);
	
	// if (isset($_GET['id'])) {
	// 	$smarty->assign('shop_id', $_GET['shop_id']);
	// } else {
	// 	$smarty->assign('shop_id', '');
	// }




	
		


	if (isset($_GET['sid'])) {
		// $shop_id = (int)$_GET['sid'];
		require_once './backend/shop.php';
		
		$id = (int) $_GET['sid'];
		$smarty->assign('current_shop', '');

		foreach ($shops_list as $key => $value) {
		    if ($value['id'] == $id) {
			    $smarty->assign('current_shop', $shops_list[$key]);
			    $smarty->assign('shop_id', $id);

			    if (strlen($shops_list[$key]["worktime"]) > 0) {
                    $work_time_array = explode(",",$shops_list[$key]["worktime"]);

                    $hours1 = floor($work_time_array[0] / 60);
                    if ($hours1 < 10) $hours1 = "0".$hours1;
                    $minutes1 = $work_time_array[0] - ($hours1 * 60);
                    if ($minutes1 < 10) $minutes1 = "0".$minutes1;

                    $hours2 = floor($work_time_array[1] / 60);
                    if ($hours2 < 10) $hours2 = "0".$hours2;
                    $minutes2 = $work_time_array[1] - ($hours2 * 60);
                    if ($minutes2 < 10) $minutes2 = "0".$minutes2;

                    $smarty->assign('current_work_time', $hours1.":".$minutes1." - ".$hours2.":".$minutes2);
                }
			}
		}

        $categories = [];
        if ($stmt = $mysqli->prepare("SELECT csid, title FROM `wsq_catshop`")) {
            $stmt->execute();
            $stmt->bind_result($csid, $title);
            while ($stmt->fetch()) $categories[] = array(
                'csid' => $csid,
                'title' => $title,
            );
            $stmt->close();
            $smarty->assign('categories', $categories);
        }

        $select_cats = [];
        if ($stmt = $mysqli->prepare("SELECT cid FROM `wsq_shopsincat` WHERE sid=?")) {
            $stmt->bind_param('s', $id);
            $stmt->execute();
            $stmt->bind_result($cid);
            while ($stmt->fetch()) $select_cats[] = array($cid);
            $stmt->close();
            $smarty->assign('select_cats', $select_cats);
        }

        $geo_regions = [];
        if ($stmt = $mysqli->prepare("SELECT geo_region_id, name FROM `geo_region`")) {
            $stmt->execute();
            $stmt->bind_result($geo_reg_id, $geo_reg_name);
            while ($stmt->fetch()) $geo_regions[] = array(
                'geo_reg_id' => $geo_reg_id,
                'geo_reg_name' => $geo_reg_name,
            );
            $stmt->close();
            $smarty->assign('geo_regions', $geo_regions);
        }

        $reg_and_city = [];
        if ($stmt = $mysqli->prepare("SELECT reg_id, city_id name FROM `wsq_shopsinreg` WHERE sid=?")) {
            $stmt->bind_param('s', $id);
            $stmt->execute();
            $stmt->bind_result($geo_reg_id, $geo_city_id);
            while ($stmt->fetch()) $reg_and_city[] = array(
                'geo_reg_id' => $geo_reg_id,
                'geo_city_id' => $geo_city_id,
            );
            $stmt->close();
            $smarty->assign('reg_and_city', $reg_and_city);
        }

        $geo_city = [];
        if ($stmt = $mysqli->prepare("SELECT geo_city_id, name FROM `geo_city` WHERE geo_region_id = ?")) {
            $stmt->bind_param('s', $reg_and_city[0]["geo_reg_id"]);
            $stmt->execute();
            $stmt->bind_result($geo_city_id, $geo_city_name);
            while ($stmt->fetch()) $geo_city[] = array(
                'geo_city_id' => $geo_city_id,
                'geo_city_name' => $geo_city_name,
            );
            $stmt->close();
            $smarty->assign('geo_city', $geo_city);
        }
	}

	$login = true;
} else {
	$login = false;
}








$smarty->assign('login', $login);
$smarty->display('cosettings.tpl');
