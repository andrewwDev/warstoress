
	<div id="container" class="cls-container login-page">

		<!-- LOGIN FORM -->
		<div class="cls-content">
		    <div class="cls-content-sm panel">
		        <div class="panel-body">
		            <div class="mar-ver pad-btm">
		                <h3 class="h4 mar-no">Вход для владельцев магазинов</h3>
		                <p class="text-muted">Войдите в свою учетную запись</p>
		            </div>
		            <form id="loginForm" method="post" action="">
		                <div class="form-group">
		                    <input type="text" name="login" class="form-control" placeholder="Логин" autofocus>
		                </div>
		                <div class="form-group">
		                    <input type="password" name="password" class="form-control" placeholder="Пароль">
		                </div>
		                <div class="checkbox pad-btm text-left">
		                    <input id="demo-form-checkbox" class="magic-checkbox" type="checkbox">
		                    <label for="demo-form-checkbox">Запомнить меня</label>
		                </div>
		                <button class="btn btn-primary btn-lg btn-block" type="submit">Войти</button>
		            </form>
		        </div>

		        <!-- <div class="pad-all">
		            <a href="pages-password-reminder.html" class="btn-link mar-rgt">Forgot password ?</a>
		            <a href="pages-register.html" class="btn-link mar-lft">Create a new account</a>

		            <div class="media pad-top bord-top">
		                <div class="pull-right">
		                    <a href="#" class="pad-rgt"><i class="demo-psi-facebook icon-lg text-primary"></i></a>
		                    <a href="#" class="pad-rgt"><i class="demo-psi-twitter icon-lg text-info"></i></a>
		                    <a href="#" class="pad-rgt"><i class="demo-psi-google-plus icon-lg text-danger"></i></a>
		                </div>
		                <div class="media-body text-left">
		                    Login with
		                </div>
		            </div>
		        </div> -->
		    </div>
		</div>

	</div>
<script>
	$( 'button[type=submit]' ).click(function(event) {
		event.preventDefault();

		$.post('backend/auth.php', $( '#loginForm' ).serialize())
		    .done(function( data ) {
		    	console.log(data);
				if (data == 'yes')
					location.reload();
				else alert(data);
		    });

  	});
</script>