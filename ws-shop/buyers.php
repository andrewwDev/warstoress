<?php
error_reporting(-1);
require_once '../ws-panel/libs/Smarty.class.php';
$smarty = new Smarty();

session_start();
if (isset($_SESSION['uid'])) {
	require_once './db.php';

	$uid = $_SESSION['uid'];
	require_once './backend/user.php';

	$smarty->assign('owner_name', $username);
    $smarty->assign('user_status', $user_status);
    $smarty->assign('avatar', $avatar);
	$smarty->assign('shops_list', $shops_list);


	if (isset($_GET['sid'])) {
		$shop_id = (int)$_GET['sid'];
        $smarty->assign('current_shop', '');

        foreach ($shops_list as $key => $value) {
            if ($value['id'] == $shop_id) {
                $smarty->assign('current_shop', $shops_list[$key]);
                require_once './backend/shop.php';

                // пагинация
                $sales = [];
                if ($stmt = $mysqli->prepare("SELECT  COUNT(*) FROM `wsq_receipt` WHERE sid=?")) {
                    $stmt->bind_param('s', $shop_id);
                    $stmt->execute();
                    $rows = $stmt->bind_result($count);
                    while($stmt->fetch()) $total = $count;
                    $per_page = 10;
                    $num_pages = ceil($total / $per_page);
                    if (isset($_GET["page"])) {
                        $page = $_GET['page']-1;
                        $active_page = $_GET['page'];
                    } else {
                        $page = 0;
                        $active_page = 1;
                    }
                    $start=abs($page*$per_page);
                    if ($stmt = $mysqli->prepare("SELECT date, uid, mid, cashback, cash,  summ FROM `wsq_receipt` WHERE sid=? LIMIT ?,?")) {
                        $stmt->bind_param('sss', $shop_id, $start, $per_page);
                        $stmt->execute();
                        $rows = $stmt->bind_result($date, $user_id, $manager_id, $cashback, $cash, $summ);
                        while ($stmt->fetch()) $sales[] = array(
                            "date" => $date,
                            "user_id" => $user_id,
                            "manager_id" => $manager_id,
                            "cashback" => $cashback,
                            "cash" => $cash/100,
                            'summ' => $summ
                        );
                        $stmt->close();
                        $user_arr = [];
                        $manager_arr = [];
                        foreach ($sales as $key => $value) {
                            if ($sql = $mysqli->prepare("SELECT name, surname FROM `wsq_users` WHERE uid=?")) {
                                $sql->bind_param('s', $value["user_id"]);
                                $sql->execute();
                                $res = $sql->bind_result($name, $surname);
                                while ($sql->fetch()) {
                                    $user_arr[$value["user_id"]] = $name." ".$surname;
                                }
                            }
                            if ($sql = $mysqli->prepare("SELECT name, surname FROM `wsq_users` WHERE uid=?")) {
                                $sql->bind_param('s', $value["manager_id"]);
                                $sql->execute();
                                $res = $sql->bind_result($name, $surname);
                                while ($sql->fetch()) {
                                    $manager_arr[$value["manager_id"]] = $name." ".$surname;
                                }
                            }
                            $sql->close();
                        }
                    }

                    $smarty->assign('sales', $sales);
                    $smarty->assign('user_arr', $user_arr);
                    $smarty->assign('manager_arr', $manager_arr);
                    $smarty->assign('active_page', $active_page);
                    $smarty->assign('num_pages', $num_pages);

                }
            }
        }




		



	}

	$login = true;
} else {
	$login = false;
}








$smarty->assign('login', $login);
$smarty->display('buyers.tpl');
