<?php
error_reporting(-1);
require_once './db.php';
require_once '../ws-panel/libs/Smarty.class.php';
$smarty = new Smarty();

if(isset($_GET["code"])) {
    $code = htmlspecialchars($_GET["code"]);
    if ($stmt = $mysqli->prepare("SELECT count(*) FROM `wsq_forgot_pass` WHERE code=?")) {
        $stmt->bind_param("s", $code);
        $stmt->execute();
        $stmt->bind_result($row);
        $stmt->fetch();
        $stmt->close();
    }
    if ($row > 0) {
        $smarty->assign('code', $code);
        $smarty->display('create_pass.tpl');
    } else {
        $smarty->display('forgot_pass.tpl');
    }
} else {
    $smarty->display('forgot_pass.tpl');
}