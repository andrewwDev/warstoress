<?php
error_reporting(-1);
require_once '../ws-panel/libs/Smarty.class.php';
$smarty = new Smarty();

session_start();
if (isset($_SESSION['uid'])) {
	require_once './db.php';

	$uid = $_SESSION['uid'];
	require_once './backend/user.php';

	$smarty->assign('owner_name', $username);
    $smarty->assign('user_status', $user_status);
    $smarty->assign('avatar', $avatar);
	$smarty->assign('shops_list', $shops_list);
	$smarty->assign('phone', $phone);

	
	/* баланс игрока */
    $user_balance = 0;
    $user_transactions = [];
    if ($stmt = $mysqli->prepare("SELECT summ FROM `wsq_transaction` WHERE uid=?")) {
        $stmt->bind_param('s', $uid);
        $stmt->execute();
        $stmt->bind_result($summ);
        while ($stmt->fetch()) $user_transactions[] = $summ;
        $stmt->close();
        $user_balance = array_sum($user_transactions);
        if ($user_balance == NULL) {
            $smarty->assign('user_balance', 0);
        } else {
            $smarty->assign('user_balance', $user_balance);
        }
    }

	if (isset($_GET['sid'])) {
		$shop_id = (int)$_GET['sid'];
		require_once './backend/shop.php';



	}

	$login = true;
} else {
	$login = false;
}








$smarty->assign('login', $login);
$smarty->display('adbonus.tpl');
