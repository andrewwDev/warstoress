<?php
error_reporting(-1);
require_once '../ws-panel/libs/Smarty.class.php';
$smarty = new Smarty();

session_start();
if (isset($_SESSION['uid'])) {
	require_once './db.php';

	$uid = $_SESSION['uid'];
	require_once './backend/user.php';

	$smarty->assign('owner_name', $username);
    $smarty->assign('user_status', $user_status);
    $smarty->assign('avatar', $avatar);
	$smarty->assign('shops_list', $shops_list);


	if (isset($_GET['sid'])) {
		$shop_id = (int)$_GET['sid'];

        $smarty->assign('current_shop', '');

        foreach ($shops_list as $key => $value) {
            if ($value['id'] == $shop_id) {
                $smarty->assign('current_shop', $shops_list[$key]);
                require_once './backend/shop.php';
                $news = [];
                if ($stmt = $mysqli->prepare("SELECT nid, title, ndate, hasimg, description FROM `wsq_news` WHERE shop_id=? AND published=1")) {
                    $stmt->bind_param('s', $shop_id);
                    $stmt->execute();
                    $stmt->bind_result($nid, $title, $ndate, $hasimg, $description);
                    while ($stmt->fetch()) $news[] = array(
                        'nid' => $nid,
                        'title' => $title,
                        'ndate' => date("d.m.y", strtotime($ndate)),
                        'hasimg' => $hasimg,
                        'description' => $description,
                    );
                    $stmt->close();
                    if (count($news) > 0) {
                        $news = array_reverse($news);
                        $time_news = (time() - strtotime($news[0]["ndate"]))/(60*60*24);
                        $smarty->assign('news', $news);
                        $smarty->assign('time_news', $time_news);
                    } else {
                        $smarty->assign('news', []);
                        $smarty->assign('time_news', 10);
                    }
                    $smarty->assign('shop_id', $shop_id);
                }
            }
        }


		



	}

	$login = true;
} else {
	$login = false;
}








$smarty->assign('login', $login);
$smarty->display('conews.tpl');