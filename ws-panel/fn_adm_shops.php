<?php
//Функция для управления магазинами
//

function adm_shops($pg=0){
    global $tmpres;
           $ononepg=30; //Отображать на одной странице
	$shoplist=array();	
	if ($pg>0){
		$strlimit="LIMIT ".($ononepg*($pg-1)).", ".$ononepg;
	}else{
		$strlimit='';
	}
	//Получаем количество листов всего
	$news_num=0;
	$sql="SELECT count(sid) FROM wsq_shops";// where published=1";
	$res = mysqli_query($tmpres, $sql);
	if ($res!=false){if (mysqli_num_rows($res)>0){$news_num=0+mysqli_fetch_row($res)[0];}} //Всего новостей
	$pagenum=ceil($news_num/$ononepg); //Всего листов
	//
	$sql="SELECT sid as id, title, (SELECT CONCAT_WS(' ', name, surname) FROM `wsq_users` WHERE wsq_users.uid = wsq_shops.ownerid) AS ownerid, published, moderator_flag, shopnick FROM wsq_shops $strlimit";
	$res = mysqli_query($tmpres, $sql);
	if ($res!=false){	
		if (mysqli_num_rows($res)>0){
			while($row=mysqli_fetch_assoc($res)){
				$shoplist[]=$row;		
			}
		}	
	}
	$resarr=array();
	$resarr[0]=$shoplist;
	$resarr[1]=$pagenum;
	return $resarr;
}

function adm_load_shop($pg=0){
    global $tmpres;
	$sql="SELECT * FROM wsq_shops WHERE sid=$pg";
	$res = mysqli_query($tmpres, $sql);
	$res_list=array();	
	if ($res!=false){	
		if (mysqli_num_rows($res)>0){
			while($row=mysqli_fetch_array($res)){
				$res_list=$row;
			}
		}	
	}
	return $res_list;
}

function adm_load_cats($pg=0){
    global $tmpres;
    $catids = [];
    $sql="SELECT cid FROM `wsq_shopsincat` WHERE sid=$pg";
    $res = mysqli_query($tmpres, $sql);
    if ($res!=false){
        if (mysqli_num_rows($res)>0){
            while($row=mysqli_fetch_array($res)){
                $catids[] = $row[0];
            }
        }
    }
    return $catids;
}
?>