<script>
var arr_cityname= new Array();
var arr_cityid= new Array();
var arr_cat_sel=new Array();
var files;  {* картинки для загрузки на сервер *}
arr_cat_sel=[{$catsel}];
</script>

 {literal}
<script type="text/javascript">
  function figupload(nid){
    
    var data = new FormData();
    $.each( files, function( key, value ){
      data.append( key, value );
      console.log(key);
      console.log(value);
      });
    data.append( 'nid',  (0+nid));
    // Отправляем запрос
    console.log(data);


    $.ajax({
        url: 'http://warstores.net/ws-panel/ajax/enj_submitfig.php?nid='+nid+'&uploadfiles',
        type: 'POST',
        data: data,
        cache: false,
        crossDomain: true,
        dataType: 'json',
        processData: false, // Не обрабатываем файлы (Don't process the files)
        contentType: false, // Так jQuery скажет серверу что это строковой запрос
        success: function( respond, textStatus, jqXHR ){
            if( typeof respond.error === 'undefined' ){
                // Файлы успешно загружены, делаем что нибудь здесь
                console.log("Изображение удачно отправлено на сервер");
                // выведем пути к загруженным файлам в блок '.ajax-respond'
                var files_path = respond.files;
                var html = '';
                //$.each( files_path, function( key, val ){ html += val +'<br>'; } )
                var src = "http://warstores.net/ws_images/news/img"+nid+".jpg?r="+Math.random();//генерируем уникальный адрес картинки
                $("#imgsrc").removeAttr('src').attr('src', src);
                //$('.ajax-respond').html( html );
            }
            else{
                console.log('ОШИБКИ ОТВЕТА сервера: ' + respond.error );
            }
        },
        error: function( jqXHR, textStatus, errorThrown ){
            console.log('ОШИБКИ AJAX запроса: ' + textStatus );
        }
    });

    return false;
  }
</script>
<style>
input.imgloader button{background-color: #444;}
</style>
{/literal}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/css/bootstrap-slider.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/bootstrap-slider.min.js"></script>
<div style="padding-left:15px; padding-right:15px;">
<h1>РЕДАКТИРОВАНИЕ МАГАЗИНА</h1>
    <form id="set_shop_form" action="ajax/adm_set_shop.php" method="post" enctype="multipart/form-data" >
        <input type="hidden" readonly name="shop_id" id="shop_id" value="{$shop_id}">
  <div class="row" style="padding:20px;">
<div class="row">
      <div class="col-md-12 col-xs-12 " id="toppanel" style="padding:5px;">
        <button id="comp_finish" type="button" class="btn btn-success pull-right">Сохранить</button>
        <button type="button" class="btn btn-default pull-right" onClick="window.location.href='adm.php?cmd=editshop'">Закрыть</button>
      </div>
  </div>
      <div id="after" style="display: none;">
          <div class="row">
              <div class="col-md-12" style="text-align: right;">
                  Данные обновлены
              </div>
          </div>
      </div>
<div class="row">
  <div class="col-md-12 col-xs-12" id="toppanel">
    <hr>

<!-- ***********************  -->
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#panel1">Информация для покупателя</a></li>
      <li><a data-toggle="tab" href="#panel2">Юридические данные</a></li>
      <li><a data-toggle="tab" href="#panel3">Тариф</a></li>      
      <li><a data-toggle="tab" href="#panel4">Модерация</a></li>
    </ul>
     
    <div class="tab-content">
<!--
      <div id="panel1" class="tab-pane fade in active">
        <h3>Панель 1</h3>
        <p>Содержимое 1 панели...</p>
      </div>
      <div id="panel2" class="tab-pane fade">
        <h3>Панель 2</h3>
        <p>Содержимое 2 панели...</p>
      </div>
      <div id="panel3" class="tab-pane fade">
        <h3>Панель 3</h3>
        <p>Содержимое 3 панели...</p>
      </div>
      <div id="panel4" class="tab-pane fade">
        <h3>Панель 4</h3>
        <p>Содержимое 4 панели...</p>
      </div>
   -->

<div id="panel1" class="tab-pane fade in active">
  <br>
<form class="form-horizontal">
      <input type="hidden" id="shop_id" value="{$shop_id}">
      <div class="form-group">
        <label class="col-xs-2 control-label">Название магазина:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_title" placeholder="Введите название магазина" name="copubname" value="{$row.title}">
        </div>
      </div>
      <div class="form-group">
        <label class="col-xs-2 control-label">Слоган:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_slogon" placeholder="Введите слоган магазина" name="copublessinfo" value="{$row.slogon}" maxlength=128>
        </div>
      </div>

    <div class="form-group">
        <label class="col-lg-3 control-label">Время работы:</label>
        <div class="col-lg-7">
            <b id="show_time">
                {if strlen($row.worktime) > 0}
                    {$current_work_time}
                {else}
                    9:00 - 18:00
                {/if}
            </b> <br>
            <input id="slider_time" name="copubworktime" type="text" class="span2" value="" data-slider-min="0" data-slider-max="1440" data-slider-tooltip="hide" data-slider-step="30" data-slider-value="[{if strlen($row.worktime) > 0}{$row.worktime}{else}540,1080{/if}]"/>
        </div>
    </div>
    <script>
        var slider_time = $('#slider_time').slider({
            formatter: function(value) {
                return 'Время работы: ' + value + ' %';
            }
        });
        slider_time.change(function(e) {
            var hours1 = Math.floor(e.value.newValue["0"] / 60);
            var minutes1 = e.value.newValue["0"] - (hours1 * 60);

            if (hours1.length == 1) hours1 = '0' + hours1;
            if (minutes1.length == 1) minutes1 = '0' + minutes1;
            if (minutes1 == 0) minutes1 = '00';
            if (hours1 == 0) {
                hours1 = 00;
                minutes1 = minutes1;
            }

            var hours2 = Math.floor(e.value.newValue["1"] / 60);
            var minutes2 = e.value.newValue["1"] - (hours2 * 60);

            if (hours2.length == 1) hours2 = '0' + hours2;
            if (minutes2.length == 1) minutes2 = '0' + minutes2;
            if (minutes2 == 0) minutes2 = '00';
            if (hours2 == 24) {
                hours2 = 00;
                minutes2 = "00";
            }

            $('#show_time').html(hours1 + ':' + minutes1 + ' - ' + hours2 + ':' + minutes2);
        });
    </script>



      <div class="form-group">
        <label class="col-xs-2 control-label">Адрес:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_address" placeholder="Введите адрес" name="copubgeoadress" value="{$row.address}" maxlength=256>
        </div>
      </div>  
      <div class="form-group">
        <label class="col-xs-2 control-label">Географические координаты:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_YGeoLoc" placeholder="00.000000, 00.000000" name="YGeoLoc" value="{$row.YGeoLoc}" maxlength=64>
        </div>
      </div> 
      <div class="form-group">
        <label class="col-xs-2 control-label">Телефон:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_phone" placeholder="" name="copubtel" value="{$row.phone}" maxlength=10>
        </div>
      </div> 
      <div class="form-group">
        <label class="col-xs-2 control-label">Сайт:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_url" placeholder="http://" name="copubsite" value="{$row.url}" maxlength=256>
        </div>
      </div> 

      <div class="form-group">
        <label class="col-xs-2 control-label">Описание магазина:</label>
        <div class="col-xs-10">
          <textarea class="form-control" id="shop_description" name="copubinfo">{$row.description}</textarea>
        </div>
      </div> 

      <div class="form-group">
        <label for="pg_keywords" class="col-xs-2 control-label">Картинки:</label>
        <div class="col-xs-10">
          <table width=100%>
            <tr>
              <td width=20%>
                <b>Картинка для логотипа 96x96</b><br><br>
                <input id="myfiles96" class="imgloader" type="file" accept="image/jpeg,image/png"> <br>
              </td>
              <td  width=10%>
                <div><a id="imglink96" href="http://warstores.net/ws_images/shoplogo/shop{$shop_id}.png" target="_blank"><img id="imgsrc1" src="http://warstores.net/ws_images/shoplogo/shop{$shop_id}.png" style="height:96px;"></a></div>
              </td>
              <td width=40%></td>
              <td width=20%>
                <b>Большая картинка 1280x960</b><br><br>
                <input id="myfiles1280" class="imgloader" type="file" accept="image/jpeg,image/png"> <br>
              </td>
              <td width=10%>
                <div><a id="imglink1280" href="http://warstores.net/ws_images/shoplogo/bigshop{$shop_id}.png" target="_blank"><img id="imgsrc2" src="http://warstores.net/ws_images/shoplogo/bigshop{$shop_id}.png" style="height:96px;"></a></div>
              </td>

          </tr>
        </table>
        </div>
      </div>

      <div class="form-group">
        <label class="col-xs-2 control-label">Категория:</label>
        <div class="col-xs-10">
        
        <select id="shop_cat" name="heading[]" class="selectpicker show-menu-arrow form-control" multiple data-max-options="2">
          {$catselarr}
{foreach $catlist as $value}         
          <option value="{$value.csid}">{$value.title}</option>
{/foreach}
        </select>
        </div>
      </div>


      <div class="form-group">
        <label for="pg_description" class="col-xs-2 control-label">Выберите регион:</label>
        <div class="col-xs-10">
        <select class="form-control" id="pg_reg_id" placeholder="Введите регион" OnChange="ReloadCity()">
            {foreach $regionlist as $value}
            <option value="{$value["geo_region_id"]}">{$value["name"]}</option>
            {/foreach}
        </select> 
        </div>
      </div>
      <div class="form-group">
        <label for="pg_description" class="col-xs-2 control-label">Выберите город:</label>
        <div class="col-xs-10">          
          <select class="form-control" id="pg_city_id" placeholder="Введите город">
            {foreach $citylist as $value}
            <option value="{$value["geo_city_id"]}">{$value["name"]}</option>
            {/foreach}
        </select> 
        </div>
      </div>

    <script>
        $('#pg_reg_id').selectpicker('val', {$city_and_reg[0]["reg_id"]});
        $('#pg_city_id').selectpicker('val', {$city_and_reg[0]["city_id"]});
    </script>

    </form>
  </div>

  <div id="panel2" class="tab-pane fade">
      <div class="form-group">
        <label class="col-xs-2 control-label">Название магазина:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_jtitle" name="courname" placeholder="Юридическое название магазина" value="{$row.ur_title}">
        </div>
      </div>

      <div class="form-group">
        <label class="col-xs-2 control-label">Хэштэг компании</label>
        <div class="col-xs-10">
           <input type="text" value="{$row.shopnick}" onkeyup="var simvols=/['А-я',':;'.,'\s','/','<>','[\]\\','#!@$%^&',''','{}?№\|_','-=+']/; if(simvols.test(this.value)) this.value=''" maxlength="12" class="form-control" name="shopnick" id="shopnick" placeholder="Хэштэг компании">
			<div id="check_shopnick" style="display: none; color: red; font-weight: bold;">
				Такой хэштег занят
			</div>
		</div>
      </div>
	  										{literal}
											<script>
                                                $( "#shopnick" ).blur(function() {
                                                    var shopnick = {shopnick: $('#shopnick').val()};
                                                    $.ajax({
                                                        url: 'backend/check_shopnick.php',
                                                        data: shopnick,
                                                        type: 'POST',
                                                        success: function (data) {
                                                            console.log(data);
                                                            if (data == 1) {
                                                                $("#check_shopnick").show();
                                                                $("#shopnick").addClass("errors");
                                                            } else {
                                                                $("#check_shopnick").hide();
                                                                $("#shopnick").removeClass("errors");
                                                            }
                                                        }
                                                    });
                                                });
											</script>
										{/literal}

       <div class="form-group">
        <label class="col-xs-2 control-label">ИНН:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_jinn" placeholder="ИНН" name="INN" value="{$row.ur_inn}">
        </div>
      </div>  

       <div class="form-group">
        <label class="col-xs-2 control-label">ОГРН:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_jogrn" placeholder="ОГРН" name="OGRN" value="{$row.ur_ogrn}">
        </div>
      </div> 

       <div class="form-group">
        <label class="col-xs-2 control-label">Контактный телефон:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_jphone" placeholder="" name="cotel" value="{$row.ur_phone}">
        </div>
      </div>

      <div class="form-group">
        <label class="col-xs-2 control-label">Юридический адрес:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_jaddress" placeholder="" name="couradress" value="{$row.ur_address}">
        </div>
      </div>

      <div class="form-group">
        <label class="col-xs-2 control-label">Фактический адрес:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_faddress" placeholder="" name="copubgeoadress" value="{$row.address}">
        </div>
      </div> 
  </div>
  
      <div id="panel3" class="tab-pane fade">


          <h3>Тариф</h3>
          <div class="form-horizontal">
              <div class="form-group">
							<label class="col-lg-3 control-label">
								<strong>Вознаграждение клиенту от чека: </strong>
							</label>
							<div class="col-lg-5">
								<input id="slideC" name="percent_client" data-slider-id='ex1Slider' type="text" data-slider-min="1" data-slider-max="20" data-slider-step="1" data-slider-value="{$row.payvalue}"/>
							</div>
							<label class="col-lg-3 control-label text-left">
								<span id="demo-range-def-val">{$row.payvalue} %</span>
							</label>
						</div>

						<div class="form-group">
							<label class="col-lg-3 control-label">
								<strong>Процент от покупки, который можно оплатить бонусами: </strong>
							</label>
							<div class="col-lg-5">
								<input id="slidePerPay" name="bonusborderpercent" data-slider-id='ex2Slider' type="text" data-slider-min="3" data-slider-max="100" data-slider-step="1" data-slider-value="{$row.bonusborderpercent}"/>
							</div>
							<label class="col-lg-3 control-label text-left">
								<span id="val-slidePerPay">{$row.bonusborderpercent} %</span>
							</label>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">
								<strong>Всего процент от чека: </strong>
							</label>
							<div class="col-lg-5">
								<input id="slideAll" name="percent_all" data-slider-id='ex2Slider' type="text" data-slider-min="3" data-slider-max="22" data-slider-step="1" data-slider-value="{$row.payvalue * 1.338}"/>
							</div>
							<label class="col-lg-3 control-label text-left">
								<span id="demo-range-step-val">{$row.payvalue * 1.338} %</span>
							</label>
						</div>
						<br>
						<div class="form-group">
							<label class="col-lg-3 control-label">Максимальное число бонусов, которые можно отдать за покупку</label>
							<div class="col-lg-7">
								<input id="realAddress" type="text" placeholder="в копейках" name="bonusbordervalue" class="form-control" value="{$row.bonusbordervalue}">
								<i class="ion-location"></i>
							</div>
						</div>
						<h4 class="text-main mar-btm">Вознаграждение сотруднику</h4>
						<div class="form-group">
							<label class="col-lg-3 control-label">За подключение клиента</label>
							<div class="col-lg-7">
								<input id="realAddress" type="text" placeholder="Размер вознаграждения фиксированны" name="paytomanbynewuser" class="form-control" value="{$row.paytomanbynewuser}">
								<i class="ion-location"></i>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">За выполнение операции (проводка чека)</label>
							<div class="col-lg-7">
								<input id="realAddress" type="text" placeholder="Размер вознаграждения фиксированны" name="paytomanbyoper" class="form-control" value="{$row.paytomanbyoper}">
								<i class="ion-location"></i>
							</div>
						</div>
									<script>
										var clientSlider = $('#slideC').slider({
											formatter: function(value) {
												return 'Процент от чека: ' + value + ' %';
											}
										});
										var perPaySlider = $('#slidePerPay').slider({
											formatter: function(value) {
												return 'Процент от покупки, который можно оплатить бонусами: ' + value + ' %';
											}
										});
										var allSlider = $('#slideAll').slider({
											formatter: function(value) {
												return 'Итого: ' + value + ' %';
											}
										});
										clientSlider.change(function(e) {
											allSlider.slider("setValue", Math.ceil(e.value.newValue * 1.338));
											$("#demo-range-def-val").text(e.value.newValue+" %");
											$("#demo-range-step-val").text(Math.ceil(e.value.newValue * 1.338) +" %");
										});
										perPaySlider.change(function(e) {
											$("#val-slidePerPay").text(e.value.newValue + " %");
										});
										allSlider.change(function(e) {
											clientSlider.slider("setValue", Math.ceil(e.value.newValue / 1.338));
											$("#demo-range-step-val").text(e.value.newValue+" %");
											$("#demo-range-def-val").text(Math.ceil(e.value.newValue / 1.338) +" %");
										});
									</script>


      </div>
      </div>
      <div id="panel4" class="tab-pane fade">
        <h3>Модерация</h3>
        <div class="form-horizontal">
              <div class="form-group">
                  <input type="radio" name="moderator_flag" value="1" {if $row.moderator_flag == 1}checked{/if}> Одобрено<br>
                  <input type="radio" name="moderator_flag" value="2" {if $row.moderator_flag == 2}checked{/if}> Отклонено<br>
                  <input type="radio" name="moderator_flag" value="3" {if $row.moderator_flag == 3}checked{/if}> Архив <br>
                  <input type="radio" name="moderator_flag" value="4" {if $row.moderator_flag == 4}checked{/if}> Заморожено
            </div>
        </div>
      </div>
<!-- ***********************  -->
 </div>


    

  </div>
</div> 
</div>
    </form>
</div>

<script>
    $( '#comp_finish' ).click(function(event) {
        event.preventDefault();

        var formData = new FormData($('#set_shop_form')[0]);

        $.ajax({
            url: 'ajax/adm_set_shop.php',
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                console.log(data);
                $("#after").show();
            }
        });
    });
</script>


 {literal}
  <script type="text/javascript">
      $(function () {
          $('#datetimepicker1').datetimepicker( {pickTime: true, language: 'ru'});

          $('input[type=file]').change(function(){files = this.files;});

          $('#shop_cat').val(arr_cat_sel);
          $('#shop_cat').selectpicker("refresh");

         
         /* $('#shop_cat').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
            var selected = $(e.currentTarget).val();
            var fl=0;
            var length = selected.length;
            if (clickedIndex==0){
              $('#shop_cat').val(1);
              $('#shop_cat').selectpicker("refresh");
            }else{
              for(var i = 0; i < length; i++) {
                if ((selected[i]==1)|(selected[i]=="1")){

                  delete(selected[i]);

                  $('#shop_cat').val(selected);
                  $('#shop_cat').selectpicker("refresh");
                }               
            }
            }
          
          });*/

      });
  </script>
  {/literal}