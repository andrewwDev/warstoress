<h2 align=center>Пользователи</h2>



<style>{literal}
  table.usrtbl {width:100%;}
	table.usrtbl td{text-align: center;vertical-align: middle;padding: 2px;}
	table.usrtbl th{background-color:#E6E6E6; text-align: center;vertical-align: middle; padding: 2px;}
{/literal} </style>

<table border=1 class="usrtbl">
  <tr>
    <th>ID</th>
    <th>vkid</th>
    <th>Имя</th>
    <th>Email</th>
    <th>Верифицировать</th>
    <th>Изменить</th>
    <th>Удалить</th>
  </tr>
  {foreach $userslist as $value}
  <tr id="{$value.id}">
      <td>{$value.id}</td>
      <td>{$value.vk_uid}</td>
      <td>{$value.name}</td>
      <td>{$value.email}</td>
      <td class="user-verification{if ($value.isapproved == 1)} verified{/if}"></td>
      <td><a href="adm.php?cmd=useredit&uid={$value.id}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
     <td><span class="glyphicon glyphicon-remove" aria-hidden="true" data-toggle="modal" data-target="#deleteUser" data-username="{$value.name}" data-uid="{$value.id}"></span></td>
  </tr>
{/foreach}
</table>

<br><br>
        {* Низжняя листалка страниц *}
        {if $pagenum > 1}

        <ul class="pagination pull-center">
        {if $pagei>1}<li><a href="adm.php?cmd=users&pg={$pagei-1}">&laquo;</a></li>{/if}
        {for $i=1 to $pagenum}
        <li><a href="adm.php?cmd=users&pg={$i}">{if $i==$pagei}<b>{$i}</b>{else}{$i}{/if}</a></li>
        {/for}
        {if $pagei<$pagenum}<li><a href="adm.php?cmd=users&pg={$pagei+1}">&raquo;</a></li>{/if}</li>
        </ul>

        {/if}
        {* КОНЕЦ Низжняя листалка страниц *}


<div id="deleteUser" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Удаление пользователя</h4>
      </div>
      <div id="result" class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" name="delete">Удалить</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Отмена</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  var htmlVerified = '<span class="glyphicon glyphicon-ok" aria-hidden="true" name="unverify"></span>';
  var htmlUnverified = '<input type="checkbox" name="verify">';

  $(function() {
    $( 'td.user-verification' ).each(function(){
      $( this ).html( $( this ).hasClass( 'verified' ) ?  htmlVerified : htmlUnverified );
    });
  });

  $( 'td.user-verification' ).click(function(event){
    //event.stopPropagation();
    var userId = $( this ).parent().attr( 'id' );
    var setTo = $( this).hasClass( 'verified' ) ? '0' : '1';
    var thisTd = $( this );

    $.post('ajax/adm_save.php', { uid: userId, pg: 'user', cmd: 'verify', set: setTo })
      .done(function( data ) {
        thisTd.html( thisTd.hasClass( 'verified' ) ?  htmlUnverified : htmlVerified );
        thisTd.toggleClass( 'verified' );
      });

  });

  $('#deleteUser').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var username = button.data('username');
    var userId = button.data('uid');
    var modal = $(this);
    modal.find( '.modal-body' ).html('Вы уверенны, что хотите удалить пользователя <strong>' + username + '</strong>');
    modal.find( 'button[name=delete]' ).val(userId);
  })

  $( 'button[name=delete]' ).click(function() {
    var userId = $( this ).val();

  $.post('ajax/adm_save.php', { uid: userId, pg: 'user', cmd: 'delete' })
    .done(function( data ) {
      $( '#deleteUser' ).modal('hide');
      location.reload();
    });

  });
</script>
