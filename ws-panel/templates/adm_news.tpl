<h2 align=center>НОВОСТИ</h2>

<div class="row" style='margin-right:20px;'>
      <div class="col-md-12 col-xs-12 " id="toppanel" style="padding:5px;">
        <button type="button" class="btn btn-success pull-right" onClick="window.location.href='adm.php?cmd=editnews'"><img src="http://warstores.net/ws-panel/images/ws_add.png" alt="Создать новость"></button>       
      </div>
  </div>

<style>{literal} 
	table.usrtbl td{text-align: center;vertical-align: middle;padding: 2px;}
	table.usrtbl th{background-color:#E6E6E6; text-align: center;vertical-align: middle; padding: 2px;}
{/literal} </style>

<table border=1 width=100% class="usrtbl">
<tr>
	<th>ID</th>
	<th>Магазин</th>
	<th>Заголовок</th>
	<th>Дата</th>
	<th>Краткое описание</th>
	<th colspan=2>Управление</th>
</tr>
{foreach $newslist as $value}
<tr id="userrow{$value.uid}">
	<td>{$value.nid}</td>
	<td>{$value.shop}</td>
	<td>{$value.title}</td>
	<td>{$value.ndate}</td>
	<td>{$value.description}</td>
	<td><span class="btn btn-primary" onClick="window.location.href='adm.php?cmd=editnews&pg={$value.nid}'"><img src="http://warstores.net/ws-panel/images/ws_edit.png" alt="Редактировать новость"></span></td>
	<td><span class="btn btn-danger" onClick="userdelete({$value.nid})"><img src="http://warstores.net/ws-panel/images/ws_del.png" alt="Создать новость"></span></td>	
</tr>
{/foreach}
</table>

<br><br>
        {* Низжняя листалка страниц *}  
        {if $pagenum > 1}
        <center>
        <ul class="pagination pull-center">
        {if $pagei>1}<li><a href="adm.php?cmd=news&pg={$pagei-1}">&laquo;</a></li>{/if}
        {for $i=1 to $pagenum}
        <li><a href="adm.php?cmd=news&pg={$i}">{if $i==$pagei}<b>{$i}</b>{else}{$i}{/if}</a></li>
        {/for}
        {if $pagei<$pagenum}<li><a href="adm.php?cmd=news&pg={$pagei+1}">&raquo;</a></li>{/if}</li>
        </ul>
        </center>
        {/if}
        {* КОНЕЦ Низжняя листалка страниц *}  



{*Диалоговые окна*}
<div id="boxes">
  <div id="dialog" class="window">    
    <h1 id="dtitle" style="background-color:#54769a; padding:0px; color:#fff; font-size:11px; padding:10px; margin: -2px;">Информация о пользователе<h1>
    <p id="dmsg">Пользователь с таким именем уже зарегистрирован</p>
    <div id="closediv" style="position:absolute; bottom:10px; right:10px;">
      <span class="btn btn-primary" href="#" role="button" id="closedlg" onClick="$('#mask, .window').hide();">Закрыть</span>
    </div>
    </div>
  <div id="mask" onClick="$('#mask, .window').hide();"></div>
</div>
{*Конец окон*}

<!-- Скрипт для инициализации элементов на странице, имеющих атрибут data-toggle="tooltip" -->
<script>
// после загрузки страницы
$(function () {
  // инициализировать все элементы на страницы, имеющих атрибут data-toggle="tooltip", как компоненты tooltip
  $('[data-toggle="tooltip"]').tooltip()
})
{literal}   
function showmydlg(str_title, str_msg) {
    $("#dtitle").text(str_title);    
    $("#dmsg").text(str_msg);
    var id = "#dialog";
    var maskHeight = $(document).height();
    var maskWidth = $(window).width();
    $('#mask').css({'width':maskWidth,'height':maskHeight});
    $('#mask').fadeIn(200);
    $('#mask').fadeTo("slow",0.6);
    var winH = $(window).height();
    var winW = $(window).width();
    $("#dialog").css('top',  winH/2-$(id).height()/2+$(window).scrollTop());
    $("#dialog").css('left', winW/2-$(id).width()/2);

    $("#closedlg").css('top',  winH/2-$(id).height()/2+$(window).scrollTop());
    $("#closedlg").css('left', winW/2-$(id).width()/2);
    $("#dialog").fadeIn(200);
    return false;
  }
  {/literal}   
</script>