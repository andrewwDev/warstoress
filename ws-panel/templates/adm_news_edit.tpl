<script>
var arr_cityname= new Array();
var arr_cityid= new Array();
var files;  {* картинки для загрузки на сервер *}
</script>

 {literal}
<script type="text/javascript">
  function figupload(nid){
    
    var data = new FormData();
    $.each( files, function( key, value ){
      data.append( key, value );
      console.log(key);
      console.log(value);
      });
    data.append( 'nid',  (0+nid));
    // Отправляем запрос
    console.log(data);


    $.ajax({
        url: 'http://warstores.net/ws-panel/ajax/enj_submitfig.php?nid='+nid+'&uploadfiles',
        type: 'POST',
        data: data,
        cache: false,
        crossDomain: true,
        dataType: 'json',
        processData: false, // Не обрабатываем файлы (Don't process the files)
        contentType: false, // Так jQuery скажет серверу что это строковой запрос
        success: function( respond, textStatus, jqXHR ){
            if( typeof respond.error === 'undefined' ){
                // Файлы успешно загружены, делаем что нибудь здесь
                console.log("Изображение удачно отправлено на сервер");
                // выведем пути к загруженным файлам в блок '.ajax-respond'
                var files_path = respond.files;
                var html = '';
                //$.each( files_path, function( key, val ){ html += val +'<br>'; } )
                var src = "http://warstores.net/ws_images/news/img"+nid+".jpg?r="+Math.random();//генерируем уникальный адрес картинки
                $("#imgsrc").removeAttr('src').attr('src', src);
                //$('.ajax-respond').html( html );
            }
            else{
                console.log('ОШИБКИ ОТВЕТА сервера: ' + respond.error );
            }
        },
        error: function( jqXHR, textStatus, errorThrown ){
            console.log('ОШИБКИ AJAX запроса: ' + textStatus );
        }
    });

    return false;
  }
</script>
<style>
input.imgloader button{background-color: #444;}
</style>
{/literal}

<div style="padding-left:15px; padding-right:15px;">
<h1>РЕДАКТИРОВАНИЕ НОВОСТИ</h1>

  <div class="row" style="padding:20px;">
<div class="row">
      <div class="col-md-12 col-xs-12 " id="toppanel" style="padding:5px;">
        <button type="button" class="btn btn-success pull-right" onClick="savenews()">Сохранить</button>
        <button type="button" class="btn btn-default pull-right" onClick="window.location.href='adm.php?cmd=news'">Закрыть</button>
      </div>
  </div>
<div class="row">
  <div class="col-md-12 col-xs-12" id="toppanel">
    <hr>

    <form class="form-horizontal">
      <input type="hidden" id="news_nid" value="{$nid}">
      <div class="form-group">
        <label for="pg_title" class="col-xs-2 control-label">Заголовок новости:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="news_title" placeholder="Введите заголовок новости" value="{$row.title}">
        </div>
      </div>
      <div class="form-group">
        <label for="pg_alias" class="col-xs-2 control-label">Дата:</label>
        <div class="col-xs-10">

          <div class='input-group date' id='datetimepicker1'>
              <input type='text' class="form-control" id="news_date" value="{$row.nfdate}"/>
              <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
              </span>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="pg_keywords" class="col-xs-2 control-label">Картинка для новости:</label>
        <div class="col-xs-10">
          <table><tr><td><input id="myfiles" class="imgloader" type="file" accept="image/jpeg,image/png"> <br></td>
            <td rowspan=2>
              <div><a id="imglink" href="http://warstores.net/ws_images/news/img{$nid}.jpg" target="_blank"><img id="imgsrc" src="http://warstores.net/ws_images/news/img{$nid}.jpg" style="height:80px;"></a></div>
            </td></tr>
        </table>
        </div>
        
      </div>
      <div class="form-group">
        <label for="pg_description" class="col-xs-2 control-label">Выберите регион:</label>
        <div class="col-xs-10">         
        
        <select class="form-control" id="pg_reg_id" placeholder="Введите регион" OnChange="ReloadCity()">
            <option {if $row.reg_id eq 0}selected{/if} value=0>Не важно</option>
            {foreach $regionlist as $value}
            <option {if $value.geo_region_id == $row.reg_id}selected{/if} value="{$value.geo_region_id}">{$value.name}</option>
            {/foreach}
        </select> 
        </div>
      </div>
      <div class="form-group">
        <label for="pg_description" class="col-xs-2 control-label">Выберите город:</label>
        <div class="col-xs-10">          
          <select class="form-control" id="pg_city_id" placeholder="Введите город">
            <option {if $row.city_id eq 0}selected{/if} value=0>Не важно</option>
            {foreach $citylist as $value}
            <option {if $value.geo_city_id == $row.city_id}selected{/if} value="{$value.geo_city_id}">{$value.name}</option>
            {/foreach}
        </select> 
        </div>
      </div>
      <div class="form-group">
        <label for="pg_description" class="col-xs-2 control-label">Краткое описание (255 символов):</label>
        <div class="col-xs-10">          
          <input type="text" class="form-control" id="news_description" placeholder="Введите описание" value="{$row.description}">        
        </div>
      </div>
      
      <div class="form-group">
        <label for="pg_source" class="col-xs-2 control-label">Текст страницы:</label>
        <div class="col-xs-offset-2 col-xs-10">
        {********************}
        {*** Это редактор ***}          
          <textarea class="mceTA" id="news_extendedtext" placeholder="Идет загрузка редактора...">{$row.extendedtext}</textarea>   
          <script type='text/javascript' src='js/tinymce.min.js'></script>                 
          <script type="text/javascript">{literal} 
            tinymce.init({
              mode : "specific_textareas",
              editor_selector : "mceTA",
              height: 250,
              language:"ru", 
              theme: 'modern',
              plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
              ],
              toolbar1: 'insertfile undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
              image_advtab: true
             });
            {/literal} 
          </script>

        {*** Конец редактора ***}
        {***********************}

        </div>
      </div>      
    </form>

  </div>
</div> 
</div>

</div>



 {literal}
  <script type="text/javascript">
      $(function () {
          $('#datetimepicker1').datetimepicker( {pickTime: true, language: 'ru'});

          $('input[type=file]').change(function(){files = this.files;});
      });
  </script>
  {/literal}