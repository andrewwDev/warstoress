<h2 align=center>Статистика</h2>



<style>{literal}
  table.usrtbl {width:100%;}
	table.usrtbl td{text-align: center;vertical-align: middle;padding: 2px;}
	table.usrtbl th{background-color:#E6E6E6; text-align: center;vertical-align: middle; padding: 2px;}
{/literal} </style>


<table border=1 class="usrtbl">
<div id="content-container">
                
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Статистика компании</h1>

                    <!--Searchbox-->
					<!--Searchbox-->
					<div class="searchbox" style="text-align: right;">
                        {if $moderator_flag == 0}
							<span style="background-color: blue; padding: 4px; color: #fff;">Компания на модерации</span>
                        {/if}
                        {if $moderator_flag == 1}
							<span style="background-color: green; padding: 4px; color: #fff;">Компания активна</span>
                        {/if}
                        {if $moderator_flag == 2}
							<span style="background-color: red; padding: 4px; color: #fff;">Компания не прошла модерацию</span>
                        {/if}
                        {if $moderator_flag == 3}
							<span style="background-color: black; padding: 4px; color: #fff;">Компания в архиве</span>
                        {/if}
					</div>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    
					<div class="row">
					    <div class="col-lg-7">
					
					        <!--Network Line Chart-->
					        <!--===================================================-->
					        <div id="demo-panel-network" class="panel">
					            <div class="panel-heading">
					                {*<div class="panel-control">*}
					                    {*<button id="demo-panel-network-refresh" data-toggle="panel-overlay" data-target="#demo-panel-network" class="btn"><i class="demo-pli-repeat-2 icon-lg"></i></button>*}
					                    {*<div class="btn-group">*}
					                        {*<button class="dropdown-toggle btn" data-toggle="dropdown" aria-expanded="false"><i class="demo-pli-gear icon-lg"></i></button>*}
					                        {*<ul class="dropdown-menu dropdown-menu-right">*}
					                            {*<li><a href="#">Сегодня</a></li>*}
					                            {*<li><a href="#">Неделя</a></li>*}
					                            {*<li><a href="#">Месяц</a></li>*}
					                            {*<li class="divider"></li>*}
					                            {*<li><a href="#">За всё время</a></li>*}
					                        {*</ul>*}
					                    {*</div>*}
					                {*</div>*}
					                <h3 class="panel-title">График привлечения клиентов и показатели</h3>
					            </div>
					
					            <!--Morris line chart placeholder-->
					            <script type="text/javascript">
					            	var shops_data = [];
					            	{foreach $shops_data as $key => $value}
					            		shops_data.push({
				            				"id": "{$value.id}",
											"day": "{$value.day}",
											"clients": "{$value.clients}",
											"newclients": "{$value.newclients}",
											"spend_total": "{$value.spend_total}",
											"spend_money": "{$value.spend_money}",
											"received": "{$value.received}",
											"responses": "{$value.responses}"
										});
					            	{/foreach}
					            </script>
					            <div id="morris-chart-network" class="morris-full-content" 
						            style="height: 170px;">
						            <div style="display: table; height: 100%; width: 100%;">
						            	<span style="display: table-cell; vertical-align: middle; text-align: center;">Слишком мало данных</span>
						            </div>
					            </div>
					
					            <!--Chart information-->
					            <div class="panel-body">
					                <div class="row pad-top">
					                    <div class="col-lg-12">
					                        <div class="media">
											<small> <p class="text-semibold text-main">Показатели</p></small>
					                            <div class="media-body pad-lft">
					                                <div class="media">
				                                        <div class="media-left pad-no">
					                                       <small><span class="text-2x text-semibold text-nowrap text-main">{$count_sales}</span></small>
					                                    </div>
					                                    <div class="media-body">
					                                        <small><p class="mar-no">Рублей от клиентов </p></small>
					                                    </div>
					                                    <div class="media-left pad-no">
					                                       <small><span class="text-2x text-semibold text-nowrap text-main">
					                                             {$count_bonus}
					                                        </span></small>
					                                    </div>
					                                    <div class="media-body">
					                                      <small><p class="mar-no">Бонусов от клиентов</p></small>
					                                    </div>
                                                        <div class="media-left pad-no">
					                                      <small><span class="text-2x text-semibold text-nowrap text-main">{$count_cashback}</span></small>
					                                    </div>
                                                        <div class="media-body">
					                                       <small><p class="mar-no">CashBack бонусов</p></small>
					                                    </div>
                                                        {*<div class="media-left pad-no">*}
					                                         {*<small><span class="text-2x text-semibold text-nowrap text-main">452</span></small>*}
					                                    {*</div>*}
                                                        {*<div class="media-body">*}
					                                      {*<small>  <p class="mar-no">Затраты Push</p> </small></p>*}
					                                    {*</div>*}
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					
					                    {*<div class="col-lg-4">*}
					                         {*<small><p class="text-semibold text-main">Итоговая прибыль</p> </small>    *}
					                        {*<ul class="list-unstyled">*}
					                            {*<li>*}
					                                {*<div class="media">*}
					                                    {*<div class="media-left pad-no">*}
					                                       {*<small>  <span class="text-2x text-semibold text-main">6</span> </small>    *}
					                                    {*</div>*}
					                                    {*<div class="media-body">*}
					                                       {*<small>  <p class="mar-no">Рублей</p> </small>    *}
					                                    {*</div>*}
                                                         {*<div class="media-left pad-no">*}
					                                       {*<small>  <span class="text-2x text-semibold text-main">0</span> </small>    *}
					                                    {*</div>*}
					                                    {*<div class="media-body">*}
					                                       {*<small>  <p class="mar-no">Бонусов</p> </small>    *}
					                                    {*</div>*}
					                                {*</div>*}
                                                    {**}
					                            {*</li>*}
					                            {**}
					                        {*</ul>*}
					                    {*</div>*}
					                </div>
					            </div>
					
					
					        </div>
					        <!--===================================================-->
					        <!--End network line chart-->
					
					    </div>
					    <div class="col-lg-5">
					        <div class="row">
					            <div class="col-sm-6 col-lg-6">
					            <script type="text/javascript">
					            	var movement_funds = [];
					            	{foreach $movement_funds as $key => $value}
					            		movement_funds.push({{$value.summ}});
					            	{/foreach}
					            </script>
					
					                <!--Sparkline Area Chart-->
					                <div class="panel panel-success panel-colorful">
					                    <div class="pad-all">
					                        <p class="text-lg text-semibold"><i class="demo-pli-data-storage icon-fw"></i>Движение бонусов</p>
					                        <p class="mar-no">
					                            <span class="pull-right text-bold">{$move_bonus_down}</span>
					                            Потраченные
					                        </p>
					                        <p class="mar-no">
					                            <span class="pull-right text-bold">{$move_bonus_up}</span>
					                            Полученные
					                        </p>
					                    </div>
					                    <div class="pad-all text-center">
					                        <!--Placeholder-->
					                        <div id="demo-sparkline-area"></div>
					                    </div>
					                </div>
					            </div>
					            <div class="col-sm-6 col-lg-6">
					
					                <!--Sparkline Line Chart-->
					                <div class="panel panel-info panel-colorful">
					                    <div class="pad-all">
					                        <p class="text-lg text-semibold"><i class="demo-pli-wallet-2 icon-fw"></i>Прибыль</p>
					                        <p class="mar-no">
					                            <span class="pull-right text-bold">{$count_sales}</span>
					                            От компании
					                        </p>
					                        <p class="mar-no">
					                            <span class="pull-right text-bold">{$profit_from_team}</span>
					                            От команды
					                        </p>
					                    </div>
					                    {*<div class="pad-all text-center">*}
					{**}
					                        {*<!--Placeholder-->*}
					                        {*<div id="demo-sparkline-line"></div>*}
					{**}
					                    {*</div>*}
					                </div>
					            </div>
					        </div>
					        <div class="row">
					            <div class="col-sm-6 col-lg-6">
					            <script type="text/javascript">
					            	var sales = [];
					            	{foreach $all_sales as $key => $value}
					            		sales.push({{$value.summ}});
					            	{/foreach}
					            </script>
					
					                <!--Sparkline bar chart -->
					                <div class="panel panel-purple panel-colorful">
					                    <div class="pad-all">
					                        <p class="text-lg text-semibold"><i class="demo-pli-bag-coins icon-fw"></i>Продажи</p>
					                        <p class="mar-no">
					                            <span class="pull-right text-bold">{$num_sales}</span>
					                            Кол-во продаж
					                        </p>
					                    </div>
					                    <div class="pad-all text-center">
					
					                        <!--Placeholder-->
					                        <div id="demo-sparkline-bar" class="box-inline"></div>
					
					                    </div>
					                </div>
					            </div>
					            <div class="col-sm-6 col-lg-6">
					
					                <!--Sparkline pie chart -->
					                <div class="panel panel-warning panel-colorful">
					                    <div class="pad-all">
					                        <p class="text-lg text-semibold"><i class="demo-pli-check icon-fw"></i>Баланс</p>
					                        <p class="mar-no">
					                            <span class="pull-right text-bold">{$move_bonus_down}</span>
					                            Всего потрачено
					                        </p>
					                        <p class="mar-no">
					                            <span class="pull-right text-bold">{$move_bonus_up}</span>
					                           Всего получено
					                        </p>
                                             <p class="mar-no">
					                            <span class="pull-right text-bold">{$move_bonus_up - $move_bonus_down}</span>
					                           Остаток
					                        </p>
					                    </div>
					                    <div class="pad-all">
					                        {*<ul class="list-group list-unstyled">*}
					                            {*<li class="mar-btm">*}
					                                {*<span class="label label-warning pull-right">15%</span>*}
					                                {*<p>Progress</p>*}
					                                {*<div class="progress progress-md">*}
					                                    {*<div style="width: 15%;" class="progress-bar progress-bar-light">*}
					                                        {*<span class="sr-only">15%</span>*}
					                                    {*</div>*}
					                                {*</div>*}
					                            {*</li>*}
					                        {*</ul>*}
					                    </div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
					
					<div class="row">
					   
					    
					    </div>
					    
					    <div class="row">
					    <div class="col-xs-12">
					        <div class="panel">
					            <div class="panel-heading">
					                <h3 class="panel-title">Детализированный список</h3>
					            </div>
					
					            <!--Data Table-->
					            <!--===================================================-->


					            <div class="panel-body">
									{if $sales}
					                <div class="pad-btm form-inline">
					                    <div class="row">
					                        <div class="col-sm-6 table-toolbar-left">
					                            <!--<button class="btn btn-purple"><i class="demo-pli-add icon-fw"></i>Add</button>-->
					                            <button class="btn btn-default"><i class="demo-pli-printer"></i></button>
					                            <!--<div class="btn-group">
					                                <button class="btn btn-default"><i class="demo-pli-information"></i></button>
					                                <button class="btn btn-default"><i class="demo-pli-recycling"></i></button>
					                            </div>-->
					                        </div>
					                    </div>
					                </div>
					                <div class="table-responsive">
										<table class="table table-striped">
											<thead>
											<tr>
												<th>Дата продажи</th>
												<th>Покупатель</th>
												<th>Сотрудник</th>
												<th>Cashback</th>
												<th class="text-center">Получено рублей</th>
												<th class="text-center">Получено бонусов</th>
												<th class="text-center">Отзыв</th>
												<th class="text-center">Возврат</th>
											</tr>
											</thead>
											<tbody>
											{foreach $sales as $key => $value}
												<tr>
													<td><span class="text-muted"><i class="fa fa-clock-o"></i> {$value["date"]}</span></td>
													<td>{$user_arr[$value["user_id"]]}</td>
													<td>{$manager_arr[$value["manager_id"]]}</td>
													<td>{$value["cashback"]}</td>
													<td class="text-center">
														{$value["cash"]}
													</td>
													<td class="text-center">{$value["summ"] - $value["cash"]*100}</td>
													<td class="text-center">-</td>
													<td class="text-center">-</td>
												</tr>
											{/foreach}
											</tbody>
										</table>
					                </div>
					                <hr>
									{if $num_pages > 0 }
										<div class="pull-right">
											<ul class="pagination text-nowrap mar-no">
												{for $page = 1 to $num_pages}
													<li class="page-number {if $page==$active_page}active{/if}">
														<a href="./overallstatistics.php?sid=2&page={$page}">{$page}</a>
													</li>
												{/for}
					                    	</ul>
					                	</div>
									{/if}
									{else}
									<div class="row">
										<div class="col-md-12">
											<p>Операций по данной компании нет</p>
										</div>
									</div>
									{/if}
					            </div>
					            <!--===================================================-->
					            <!--End Data Table-->
					
					        </div>
					    </div>
					</div>

					</div>
					
					
					
					
					
                </div>
</table>

<br><br>
        {* Низжняя листалка страниц *}  
        {if $pagenum > 1}
        <center>
        <ul class="pagination pull-center">
        {if $pagei>1}<li><a href="adm.php?cmd=news&pg={$pagei-1}">&laquo;</a></li>{/if}
        {for $i=1 to $pagenum}
        <li><a href="adm.php?cmd=news&pg={$i}">{if $i==$pagei}<b>{$i}</b>{else}{$i}{/if}</a></li>
        {/for}
        {if $pagei<$pagenum}<li><a href="adm.php?cmd=news&pg={$pagei+1}">&raquo;</a></li>{/if}</li>
        </ul>
        </center>
        {/if}
        {* КОНЕЦ Низжняя листалка страниц *}  



{*Диалоговые окна*}
<div id="boxes">
  <div id="dialog" class="window">    
    <h1 id="dtitle" style="background-color:#54769a; padding:10px; color:#fff; font-size:11px; margin: -2px;">Информация о пользователе</h1>
    <p id="dmsg">Пользователь с таким именем уже зарегистрирован</p>
    <div id="closediv" style="position:absolute; bottom:10px; right:10px;">
      <span class="btn btn-primary" href="#" role="button" id="closedlg" onClick="$('#mask, .window').hide();">Закрыть</span>
    </div>
    </div>
  

  <div id="dialog2" class="window">    
    <h1 id="dtitle2" style="background-color:#54769a; padding:10px; color:#fff; font-size:11px; margin: -2px;">Пополнить баланс магазина</h1>
      <br><br>
      <div class="form-group">
        <input type="text" class="form-control" id="cashsumm" placeholder="Зачислить руб на счет" value="">
      </div>

    <div id="closediv2" style="position:absolute; bottom:10px; right:10px;">
      <span class="btn btn-primary" href="#" role="button" id="adddlg2" onClick="addmoney();">Зачислить</span>
      <span class="btn btn-primary" href="#" role="button" id="closedlg2" onClick="$('#mask, .window').hide();">Закрыть</span>
    </div>
    </div>

    <div id="mask" onClick="$('#mask, .window').hide();"></div>
</div>
{*Конец окон*}

<!-- Скрипт для инициализации элементов на странице, имеющих атрибут data-toggle="tooltip" -->
<script>
var cursid=0;
// после загрузки страницы
$(function () {
  // инициализировать все элементы на страницы, имеющих атрибут data-toggle="tooltip", как компоненты tooltip
  $('[data-toggle="tooltip"]').tooltip()
})
{literal}   
function showmydlg(str_title, str_msg) {
    $("#dtitle").text(str_title);    
    $("#dmsg").text(str_msg);
    var id = "#dialog";
    var maskHeight = $(document).height();
    var maskWidth = $(window).width();
    $('#mask').css({'width':maskWidth,'height':maskHeight});
    $('#mask').fadeIn(200);
    $('#mask').fadeTo("slow",0.6);
    var winH = $(window).height();
    var winW = $(window).width();
    $("#dialog").css('top',  winH/2-$(id).height()/2+$(window).scrollTop());
    $("#dialog").css('left', winW/2-$(id).width()/2);

    $("#closedlg").css('top',  winH/2-$(id).height()/2+$(window).scrollTop());
    $("#closedlg").css('left', winW/2-$(id).width()/2);
    $("#dialog").fadeIn(200);
    return false;
  }

  function showmydlg2(sid) {
    cursid=sid;
    var id = "#dialog2";
    var maskHeight = $(document).height();
    var maskWidth = $(window).width();
    $('#mask2').css({'width':maskWidth,'height':maskHeight});
    $('#mask2').fadeIn(200);
    $('#mask2').fadeTo("slow",0.6);
    var winH = $(window).height();
    var winW = $(window).width();
    $("#dialog2").css('top',  winH/2-$(id).height()/2+$(window).scrollTop());
    $("#dialog2").css('left', winW/2-$(id).width()/2);

    $("#closedlg2").css('top',  winH/2-$(id).height()/2+$(window).scrollTop());
    $("#closedlg2").css('left', winW/2-$(id).width()/2);
    $("#dialog2").fadeIn(200);
    return false;
  }

  function addmoney(){    
    var cash=$('#cashsumm').val();
    $('#cashsumm').val("");
    $('#mask, .window').hide();
    var data = new FormData();
    data.append( 'sid',  (0+cursid));
    data.append( 'cash',  Math.abs(cash*100));    
    $.ajax({
        url: 'http://warstores.net/ws-panel/ajax/adm_shop_balance.php',
        type: 'POST',
        data: data,
        cache: false,
        crossDomain: true,
        dataType: 'json',
        processData: false, // Не обрабатываем файлы (Don't process the files)
        contentType: false, // Так jQuery скажет серверу что это строковой запрос
        success: function( respond, textStatus, jqXHR ){
          console.log("Баланс пополнен");
        },
        error: function( jqXHR, textStatus, errorThrown ){
            console.log('ОШИБКИ AJAX запроса: ' + textStatus );
        }
    });

    //showmydlg("Зачислить на счет магазина "+cursid, "Надо зачислить "+(cash*100)+" коп.");
    cursid=0;
  }
  {/literal}   
</script>