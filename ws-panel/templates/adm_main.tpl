﻿<!DOCTYPE html>
<html lang="ru">
  <head>
    <title>Панель управления</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="{$description}" />
    <meta name="keywords" content="{$keywords}" />
    {if isset($noindexflag)}
    {if $noindexflag eq '1'}
    <meta name="ROBOTS" content="noindex,nofollow" />
    {/if}
    {/if}

    <meta name="ROBOTS" content="noindex,nofollow" /> <!--TODO!!!!-->

    <script type='text/javascript' src='js/jquery-2.1.4.min.js'></script>
    <script type='text/javascript' src='js/adm.js'></script>
    {*<script type='text/javascript' src='js/jquery-ui.js'></script>*}
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script type="text/javascript" src="js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-select.min.js"></script>

    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="css/bootstrap-select.min.css" />
    <link href="images/setting.ico" rel="shortcut icon" type="image/png" />
    <link href="css/jquery-ui.css" rel="css/stylesheet" media="screen"></link>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
    <link href="css/golos.css" rel="stylesheet" media="screen">
  </head>
  <body>

    <style>
    .mymenu{
      margin: 5px;
      width: 140px;
    }
    </style>
{if $isadmin eq '1'}
<table><tr><td style="vertical-align:top; width:150px;">
<!--Меню -->
<img src="http://warstores.net/ws-panel/images/ws_site_logo.png" style="width:150px; mergin-bottom:10px;">
<button type="button" class="btn btn-panel mymenu" onclick="window.location.href='http://warstores.net/ws-panel/adm.php?cmd=static';">Статистика</button>
<button type="button" class="btn btn-panel mymenu" onclick="window.location.href='http://warstores.net/ws-panel/adm.php?cmd=news';">Новости</button>
<button type="button" class="btn btn-panel mymenu" onclick="window.location.href='http://warstores.net/ws-panel/adm.php?cmd=shops';">Магазины</button>
<button type="button" class="btn btn-panel mymenu" onclick="window.location.href='http://warstores.net/ws-panel/adm.php?cmd=users';">Пользователи</button>
<button type="button" class="btn btn-panel mymenu" onclick="window.location.href='http://warstores.net/ws-panel/adm.php?logout';">Выход</button>
<!--КОНЕЦ Меню -->
</td><td style="vertical-align:top; width:100%">
<!-- Основной блок -->
<div style="padding:5px; background-color:#FFF; border:1px solid #E6E6E6;" id="maincontent">

            {if $cmd eq 'pg'}{include file="adm_$page.tpl"} {/if}

</div>
<!-- КОНЕЦ Основной блок -->
</td></tr>
</table>
{else}
  {include file="adm_login.tpl"}
{/if}

<input type="hidden" id="sorttype" value="1">
<input type="hidden" id="lotlistmode" value="0">
<input type="hidden" id="userlistmode" value="0">



<div id="myModalBox" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Идет отправка данных на сервер</h4>
      </div>
      <!-- Основной текст сообщения -->
      <div class="modal-body" style="width:100%">
        <center><img src="http://breakbox.ru/images/loading.gif"><br><br><b>Сохранение...</b></center>
      </div>
    </div>
  </div>
</div>



<script src="js/bootstrap.min.js"></script>
<script>
  $(document).ready(function(){

  });
</script>
</body>
</html>