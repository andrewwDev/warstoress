﻿
	<div id="container" class="cls-container login-page">

		<!-- LOGIN FORM -->
		<div class="cls-content">
		    <div class="cls-content-sm panel" style="width:20%; margin-left:40%; margin-right:40%; text-align:center;">
		        <div class="panel-body">
		            <div class="mar-ver pad-btm">
		                <h3 class="h4 mar-no">Добро пожаловать в панель управления.</h3>
		                <p class="text-muted">Войдите в свою учетную запись</p>
		            </div>
		            <form class="form-horizontal" action="adm.php" method="post" id="my_form_auth" enctype="multipart/form-data">
                    <input type="hidden" name="pass" id="pass" value="" style="border-radius: 0;">
		                <div class="form-group">
		                    <input type="text" name="user" class="form-control" placeholder="Логин" autofocus id="user" value="">
		                </div>
		                <div class="form-group">
		                    <input type="password" name="pass" class="form-control" placeholder="Пароль" id="pwd" value="">
		                </div>
		                <input type="submit" id="submit" name="login" value="Войти" class="btn btn-success btn-lg">
		            </form>
		        </div>
		    </div>
		</div>

	</div>
<script>
	$( 'button[type=submit]' ).click(function(event) {
		event.preventDefault();

		$.post('backend/auth.php', $( '#loginForm' ).serialize())
		    .done(function( data ) {
		    	console.log(data);
				if (data == 'yes')
					location.reload();
				else alert(data);
		    });

  	});
</script>