<h2>РЕДАКТИРОВАНИЕ ПОЛЬЗОВАТЕЛЯ</h2>

<div class="row">
  <div class="col-md-12 col-xs-12">
    <form name="userForm">
      <input type="hidden" name="uid" value="{$smarty.get.uid}">
      <div class="form-group">
        <label class="col-xs-2 control-label">Имя:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" name="name" placeholder="Имя" value="{$user.name}">
        </div>
      </div>

      <div class="form-group">
        <label class="col-xs-2 control-label">Фамилия:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" name="surname" placeholder="Фамилия" value="{$user.surname}">
        </div>
      </div>

       <div class="form-group">
        <label class="col-xs-2 control-label">Логин:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" name="login" placeholder="Логин" value="{$user.login}">
        </div>
      </div>

       <div class="form-group">
        <label class="col-xs-2 control-label">Email:</label>
        <div class="col-xs-10">
          <input type="email" class="form-control" name="email" placeholder="Email" value="{$user.email}">
        </div>
      </div>

      <div class="form-group">
        <label class="col-xs-2 control-label">Дата рождения:</label>
        <div class="col-xs-10">
          <input type="date" class="form-control" name="birthday" placeholder="Дата рождения" value="{$user.birthday}">
        </div>
      </div>

      <div class="form-group">
        <label class="col-xs-2 control-label">Статус:</label>
        <div class="col-xs-10">
           {html_options name=status options=$status selected=$user.status}
        </div>
      </div>

      <button type="button" class="btn btn-success pull-right" name="save">Сохранить</button>
      <button type="button" class="btn btn-default pull-right" onClick="window.location.href='adm.php?cmd=users'">Вернуться к списку пользователей</button>
    </form>
  </div>
</div>

<div id="succes" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div id="result" class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Продолжить редактирование</button>
        <button type="button" class="btn btn-primary" onClick="window.location.href='adm.php?cmd=users'">Вернуться к списку пользователей</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  $('button[name=save]').click( function() {
    var postData = $.param({ pg: "user", cmd: "edit" }) + '&' +  $('form[name=userForm]').serialize();
    //console.log(postData);
    $.post( 'ajax/adm_save.php', postData , function(data) {
      $('#result').html(data);
      $('#succes').modal('show');        }
    );
  });
</script>






