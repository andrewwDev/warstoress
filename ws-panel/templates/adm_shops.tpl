<h2 align="center">МАГАЗИНЫ</h2>

<div class="row" style='margin-right:20px;'>
      <div class="col-md-12 col-xs-12 " id="toppanel" style="padding:5px;">
        <button type="button" class="btn btn-success pull-right" onClick="window.location.href='adm.php?cmd=editnews'"><img src="http://warstores.net/ws-panel/images/ws_add.png" alt="Создать новость"></button>       
      </div>
  </div>

<style>{literal} 
  table.usrtbl {width:100%;}
	table.usrtbl td{text-align: center;vertical-align: middle;padding: 2px;}
	table.usrtbl th{background-color:#E6E6E6; text-align: center;vertical-align: middle; padding: 2px;}
{/literal} </style>

<table border=1 class="usrtbl">
<tr>
	<th>ID</th>
  <th>sid</th>
	<th style="width:40%">Магазин</th>
	<th>Владелец</th>
	<th>Shopnick</th>
  <th>Пополнить</th>
  <th>Статус</th>
  <th>Баланс</th>
	<th>Управление</th>
</tr>
{foreach $shoplist as $value}
<tr id="shop{$value.id}">
	<td>{$value.id}</td>
  <td>sid</td>
	<td>{$value.title}</td>
	<td>{$value.ownerid}</td>
	<td>{$value.shopnick}</td>
  <th><span OnClick="showmydlg2({$value.id})">Пополнить счет</span></th>
	{*<td>{if ($value.published==1)}<img src="http://warstores.net/ws-panel/images/ic_cb_on.png">{else}<img src="http://warstores.net/ws-panel/images/ic_cb_off.png">{/if}</td>*}
    <td>
        {if ($value.moderator_flag==0)}
            <span style="background-color: blue; padding: 4px; color: #fff;">На модерации</span>
        {elseif ($value.moderator_flag==1)}
            <span style="background-color: green; padding: 4px; color: #fff;">Одобрен</span>
        {elseif ($value.moderator_flag==2)}
            <span style="background-color: red; padding: 4px; color: #fff;">Отклонен</span>
        {elseif ($value.moderator_flag==3)}
            <span style="background-color: brown; padding: 4px; color: #fff;">Архив</span>
        {elseif ($value.moderator_flag==4)}
            <span style="background-color: black; padding: 4px; color: #fff;">Заморожен</span>
        {/if}
    </td>
    <td>

    </td>
	<td><span class="btn btn-primary" onClick="window.location.href='adm.php?cmd=editshop&pg={$value.id}'"><img src="http://warstores.net/ws-panel/images/ws_edit.png" alt="Редактировать магазин"></span></td>
	<td>
    {*<span class="btn btn-danger" onClick="userdelete({$value.id})"><img src="http://warstores.net/ws-panel/images/ws_del.png" alt=""></span>*}
  </td>	
</tr>
{/foreach}
</table>

<br><br>
        {* Низжняя листалка страниц *}  
        {if $pagenum > 1}
        <center>
        <ul class="pagination pull-center">
        {if $pagei>1}<li><a href="adm.php?cmd=news&pg={$pagei-1}">&laquo;</a></li>{/if}
        {for $i=1 to $pagenum}
        <li><a href="adm.php?cmd=news&pg={$i}">{if $i==$pagei}<b>{$i}</b>{else}{$i}{/if}</a></li>
        {/for}
        {if $pagei<$pagenum}<li><a href="adm.php?cmd=news&pg={$pagei+1}">&raquo;</a></li>{/if}</li>
        </ul>
        </center>
        {/if}
        {* КОНЕЦ Низжняя листалка страниц *}  



{*Диалоговые окна*}
<div id="boxes">
  <div id="dialog" class="window">    
    <h1 id="dtitle" style="background-color:#54769a; padding:10px; color:#fff; font-size:11px; margin: -2px;">Информация о пользователе</h1>
    <p id="dmsg">Пользователь с таким именем уже зарегистрирован</p>
    <div id="closediv" style="position:absolute; bottom:10px; right:10px;">
      <span class="btn btn-primary" href="#" role="button" id="closedlg" onClick="$('#mask, .window').hide();">Закрыть</span>
    </div>
    </div>
  

  <div id="dialog2" class="window">    
    <h1 id="dtitle2" style="background-color:#54769a; padding:10px; color:#fff; font-size:11px; margin: -2px;">Пополнить баланс магазина</h1>
      <br><br>
      <div class="form-group">
        <input type="text" class="form-control" id="cashsumm" placeholder="Зачислить руб на счет" value="">
      </div>

    <div id="closediv2" style="position:absolute; bottom:10px; right:10px;">
      <span class="btn btn-primary" href="#" role="button" id="adddlg2" onClick="addmoney();">Зачислить</span>
      <span class="btn btn-primary" href="#" role="button" id="closedlg2" onClick="$('#mask, .window').hide();">Закрыть</span>
    </div>
    </div>

    <div id="mask" onClick="$('#mask, .window').hide();"></div>
</div>
{*Конец окон*}

<!-- Скрипт для инициализации элементов на странице, имеющих атрибут data-toggle="tooltip" -->
<script>
var cursid=0;
// после загрузки страницы
$(function () {
  // инициализировать все элементы на страницы, имеющих атрибут data-toggle="tooltip", как компоненты tooltip
  $('[data-toggle="tooltip"]').tooltip()
})
{literal}   
function showmydlg(str_title, str_msg) {
    $("#dtitle").text(str_title);    
    $("#dmsg").text(str_msg);
    var id = "#dialog";
    var maskHeight = $(document).height();
    var maskWidth = $(window).width();
    $('#mask').css({'width':maskWidth,'height':maskHeight});
    $('#mask').fadeIn(200);
    $('#mask').fadeTo("slow",0.6);
    var winH = $(window).height();
    var winW = $(window).width();
    $("#dialog").css('top',  winH/2-$(id).height()/2+$(window).scrollTop());
    $("#dialog").css('left', winW/2-$(id).width()/2);

    $("#closedlg").css('top',  winH/2-$(id).height()/2+$(window).scrollTop());
    $("#closedlg").css('left', winW/2-$(id).width()/2);
    $("#dialog").fadeIn(200);
    return false;
  }

  function showmydlg2(sid) {
    cursid=sid;
    var id = "#dialog2";
    var maskHeight = $(document).height();
    var maskWidth = $(window).width();
    $('#mask2').css({'width':maskWidth,'height':maskHeight});
    $('#mask2').fadeIn(200);
    $('#mask2').fadeTo("slow",0.6);
    var winH = $(window).height();
    var winW = $(window).width();
    $("#dialog2").css('top',  winH/2-$(id).height()/2+$(window).scrollTop());
    $("#dialog2").css('left', winW/2-$(id).width()/2);

    $("#closedlg2").css('top',  winH/2-$(id).height()/2+$(window).scrollTop());
    $("#closedlg2").css('left', winW/2-$(id).width()/2);
    $("#dialog2").fadeIn(200);
    return false;
  }

  function addmoney(){    
    var cash=$('#cashsumm').val();
    $('#cashsumm').val("");
    $('#mask, .window').hide();
    var data = new FormData();
    data.append( 'sid',  (0+cursid));
    data.append( 'cash',  Math.abs(cash*100));    
    $.ajax({
        url: 'http://warstores.net/ws-panel/ajax/adm_shop_balance.php',
        type: 'POST',
        data: data,
        cache: false,
        crossDomain: true,
        dataType: 'json',
        processData: false, // Не обрабатываем файлы (Don't process the files)
        contentType: false, // Так jQuery скажет серверу что это строковой запрос
        success: function( respond, textStatus, jqXHR ){
          console.log("Баланс пополнен");
        },
        error: function( jqXHR, textStatus, errorThrown ){
            console.log('ОШИБКИ AJAX запроса: ' + textStatus );
        }
    });

    //showmydlg("Зачислить на счет магазина "+cursid, "Надо зачислить "+(cash*100)+" коп.");
    cursid=0;
  }
  {/literal}   
</script>