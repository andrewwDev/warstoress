//*************ADM-NEWS*****************
function ReloadCity(){//Перезагрузить список городов
	var reg_id=$("#pg_reg_id").val();
	if (reg_id==0){
		$("#pg_city_id").empty();
	}else{
			var formData = {rid:reg_id}; //Array */
		$("#myModalBox").modal('show');
		$.ajax({type: "POST", crossDomain: true, url: "ajax/get_city.php", data: formData,
		  success: function(msg){	

			var ajaxres = JSON.parse(msg);
			var sel=$("#pg_city_id");
			arr_cityid=ajaxres.city_id;
			arr_cityname=ajaxres.city_name;
			num=ajaxres.num;
			sel.empty();
			sel.append($('<option value=0>Не важно</option>'));
			for (var i = 0; i < num; i++) {
				sel.append($('<option value='+arr_cityid[i]+'>'+arr_cityname[i]+'</option>'));
			};
			$("#pg_city_id :first").attr("selected", "selected");

			
			$("#myModalBox").modal('hide');
		  },
		  error:function(msg){
			$("#myModalBox").modal('hide');
		  }
		});
	}
	

}
function savenews(){
	var textMCE = tinyMCE.activeEditor.getContent();
	var fd = new FormData;
	fd.append('pg','editnews');
	fd.append('cmd','save');
	fd.append('nid',$('#news_nid').val());
	fd.append('title',$('#news_title').val());
	fd.append('date',$('#news_date').val());
	fd.append('reg_id',$('#pg_reg_id').val());
	fd.append('city_id',$('#pg_city_id').val());
	fd.append('description',$('#news_description').val());
	fd.append('pg_source',textMCE);
	fd.append('file',$('#myfiles').prop('files')[0])

	$("#myModalBox").modal('show');
	$.ajax({type: "POST", 
		crossDomain: true, 
		url: "ajax/adm_save.php", 
		data: fd,
		processData: false,
        contentType: false,
	  success: function(msg){		
		$("#myModalBox").modal('hide');
		var ajaxres = JSON.parse(msg);
		if (ajaxres.nid>0){
			if ($('#news_nid').val()!=ajaxres.nid){
				window.location.href='http://warstores.net/ws-panel/adm.php?cmd=editnews&pg='+ajaxres.nid;
			}
		}		
	  },
	  error:function(msg){
		$("#myModalBox").modal('hide');
	  }
	});	
	
}

function createpage(){
var formData = {
		pg:'editpg',
		cmd:'create',
		}; //Array */
	$("#myModalBox").modal('show');
	$.ajax({type: "POST", crossDomain: true, url: "ajax/adm_save.php", data: formData,
	  success: function(msg){		
		var ajaxres = JSON.parse(msg);
		console.log("pid="+ajaxres.pid);
		if (ajaxres.pid>0){
			window.location.href='http://breakbox.ru/adm.php?cmd=editpg&pg='+ajaxres.pid;
		}
		$("#myModalBox").modal('hide');
	  },
	  error:function(msg){
		$("#myModalBox").modal('hide');
	  }
	});		
}

function delpage(pid){
var formData = {
		pg:'editpg',
		cmd:'del',
		pid:pid
		}; //Array */
	$.ajax({type: "POST", crossDomain: true, url: "ajax/adm_save.php", data: formData,
	  success: function(msg){		
		$('#tblrow'+pid).hide();
		$("#myDlgDelete").modal('hide');
	  },
	  error:function(msg){
		$("#myDlgDelete").modal('hide');
	  }
	});		
}