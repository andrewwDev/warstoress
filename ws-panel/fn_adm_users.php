<?php
//Функция для управления магазинами
//
function adm_users ( $pg = 0 ) {
    global $tmpres;
	$ononepg = 30; //Отображать на одной странице
	$userslist = array();
	//if ($pg>0) {
	// 	$strlimit="LIMIT ".($ononepg*($pg-1)).", ".$ononepg;
	// } else {
	// 	$strlimit='';
	// }

	$strlimit = ($pg) ? "LIMIT ".($ononepg*($pg-1)).", ".$ononepg  : '';

	//Получаем количество листов всего
	$users_num = 0;
	$sql = "SELECT count(sid) FROM `wsq_users`";
	$res = mysqli_query($tmpres, $sql);
	if ($res!=false) {
		if (mysqli_num_rows($res)>0) {
			$users_num .= mysqli_fetch_row($res)[0];
		}
	} //Всего новостей
	$pagenum = ceil($users_num/$ononepg); //Всего листов
	//
	$sql = "SELECT uid AS id, email, vk_uid, CONCAT_WS(' ', name, surname) AS name, isapproved FROM wsq_users $strlimit";
	$res = mysqli_query($tmpres, $sql) or die(mysqli_error($tmpres));

	if (mysqli_num_rows($res)>0){
		while($row = mysqli_fetch_assoc($res)){
			$userslist[]=$row;
		}
	}

	$resarr = array(
		'0' => $userslist,
		'1' => $pagenum
	);
	return $resarr;
}



function adm_user_edit( $uid )
{
    global $tmpres;
	$sql = "SELECT * FROM `wsq_users` WHERE uid = $uid";
	$res = mysqli_query($tmpres, $sql) or die(mysqli_error($tmpres));
	if (mysqli_num_rows($res) !== 1) die($uid . 'is not found or not unique');

	$user = mysqli_fetch_assoc($res);

	return $user;

}
?>