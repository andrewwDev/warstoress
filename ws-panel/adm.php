<?php
error_reporting(-1);
session_start();
header("Content-Type: text/html; charset=utf-8");
$homedir = "/var/www/u0413200/data/www/warstores.net/ws-panel/";
$pagenum = 0;
$mode = 0;

if (isset($_POST['cmd'])) {
    $cmd = $_POST['cmd'];
}

if (isset($_GET['cmd'])) {
    $cmd = $_GET['cmd'];
}

if (isset($_POST['pg'])) {
    $pagenum = 0 + $_POST['pg'];
}

if (isset($_GET['pg'])) {
    $pagenum = 0 + $_GET['pg'];
}

if (isset($_POST['mode'])) {
    $mode = 0 + $_POST['mode'];
}

if (isset($_GET['mode'])) {
    $mode = 0 + $_GET['mode'];
}

require_once ($homedir . 'configs/dbconfiguration.php');

// $tmpres = mysql_connect($hostname, $username, $password); // OR DIE("Не могу создать соединение ");
// $tmpres = mysql_select_db($dbName); // or die(mysql_error());

require_once ($homedir . 'fn_userautorization.php');
require_once ($homedir . 'fn_adm_news.php');
require_once ($homedir . 'fn_adm_shops.php');
require_once ($homedir . 'fn_adm_users.php');
require_once ($homedir . 'fn_adm_static.php');
require_once ($homedir . 'fn_regions.php');

$strmsg = "";
$isadmin = false;

if (isset($_POST['login'])) {
    if (get_magic_quotes_gpc()) { //Если слеши автоматически добавляются
        $_POST['user'] = stripslashes($_POST['user']);
        $_POST['pass'] = stripslashes($_POST['pass']);
    }
    $user = mysqli_real_escape_string($tmpres, $_POST['user']);
    $pass = mysqli_real_escape_string($tmpres, $_POST['pass']);
    $logres = login($user, $pass);
    if ($logres[0] == true) {
        $isadmin = true;
        $isshop = true;
        $strmsg = "<p>Вы успешно авторизовались.</p>";
        if ($logres[1] != 3) {
            $isadmin = false;
        }
        if ($logres[1] != 2) {
            $isshop = false;
        }
    } else {
        logout();
        header('Location: http://warstores.net/ws-panel/adm.php');
    }
} else {
    //Проверка на авторизованность
    if (isset($_SESSION['auid'])) {
        $isadmin = check_user_adm($_SESSION['auid']);
    }
}

if (isset($_GET['logout'])) {
     logout();
	// echo 'logout';
    die(header('Location: http://warstores.net/ws-panel/adm.php'));
}

require_once ($homedir . 'libs/Smarty.class.php');
$smarty = new Smarty();

if (!$isadmin) {
    $smarty->assign('isadmin', "0");
    $smarty->assign('cmd', "pg");
    $page = 'login';
} else {

    if (!isset($cmd)){
		header('Location: http://warstores.net/ws-panel/adm.php?cmd=news');
	}

    $smarty->assign('isadmin', "1");
    if (isset($cmd)) {
        if ($cmd == 'static') {
            $smarty->assign('cmd', "pg");
            $page = "static";
            $res_arr = adm_static();
            $smarty->assign('money_summ', $res_arr[19]);
        } elseif ($cmd == 'news') {
            $smarty->assign('cmd', "pg");
            $page = "news";
            $res_arr = adm_news($pagenum);
            $smarty->assign('newslist', $res_arr[0]);
            $smarty->assign('pagenum', $res_arr[1]);
            $smarty->assign('pagei', $pagenum);
        } elseif ($cmd == 'editnews') {
            $smarty->assign('cmd', "pg");
            $res_arr = adm_load_news($pagenum);
            if (empty($res_arr)) {
                $res_arr["nfdate"] = date("d.m.Y H:i");
            }
            $page = "news_edit";
            $smarty->assign('row', $res_arr);
            $smarty->assign('nid', $res_arr["nid"]);
            $smarty->assign('regionlist', get_region());
            $smarty->assign('citylist', get_city($res_arr["reg_id"]));
        } elseif ($cmd == 'shops') {
            $smarty->assign('cmd', "pg");
            $page = "shops";
            $res_arr = adm_shops($pagenum);
            $smarty->assign('shoplist', $res_arr[0]);
            $smarty->assign('pagenum', $res_arr[1]);
            $smarty->assign('pagei', $pagenum);
        } elseif ($cmd == 'editshop') {
            $smarty->assign('cmd', "pg");
            $res_arr = adm_load_shop($pagenum);
            if (empty($res_arr)) {
                $res_arr["nfdate"] = date("d.m.Y H:i");
            }
            $page = "shop_edit";
            $catids = adm_load_cats($pagenum);
            $smarty->assign('catlist', get_catalogs());
            $str = join(",", $catids);
            if (strlen($res_arr["worktime"]) > 0) {
                $work_time_array = explode(",", $res_arr["worktime"]);
                $hours1 = floor($work_time_array[0] / 60);
                if ($hours1 < 10) $hours1 = "0" . $hours1;
                $minutes1 = $work_time_array[0] - ($hours1 * 60);
                if ($minutes1 < 10) $minutes1 = "0" . $minutes1;
                $hours2 = floor($work_time_array[1] / 60);
                if ($hours2 < 10) $hours2 = "0" . $hours2;
                $minutes2 = $work_time_array[1] - ($hours2 * 60);
                if ($minutes2 < 10) $minutes2 = "0" . $minutes2;
                $smarty->assign('current_work_time', $hours1 . ":" . $minutes1 . " - " . $hours2 . ":" . $minutes2);
            }
            $geo_regions = [];
            $sql = "SELECT geo_region_id, name FROM `geo_region`";
            $res = mysqli_query($tmpres, $sql);
            if ($res != false) {
                if (mysqli_num_rows($res) > 0) {
                    while ($row = mysqli_fetch_array($res)) {
                        $geo_regions[] = $row;
                    }
                }
            }
            $city_and_reg = [];
            $sql = "SELECT city_id, reg_id FROM `wsq_shopsinreg` WHERE sid = " . $res_arr["sid"];
            $res = mysqli_query($tmpres, $sql);
            if ($res != false) {
                if (mysqli_num_rows($res) > 0) {
                    while ($row = mysqli_fetch_array($res)) {
                        $city_and_reg[] = $row;
                    }
                }
            }
            $citylist = [];
            $sql = "SELECT geo_city_id, name FROM `geo_city` WHERE geo_region_id = " . $city_and_reg[0]["reg_id"];
            $res = mysqli_query($tmpres, $sql);
            if ($res != false) {
                if (mysqli_num_rows($res) > 0) {
                    while ($row = mysqli_fetch_array($res)) {
                        $citylist[] = $row;
                    }
                }
            }
            $smarty->assign('catsel', join(',', $catids)); //Выбранные каталоги
            $smarty->assign('row', $res_arr);
            $smarty->assign('shop_id', $res_arr["sid"]);
            $smarty->assign('regionlist', $geo_regions);
            $smarty->assign('city_and_reg', $city_and_reg);
            $smarty->assign('citylist', $citylist);
        } elseif ($cmd == 'users') {
            $smarty->assign('cmd', "pg");
            $page = "users";
            $res_arr = adm_users($pagenum);
            $smarty->assign('userslist', $res_arr[0]);
            $smarty->assign('pagenum', $res_arr[1]);
            $smarty->assign('pagei', $pagenum);
        } elseif ($cmd == 'useredit') {
            $smarty->assign('cmd', "pg");
            $page = "user_edit";
            $res_arr = adm_user_edit($_GET['uid']);
            $smarty->assign('user', $res_arr);
            $smarty->assign('status', array(1 => "обычный", 2 => "магазин", 3 => "super admin", 4 => "модератор", 5 => "сотрудник"));
        }
    }
}
$smarty->assign('page', $page);
$smarty->display('adm_main.tpl');
?>