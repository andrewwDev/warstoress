<?php
function resizeimg($fname, $width,$newfname){
    $uploaddir="/var/www/u0413200/data/www/warstores.net/ws_images/news/";
    // исходное изображение
    $source = $uploaddir.$fname;
    $ext=strtolower(pathinfo($source, PATHINFO_EXTENSION));
    // путь для сохранения новой картинки
    $new_file = $uploaddir.$newfname;
    $size = getimagesize($source);  //узнаем размеры исходной картинки  
    if ($width==0){$width=$size[0];}
    if ($size[0]<$width){ //Если картинка меньше, чем нам нужно, то ничего не делаем и просто выходим.
        return false;
    }else{
    $ratio = $width / $size[0];//пропорция ширины
    $height = floor($size[1] * $ratio); // высчитываем новую высоту картинки    
    $img = imagecreatetruecolor($width, $height);// создаем холст пропорциональное сжатой картинке
    // загружаем исходную картинку
    if ($ext=="png"){
        $photo = imagecreatefrompng($source);
    }else{
        $photo = imagecreatefromjpeg($source);
    }   
    imagecopyresampled($img, $photo, 0, 0, 0, 0, $width, $height, $size[ 0], $size[1]); // копируем на холст сжатую картинку с учетом расхождений
    imagejpeg($img, $new_file,90); // сохраняем результат
    // очищаем память после выполнения скрипта
    imagedestroy($img);
    imagedestroy($photo);       
    return true;
    }
    
}

function resizer($fname){
    //Оптимизировать картинку под различные расширения и сохранить ее 
    $p=strpos($fname,".");
    if ($p>0){
        $basename=substr($fname, 0,$p); //Достаем имя до точки
        //$ext=substr($fname, $p);//Достаем имя до точки
        $sizes = array ("xxxh" => 1280, "xxh" => 960, "xh" => 640, "h" => 480, "m" => 320);//массив с размерами изображений
        foreach ($sizes as $key => $value) {
            resizeimg($fname, $value,$basename."_".$key.".jpg");
        }
        return true;
    }else{
        return false;
    }
    //resizeimg($fname, $width,$newfname);
}

function saveimagefornews($nid){ //Сохранить файл картинки новостей на диск
    //Вернуть список пользователей текущего аукциона
    $uploaddir="/var/www/u0413200/data/www/warstores.net/ws_images/news/";
    $data = array();
    $error = false;
    $files = array();
    if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );
    // переместим файлы из временной директории в указанную
    foreach( $_FILES as $file ){

            $filename2="img".$nid.".".pathinfo($file['name'], PATHINFO_EXTENSION);
            //Удаляем все старые файлы, связанные с этой новостью
            if (file_exists($uploaddir."img".$nid.".png")){unlink($uploaddir."img".$nid.".png");}
            if (file_exists($uploaddir."img".$nid.".jpg")){unlink($uploaddir."img".$nid.".jpg");}
            if (file_exists($uploaddir."img".$nid.".jpeg")){unlink($uploaddir."img".$nid.".jpeg");}
            if (file_exists($uploaddir."img".$nid.".gif")){unlink($uploaddir."img".$nid.".gif");}
            if (file_exists($uploaddir."img".$nid."_m.jpg")){unlink($uploaddir."img".$nid."_m.jpg");}
            if (file_exists($uploaddir."img".$nid."_h.jpg")){unlink($uploaddir."img".$nid."_h.jpg");}
            if (file_exists($uploaddir."img".$nid."_xh.jpg")){unlink($uploaddir."img".$nid."_xh.jpg");}
            if (file_exists($uploaddir."img".$nid."_xxh.jpg")){unlink($uploaddir."img".$nid."_xxh.jpg");}
            if (file_exists($uploaddir."img".$nid."_xxxh.jpg")){unlink($uploaddir."img".$nid."_xxxh.jpg");}

            if( move_uploaded_file( $file['tmp_name'], $uploaddir . $filename2 ) ){
                $files[] = realpath( $uploaddir . $filename2);
                resizer($filename2);//создаем картинки всех размеров
                resizeimg($filename2, 0,"img".$nid.".jpg");//Пересжимаем присланый файл, чтобы хакеры вообще не пролезли
                if ($filename2!="img".$nid.".jpg"){
                    if (file_exists($uploaddir.$filename2)){unlink($uploaddir.$filename2);}
                }
            }else{
                $error = true;
            }
        }
    return $res;
}


function saveimageforcorp($nid){ //Сохранить файл картинки новостей на диск
    //Вернуть список пользователей текущего аукциона
    $uploaddir="/var/www/u0413200/data/www/warstores.net/ws_images/shoplogo/";
    $data = array();

    $error = false;
    $files = array();
    if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );
    // переместим файлы из временной директории в указанную
    if ($_FILES["img_logo"]) {
        $filename2="shop".$nid.".".pathinfo($_FILES["img_logo"]['name'], PATHINFO_EXTENSION);
        move_uploaded_file( $_FILES["img_logo"]['tmp_name'], $uploaddir . $filename2);
    }
    if ($_FILES["img_big"]) {
        $filename2="bigshop".$nid.".".pathinfo($_FILES["img_big"]['name'], PATHINFO_EXTENSION);
        move_uploaded_file( $_FILES["img_big"]['tmp_name'], $uploaddir . $filename2);
    }
}
?>