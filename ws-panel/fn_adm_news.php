<?php
//Функция для управления новостями
//
function adm_news($pg=0){
    global $tmpres;
	$ononepg=30; //Отображать на одной странице
	$userslist=array();	
	if ($pg>0){
		$strlimit="LIMIT ".($ononepg*($pg-1)).", ".$ononepg;
	}else{
		$strlimit='';
	}
	//Получаем количество листов всего
	$news_num=0;
	$sql="SELECT count(nid) FROM wsq_news where published=1";
	$res = mysqli_query($tmpres, $sql);
	if ($res!=false){if (mysqli_num_rows($res)>0){$news_num=0+mysqli_fetch_row($res)[0];}} //Всего новостей
	$pagenum=ceil($news_num/$ononepg); //Всего листов
	//
	$sql="SELECT t1.nid,t1.title,DATE_FORMAT(t1.ndate,'%d.%m.%Y %H:%i') as ndate,t1.description,t1.published,t2.title as shop FROM wsq_news as t1 LEFT JOIN wsq_shops as t2 ON t1.shop_id=t2.sid where t1.published=1 ORDER BY t1.ndate DESC $strlimit";
	$j=0;
	$res = mysqli_query($tmpres, $sql);
	if ($res!=false){	
		if (mysqli_num_rows($res)>0){
			while($row=mysqli_fetch_array($res)){
				$userslist[]=$row;
				if (isset($userslist[$j]["shop_id"])){
					if ($userslist[$j]["shop_id"]==0){$userslist[$j]["shop"]="WarStores";}
				}
				$j=$j+1;
			}
		}	
	}
	$resarr=array();
	$resarr[0]=$userslist;
	$resarr[1]=$pagenum;
	return $resarr;
}

function adm_load_news($pg=0){
    global $tmpres;
	$sql="SELECT *, DATE_FORMAT(ndate,'%d.%m.%Y %H:%i') as nfdate FROM wsq_news WHERE nid=$pg";
	$res = mysqli_query($tmpres, $sql);
	$userslist=array();	
	if ($res!=false){	
		if (mysqli_num_rows($res)>0){
			while($row=mysqli_fetch_array($res)){
				$userslist=$row;
			}
		}	
	}
	return $userslist;
}

?>