<?php /* Smarty version 3.1.27, created on 2017-06-18 18:21:50
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_login.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:21195382195946b6ae55eb48_17111432%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4406f3810b841208a33166bf4992584dca730a7b' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_login.tpl',
      1 => 1497806507,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21195382195946b6ae55eb48_17111432',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5946b6ae580653_43401347',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5946b6ae580653_43401347')) {
function content_5946b6ae580653_43401347 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '21195382195946b6ae55eb48_17111432';
?>

	<div id="container" class="cls-container login-page">

		<!-- LOGIN FORM -->
		<div class="cls-content">
		    <div class="cls-content-sm panel" style="width:20%; margin-left:40%; margin-right:40%; text-align:center;">
		        <div class="panel-body">
		            <div class="mar-ver pad-btm">
		                <h3 class="h4 mar-no">Добро пожаловать в панель управления.</h3>
		                <p class="text-muted">Войдите в свою учетную запись</p>
		            </div>
		            <form class="form-horizontal" action="adm.php" method="post" id="my_form_auth" enctype="multipart/form-data">
                    <input type="hidden" name="pass" id="pass" value="" style="border-radius: 0;">
		                <div class="form-group">
		                    <input type="text" name="user" class="form-control" placeholder="Логин" autofocus id="user" value="">
		                </div>
		                <div class="form-group">
		                    <input type="password" name="pass" class="form-control" placeholder="Пароль" id="pwd" value="">
		                </div>
		                <input type="submit" id="submit" name="login" value="Войти" class="btn btn-success btn-lg">
		            </form>
		        </div>
		    </div>
		</div>

	</div>
<?php echo '<script'; ?>
>
	$( 'button[type=submit]' ).click(function(event) {
		event.preventDefault();

		$.post('backend/auth.php', $( '#loginForm' ).serialize())
		    .done(function( data ) {
		    	console.log(data);
				if (data == 'yes')
					location.reload();
				else alert(data);
		    });

  	});
<?php echo '</script'; ?>
><?php }
}
?>