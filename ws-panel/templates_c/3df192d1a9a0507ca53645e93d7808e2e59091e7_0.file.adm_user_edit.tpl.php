<?php /* Smarty version 3.1.27, created on 2017-03-27 06:22:11
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_user_edit.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:168305297158d8a183b68d40_32081573%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3df192d1a9a0507ca53645e93d7808e2e59091e7' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_user_edit.tpl',
      1 => 1490566136,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '168305297158d8a183b68d40_32081573',
  'variables' => 
  array (
    'user' => 0,
    'status' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_58d8a18427f9e4_97260414',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_58d8a18427f9e4_97260414')) {
function content_58d8a18427f9e4_97260414 ($_smarty_tpl) {
if (!is_callable('smarty_function_html_options')) require_once '/var/www/u0413200/data/www/warstores.net/ws-panel/libs/plugins/function.html_options.php';

$_smarty_tpl->properties['nocache_hash'] = '168305297158d8a183b68d40_32081573';
?>
<h2>РЕДАКТИРОВАНИЕ ПОЛЬЗОВАТЕЛЯ</h2>

<div class="row">
  <div class="col-md-12 col-xs-12">
    <form name="userForm">
      <input type="hidden" name="uid" value="<?php echo $_GET['uid'];?>
">
      <div class="form-group">
        <label class="col-xs-2 control-label">Имя:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" name="name" placeholder="Имя" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['name'];?>
">
        </div>
      </div>

      <div class="form-group">
        <label class="col-xs-2 control-label">Фамилия:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" name="surname" placeholder="Фамилия" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['surname'];?>
">
        </div>
      </div>

       <div class="form-group">
        <label class="col-xs-2 control-label">Логин:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" name="login" placeholder="Логин" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['login'];?>
">
        </div>
      </div>

       <div class="form-group">
        <label class="col-xs-2 control-label">Email:</label>
        <div class="col-xs-10">
          <input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['email'];?>
">
        </div>
      </div>

      <div class="form-group">
        <label class="col-xs-2 control-label">Дата рождения:</label>
        <div class="col-xs-10">
          <input type="date" class="form-control" name="birthday" placeholder="Дата рождения" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['birthday'];?>
">
        </div>
      </div>

      <div class="form-group">
        <label class="col-xs-2 control-label">Статус:</label>
        <div class="col-xs-10">
           <?php echo smarty_function_html_options(array('name'=>'status','options'=>$_smarty_tpl->tpl_vars['status']->value,'selected'=>$_smarty_tpl->tpl_vars['user']->value['status']),$_smarty_tpl);?>

        </div>
      </div>

      <button type="button" class="btn btn-success pull-right" name="save">Сохранить</button>
      <button type="button" class="btn btn-default pull-right" onClick="window.location.href='adm.php?cmd=users'">Вернуться к списку пользователей</button>
    </form>
  </div>
</div>

<div id="succes" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div id="result" class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Продолжить редактирование</button>
        <button type="button" class="btn btn-primary" onClick="window.location.href='adm.php?cmd=users'">Вернуться к списку пользователей</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php echo '<script'; ?>
>
  $('button[name=save]').click( function() {
    var postData = $.param({ pg: "user", cmd: "edit" }) + '&' +  $('form[name=userForm]').serialize();
    //console.log(postData);
    $.post( 'ajax/adm_save.php', postData , function(data) {
      $('#result').html(data);
      $('#succes').modal('show');        }
    );
  });
<?php echo '</script'; ?>
>






<?php }
}
?>