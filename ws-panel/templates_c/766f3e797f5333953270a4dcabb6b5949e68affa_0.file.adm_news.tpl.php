<?php /* Smarty version 3.1.27, created on 2017-10-25 15:04:00
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_news.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:45538898959f07db0d89af6_63900060%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '766f3e797f5333953270a4dcabb6b5949e68affa' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_news.tpl',
      1 => 1508674443,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '45538898959f07db0d89af6_63900060',
  'variables' => 
  array (
    'newslist' => 0,
    'value' => 0,
    'pagenum' => 0,
    'pagei' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59f07db0daee54_09754955',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59f07db0daee54_09754955')) {
function content_59f07db0daee54_09754955 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '45538898959f07db0d89af6_63900060';
?>
<h2 align=center>НОВОСТИ</h2>

<div class="row" style='margin-right:20px;'>
      <div class="col-md-12 col-xs-12 " id="toppanel" style="padding:5px;">
        <button type="button" class="btn btn-success pull-right" onClick="window.location.href='adm.php?cmd=editnews'"><img src="http://warstores.net/ws-panel/images/ws_add.png" alt="Создать новость"></button>       
      </div>
  </div>

<style> 
	table.usrtbl td{text-align: center;vertical-align: middle;padding: 2px;}
	table.usrtbl th{background-color:#E6E6E6; text-align: center;vertical-align: middle; padding: 2px;}
 </style>

<table border=1 width=100% class="usrtbl">
<tr>
	<th>ID</th>
	<th>Магазин</th>
	<th>Заголовок</th>
	<th>Дата</th>
	<th>Краткое описание</th>
	<th colspan=2>Управление</th>
</tr>
<?php
$_from = $_smarty_tpl->tpl_vars['newslist']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
<tr id="userrow<?php echo $_smarty_tpl->tpl_vars['value']->value['uid'];?>
">
	<td><?php echo $_smarty_tpl->tpl_vars['value']->value['nid'];?>
</td>
	<td><?php echo $_smarty_tpl->tpl_vars['value']->value['shop'];?>
</td>
	<td><?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
</td>
	<td><?php echo $_smarty_tpl->tpl_vars['value']->value['ndate'];?>
</td>
	<td><?php echo $_smarty_tpl->tpl_vars['value']->value['description'];?>
</td>
	<td><span class="btn btn-primary" onClick="window.location.href='adm.php?cmd=editnews&pg=<?php echo $_smarty_tpl->tpl_vars['value']->value['nid'];?>
'"><img src="http://warstores.net/ws-panel/images/ws_edit.png" alt="Редактировать новость"></span></td>
	<td><span class="btn btn-danger" onClick="userdelete(<?php echo $_smarty_tpl->tpl_vars['value']->value['nid'];?>
)"><img src="http://warstores.net/ws-panel/images/ws_del.png" alt="Создать новость"></span></td>	
</tr>
<?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
</table>

<br><br>
          
        <?php if ($_smarty_tpl->tpl_vars['pagenum']->value > 1) {?>
        <center>
        <ul class="pagination pull-center">
        <?php if ($_smarty_tpl->tpl_vars['pagei']->value > 1) {?><li><a href="adm.php?cmd=news&pg=<?php echo $_smarty_tpl->tpl_vars['pagei']->value-1;?>
">&laquo;</a></li><?php }?>
        <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['pagenum']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['pagenum']->value)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
        <li><a href="adm.php?cmd=news&pg=<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php if ($_smarty_tpl->tpl_vars['i']->value == $_smarty_tpl->tpl_vars['pagei']->value) {?><b><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</b><?php } else {
echo $_smarty_tpl->tpl_vars['i']->value;
}?></a></li>
        <?php }} ?>
        <?php if ($_smarty_tpl->tpl_vars['pagei']->value < $_smarty_tpl->tpl_vars['pagenum']->value) {?><li><a href="adm.php?cmd=news&pg=<?php echo $_smarty_tpl->tpl_vars['pagei']->value+1;?>
">&raquo;</a></li><?php }?></li>
        </ul>
        </center>
        <?php }?>
          




<div id="boxes">
  <div id="dialog" class="window">    
    <h1 id="dtitle" style="background-color:#54769a; padding:0px; color:#fff; font-size:11px; padding:10px; margin: -2px;">Информация о пользователе<h1>
    <p id="dmsg">Пользователь с таким именем уже зарегистрирован</p>
    <div id="closediv" style="position:absolute; bottom:10px; right:10px;">
      <span class="btn btn-primary" href="#" role="button" id="closedlg" onClick="$('#mask, .window').hide();">Закрыть</span>
    </div>
    </div>
  <div id="mask" onClick="$('#mask, .window').hide();"></div>
</div>


<!-- Скрипт для инициализации элементов на странице, имеющих атрибут data-toggle="tooltip" -->
<?php echo '<script'; ?>
>
// после загрузки страницы
$(function () {
  // инициализировать все элементы на страницы, имеющих атрибут data-toggle="tooltip", как компоненты tooltip
  $('[data-toggle="tooltip"]').tooltip()
})
   
function showmydlg(str_title, str_msg) {
    $("#dtitle").text(str_title);    
    $("#dmsg").text(str_msg);
    var id = "#dialog";
    var maskHeight = $(document).height();
    var maskWidth = $(window).width();
    $('#mask').css({'width':maskWidth,'height':maskHeight});
    $('#mask').fadeIn(200);
    $('#mask').fadeTo("slow",0.6);
    var winH = $(window).height();
    var winW = $(window).width();
    $("#dialog").css('top',  winH/2-$(id).height()/2+$(window).scrollTop());
    $("#dialog").css('left', winW/2-$(id).width()/2);

    $("#closedlg").css('top',  winH/2-$(id).height()/2+$(window).scrollTop());
    $("#closedlg").css('left', winW/2-$(id).width()/2);
    $("#dialog").fadeIn(200);
    return false;
  }
     
<?php echo '</script'; ?>
><?php }
}
?>