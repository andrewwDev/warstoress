<?php /* Smarty version 3.1.27, created on 2017-10-25 15:02:48
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_login.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:205878593859f07d68202af6_02757319%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '715b22ba728d595f10d7140b3cd33926c1b0a11b' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_login.tpl',
      1 => 1508674443,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '205878593859f07d68202af6_02757319',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59f07d682064f7_38843513',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59f07d682064f7_38843513')) {
function content_59f07d682064f7_38843513 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '205878593859f07d68202af6_02757319';
?>

	<div id="container" class="cls-container login-page">

		<!-- LOGIN FORM -->
		<div class="cls-content">
		    <div class="cls-content-sm panel" style="width:20%; margin-left:40%; margin-right:40%; text-align:center;">
		        <div class="panel-body">
		            <div class="mar-ver pad-btm">
		                <h3 class="h4 mar-no">Добро пожаловать в панель управления.</h3>
		                <p class="text-muted">Войдите в свою учетную запись</p>
		            </div>
		            <form class="form-horizontal" action="adm.php" method="post" id="my_form_auth" enctype="multipart/form-data">
                    <input type="hidden" name="pass" id="pass" value="" style="border-radius: 0;">
		                <div class="form-group">
		                    <input type="text" name="user" class="form-control" placeholder="Логин" autofocus id="user" value="">
		                </div>
		                <div class="form-group">
		                    <input type="password" name="pass" class="form-control" placeholder="Пароль" id="pwd" value="">
		                </div>
		                <input type="submit" id="submit" name="login" value="Войти" class="btn btn-success btn-lg">
		            </form>
		        </div>
		    </div>
		</div>

	</div>
<?php echo '<script'; ?>
>
	$( 'button[type=submit]' ).click(function(event) {
		event.preventDefault();

		$.post('backend/auth.php', $( '#loginForm' ).serialize())
		    .done(function( data ) {
		    	console.log(data);
				if (data == 'yes')
					location.reload();
				else alert(data);
		    });

  	});
<?php echo '</script'; ?>
><?php }
}
?>