<?php /* Smarty version 3.1.27, created on 2017-11-02 10:09:32
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_news_edit.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:160250832359fac4ace63b09_76515532%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '89ee9fe5441cd67801d31ebff272134ded1acf6b' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_news_edit.tpl',
      1 => 1508674443,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '160250832359fac4ace63b09_76515532',
  'variables' => 
  array (
    'nid' => 0,
    'row' => 0,
    'regionlist' => 0,
    'value' => 0,
    'citylist' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59fac4acea1145_99951205',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59fac4acea1145_99951205')) {
function content_59fac4acea1145_99951205 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '160250832359fac4ace63b09_76515532';
?>
<?php echo '<script'; ?>
>
var arr_cityname= new Array();
var arr_cityid= new Array();
var files;  
<?php echo '</script'; ?>
>

 
<?php echo '<script'; ?>
 type="text/javascript">
  function figupload(nid){
    
    var data = new FormData();
    $.each( files, function( key, value ){
      data.append( key, value );
      console.log(key);
      console.log(value);
      });
    data.append( 'nid',  (0+nid));
    // Отправляем запрос
    console.log(data);


    $.ajax({
        url: 'http://warstores.net/ws-panel/ajax/enj_submitfig.php?nid='+nid+'&uploadfiles',
        type: 'POST',
        data: data,
        cache: false,
        crossDomain: true,
        dataType: 'json',
        processData: false, // Не обрабатываем файлы (Don't process the files)
        contentType: false, // Так jQuery скажет серверу что это строковой запрос
        success: function( respond, textStatus, jqXHR ){
            if( typeof respond.error === 'undefined' ){
                // Файлы успешно загружены, делаем что нибудь здесь
                console.log("Изображение удачно отправлено на сервер");
                // выведем пути к загруженным файлам в блок '.ajax-respond'
                var files_path = respond.files;
                var html = '';
                //$.each( files_path, function( key, val ){ html += val +'<br>'; } )
                var src = "http://warstores.net/ws_images/news/img"+nid+".jpg?r="+Math.random();//генерируем уникальный адрес картинки
                $("#imgsrc").removeAttr('src').attr('src', src);
                //$('.ajax-respond').html( html );
            }
            else{
                console.log('ОШИБКИ ОТВЕТА сервера: ' + respond.error );
            }
        },
        error: function( jqXHR, textStatus, errorThrown ){
            console.log('ОШИБКИ AJAX запроса: ' + textStatus );
        }
    });

    return false;
  }
<?php echo '</script'; ?>
>
<style>
input.imgloader button{background-color: #444;}
</style>


<div style="padding-left:15px; padding-right:15px;">
<h1>РЕДАКТИРОВАНИЕ НОВОСТИ</h1>

  <div class="row" style="padding:20px;">
<div class="row">
      <div class="col-md-12 col-xs-12 " id="toppanel" style="padding:5px;">
        <button type="button" class="btn btn-success pull-right" onClick="savenews()">Сохранить</button>
        <button type="button" class="btn btn-default pull-right" onClick="window.location.href='adm.php?cmd=news'">Закрыть</button>
      </div>
  </div>
<div class="row">
  <div class="col-md-12 col-xs-12" id="toppanel">
    <hr>

    <form class="form-horizontal">
      <input type="hidden" id="news_nid" value="<?php echo $_smarty_tpl->tpl_vars['nid']->value;?>
">
      <div class="form-group">
        <label for="pg_title" class="col-xs-2 control-label">Заголовок новости:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="news_title" placeholder="Введите заголовок новости" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['title'];?>
">
        </div>
      </div>
      <div class="form-group">
        <label for="pg_alias" class="col-xs-2 control-label">Дата:</label>
        <div class="col-xs-10">

          <div class='input-group date' id='datetimepicker1'>
              <input type='text' class="form-control" id="news_date" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['nfdate'];?>
"/>
              <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
              </span>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="pg_keywords" class="col-xs-2 control-label">Картинка для новости:</label>
        <div class="col-xs-10">
          <table><tr><td><input id="myfiles" class="imgloader" type="file" accept="image/jpeg,image/png"> <br></td>
            <td rowspan=2>
              <div><a id="imglink" href="http://warstores.net/ws_images/news/img<?php echo $_smarty_tpl->tpl_vars['nid']->value;?>
.jpg" target="_blank"><img id="imgsrc" src="http://warstores.net/ws_images/news/img<?php echo $_smarty_tpl->tpl_vars['nid']->value;?>
.jpg" style="height:80px;"></a></div>
            </td></tr>
        </table>
        </div>
        
      </div>
      <div class="form-group">
        <label for="pg_description" class="col-xs-2 control-label">Выберите регион:</label>
        <div class="col-xs-10">         
        
        <select class="form-control" id="pg_reg_id" placeholder="Введите регион" OnChange="ReloadCity()">
            <option <?php if ($_smarty_tpl->tpl_vars['row']->value['reg_id'] == 0) {?>selected<?php }?> value=0>Не важно</option>
            <?php
$_from = $_smarty_tpl->tpl_vars['regionlist']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
            <option <?php if ($_smarty_tpl->tpl_vars['value']->value['geo_region_id'] == $_smarty_tpl->tpl_vars['row']->value['reg_id']) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['value']->value['geo_region_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value['name'];?>
</option>
            <?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
        </select> 
        </div>
      </div>
      <div class="form-group">
        <label for="pg_description" class="col-xs-2 control-label">Выберите город:</label>
        <div class="col-xs-10">          
          <select class="form-control" id="pg_city_id" placeholder="Введите город">
            <option <?php if ($_smarty_tpl->tpl_vars['row']->value['city_id'] == 0) {?>selected<?php }?> value=0>Не важно</option>
            <?php
$_from = $_smarty_tpl->tpl_vars['citylist']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
            <option <?php if ($_smarty_tpl->tpl_vars['value']->value['geo_city_id'] == $_smarty_tpl->tpl_vars['row']->value['city_id']) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['value']->value['geo_city_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value['name'];?>
</option>
            <?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
        </select> 
        </div>
      </div>
      <div class="form-group">
        <label for="pg_description" class="col-xs-2 control-label">Краткое описание (255 символов):</label>
        <div class="col-xs-10">          
          <input type="text" class="form-control" id="news_description" placeholder="Введите описание" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['description'];?>
">        
        </div>
      </div>
      
      <div class="form-group">
        <label for="pg_source" class="col-xs-2 control-label">Текст страницы:</label>
        <div class="col-xs-offset-2 col-xs-10">
        
                  
          <textarea class="mceTA" id="news_extendedtext" placeholder="Идет загрузка редактора..."><?php echo $_smarty_tpl->tpl_vars['row']->value['extendedtext'];?>
</textarea>   
          <?php echo '<script'; ?>
 type='text/javascript' src='js/tinymce.min.js'><?php echo '</script'; ?>
>                 
          <?php echo '<script'; ?>
 type="text/javascript"> 
            tinymce.init({
              mode : "specific_textareas",
              editor_selector : "mceTA",
              height: 250,
              language:"ru", 
              theme: 'modern',
              plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
              ],
              toolbar1: 'insertfile undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
              image_advtab: true
             });
             
          <?php echo '</script'; ?>
>

        
        

        </div>
      </div>      
    </form>

  </div>
</div> 
</div>

</div>



 
  <?php echo '<script'; ?>
 type="text/javascript">
      $(function () {
          $('#datetimepicker1').datetimepicker( {pickTime: true, language: 'ru'});

          $('input[type=file]').change(function(){files = this.files;});
      });
  <?php echo '</script'; ?>
>
  <?php }
}
?>