<?php /* Smarty version 3.1.27, created on 2017-03-28 09:40:37
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_users.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:115115038158da2185a33521_89731455%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f6a53b0fd1c381234e42a0b440103cc73af2a370' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_users.tpl',
      1 => 1490701226,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '115115038158da2185a33521_89731455',
  'variables' => 
  array (
    'userslist' => 0,
    'value' => 0,
    'pagenum' => 0,
    'pagei' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_58da2185a7a7c1_63151408',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_58da2185a7a7c1_63151408')) {
function content_58da2185a7a7c1_63151408 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '115115038158da2185a33521_89731455';
?>
<h2 align=center>Пользователи</h2>



<style>
  table.usrtbl {width:100%;}
	table.usrtbl td{text-align: center;vertical-align: middle;padding: 2px;}
	table.usrtbl th{background-color:#E6E6E6; text-align: center;vertical-align: middle; padding: 2px;}
 </style>

<table border=1 class="usrtbl">
<tr>
	<th>ID</th>
  <th>Имя</th>
  <th>Email</th>
	<th>Верифицировать</th>
  <th>Изменить</th>
  <th>Удалить</th>
</tr>
<?php
$_from = $_smarty_tpl->tpl_vars['userslist']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
<tr id="<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
">
	<td><?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
</td>
	<td><?php echo $_smarty_tpl->tpl_vars['value']->value['name'];?>
</td>
	<td><?php echo $_smarty_tpl->tpl_vars['value']->value['email'];?>
</td>
	<td class="user-verification<?php if (($_smarty_tpl->tpl_vars['value']->value['isapproved'] == 1)) {?> verified<?php }?>"></td>
  <td><a href="adm.php?cmd=useredit&uid=<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
  <td><span class="glyphicon glyphicon-remove" aria-hidden="true" data-toggle="modal" data-target="#deleteUser" data-username="<?php echo $_smarty_tpl->tpl_vars['value']->value['name'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
"></span></td>
</tr>
<?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
</table>

<br><br>
        
        <?php if ($_smarty_tpl->tpl_vars['pagenum']->value > 1) {?>
        <center>
        <ul class="pagination pull-center">
        <?php if ($_smarty_tpl->tpl_vars['pagei']->value > 1) {?><li><a href="adm.php?cmd=users&pg=<?php echo $_smarty_tpl->tpl_vars['pagei']->value-1;?>
">&laquo;</a></li><?php }?>
        <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['pagenum']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['pagenum']->value)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
        <li><a href="adm.php?cmd=users&pg=<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php if ($_smarty_tpl->tpl_vars['i']->value == $_smarty_tpl->tpl_vars['pagei']->value) {?><b><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</b><?php } else {
echo $_smarty_tpl->tpl_vars['i']->value;
}?></a></li>
        <?php }} ?>
        <?php if ($_smarty_tpl->tpl_vars['pagei']->value < $_smarty_tpl->tpl_vars['pagenum']->value) {?><li><a href="adm.php?cmd=users&pg=<?php echo $_smarty_tpl->tpl_vars['pagei']->value+1;?>
">&raquo;</a></li><?php }?></li>
        </ul>
        </center>
        <?php }?>
        


<div id="deleteUser" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Удаление пользователя</h4>
      </div>
      <div id="result" class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" name="delete">Удалить</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Отмена</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php echo '<script'; ?>
>
  var htmlVerified = '<span class="glyphicon glyphicon-ok" aria-hidden="true" name="unverify"></span>';
  var htmlUnverified = '<input type="checkbox" name="verify">';

  $(function() {
    $( 'td.user-verification' ).each(function(){
      $( this ).html( $( this ).hasClass( 'verified' ) ?  htmlVerified : htmlUnverified );
    });
  });

  $( 'td.user-verification' ).click(function(event){
    //event.stopPropagation();
    var userId = $( this ).parent().attr( 'id' );
    var setTo = $( this).hasClass( 'verified' ) ? '0' : '1';
    var thisTd = $( this );

    $.post('ajax/adm_save.php', { uid: userId, pg: 'user', cmd: 'verify', set: setTo })
      .done(function( data ) {
        thisTd.html( thisTd.hasClass( 'verified' ) ?  htmlUnverified : htmlVerified );
        thisTd.toggleClass( 'verified' );
      });

  });

  $('#deleteUser').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var username = button.data('username');
    var userId = button.data('uid');
    var modal = $(this);
    modal.find( '.modal-body' ).html('Вы уверенны, что хотите удалить пользователя <strong>' + username + '</strong>');
    modal.find( 'button[name=delete]' ).val(userId);
  })

  $( 'button[name=delete]' ).click(function() {
    var userId = $( this ).val();

  $.post('ajax/adm_save.php', { uid: userId, pg: 'user', cmd: 'delete' })
    .done(function( data ) {
      $( '#deleteUser' ).modal('hide');
      location.reload();
    });

  });
<?php echo '</script'; ?>
>
<?php }
}
?>