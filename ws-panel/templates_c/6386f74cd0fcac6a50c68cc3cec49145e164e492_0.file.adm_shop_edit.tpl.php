<?php /* Smarty version 3.1.27, created on 2017-08-09 08:17:48
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_shop_edit.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1356241875598ab71c761628_27736309%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6386f74cd0fcac6a50c68cc3cec49145e164e492' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_shop_edit.tpl',
      1 => 1502263065,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1356241875598ab71c761628_27736309',
  'variables' => 
  array (
    'catsel' => 0,
    'shop_id' => 0,
    'row' => 0,
    'current_work_time' => 0,
    'catselarr' => 0,
    'catlist' => 0,
    'value' => 0,
    'regionlist' => 0,
    'citylist' => 0,
    'city_and_reg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_598ab71c812667_46853118',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_598ab71c812667_46853118')) {
function content_598ab71c812667_46853118 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1356241875598ab71c761628_27736309';
?>
<?php echo '<script'; ?>
>
var arr_cityname= new Array();
var arr_cityid= new Array();
var arr_cat_sel=new Array();
var files;  
arr_cat_sel=[<?php echo $_smarty_tpl->tpl_vars['catsel']->value;?>
];
<?php echo '</script'; ?>
>

 
<?php echo '<script'; ?>
 type="text/javascript">
  function figupload(nid){
    
    var data = new FormData();
    $.each( files, function( key, value ){
      data.append( key, value );
      console.log(key);
      console.log(value);
      });
    data.append( 'nid',  (0+nid));
    // Отправляем запрос
    console.log(data);


    $.ajax({
        url: 'http://warstores.net/ws-panel/ajax/enj_submitfig.php?nid='+nid+'&uploadfiles',
        type: 'POST',
        data: data,
        cache: false,
        crossDomain: true,
        dataType: 'json',
        processData: false, // Не обрабатываем файлы (Don't process the files)
        contentType: false, // Так jQuery скажет серверу что это строковой запрос
        success: function( respond, textStatus, jqXHR ){
            if( typeof respond.error === 'undefined' ){
                // Файлы успешно загружены, делаем что нибудь здесь
                console.log("Изображение удачно отправлено на сервер");
                // выведем пути к загруженным файлам в блок '.ajax-respond'
                var files_path = respond.files;
                var html = '';
                //$.each( files_path, function( key, val ){ html += val +'<br>'; } )
                var src = "http://warstores.net/ws_images/news/img"+nid+".jpg?r="+Math.random();//генерируем уникальный адрес картинки
                $("#imgsrc").removeAttr('src').attr('src', src);
                //$('.ajax-respond').html( html );
            }
            else{
                console.log('ОШИБКИ ОТВЕТА сервера: ' + respond.error );
            }
        },
        error: function( jqXHR, textStatus, errorThrown ){
            console.log('ОШИБКИ AJAX запроса: ' + textStatus );
        }
    });

    return false;
  }
<?php echo '</script'; ?>
>
<style>
input.imgloader button{background-color: #444;}
</style>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/css/bootstrap-slider.min.css" />
<?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/bootstrap-slider.min.js"><?php echo '</script'; ?>
>
<div style="padding-left:15px; padding-right:15px;">
<h1>РЕДАКТИРОВАНИЕ МАГАЗИНА</h1>
    <form id="set_shop_form" action="ajax/adm_set_shop.php" method="post" enctype="multipart/form-data" >
        <input type="hidden" readonly name="shop_id" id="shop_id" value="<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
">
  <div class="row" style="padding:20px;">
<div class="row">
      <div class="col-md-12 col-xs-12 " id="toppanel" style="padding:5px;">
        <button id="comp_finish" type="button" class="btn btn-success pull-right">Сохранить</button>
        <button type="button" class="btn btn-default pull-right" onClick="window.location.href='adm.php?cmd=editshop'">Закрыть</button>
      </div>
  </div>
      <div id="after" style="display: none;">
          <div class="row">
              <div class="col-md-12" style="text-align: right;">
                  Данные обновлены
              </div>
          </div>
      </div>
<div class="row">
  <div class="col-md-12 col-xs-12" id="toppanel">
    <hr>

<!-- ***********************  -->
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#panel1">Информация для покупателя</a></li>
      <li><a data-toggle="tab" href="#panel2">Юридические данные</a></li>
      <li><a data-toggle="tab" href="#panel3">Тариф</a></li>      
      <li><a data-toggle="tab" href="#panel4">Модерация</a></li>
    </ul>
     
    <div class="tab-content">
<!--
      <div id="panel1" class="tab-pane fade in active">
        <h3>Панель 1</h3>
        <p>Содержимое 1 панели...</p>
      </div>
      <div id="panel2" class="tab-pane fade">
        <h3>Панель 2</h3>
        <p>Содержимое 2 панели...</p>
      </div>
      <div id="panel3" class="tab-pane fade">
        <h3>Панель 3</h3>
        <p>Содержимое 3 панели...</p>
      </div>
      <div id="panel4" class="tab-pane fade">
        <h3>Панель 4</h3>
        <p>Содержимое 4 панели...</p>
      </div>
   -->

<div id="panel1" class="tab-pane fade in active">
  <br>
<form class="form-horizontal">
      <input type="hidden" id="shop_id" value="<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
">
      <div class="form-group">
        <label class="col-xs-2 control-label">Название магазина:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_title" placeholder="Введите название магазина" name="copubname" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['title'];?>
">
        </div>
      </div>
      <div class="form-group">
        <label class="col-xs-2 control-label">Слоган:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_slogon" placeholder="Введите слоган магазина" name="copublessinfo" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['slogon'];?>
" maxlength=128>
        </div>
      </div>

    <div class="form-group">
        <label class="col-lg-3 control-label">Время работы:</label>
        <div class="col-lg-7">
            <b id="show_time">
                <?php if (strlen($_smarty_tpl->tpl_vars['row']->value['worktime']) > 0) {?>
                    <?php echo $_smarty_tpl->tpl_vars['current_work_time']->value;?>

                <?php } else { ?>
                    9:00 - 18:00
                <?php }?>
            </b> <br>
            <input id="slider_time" name="copubworktime" type="text" class="span2" value="" data-slider-min="0" data-slider-max="1440" data-slider-tooltip="hide" data-slider-step="30" data-slider-value="[<?php if (strlen($_smarty_tpl->tpl_vars['row']->value['worktime']) > 0) {
echo $_smarty_tpl->tpl_vars['row']->value['worktime'];
} else { ?>540,1080<?php }?>]"/>
        </div>
    </div>
    <?php echo '<script'; ?>
>
        var slider_time = $('#slider_time').slider({
            formatter: function(value) {
                return 'Время работы: ' + value + ' %';
            }
        });
        slider_time.change(function(e) {
            var hours1 = Math.floor(e.value.newValue["0"] / 60);
            var minutes1 = e.value.newValue["0"] - (hours1 * 60);

            if (hours1.length == 1) hours1 = '0' + hours1;
            if (minutes1.length == 1) minutes1 = '0' + minutes1;
            if (minutes1 == 0) minutes1 = '00';
            if (hours1 == 0) {
                hours1 = 00;
                minutes1 = minutes1;
            }

            var hours2 = Math.floor(e.value.newValue["1"] / 60);
            var minutes2 = e.value.newValue["1"] - (hours2 * 60);

            if (hours2.length == 1) hours2 = '0' + hours2;
            if (minutes2.length == 1) minutes2 = '0' + minutes2;
            if (minutes2 == 0) minutes2 = '00';
            if (hours2 == 24) {
                hours2 = 00;
                minutes2 = "00";
            }

            $('#show_time').html(hours1 + ':' + minutes1 + ' - ' + hours2 + ':' + minutes2);
        });
    <?php echo '</script'; ?>
>



      <div class="form-group">
        <label class="col-xs-2 control-label">Адрес:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_address" placeholder="Введите адрес" name="copubgeoadress" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['address'];?>
" maxlength=256>
        </div>
      </div>  
      <div class="form-group">
        <label class="col-xs-2 control-label">Географические координаты:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_YGeoLoc" placeholder="00.000000, 00.000000" name="YGeoLoc" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['YGeoLoc'];?>
" maxlength=64>
        </div>
      </div> 
      <div class="form-group">
        <label class="col-xs-2 control-label">Телефон:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_phone" placeholder="" name="copubtel" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['phone'];?>
" maxlength=10>
        </div>
      </div> 
      <div class="form-group">
        <label class="col-xs-2 control-label">Сайт:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_url" placeholder="http://" name="copubsite" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['url'];?>
" maxlength=256>
        </div>
      </div> 

      <div class="form-group">
        <label class="col-xs-2 control-label">Описание магазина:</label>
        <div class="col-xs-10">
          <textarea class="form-control" id="shop_description" name="copubinfo"><?php echo $_smarty_tpl->tpl_vars['row']->value['description'];?>
</textarea>
        </div>
      </div> 

      <div class="form-group">
        <label for="pg_keywords" class="col-xs-2 control-label">Картинки:</label>
        <div class="col-xs-10">
          <table width=100<?php echo '%>';?>
            <tr>
              <td width=20<?php echo '%>';?>
                <b>Картинка для логотипа 96x96</b><br><br>
                <input id="myfiles96" class="imgloader" type="file" accept="image/jpeg,image/png"> <br>
              </td>
              <td  width=10<?php echo '%>';?>
                <div><a id="imglink96" href="http://warstores.net/ws_images/shoplogo/shop<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
.png" target="_blank"><img id="imgsrc1" src="http://warstores.net/ws_images/shoplogo/shop<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
.jpg" style="height:96px;"></a></div>
              </td>
              <td width=40<?php echo '%>';?></td>
              <td width=20<?php echo '%>';?>
                <b>Большая картинка 1280x960</b><br><br>
                <input id="myfiles1280" class="imgloader" type="file" accept="image/jpeg,image/png"> <br>
              </td>
              <td width=10<?php echo '%>';?>
                <div><a id="imglink1280" href="http://warstores.net/ws_images/shoplogo/bigshop<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
.jpg" target="_blank"><img id="imgsrc2" src="http://warstores.net/ws_images/shoplogo/bigshop<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
.jpg" style="height:96px;"></a></div>
              </td>

          </tr>
        </table>
        </div>
      </div>

      <div class="form-group">
        <label class="col-xs-2 control-label">Категория:</label>
        <div class="col-xs-10">
        
        <select id="shop_cat" name="heading[]" class="selectpicker show-menu-arrow form-control" multiple data-max-options="2">
          <?php echo $_smarty_tpl->tpl_vars['catselarr']->value;?>

<?php
$_from = $_smarty_tpl->tpl_vars['catlist']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>         
          <option value="<?php echo $_smarty_tpl->tpl_vars['value']->value['csid'];?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
</option>
<?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
        </select>
        </div>
      </div>


      <div class="form-group">
        <label for="pg_description" class="col-xs-2 control-label">Выберите регион:</label>
        <div class="col-xs-10">
        <select class="form-control" id="pg_reg_id" placeholder="Введите регион" OnChange="ReloadCity()">
            <?php
$_from = $_smarty_tpl->tpl_vars['regionlist']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
            <option value="<?php echo $_smarty_tpl->tpl_vars['value']->value["geo_region_id"];?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value["name"];?>
</option>
            <?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
        </select> 
        </div>
      </div>
      <div class="form-group">
        <label for="pg_description" class="col-xs-2 control-label">Выберите город:</label>
        <div class="col-xs-10">          
          <select class="form-control" id="pg_city_id" placeholder="Введите город">
            <?php
$_from = $_smarty_tpl->tpl_vars['citylist']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
            <option value="<?php echo $_smarty_tpl->tpl_vars['value']->value["geo_city_id"];?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value["name"];?>
</option>
            <?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
        </select> 
        </div>
      </div>

    <?php echo '<script'; ?>
>
        $('#pg_reg_id').selectpicker('val', <?php echo $_smarty_tpl->tpl_vars['city_and_reg']->value[0]["reg_id"];?>
);
        $('#pg_city_id').selectpicker('val', <?php echo $_smarty_tpl->tpl_vars['city_and_reg']->value[0]["city_id"];?>
);
    <?php echo '</script'; ?>
>

    </form>
  </div>

  <div id="panel2" class="tab-pane fade">
      <div class="form-group">
        <label class="col-xs-2 control-label">Название магазина:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_jtitle" name="courname" placeholder="Юридическое название магазина" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['ur_title'];?>
">
        </div>
      </div>

      <div class="form-group">
        <label class="col-xs-2 control-label">Хэштэг компании</label>
        <div class="col-xs-10">
           <input type="text" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['shopnick'];?>
" onkeyup="var simvols=/['А-я',':;'.,'\s','/','<>','[\]\\','#!@$%^&',''','{}?№\|_','-=+']/; if(simvols.test(this.value)) this.value=''" maxlength="12" class="form-control" name="shopnick" id="shopnick" placeholder="Хэштэг компании">
			<div id="check_shopnick" style="display: none; color: red; font-weight: bold;">
				Такой хэштег занят
			</div>
		</div>
      </div>
	  										
											<?php echo '<script'; ?>
>
                                                $( "#shopnick" ).blur(function() {
                                                    var shopnick = {shopnick: $('#shopnick').val()};
                                                    $.ajax({
                                                        url: 'backend/check_shopnick.php',
                                                        data: shopnick,
                                                        type: 'POST',
                                                        success: function (data) {
                                                            console.log(data);
                                                            if (data == 1) {
                                                                $("#check_shopnick").show();
                                                                $("#shopnick").addClass("errors");
                                                            } else {
                                                                $("#check_shopnick").hide();
                                                                $("#shopnick").removeClass("errors");
                                                            }
                                                        }
                                                    });
                                                });
											<?php echo '</script'; ?>
>
										

       <div class="form-group">
        <label class="col-xs-2 control-label">ИНН:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_jinn" placeholder="ИНН" name="INN" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['ur_inn'];?>
">
        </div>
      </div>  

       <div class="form-group">
        <label class="col-xs-2 control-label">ОГРН:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_jogrn" placeholder="ОГРН" name="OGRN" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['ur_ogrn'];?>
">
        </div>
      </div> 

       <div class="form-group">
        <label class="col-xs-2 control-label">Контактный телефон:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_jphone" placeholder="" name="cotel" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['ur_phone'];?>
">
        </div>
      </div>

      <div class="form-group">
        <label class="col-xs-2 control-label">Юридический адрес:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_jaddress" placeholder="" name="couradress" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['ur_address'];?>
">
        </div>
      </div>

      <div class="form-group">
        <label class="col-xs-2 control-label">Фактический адрес:</label>
        <div class="col-xs-10">
          <input type="text" class="form-control" id="shop_faddress" placeholder="" name="copubgeoadress" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['address'];?>
">
        </div>
      </div> 
  </div>
  
      <div id="panel3" class="tab-pane fade">


          <h3>Тариф</h3>
          <div class="form-horizontal">
              <div class="form-group">
							<label class="col-lg-3 control-label">
								<strong>Вознаграждение клиенту от чека: </strong>
							</label>
							<div class="col-lg-5">
								<input id="slideC" name="percent_client" data-slider-id='ex1Slider' type="text" data-slider-min="1" data-slider-max="20" data-slider-step="1" data-slider-value="<?php echo $_smarty_tpl->tpl_vars['row']->value['payvalue'];?>
"/>
							</div>
							<label class="col-lg-3 control-label text-left">
								<span id="demo-range-def-val"><?php echo $_smarty_tpl->tpl_vars['row']->value['payvalue'];?>
 %</span>
							</label>
						</div>

						<div class="form-group">
							<label class="col-lg-3 control-label">
								<strong>Процент от покупки, который можно оплатить бонусами: </strong>
							</label>
							<div class="col-lg-5">
								<input id="slidePerPay" name="bonusborderpercent" data-slider-id='ex2Slider' type="text" data-slider-min="3" data-slider-max="100" data-slider-step="1" data-slider-value="<?php echo $_smarty_tpl->tpl_vars['row']->value['bonusborderpercent'];?>
"/>
							</div>
							<label class="col-lg-3 control-label text-left">
								<span id="val-slidePerPay"><?php echo $_smarty_tpl->tpl_vars['row']->value['bonusborderpercent'];?>
 %</span>
							</label>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">
								<strong>Всего процент от чека: </strong>
							</label>
							<div class="col-lg-5">
								<input id="slideAll" name="percent_all" data-slider-id='ex2Slider' type="text" data-slider-min="3" data-slider-max="22" data-slider-step="1" data-slider-value="<?php echo $_smarty_tpl->tpl_vars['row']->value['payvalue']*1.338;?>
"/>
							</div>
							<label class="col-lg-3 control-label text-left">
								<span id="demo-range-step-val"><?php echo $_smarty_tpl->tpl_vars['row']->value['payvalue']*1.338;?>
 %</span>
							</label>
						</div>
						<br>
						<div class="form-group">
							<label class="col-lg-3 control-label">Максимальное число бонусов, которые можно отдать за покупку</label>
							<div class="col-lg-7">
								<input id="realAddress" type="text" placeholder="в копейках" name="bonusbordervalue" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['bonusbordervalue'];?>
">
								<i class="ion-location"></i>
							</div>
						</div>
						<h4 class="text-main mar-btm">Вознаграждение сотруднику</h4>
						<div class="form-group">
							<label class="col-lg-3 control-label">За подключение клиента</label>
							<div class="col-lg-7">
								<input id="realAddress" type="text" placeholder="Размер вознаграждения фиксированны" name="paytomanbynewuser" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['paytomanbynewuser'];?>
">
								<i class="ion-location"></i>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">За выполнение операции (проводка чека)</label>
							<div class="col-lg-7">
								<input id="realAddress" type="text" placeholder="Размер вознаграждения фиксированны" name="paytomanbyoper" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['paytomanbyoper'];?>
">
								<i class="ion-location"></i>
							</div>
						</div>
									<?php echo '<script'; ?>
>
										var clientSlider = $('#slideC').slider({
											formatter: function(value) {
												return 'Процент от чека: ' + value + ' %';
											}
										});
										var perPaySlider = $('#slidePerPay').slider({
											formatter: function(value) {
												return 'Процент от покупки, который можно оплатить бонусами: ' + value + ' %';
											}
										});
										var allSlider = $('#slideAll').slider({
											formatter: function(value) {
												return 'Итого: ' + value + ' %';
											}
										});
										clientSlider.change(function(e) {
											allSlider.slider("setValue", Math.ceil(e.value.newValue * 1.338));
											$("#demo-range-def-val").text(e.value.newValue+" %");
											$("#demo-range-step-val").text(Math.ceil(e.value.newValue * 1.338) +" %");
										});
										perPaySlider.change(function(e) {
											$("#val-slidePerPay").text(e.value.newValue + " %");
										});
										allSlider.change(function(e) {
											clientSlider.slider("setValue", Math.ceil(e.value.newValue / 1.338));
											$("#demo-range-step-val").text(e.value.newValue+" %");
											$("#demo-range-def-val").text(Math.ceil(e.value.newValue / 1.338) +" %");
										});
									<?php echo '</script'; ?>
>


      </div>
      </div>
      <div id="panel4" class="tab-pane fade">
        <h3>Модерация</h3>
        <div class="form-horizontal">
              <div class="form-group">
                  <input type="radio" name="moderator_flag" value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['moderator_flag'] == 1) {?>checked<?php }?>> Одобрено<br>
                  <input type="radio" name="moderator_flag" value="2" <?php if ($_smarty_tpl->tpl_vars['row']->value['moderator_flag'] == 2) {?>checked<?php }?>> Отклонено<br>
                  <input type="radio" name="moderator_flag" value="3" <?php if ($_smarty_tpl->tpl_vars['row']->value['moderator_flag'] == 3) {?>checked<?php }?>> Архив <br>
                  <input type="radio" name="moderator_flag" value="4" <?php if ($_smarty_tpl->tpl_vars['row']->value['moderator_flag'] == 4) {?>checked<?php }?>> Заморожено
            </div>
        </div>
      </div>
<!-- ***********************  -->
 </div>


    

  </div>
</div> 
</div>
    </form>
</div>

<?php echo '<script'; ?>
>
    $( '#comp_finish' ).click(function(event) {
        event.preventDefault();

        var formData = new FormData($('#set_shop_form')[0]);

        $.ajax({
            url: 'ajax/adm_set_shop.php',
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                console.log(data);
                $("#after").show();
            }
        });
    });
<?php echo '</script'; ?>
>


 
  <?php echo '<script'; ?>
 type="text/javascript">
      $(function () {
          $('#datetimepicker1').datetimepicker( {pickTime: true, language: 'ru'});

          $('input[type=file]').change(function(){files = this.files;});

          $('#shop_cat').val(arr_cat_sel);
          $('#shop_cat').selectpicker("refresh");

         
         /* $('#shop_cat').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
            var selected = $(e.currentTarget).val();
            var fl=0;
            var length = selected.length;
            if (clickedIndex==0){
              $('#shop_cat').val(1);
              $('#shop_cat').selectpicker("refresh");
            }else{
              for(var i = 0; i < length; i++) {
                if ((selected[i]==1)|(selected[i]=="1")){

                  delete(selected[i]);

                  $('#shop_cat').val(selected);
                  $('#shop_cat').selectpicker("refresh");
                }               
            }
            }
          
          });*/

      });
  <?php echo '</script'; ?>
>
  <?php }
}
?>