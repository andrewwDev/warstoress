<?php /* Smarty version 3.1.27, created on 2017-09-10 21:29:51
         compiled from "/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_main.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:134092199759b5a0bf2693f4_53590980%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '30c240cc865b401061e0eb187a0798ad6f9dad0f' => 
    array (
      0 => '/var/www/u0413200/data/www/warstores.net/ws-panel/templates/adm_main.tpl',
      1 => 1505075385,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '134092199759b5a0bf2693f4_53590980',
  'variables' => 
  array (
    'description' => 0,
    'keywords' => 0,
    'noindexflag' => 0,
    'isadmin' => 0,
    'cmd' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59b5a0bf555242_48267003',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59b5a0bf555242_48267003')) {
function content_59b5a0bf555242_48267003 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '134092199759b5a0bf2693f4_53590980';
?>
<!DOCTYPE html>
<html lang="ru">
  <head>
    <title>Панель управления</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['description']->value;?>
" />
    <meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['keywords']->value;?>
" />
    <?php if (isset($_smarty_tpl->tpl_vars['noindexflag']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['noindexflag']->value == '1') {?>
    <meta name="ROBOTS" content="noindex,nofollow" />
    <?php }?>
    <?php }?>

    <meta name="ROBOTS" content="noindex,nofollow" /> <!--TODO!!!!-->

    <?php echo '<script'; ?>
 type='text/javascript' src='js/jquery-2.1.4.min.js'><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type='text/javascript' src='js/adm.js'><?php echo '</script'; ?>
>
    
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <?php echo '<script'; ?>
 type="text/javascript" src="js/moment-with-locales.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="js/bootstrap-datetimepicker.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="js/bootstrap-select.min.js"><?php echo '</script'; ?>
>

    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="css/bootstrap-select.min.css" />
    <link href="images/setting.ico" rel="shortcut icon" type="image/png" />
    <link href="css/jquery-ui.css" rel="css/stylesheet" media="screen"></link>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <?php echo '<script'; ?>
 src="../../assets/js/html5shiv.js"><?php echo '</script'; ?>
>
      <?php echo '<script'; ?>
 src="../../assets/js/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    <link href="css/golos.css" rel="stylesheet" media="screen">
  </head>
  <body>

    <style>
    .mymenu{
      margin: 5px;
      width: 140px;
    }
    </style>
<?php if ($_smarty_tpl->tpl_vars['isadmin']->value == '1') {?>
<table><tr><td style="vertical-align:top; width:150px;">
<!--Меню -->
<img src="http://warstores.net/ws-panel/images/ws_site_logo.png" style="width:150px; mergin-bottom:10px;">
<button type="button" class="btn btn-panel mymenu" onclick="window.location.href='http://warstores.net/ws-panel/adm.php?cmd=static';">Статистика</button>
<button type="button" class="btn btn-panel mymenu" onclick="window.location.href='http://warstores.net/ws-panel/adm.php?cmd=news';">Новости</button>
<button type="button" class="btn btn-panel mymenu" onclick="window.location.href='http://warstores.net/ws-panel/adm.php?cmd=shops';">Магазины</button>
<button type="button" class="btn btn-panel mymenu" onclick="window.location.href='http://warstores.net/ws-panel/adm.php?cmd=users';">Пользователи</button>
<button type="button" class="btn btn-panel mymenu" onclick="window.location.href='http://warstores.net/ws-panel/adm.php?logout';">Выход</button>
<!--КОНЕЦ Меню -->
</td><td style="vertical-align:top; width:100%">
<!-- Основной блок -->
<div style="padding:5px; background-color:#FFF; border:1px solid #E6E6E6;" id="maincontent">

            <?php if ($_smarty_tpl->tpl_vars['cmd']->value == 'pg') {
echo $_smarty_tpl->getSubTemplate ("adm_".((string)$_smarty_tpl->tpl_vars['page']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>
 <?php }?>

</div>
<!-- КОНЕЦ Основной блок -->
</td></tr>
</table>
<?php } else { ?>
  <?php echo $_smarty_tpl->getSubTemplate ("adm_login.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }?>

<input type="hidden" id="sorttype" value="1">
<input type="hidden" id="lotlistmode" value="0">
<input type="hidden" id="userlistmode" value="0">



<div id="myModalBox" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Идет отправка данных на сервер</h4>
      </div>
      <!-- Основной текст сообщения -->
      <div class="modal-body" style="width:100%">
        <center><img src="http://breakbox.ru/images/loading.gif"><br><br><b>Сохранение...</b></center>
      </div>
    </div>
  </div>
</div>



<?php echo '<script'; ?>
 src="js/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
  $(document).ready(function(){

  });
<?php echo '</script'; ?>
>
</body>
</html><?php }
}
?>