﻿<?php
//Вернуть список каталогов для списка магазинов
//Авторизация не требуется. Информация возвращается любому обратившемуся
require_once("dbconfiguration.php");  //Подключаемся к базе
require_once("fn_gettblver.php");
	  //Функция, возвращающая актуальную версию таблицы

function mysqli_result($res, $row, $field=0) {
    $res->data_seek($row);
    $datarow = $res->fetch_array();
    return $datarow[$field];
}

$reg_id=0;

$ver = 0;
if($_POST){
	
$reg_id=$_POST['reg'];	//Номер региона
	
$ver=0+$_POST['ver'];

}else{
	
if (isset($_GET['reg']))$reg_id=$_GET['reg'];	
	
if (isset($_GET['ver']))$ver=0+$_GET['ver'];

}


$jsonData = array();

$jsonData["ver"]=getTblVer("catalogs");

if ($ver!=$jsonData["ver"]){
//Проверим текущую версию в базе
	//Этот запрос возвращает магазины из всех регионов
	//$query = "SELECT t2.cid, t3.title, t3.img_ver, count(t2.sid) as catcount FROM wsq_shopsincat as t2, wsq_catshop as t3 WHERE t3.csid=t2.cid AND t2.sid IN (SELECT sid FROM wsq_shops WHERE moderator_flag=1) GROUP BY t2.cid ORDER BY t3.title";
	
	//Закоментировано. Эти запросы позволяют получить магазин из конкретного региона
	
if ($reg_id==0){
		
$query = "SELECT t2.cid, t3.title, t3.img_ver, count(t2.sid) as catcount FROM wsq_shopsincat as t2, wsq_catshop as t3 WHERE t3.csid=t2.cid and t2.sid IN (SELECT sid FROM wsq_shops WHERE moderator_flag=1) GROUP BY t2.cid ORDER BY t3.title";
	}else{
		
$query = "SELECT t2.cid, t3.title, t3.img_ver, count(t1.sid) as catcount FROM wsq_shopsinreg as t1, wsq_shopsincat as t2, wsq_catshop as t3 WHERE (t1.city_id=$reg_id or t1.reg_id=$reg_id) and t2.sid=t1.sid  and t3.csid=t2.cid and t2.sid IN (SELECT sid FROM wsq_shops WHERE moderator_flag=1) GROUP BY t2.cid ORDER BY t3.title";
	
}
	
$res = mysqli_query($tmpres, $query);

	
$flres=false;
	
$jsonData["cats"]=array();


$col = 0;	
if ($res!=false){
		
if (mysqli_num_rows($res)>0){
			
while ($row=mysqli_fetch_assoc($res)){
				
$jsonData["cats"][] = $row;
	
$col++;		
}

			
$flres=true;
	
	}
	
	
}
if($col==0){
$query = "SELECT t2.cid, t3.title, t3.img_ver, count(t2.sid) as catcount FROM wsq_shopsincat as t2, wsq_catshop as t3 WHERE t3.csid=t2.cid and t2.sid IN (SELECT sid FROM wsq_shops WHERE moderator_flag=1) GROUP BY t2.cid ORDER BY t3.title";
$res = mysqli_query($tmpres,$query);


if ($res!=false){

if (mysqli_num_rows($res)>0){

//echo 'null';
while ($row=mysqli_fetch_assoc($res)){
				
$jsonData["cats"][] = $row;
	
$col++;		
}

			
$flres=true;
	
	
}

}

}

	//считаем количество магазинов
    
if ($reg_id == 0){
 
       $summ_query="SELECT sid FROM wsq_shops WHERE moderator_flag=1";
    
}else if($col==0){

	 
$summ_query="SELECT sid FROM wsq_shops WHERE moderator_flag=1";
}
else{
$summ_query="SELECT wsq_shopsinreg.sid ,wsq_shops.sid FROM wsq_shopsinreg,wsq_shops WHERE wsq_shopsinreg.reg_id = $reg_id AND wsq_shops.moderator_flag=1 AND wsq_shopsinreg.sid = wsq_shops.sid";
}	

$summ_result=mysqli_query($tmpres,$summ_query);
	
$summ=mysqli_num_rows($summ_result);


	
$cat1[]=array('cid' => 1, 'title' => "Все категории",'img_ver' => 1,'catcount' => $col);
	
$jsonData["cats"]=array_merge( $cat1, $jsonData["cats"] );
}


echo json_encode($jsonData, JSON_UNESCAPED_UNICODE);



mysqli_close($tmpres); 
?>