<?php
//Скрипт перерасчета рангов за текущий период.
//Алгоритм работы:
//1 Строится вспомогательная таблица wsq_xxx_rank, куда на основании данных wsq_transaction за прошлый месяц выгружаются сумма плдученых бонусов и сумма реферальных начисления
//2 Извлекаем данные из wsq_xxx_rank во вспомогательные массивы
//3 Извлекаем строку с id пользователями-рефералами во вспомогательный массив $user_friends из таблицы wsq_user_ref_str
//4 В цикле for проходимся по рангам с 1 по ч и проверяем выполнение условия повышения ранга
//5 Записываем новые ранги в таблицу wsq_users

set_time_limit(24*60*60);//Увеличиваем максимально евремя работы скрипта в сек
$start = microtime(true); 
echo "Запущен скрипт пересчета рангов... <br>\n";

$uid=0; //ID менеджера, который провел оплату
$sid="";//

if($_POST){
	if (isset($_POST['uid'])){$uid=0+$_POST['uid'];}
	if (isset($_POST['sid'])){$sid=$_POST['sid'];}
}else{
	if (isset($_GET['uid'])) {$uid=0+$_GET['uid'];}
	if (isset($_GET['sid'])) {$sid=$_GET['sid'];}
}

require_once("dbconfiguration.php");  //Подключаемся к базе
require_once("fn_gettblver.php");	  //Функция, возвращающая актуальную версию таблицы
//require_once("checksid.php");  //Проверяем пользователя



//ЧИСТИМ ТАБЛИЦУ
$res = mysql_query("DELETE FROM wsq_xxx_rank");

//
//$query = "INSERT INTO wsq_xxx_rank (uid, summ, step, rank) (SELECT uid, sum(summ) as summ, 0 as step, 0 as rank FROM wsq_transaction WHERE troper=55 group by uid)";
//Все за прошлый месяц
$query =   "INSERT INTO wsq_xxx_rank (uid, summ, step, rank) (SELECT uid, sum(summ) as summ, 0 as step, 0 as rank FROM wsq_transaction WHERE troper=55  and date > LAST_DAY( DATE_SUB( CURDATE( ) , INTERVAL 2 MONTH ) ) + INTERVAL 1 DAY AND date < DATE_ADD( LAST_DAY( CURDATE( ) - INTERVAL 1 MONTH ) , INTERVAL 1 DAY ) group by uid)"; 
$res = mysql_query($query);

$query = "DELETE FROM wsq_xxx_rank WHERE summ<".$rank_bonus_usr[1]; 
$res = mysql_query($query);

//$query = "UPDATE wsq_xxx_rank left join (SELECT uid, sum(summ) as summref2 FROM wsq_transaction WHERE troper=56 group by uid) b on wsq_xxx_rank.uid = b.uid set wsq_xxx_rank.summref=b.summref2";
// Все за прошлый месяц
$query = "UPDATE wsq_xxx_rank left join (SELECT uid, sum(summ) as summref2 FROM wsq_transaction WHERE troper=56 and date > LAST_DAY( DATE_SUB( CURDATE( ) , INTERVAL 2 MONTH ) ) + INTERVAL 1 DAY AND date < DATE_ADD( LAST_DAY( CURDATE( ) - INTERVAL 1 MONTH ) , INTERVAL 1 DAY ) group by uid) b on wsq_xxx_rank.uid = b.uid set wsq_xxx_rank.summref=b.summref2";

$res = mysql_query($query);

 
 //Извлекаем данные из wsq_xxx_rank во вспомогательные массивы
$user_summ=array();
$user_summref=array();
$user_rank=array();
$res = mysql_query("SELECT * FROM wsq_xxx_rank");
if ($res!=false){
	if (mysql_num_rows($res)>0){
		while($row=mysql_fetch_array($res)){
			$userid=$row['uid'];
			$user_summ[$userid]=$row['summ'];
			$user_summref[$userid]=$row['summref'];
			$user_rank[$userid]=$row['rank'];
		}
	}	
}
//Достаем всех рефералов пользователя во вспомогательный массив
$user_friends=array();
$res = mysql_query("SELECT uid,friends1,friends2,friends3,friends4,friends5 FROM wsq_user_ref_str");
if ($res!=false){
	if (mysql_num_rows($res)>0){
		while($row=mysql_fetch_array($res)){
			$userid=$row['uid'];
			$user_friends[$userid]="".$row['friends1'].",".$row['friends2'].",".$row['friends3'].",".$row['friends4'].",".$row['friends5'];
		}
	}	
}

//Проверка в цикле условий ранга
 for ($rank_i=1; $rank_i<4 ; $rank_i++) { 
 	foreach ($user_rank as $u_id => $rank) {
 		
 		if (($user_summ[$u_id]>=$rank_bonus_usr[$rank_i])and($user_summref[$u_id]>=$rank_bonus_ref[$rank_i])){//Условие по бонусам
 			//Проверяем условие по рефералам			 			
 			$refs=0;
 			if (isset($user_friends[$u_id])){
 				$arr=explode(',', $user_friends[$u_id]);
 				$imax=count($arr);
 				for ($i=0; $i < $imax; $i++) { 
					if (strlen($arr[$i])>0){
						$rid=$arr[$i];
						if (isset($user_rank[$rid])){ //проверяем, есть ли вообще у юзера ранг (если нет, то значит не было выплат в периоде и он 0)
							if ($user_rank[$rid]>=($rank_i-1)){$refs=$refs+1;}						
						}else{//Это пользователь с нулевым рангом
							if ($rank_i==1){$refs=$refs+1;} //Этот пользователь учитывается только при расчете ранга 1
						}
						//Проверяем условие присвоение нового ранга
						if ($refs>=$rank_ref_usr[$rank_i]){
							$user_rank[$u_id]=$rank_i;
							echo "Повышен ранг пользователя $u_id до $rank_i<br>\n";
							break;
						}
					}
				}
 			}
 		}
 	}
 }
 //Записываем пересчитанные ранги в таблицы wsq_xxx_rank и wsq_users

//Обновляем ранги в базе в соответствии с новыми данными
$res = mysql_query("UPDATE wsq_users SET rank=0");
foreach ($user_rank as $u_id => $rank) {
	if ($rank>0){
		$res = mysql_query("UPDATE wsq_users SET rank=$rank WHERE uid=$u_id");
		//$res = mysql_query("UPDATE wsq_xxx_rank SET rank=$rank WHERE uid=$u_id"); //Это просто отладка. Эта таблица нигде не используется, кроме этого скрипта. Это строка позволяет наглядно посмотреть результат пересчета
	}
}
echo "Скрипт выполнен<br>\n";
echo 'Время выполнения скрипта: '.(microtime(true) - $start)." сек.<br>\n";
echo "\n<br>max_time=".ini_get('max_execution_time')." сек.<br>\n";
?>