<?php
//Вернть список магазинов пользователя
$uid=0;
$sid="";
if($_POST){
	$uid=0+$_POST['uid'];
	$sid=$_POST['sid'];
}else{
	$uid=0+$_GET['uid'];
	$sid=$_GET['sid'];
}
require_once("dbconfiguration.php");  //Подключаемся к базе
require_once("checksid2.php");  //Проверяем пользователя


$jsonData = array();
$jsonData["status"]=1;

$s_id=array(); //sid
$s_title=array(); //title
$s_nick=array(); //nick
$s_type=array(); //тип магазина 1 - владелец, 2 - сотрудник

$query = "SELECT sid, title, IFNULL(shopnick,'#') as shopnick FROM wsq_shops WHERE ownerid=$uid AND moderator_flag=1";
$res = mysqli_query($tmpres, $query);
$j=0;
if ($res!=false){
	if (mysqli_num_rows($res)>0){
		while ($row=mysqli_fetch_assoc($res)){
			$s_id[$j]=$row["sid"];
			$s_title[$j]=str_replace(",", "", $row["title"]);
			$s_nick[$j]=str_replace(",", "",$row["shopnick"]);
			$s_type[$j]=1;
			$j=$j+1;
		}
	}	
}
//Достаем магазины, где пользователь числится сотрудником.
$query = "SELECT DISTINCT t1.sid, t1.title, IFNULL(t1.shopnick,'#') as shopnick FROM wsq_shops as t1, wsq_managers as t2 WHERE t1.sid=t2.shopid and t2.uid=$uid";
$res = mysqli_query($tmpres, $query);
if ($res!=false){
	if (mysqli_num_rows($res)>0){
		while ($row=mysqli_fetch_assoc($res)){
			$s_id[$j]=$row["sid"];
			$s_title[$j]=str_replace(",", "", $row["title"]);
			$s_nick[$j]=str_replace(",", "",$row["shopnick"]);
			$s_type[$j]=2;
			$j=$j+1;
		}
	}	
}

$jsonData["s_count"]=$j;
$jsonData["s_id"]=join(",",$s_id); //sid
$jsonData["s_title"]=join(",",$s_title); //title
$jsonData["s_nick"]=join(",",$s_nick); //nick
$jsonData["s_type"]=join(",",$s_type); //nick


echo json_encode($jsonData, JSON_UNESCAPED_UNICODE);

mysqli_close($tmpres); 
?>