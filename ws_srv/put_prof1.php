<?php
//Сохранить изменение профиля

$uid=0; //ID менеджера, который провел оплату
$sid=0;
$name="";
$surname="";
$email="";
$birthday="";
$phone="";
$city="";
$pwdhash="";
$notif_flags=0;
$referer="";

if($_POST){
	if (isset($_POST['uid'])){$uid=0+$_POST['uid'];}
	if (isset($_POST['notif_flags'])){$notif_flags=0+$_POST['notif_flags'];}
	if (isset($_POST['sid'])){$sid=$_POST['sid'];}
	if (isset($_POST['name'])){$name=$_POST['name'];}
	if (isset($_POST['surname'])){$surname=$_POST['surname'];}
	if (isset($_POST['email'])){$email=$_POST['email'];}
	if (isset($_POST['birthday'])){$birthday=$_POST['birthday'];}
	if (isset($_POST['phone'])){$phone=$_POST['phone'];}
	if (isset($_POST['city'])){$city=$_POST['city'];}
	if (isset($_POST['pwdhash'])){$pwdhash=$_POST['pwdhash'];}
	if (isset($_POST['referer'])){$referer=$_POST['referer'];}
}

require_once("dbconfiguration.php");  //Подключаемся к базе
require_once("checksid.php");  //Проверяем пользователя
require_once("fn_gettblver.php");	  //Функция, возвращающая актуальную версию таблицы
require_once("fn_getuidfromrefnum.php"); //Подключаем функции для извлечения id реф мастера
require_once("fn_update_ref_struct.php"); //Подключаем функции пересборки реф структуры (поле ref_struct в wsq_users)
$name=mysql_real_escape_string($name);
$surname=mysql_real_escape_string($surname);
$email=mysql_real_escape_string($email);
$birthday=mysql_real_escape_string($birthday);
$pwdhash=mysql_real_escape_string($pwdhash);
$phone=mysql_real_escape_string($phone);
$city=mysql_real_escape_string($city);


$errmsg="";
$err=0;
//Проверка на то, что все заполнено корректно
	

	$email_tmp = str_replace(array("@",".","-","_"), "", $email);
	$birthday_tmp = str_replace(array("/",".","-"), "", $birthday);
	//Опасные символы
	$vowels = array("'", "`", "\"", "\\", "/", "--", "==","%","#","&","*","\n","\r","\t");		
	//Проверка корректности присланных данных
if ($email!="null"){
	if (strlen($email)>0){
		if(!preg_match("/^[a-zA-Z0-9]+$/",$email_tmp)){
		    $errmsg = "Email содержит недопустимые символы.";
		}elseif(strpos($email,"--")>0){
			$errmsg = "Email содержит недопустимые символы.";
		}elseif(strpos($email,"@")<=0){
			$errmsg = "Email задан неверно.	";
		}
		$emailstr=", email='$email'";
	}else{
		$emailstr="";
	}
}


	if(!preg_match("/^[\d]+$/",$birthday_tmp)){	    	
	    $errmsg = "Дата рождения содержит недопустимые символы. $birthday_tmp";
	    if (strlen($birthday_tmp)==0){$errmsg = "";}
	}

	if(strlen($birthday)>10){
		$errmsg = "Дата рождения задана неверно.";
	}elseif(preg_match("/[^(\w)|(\x7F-\xFF)|(\s)]/",$name)){
		$errmsg = "Имя содержит недопустимые символы.";
	}elseif(preg_match("/[^(\w)|(\x7F-\xFF)|(\s)]/",$surname)){
		$errmsg = "Фамилия содержит недопустимые символы.";
	}	
//Преобразуем дату к формату MySQL
if (strlen($birthday)>0){
	$birthday_arr=explode(".", $birthday);
	if (count($birthday_arr)==3){$birthday=$birthday_arr[2]."-".$birthday_arr[1]."-".$birthday_arr[0];}
}

if (strlen($errmsg )==0){
	if (strlen($pwdhash)>0){
		$pwdstr=", pwdhash='$pwdhash'";
	}else{
		$pwdstr="";
	}
	if (strlen($phone)>0){
		$phonestr=", phone='$phone'";
	}else{
		$phonestr="";
	}

	$refererstr="";
	$ref_fl=0;
	if (strlen($referer)>0){
		$ref_id=getrefid($referer);//Декодируем id мастера.
		if ($ref_id>0){
			//Проверяем, что это поле у пользователя еще не задано. Чтобы пресечь хакерское переписывание структуры.
			$query="SELECT refid FROM wsq_users WHERE uid=$uid";
			$res = mysql_query($query);			
			if ($res!=false){
			  if (mysql_num_rows($res)>0){
				while($row=mysql_fetch_array($res)){
					if ((0+$row['refid'])>0){
						$ref_fl=1;//Реф мастер задан
						break;
					}
				}				
			  } 
			}

			//Подобный блок в singup
			//Проверяем, что текущий пользователь отсутствует в реферальной структуре мастера, чтобы избежать зацикливания
			$rs="";
			$query="SELECT ref_struct_full FROM wsq_users WHERE uid=$ref_id";
			$res = mysql_query($query);			
			if ($res!=false){
			  if (mysql_num_rows($res)>0){
				while($row=mysql_fetch_array($res)){
					$rs=$row['ref_struct_full'];
					$errmsg=$errmsg."Нельзя порекомендовать вашему рекомендателю";
				}				
			  } 
			}
			$rs_arr=explode(",", $rs);
			if (in_array($uid,$rs_arr)){$ref_fl=1;} //Пользователь находится в реферальной структуре и возможна закольцовка
			//Конец проверки

			if ($ref_fl==0){
				$refererstr=", refid=$ref_id";	
			}

		} 		
	}

	$res = mysql_query("UPDATE wsq_users SET name='$name', surname='$surname', birthday ='$birthday', city ='$city', notifications=$notif_flags $emailstr $pwdstr $phonestr $refererstr WHERE uid=$uid");		

	if ($ref_fl==0){//Надо пересобрать реф структуру этого пользователя, поскольку был изменен реф мастер
		$ref=update_ref_struct($uid);		
		$ref_arr=$ref[0]; //Массив с id пользователями над этим
		if (count($ref_arr)>0){
			$res = mysql_query("INSERT INTO wsq_user_ref_str (uid, friends1) VALUE ($uid, '')");
			for ($i=0; $i <count($ref_arr) ; $i++) { 
				mysql_query("UPDATE wsq_user_ref_str SET friends_count=friends_count+1, friends".($i+1)."_count=friends".($i+1)."_count+1, friends".($i+1)."=CONCAT(friends1,',".$uid."') WHERE uid=".$ref_arr[$i]);	
			}
		}
	}

	if ($res==false){
		echo "[{\"status\":\"0\", \"err\":\"1\", \"errmsg\":\"Ошибка при записи в базу.\"}]";
	}else{
		echo "[{\"status\":\"1\", \"err\":\"0\"}]";
	}
}else{
	echo "[{\"status\":\"0\", \"err\":\"1\", \"errmsg\":\"$errmsg\"}]";
}


?>