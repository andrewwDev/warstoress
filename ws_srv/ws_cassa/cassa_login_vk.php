<?php
//Скрипт достает vk_id пользователя по его токену.
$vkid='';
$token='';
$email='';
if($_GET){
    if (isset($_GET['vkid'])){$vkid=$_GET['vkid'];}
    if (isset($_GET['token'])){$token=$_GET['token'];}
    if (isset($_GET['email'])){$email=$_GET['email'];}
}

function get_vkuid_by_token($token){
	$url="https://api.vk.com/method/users.get?access_token=$token&fields=uid,first_name,last_name,bdate,photo_100,photo_max";
	## запрос 
      $ch = curl_init();     
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HEADER, true); // получить список заголовков
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');   
      $result = curl_exec($ch);
      curl_close($ch);
    ## Парсим ответ    
      //echo $result;
      $res=NULL;
      $i=strpos($result,'{"response":');
      if ($i>0){
      	$i=$i+13;
      	$res=substr($result, $i,-2);
      	$res=json_decode($res,	JSON_UNESCAPED_UNICODE);
      	$birthday_arr=explode(".", $res["bdate"]);
		if (count($birthday_arr)==3){$res["bdate"]=$birthday_arr[2]."-".$birthday_arr[1]."-".$birthday_arr[0];}
      }   
      if (is_null($res)){
      	$res=array("uid"=>0, "first_name"=>"", "last_name"=>"", "bdate"=>"", "photo_100"=>"", "photo_max"=>"");
      }   
      return $res;
  }


//Проверяем на корректность полученные данные
$errmsg = "";
$err=0;
if(!preg_match("/^[a-zA-Z0-9]+$/",$vkid)){
	$errmsg = "Не найден пользователь вконтакте.";
	$err=2;
}
if(!preg_match("/^[a-zA-Z0-9]+$/",$token)){
	$errmsg = "Не найден пользователь вконтакте.";
	$err=2;
}
$vowels = array("--", "\\", "%");
$email=str_replace("--", "", $email);
$vowels = array("_", ".", "@","-");
$email1 = str_replace($vowels, "", $email);

if(!preg_match("/^[a-zA-Z0-9]+$/",$email1)){
	$errmsg = "Не найден пользователь вконтакте.";
	$err=2;
}

$vk_user=get_vkuid_by_token($token);

if ($vk_user["uid"]==0){
	$errmsg = "Не найден пользователь вконтакте.";
	$err=2;
	echo "[{\"status\":\"0\", \"err\":\"$err\", \"errmsg\":\"$errmsg\"}]";
	exit;
}
if ($vk_user["uid"]!=$vkid){
	$errmsg = "Id пользователя не соответствует токену.";
	$err=2;
	echo "[{\"status\":\"0\", \"err\":\"$err\", \"errmsg\":\"$errmsg\"}]";
	exit;
}

require_once("fn_generatecode.php"); //Подключаем функцию, генерирующую код активации
require_once("fn_getsid.php");  	 //Подключаем функцию, генерирующую sid
require_once("dbconfiguration.php"); //Подключаемся к базе
require_once("fn_generatecode.php"); //Подключаем функцию, генерирующую код активации

$email=mysql_real_escape_string($email);
$token=mysql_real_escape_string($token);



$query = "SELECT uid, active, status, name, surname, rank, vk_photo_100, vk_photo_big, guild_id, guild_title, notifications, birthday, phone, email, refid FROM wsq_users WHERE vk_uid='".$vk_user["uid"]."'";
$res = mysql_query($query);
$uid=-1;
if ($res!=false){
	if (mysql_num_rows($res)>0){
		$row=mysql_fetch_array($res);		
		$uid=0+$row['uid'];
		$active=0+$row['active'];
		$ustatus=0+$row['status'];
		$urank=0+$row['rank'];
		$guild_id=0+$row['guild_id'];
		$uphoto=$row['vk_photo_100'];
		$uphoto_big=$row['vk_photo_big'];
		$name=$row['name'];
		$surname=$row['surname'];
		$guild_title=$row['guild_title'];
		$notifications=$row['notifications'];
		$birthday=$row['birthday'];
		$phone=$row['phone'];
		$email=$row['email'];
		$ref_uid=0+$row['refid'];
		$sidhash=getsid();
		
		$uname=trim("$name $surname");
		if (strlen($uname)<2){$uname=$login;}

		$ipadr=$_SERVER['REMOTE_ADDR'];
		$query = "INSERT INTO wsq_session_cassa (uid,sidhash,ip,logintime) values ($uid, '$sidhash' , '$ipadr', now())";
		$res = mysql_query($query);	
	}	
}

if ($uid==-1){
//Пробуем зарегистрировать нового пользователя
	$login="vk_".$vk_user["uid"]; //Обычный login не может содержать '_'
	$name=$vk_user["first_name"];
	$surname=$vk_user["last_name"];
	$birthday=$vk_user["bdate"]; //ЧЧ.ММ.ГГГГ
	$photo_100=$vk_user["photo_100"];
	$photo_big=$vk_user["photo_max"];

	$notifications=0;
	$ref_uid=0;
	$guild_id=0;
	$uphoto=$photo_100;
	$uphoto_big=$photo_big;
	$guild_title="";
	$urank=0;
	$active=1;//ВК пользователь активирован по-умолчанию;
	$uname=trim("$name $surname");
	if (strlen($uname)<2){$uname=$login;}

	//Регистрируем нового пользователя.
	$pwd=generate_password(12);
	$pwdhash=md5($pwd);
	$code=generatecode(7);
	/////////////Отправляем email
	/*	$subject = "WarStores: активация пользователя"; 
		$message = ' <p>Поздравляем, Вы успешно зарегистрировались в системе WarStores. Для активации запустите мобильное приложение и введите код <b>'.$code.'</b><p>Логин: '.$login.'<br> Пароль: '.$pwd.'<br><br>Код активации: '.$code.'.<p><p>С Уважением, <br> Команда WarStores';
		$headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
		$headers .= "From: noreply@warstores.net <noreply@warstores.net>\r\n"; 
		mail($email, $subject, $message, $headers); 		*/					 
	///////////
	$sidhash=getsid();

	$query = "INSERT INTO wsq_users (refid,login,email,name,surname,birthday,pwdhash,vk_uid,vk_photo_100,vk_photo_big,active) values ($ref_uid, '$login', '$email','$name','$surname','$birthday','$pwdhash','".$vk_user["uid"]."', '$photo_100', '$photo_big', 1)";
	$res = mysql_query($query);

	$query = "SELECT uid from wsq_users WHERE login='$login'";
	$res = mysql_query($query);
	$uid=-1;
	if ($res!=false){
		if (mysql_num_rows($res)>0){
			$row=mysql_fetch_array($res);
			$uid=0+$row['uid'];
		}	
	}

	$res = mysql_query("INSERT INTO wsq_user_rank (uid, shops, builds, friends, summ, summ_today, updatedate) VALUE ($uid, 0, 0, 0, 0, 0, NOW())");	//Таблица для пересчета рангов
	
	if ($uid==-1){
		$errmsg="При добавлении в базу данных информации о пользователе $login возникла ошибка.";
	}else{
		//Ссессия
		$ipadr=$_SERVER['REMOTE_ADDR'];
		$query = "INSERT INTO wsq_session_cassa (uid,sidhash,ip,logintime) values ($uid, '$sidhash' , '$ipadr', now())";
		$res = mysql_query($query);
		//код активации
		//$query = "INSERT INTO wsq_activation (uid,code,mode,created) values ($uid, '$code' , 1,now())";
		//$res = mysql_query($query);
	}


}

if ($err>0){
	echo "[{\"status\":\"0\", \"err\":\"$err\", \"errmsg\":\"$errmsg\"}]";
}else{
   //echo "[{\"status\":\"1\", \"err\":\"$err\", \"uid\":\"$uid\", \"active\":\"$active\", \"ustatus\":\"$ustatus\", \"sid\":\"$sidhash\"}]";
	echo "[{\"status\":\"1\", \"uid\":\"$uid\", \"active\":\"$active\", \"ustatus\":\"$ustatus\", \"sid\":\"$sidhash\", \"uname\":\"$uname\", \"urank\":\"$urank\", \"guild_id\":\"$guild_id\", \"guild\":\"$guild_title\", \"uphoto\":\"$uphoto\", \"uphoto_big\":\"$uphoto_big\", \"notifications\":\"$notifications\", \"name\":\"$name\", \"surname\":\"$surname\", \"birthday\":\"$birthday\", \"phone\":\"$phone\", email:\"$email\", ref_id:\"$ref_uid\"}]";
}

mysql_close(); 
?>