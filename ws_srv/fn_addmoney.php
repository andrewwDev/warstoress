<?php
//Функции для работы(пополнения) с балансом магазина. 
//При вводе денег в систему через сайт нужно вызвать add_money_to_balance($cash, $sid, $uid,  1, 1, 0)

require_once 'dbconfiguration.php';

function add_money_to_balance($cash, $sid, $uid,  $troper, $btype, $sid37) {	
	//Положить деньги на счет магазина
	//$cash - сколько
	//$sid - куда
	//$uid - кто
	//$troper - код операции (https://bitbucket.org/warstores/warstoreuser/wiki/%D0%A2%D0%B8%D0%BF%D1%8B%20%D0%A2%D1%80%D0%B0%D0%BD%D0%B7%D0%B0%D0%BA%D1%86%D0%B8%D0%B9)
	//$btype -тип бонусов (0-бонусы, 1 - деньги)
	$fl=false;
	if ($troper==1){$fl=true; $comment="Пополнение баланса."; $btype=1;}//Зачисление средств через сайт
	if ($troper==2){$fl=true; $comment="Перевод со счета владельца."; $btype=0;} //Зачисление средств со счета пользователя-владельца. Должна вызываться только из функции money_to_shop_from_user
	if ($troper==3){$fl=true; $comment="Перевод со счета другого магазина ($sid37).";}//Зачисление средств со счета другого магазина пользователя-владельца. Должна вызываться только из функции money_to_shop_from_shop
	if ($troper==12){$fl=true; $comment="Пополнение баланса администрацией WarStores."; $btype=1;}//Зачисление средств через панель администратора

	if ($fl==true){//Проверяем, что это нормальная операция
		$sid_summ=$cash;

		$res = mysql_query("INSERT INTO wsq_transaction_shop (sid,uid,mid,sid37,rec_id,troper,summ, btype, comment, date, ip) VALUE ($sid, $uid, $uid, $sid37, 0, $troper, $sid_summ,  $btype, \"".$comment." \", NOW(), '".$_SERVER['REMOTE_ADDR']."')");

		//Проверяем, есть ли у магазина невыданные бонусы и выдаем их по возможности
		$query="SELECT tid, sid,summ FROM wsq_transaction WHERE sid=$sid and paystatus=0 ORDER BY date";
		$res = mysql_query($query);
		if ($res!=false){
		  if (mysql_num_rows($res)>0){
			while($row=mysql_fetch_array($res)){
				$summ=0+$row['summ'];
				if ($sid_summ<$summ){break;}
				$sid_summ=$sid_summ-$summ;
				$res_u=mysql_query("UPDATE wsq_transaction SET paystatus=1 WHERE tid=".$row['tid']);
			}				
		  } 
		}
	}else{
		$sid_summ=0;
	}
    return $sid_summ;
  }

function money_to_shop_from_user($cash, $sid, $uid){
	//Это troper=2 - зачисление со счета пользователя-владельца, 

	//Определяем сколько денег у пользователя
	$query = "SELECT SUM(summ) FROM wsq_transaction WHERE uid=$uid";
	$user_balance=0;
	$res = mysql_query($query);
	if ($res!=false){
		if (mysql_num_rows($res)>0){
			$user_balance=mysql_result($res, 0);
		}
	}

	//Проверяем что uid владелец магазина sid
	$fl=-1;
	$query = "SELECT sid, ownerid FROM wsq_shops WHERE sid=$sid";
	$res = mysql_query($query);
	if ($res!=false){
		if (mysql_num_rows($res)>0){
			while ($row=mysql_fetch_assoc($res)){				
				if ($uid==$row["ownerid"]){$fl=1;}
			}
		}
	}

	if ($fl==1){
		if ($user_balance>=$cash){
			//Списываем со счета пользователя
			$res = mysql_query("INSERT INTO wsq_transaction (sid,uid,mid,rec_id,troper,summ,comment,date,ip) VALUE ($sid, $uid, $uid, 0, 52, -".$cash.", \"Перевод со счета владельца на счет магазина.\", NOW(), '".$_SERVER['REMOTE_ADDR']."')");
			//Заносим на счет магазина
			add_money_to_balance($cash, $sid, $uid,  2, 0,0);	
		}
		$fnres=true;
	}else{
		$fnres=false;
	}

	return $fnres;
}

function money_to_shop_from_shop($cash, $sid_to, $sid_from, $uid){
	//Это troper=3 - зачисление со счета другого магазина, 

	//Определяем, что на счету магазина 
	$shop_balance=0;
	$query = "SELECT SUM(summ) FROM wsq_transaction_shop WHERE sid=$sid_from";
	$res = mysql_query($query);
	if ($res!=false){
		if (mysql_num_rows($res)>0){
			$shop_balance=mysql_result($res, 0);
		}
	}
	//Баланс, который в настоящих деньгах
	$shop_balance_cop=0;
	$query = "SELECT SUM(summ) FROM wsq_transaction_shop WHERE sid=$sid_from and btype=1";
	$res = mysql_query($query);
	if ($res!=false){
		if (mysql_num_rows($res)>0){
			$shop_balance_money=mysql_result($res, 0);
		}
	}

	//Проверяем, что оба магазина принадлежат uid
	$fl=0;
	$query = "SELECT sid, ownerid FROM wsq_shops WHERE sid=$sid_from or sid=$sid_to";
	$user_balance=0;
	$res = mysql_query($query);
	if ($res!=false){
		if (mysql_num_rows($res)>0){
			while ($row=mysql_fetch_assoc($res)){				
				if ($uid==$row["ownerid"]){$fl=$fl+1;}
			}
		}
	}	

	if ($fl==2){
		$shop_balance_bonus=$shop_balance-$shop_balance_money;		
		if ($cash<=$shop_balance){//Хватит ли денег на счету пользователя
			$cash_money=0;
			$cash_bonus=$cash;
			if ($shop_balance_bonus<$cash_bonus){
				$cash_bonus=$shop_balance_bonus;
				$cash_money=$cash-$shop_balance_bonus;
			}
			//Списываем бонусы со счета магазина
			$res = mysql_query("INSERT INTO wsq_transaction_shop (sid,uid,mid,sid37,rec_id,troper,summ, btype, comment, date, ip) VALUE ($sid_from, $uid, $uid, $sid_to, 0, 7, -".$cash_bonus.",  0, \"Перевод бонусов на счет другого магазина\", NOW(), '".$_SERVER['REMOTE_ADDR']."')");
			$res = mysql_query("INSERT INTO wsq_transaction_shop (sid,uid,mid,sid37,rec_id,troper,summ, btype, comment, date, ip) VALUE ($sid_from, $uid, $uid, $sid_to, 0, 7, -".$cash_money.",  1, \"Перевод денег на счет другого магазина\", NOW(), '".$_SERVER['REMOTE_ADDR']."')");

			add_money_to_balance($cash_bonus, $sid_to, $uid,  3, 0, $sid_from);
			add_money_to_balance($cash_money, $sid_to, $uid,  3, 1, $sid_from);
		}
		$fnres=true;
	}else{
		$fnres=false;
	}
	return $fnres;
}



?>