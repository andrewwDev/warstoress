<?php
//Баланс магазинов пользователя
$uid=0;
$sid="";
$shopid=0;

$from=0; //Перевести деньги со счета магазина from на счет to summ копеек
$to=0;
$summ=0;
if($_POST){
	$uid=0+$_POST['uid'];
	$sid=$_POST['sid'];
	$shopid=$_POST['shopid'];
	if (isset($_POST['from'])){$from=0+$_POST['from'];}
	if (isset($_POST['to'])){$to=0+$_POST['to'];}
	if (isset($_POST['summ'])){$summ=$_POST['summ'];}

}else{
	$uid=$_GET['uid'];
	$sid=$_GET['sid'];
	$shopid=$_GET['shopid'];
}

require_once("dbconfiguration.php");  //Подключаемся к базе
require_once("checksid2.php");  //Проверяем пользователя
require_once("fn_addmoney.php"); //Перевод денег

//Пытаемся сделать запрашиваемый перевод
$summ=0+str_replace(",", ".", $summ);
if ($from!=$to){
	if ($summ>0){
		money_to_shop_from_shop($summ*100, $to, $from, $uid); //Переводим деньги со счета на счет
	}
}


$total=array(); //Потрачено всего
$bonus=array(); //Потрачено бонусов
$cash=array(); //Потрачено копеек
$date_arr=array(); //дата
$daysago_arr=array(); //прошло дней с сегодняшнего дня

$s_id=array();
$s_title=array();
$s_summ=array();

//Достаем список магазинов пользователя
$balance=0;
$fl_acess=false; //Есть ли у пользователя право запрашивать баланс
$query = "SELECT sid, title, IFNULL(shopnick,'#') as shopnick FROM wsq_shops WHERE ownerid=$uid";
$res = mysql_query($query);
$j=0;
if ($res!=false){
	if (mysql_num_rows($res)>0){
		while ($row=mysql_fetch_assoc($res)){
			$cursid=$row["sid"];
			$s_id[$j]=$cursid;
			$s_title[$cursid]=str_replace(",", "", $row["title"]);
			$s_summ[$cursid]=0;
			$j++;

		}
	}	
}
$usershops=join(",",$s_id);

//Достаем балансы магазинов
$query = "SELECT sid,sum(summ) as summ FROM wsq_transaction_shop where sid in ($usershops) GROUP BY sid";
$res = mysql_query($query);
if ($res!=false){
	if (mysql_num_rows($res)>0){
		while ($row=mysql_fetch_assoc($res)){
			$cursid=$row["sid"];
			$s_summ[$cursid]=0+$row["summ"];
			if ($cursid==$shopid){$balance=0+$row["summ"]; $fl_acess=true;}
		}
	}	
}

if ($fl_acess==true){
	$summ_before30=0; //баланс за 30 дней до рассматриваемого периода
	$query = "SELECT sum(summ) as summ FROM wsq_transaction_shop WHERE sid=$shopid and date<DATE_ADD(NOW(), INTERVAL -30 DAY)";
	$res = mysql_query($query);
	if ($res!=false){
		if (mysql_num_rows($res)>0){
			$row=mysql_fetch_assoc($res);
			$summ_before30=0+$row["summ"];		
		}	
	}

	$query = "SELECT sum(summ) as summ, btype, TO_DAYS(NOW()) - TO_DAYS(date) as daysago, troper  FROM `wsq_transaction_shop` WHERE sid=$shopid and date>=DATE_ADD(NOW(), INTERVAL -30 DAY) group by btype, daysago  ORDER BY daysago DESC";
	$res = mysql_query($query);
	$j=0;
	$curday=30;
	$bonus[0]=0;
	$cash[0]=0;
	$total[0]=$summ_before30;
	$daysago_arr[0]=30;
	if ($res!=false){
		while ($row=mysql_fetch_assoc($res)){				
				$cur_day=$row["daysago"];
				$cur_summ=$row["summ"];
				$cur_btype=$row["btype"];
				if ($curday!=$cur_day){
					$curday=$cur_day;

					$j=$j+1;
					$daysago_arr[$j]=$row["daysago"];
					$bonus[$j]=0;
					$cash[$j]=0;
					if ($j>0){$total[$j]=$total[$j-1];}else{$total[$j]=$summ_before30;}
				}
				if ($cur_btype==1){//копейки
					$cash[$j]=$cur_summ;					
				}else{//бонусы
					$bonus[$j]=$cur_summ;				
				}
				$total[$j]=$total[$j]+$cur_summ;
				
				
		}
	}
}

$jsonData = array();
$jsonData["status"]=1;
$jsonData["s_title"]=$s_title;
$jsonData["s_summ"]=$s_summ; //title
$jsonData["balance"]=$balance; //title
//Графики
$jsonData["fig_total"]=$total; //Потрачено всего
$jsonData["fig_spend_b"]=$bonus; //Потрачено бонусов
$jsonData["fig_spend_c"]=$cash; //Потрачено копеек
$jsonData["fig_dayago"]=$daysago_arr; //прошло дней с сегодняшнего дня

echo json_encode($jsonData, JSON_UNESCAPED_UNICODE);

mysql_close(); 
?>