<?php
//Присоединить аккаунт VK к профилю пользователя
//Скрипт достает vk_id пользователя по его токину.
$vkid='';
$token='';
$email='';
$uid=0;
$sid="";

if($_GET){
    if (isset($_GET['vkid'])){$vkid=$_GET['vkid'];}
    if (isset($_GET['token'])){$token=$_GET['token'];}
    if (isset($_GET['email'])){$email=$_GET['email'];}
    if (isset($_GET['uid'])){$uid=0+$_GET['uid'];}
    if (isset($_GET['sid'])){$sid=$_GET['sid'];}
}

function get_vkuid_by_token($token){
	$url="https://api.vk.com/method/users.get?access_token=$token&fields=uid,first_name,last_name,bdate,photo_100,photo_max";
	## запрос 
      $ch = curl_init();     
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HEADER, true); // получить список заголовков
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');   
      $result = curl_exec($ch);
      curl_close($ch);
    ## Парсим ответ    
      //echo $result;
      $res=NULL;
      $i=strpos($result,'{"response":');
      if ($i>0){
      	$i=$i+13;
      	$res=substr($result, $i,-2);
      	$res=json_decode($res,	JSON_UNESCAPED_UNICODE);
      	$birthday_arr=explode(".", $res["bdate"]);
		if (count($birthday_arr)==3){$res["bdate"]=$birthday_arr[2]."-".$birthday_arr[1]."-".$birthday_arr[0];}
      }   
      if (is_null($res)){
      	$res=array("uid"=>0, "first_name"=>"", "last_name"=>"", "bdate"=>"", "photo_100"=>"", "photo_max"=>"");
      }   
      return $res;
  }


//Проверяем на корректность полученные данные
$errmsg = "";
$err=0;
if(!preg_match("/^[a-zA-Z0-9]+$/",$vkid)){
	$errmsg = "Не найден пользователь вконтакте.";
	$err=2;
}
if(!preg_match("/^[a-zA-Z0-9]+$/",$token)){
	$errmsg = "Не найден пользователь вконтакте.";
	$err=2;
}
$vowels = array("--", "\\", "%");
$email=str_replace("--", "", $email);
$vowels = array("_", ".", "@","-");
$email1 = str_replace($vowels, "", $email);

if(!preg_match("/^[a-zA-Z0-9]+$/",$email1)){
	$errmsg = "Не найден пользователь вконтакте.";
	$err=2;
}

$vk_user=get_vkuid_by_token($token);

if ($vk_user["uid"]==0){
	$errmsg = "Не найден пользователь вконтакте.";
	$err=2;
	echo "[{\"status\":\"0\", \"err\":\"$err\", \"errmsg\":\"$errmsg\"}]";
	exit;
}
if ($vk_user["uid"]!=$vkid){
	$errmsg = "Id пользователя не соответствует токену.";
	$err=2;
	echo "[{\"status\":\"0\", \"err\":\"$err\", \"errmsg\":\"$errmsg\"}]";
	exit;
}

require_once("fn_generatecode.php"); //Подключаем функцию, генерирующую код активации
require_once("fn_getsid.php");  	 //Подключаем функцию, генерирующую sid
require_once("dbconfiguration.php"); //Подключаемся к базе
require_once("checksid.php");  //Проверяем пользователя

$email=mysql_real_escape_string($email);
$token=mysql_real_escape_string($token);

if ($uid>0){
//Проверяем, что этот пользователь уже не засвечен где-нибудь
$query = "SELECT uid FROM wsq_users WHERE vk_uid='".$vk_user["uid"]."'";
$res = mysql_query($query);
$fl=0;
if ($res!=false){
  if (mysql_num_rows($res)>0){$fl=1;} 
}

if ($fl==0){
//Обновляем данные пользователя
  $vk_uid=$vk_user["uid"]; //Обычный login не может содержать '_'
  $photo_100=$vk_user["photo_100"];
  $photo_big=$vk_user["photo_max"];

  $query = "UPDATE wsq_users SET vk_uid=$vk_uid,vk_photo_100='$photo_100',vk_photo_big='$photo_big' WHERE uid=$uid";
  $res = mysql_query($query);  
}else{
    $errmsg = "Данный аккаунт вконтакте уже привязан к системе";
    $err=3;
    echo "[{\"status\":\"0\", \"err\":\"$err\", \"errmsg\":\"$errmsg\"}]";
}



	

}

if ($err>0){
	echo "[{\"status\":\"0\", \"err\":\"$err\", \"errmsg\":\"$errmsg\"}]";
}else{
   //echo "[{\"status\":\"1\", \"err\":\"$err\", \"uid\":\"$uid\", \"active\":\"$active\", \"ustatus\":\"$ustatus\", \"sid\":\"$sidhash\"}]";
	echo "[{\"status\":\"1\", \"err\":\"0\", \"uphoto\":\"$photo_100\", \"uphoto_big\":\"$photo_big\"}]";
}

mysql_close(); 
?>