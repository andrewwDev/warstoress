<?php
//Вернуть данные для страницы мои друзья
//Авторизация требуется. Информация возвращается любому обратившемуся
$uid=0;
$sid="";
if($_POST){
	if (isset($_POST['uid'])){$uid=0+$_POST['uid'];}
	if (isset($_POST['sid'])){$sid=$_POST['sid'];}
	if (isset($_POST['ver'])){$ver=0+$_POST['ver'];}	
}else{
	if (isset($_GET['uid'])){$uid=0+$_GET['uid'];}
	if (isset($_GET['sid'])){$sid=$_GET['sid'];}
	if (isset($_GET['ver'])){$ver=0+$_GET['ver'];}
	
}
require_once("dbconfiguration.php");  //Подключаемся к базе
require_once("checksid.php");  //Проверяем пользователя
require_once("fn_gettblver.php");	  //Функция, возвращающая актуальную версию таблицы


$jsonData = array();
$jsonData["ver"]=getTblVer("friends");

$fr_str=",,,";
$query = "SELECT friends1,friends2,friends3,friends4,friends5 FROM  wsq_user_ref_str WHERE uid=$uid";
$res = mysqli_query($tmpres, $query);
if ($res!=false){
	if (mysqli_num_rows($res)>0){
		while ($row=mysqli_fetch_assoc($res)){
			$fr_str=$fr_str.$row['friends1'];
			$fr_str=$fr_str.",".$row['friends2'];
			$fr_str=$fr_str.",".$row['friends3'];
			$fr_str=$fr_str.",".$row['friends4'];
			$fr_str=$fr_str.",".$row['friends5'];
		}
		$flres=true;
	}	
}

//Убиваем двойные запятаи
$len1=-1;
while($len1!=strlen($fr_str)){
	$len1=strlen($fr_str);	
	$fr_str=str_replace(",,", ",", $fr_str);	
}
if ($fr_str[$len1-1]==','){$fr_str=substr($fr_str, 0,$len1-1);}
if ($fr_str[0]==','){$fr_str=substr($fr_str, 1);}
//

//Разбиваем на массив
$friends_id=explode(",",$fr_str);
$friends_id=array_unique($friends_id);
$fr_str=join(",",$friends_id);

$arr_fr=array();
$arr_name=array();
$arr_rank=array();
$arr_photo=array();
$arr_date=array();
$arr_udate=array();
$arr_summ=array();
//Достаем данные о пользователях
if (count($friends_id)>0){
	$query ="select uid, name, surname, login, rank, vk_photo_100 from wsq_users where uid in ($fr_str)";
	$res = mysqli_query($tmpres, $query);
	$flres=false;
	$k=0;
	if ($res!=false){
		if (mysqli_num_rows($res)>0){
			while ($row=mysqli_fetch_assoc($res)){				
				$id=$row['uid'];
				$uname=$row['name'].' '.$row['surname'];
				if (strlen($uname)==1){$uname=$row['login'];}				
				$arr_name[$id]=$uname;
				$arr_rank[$id]=0+$row['rank'];
				$arr_photo[$id]="".$row['vk_photo_100'];
				$arr_date[$id]="";
				$arr_summ[$id]=0;
				$arr_udate[$id]=$k++;

			}
			$flres=true;
		}	
	}	

	//Достаем cумму бонусов
	$query ="SELECT uid, sum(summ) as summ, DATE_FORMAT(max(date), '%d.%m.%y %H:%i') as tr_date,  UNIX_TIMESTAMP(max(date))-1400000000 as udate from wsq_transaction where uid in ($fr_str) and troper=55 and paystatus=1 and date> (LAST_DAY( DATE_SUB( CURDATE( ) , INTERVAL 1 MONTH ) ) + INTERVAL 1 DAY) GROUP BY uid";
	$res = mysqli_query($tmpres, $query);
	$flres=false;
	if ($res!=false){
		if (mysqli_num_rows($res)>0){
			while ($row=mysqli_fetch_assoc($res)){
				$id=$row['uid'];				
				$arr_date[$id]=$row['tr_date'];
				$arr_udate[$id]=$row['udate'];
				$arr_summ[$id]=0+$row['summ'];
			}
			$flres=true;
		}	
	}	
}

arsort($arr_udate); //Сортируем по дате последней покупки

foreach ($arr_udate as $id => $value) {
	$friend=array();
	$friend["id"]=$id;	
	$friend["name"]=$arr_name[$id];
	$friend["rank"]=$arr_rank[$id];
	$friend["photo"]=$arr_photo[$id];
	$friend["date"]=$arr_date[$id];
	$friend["summ"]=$arr_summ[$id];
	$arr_fr[]=$friend;
}


$jsonData["friends"]=$arr_fr; 

echo json_encode($jsonData, JSON_UNESCAPED_UNICODE);

mysqli_close($tmpres); 
?>