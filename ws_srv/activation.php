<?php
    //Активация пользователя
    //Обнуляем переменные

    $uid=0;
    $sid="";
    $code=0;
    $result = [];
    //Пароль генерируется на стороне сервера и отправляется пользователю письмом.

    if(isset($_GET["uid"]) and isset($_GET["code"])) {
        $uid=$_GET['uid'];
        $code=$_GET['code'];
    } elseif(isset($_POST["uid"]) and isset($_POST["code"])) {
        $uid=$_POST['uid'];
        $code=$_POST['code'];
    }

    require_once("fn_update_ref_struct.php");  //Построить реферальное дерево над пользователем
    require_once("dbconfiguration.php");  //Подключаемся к базе
    //require_once("checksid.php");  //проверяем право доступа

    //$query = "DELETE FROM wsq_activation WHERE (NOW()-created)>200000000"; //Сносим старые коды

    if(!preg_match("/^[0-9]+$/",$code)){
        //[{\"status\":\"0\", \"err\":\"5\", \"errmsg\":\"Неверно введен код активации.\"}]
        $result = [
            "message" => "Неверно введен код активации.",
            "statusCode" => false
        ];
    }

    $query = "SELECT uid, code FROM wsq_activation WHERE uid=$uid and mode=1";

    $uid = mysqli_real_escape_string($tmpres, $uid);
    $code = mysqli_real_escape_string($tmpres, $code);

    $res = mysqli_query($tmpres, $query);
    $dbcode=-2;
    if ($res!=false){
        if (mysqli_num_rows($res)>0){
            $row=mysqli_fetch_array($res);
            $dbcode=$row['code'];
        }
    }

    if ($dbcode==$code){
        $query = "UPDATE  wsq_users SET active=1 WHERE uid=$uid";
        $res = mysqli_query($tmpres, $query);
        $query = "DELETE FROM wsq_activation WHERE uid=$uid and mode=1";
        $res = mysqli_query($tmpres, $query);
        //Обновляем реф структуру

        $ref=update_ref_struct($uid);
        //Обновляем реферальную ветку, которая используется для пересчета рангов
        $ref_arr=$ref[0]; //Массив с id пользователями над этим
        if (count($ref_arr)>0){
            $res = mysqli_query($tmpres, "INSERT INTO wsq_user_ref_str (uid, friends1) VALUE ($uid, '')");
            for ($i=0; $i <count($ref_arr) ; $i++) {
                mysqli_query($tmpres,"UPDATE wsq_user_ref_str SET friends_count=friends_count+1, friends".($i+1)."_count=friends".($i+1)."_count+1, friends".($i+1)."=CONCAT(friends1,',".$uid."') WHERE uid=".$ref_arr[$i]);
            }
        }
        //[{\"status\":\"2\", \"msg\":\"Авторизация прошла успешно.\"}]
        $result = [
            "message" => "Авторизация прошла успешно.",
            "statusCode" => true
        ];
        session_start();
        $_SESSION['uid'] = $uid;
//        echo "Авторизация прошла успешно.";
//        echo "<script>setTimeout(()=>{window.location = 'http://warstores.net/ws-shop/'}, 1000);</script>";
    }else{
        //[{\"status\":\"0\", \"err\":\"4\", \"errmsg\":\"Неверно введен код активации.\"}]
//        echo "Неверно введен код активации.";
        $result = [
            "message" => "Неверно введен код активации.",
            "statusCode" => false
        ];
    }


    mysqli_close($tmpres);
//                            ob_end_flush();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <?
        if($result["statusCode"]){
            echo '<meta http-equiv="refresh" content="1;url=http://warstores.net/ws-shop/" />';
        }
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Верификация</title>


    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>


    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="../ws-shop/css/bootstrap.min.css" rel="stylesheet">


    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="../ws-shop/css/nifty.css" rel="stylesheet">


    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="../ws-shop/css/demo/nifty-demo-icons.min.css" rel="stylesheet">


        
    <!--Demo [ DEMONSTRATION ]-->
    <link href="../ws-shop/css/demo/nifty-demo.min.css" rel="stylesheet">


    <!--Magic Checkbox [ OPTIONAL ]-->
    <link href="../ws-shop/plugins/magic-check/css/magic-check.min.css" rel="stylesheet">





    
    <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="../ws-shop/plugins/pace/pace.min.css" rel="stylesheet">
    <script src="../ws-shop/plugins/pace/pace.min.js"></script>


    <!--jQuery [ REQUIRED ]-->
    <script src="../ws-shop/js/jquery.min.js"></script>


    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="../ws-shop/js/bootstrap.min.js"></script>


    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="../ws-shop/js/nifty.min.js"></script>






    <!--=================================================-->
    
    <!--Background Image [ DEMONSTRATION ]-->
    <script src="../ws-shop/js/demo/bg-images.js"></script>



    
    <!--=================================================

    REQUIRED
    You must include this in your project.


    RECOMMENDED
    This category must be included but you may modify which plugins or components which should be included in your project.


    OPTIONAL
    Optional plugins. You may choose whether to include it in your project or not.


    DEMONSTRATION
    This is to be removed, used for demonstration purposes only. This category must not be included in your project.


    SAMPLE
    Some script samples which explain how to initialize plugins or components. This category should not be included in your project.


    Detailed information and more samples can be found in the document.

    =================================================-->
        

</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body>
	<div id="container" class="cls-container">
		
		<!-- BACKGROUND IMAGE -->
		<!--===================================================-->
		<div id="bg-overlay"></div>
		
		
		<!-- LOGIN FORM -->
		<!--===================================================-->
		<div class="cls-content" >
		    <div class="cls-content-sm panel">
		        <div class="panel-body" >
		            <div class="mar-ver pad-btm" >
					    <literal>
                            <? echo $result["message"]; ?>
						</literal>
                    </div>
		        </div>
		    </div>
		</div>
		<!--===================================================-->	
	</div>
	<!--===================================================-->
	<!-- END OF CONTAINER -->


		</body>
</html>

