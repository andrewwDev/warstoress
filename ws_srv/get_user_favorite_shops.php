<?php
//Любимые места пользователя
$uid=0;
$sid="";
$shopid=0;
if($_POST){
	$uid=0+$_POST['uid'];
	$sid=$_POST['sid'];
}else{
	$uid=0+$_GET['uid'];
	$sid=$_GET['sid'];
}
require_once("dbconfiguration.php");  //Подключаемся к базе
require_once("checksid.php");  //Проверяем пользователя
require_once("fn_gettblver.php");	  //Функция, возвращающая актуальную версию таблицы

//Получаем время, которое осталось для сброса периода
$t1=time();
$arr=getdate();
$t2=mktime(0, 0, 0 , $arr["mon"]+1, 1, $arr["year"]);
$dt=$t2-$t1; //Столько секунд осталось до 1-го числа следующего месяца
	//$days=floor($dt/24/3600);
	//$hours=floor(($dt-$days*24*3600)/3600);
	//$minutes=floor(($dt-$days*24*3600-$hours*3600)/60);
	//$seconds=floor($dt-$days*24*3600-$hours*3600-$minutes*60);
	//echo "\n\n До начала следующего месяца $days д. $hours:$minutes:$seconds\n\n";


$jsonData = array();
$status=1;

$arr_shops_id=array();
$arr_shop_title=array();
$arr_shop_refid=array(); //id захватчика магазина
$arr_shop_photo=array();
$arr_bonus_to_shop=array();
$arr_bonus_from_shop=array();
$arr_bonus_zahvat=array();
$arr_log1_id=array();
$arr_log2_id=array();
$arr_log1_name=array();
$arr_log2_name=array();
$arr_num1=array();
$arr_num2=array();
$arr_order2=array(); //порядковый номер в рейтинге

$query = "SELECT sid, troper, sum(summ) as summ, UNIX_TIMESTAMP(max(date)) as lastdate FROM wsq_transaction WHERE uid=$uid and (troper=54 or troper=55 or troper=61) group by sid,troper order by troper,lastdate DESC";
$res = mysql_query($query);
if ($res!=false){
	if (mysql_num_rows($res)>0){
		while ($row=mysql_fetch_assoc($res)){
			$shop=$row["sid"];
			$arr_shops_id[]=$shop;
			if ($row["troper"]==54){$arr_bonus_to_shop[$shop]=0+abs($row["summ"]);}
			if ($row["troper"]==55){$arr_bonus_from_shop[$shop]=0+abs($row["summ"]);}
			if ($row["troper"]==61){$arr_bonus_zahvat[$shop]=0+abs($row["summ"]);}
		}
		$flres=true;
	}	
}
$arr_shops_id=array_unique($arr_shops_id);

//Достаем захватчиков магазина
foreach ($arr_shops_id as $key => $shopid) {
	$arr_log1_id[$shopid]=0;
	$arr_log2_id[$shopid]=0;
	$arr_log1_name[$shopid]="";
	$arr_log2_name[$shopid]="";
	$arr_num1[$shopid]=0;
	$arr_num2[$shopid]=0;
	$arr_order2[$shopid]=0;
	$query ="select t1.uid, t1.num, t2.name, t2.surname, t2.login, t2.vk_photo_100 as photo FROM (SELECT uid, count(uid) as num FROM wsq_transaction WHERE date>(LAST_DAY( DATE_SUB( CURDATE( ) , INTERVAL 1 MONTH ) ) + INTERVAL 1 DAY) and troper=55 and sid=$shopid GROUP BY uid ORDER BY num DESC) as t1, wsq_users as t2 WHERE t1.uid=t2.uid";	
	$res = mysql_query($query);
	if ($res!=false){
		if (mysql_num_rows($res)>0){
			$row=mysql_fetch_assoc($res);
			$arr_shop_photo[$shopid]=$row["photo"];
			$arr_log1_id[$shopid]=$row["uid"];			
			$arr_num1[$shopid]=$row["num"];
			$arr_log1_name[$shopid]=$row["name"]." ".$row["surname"];
			if (strlen($arr_log1_name[$shopid])==1){$arr_log1_name[$shopid]=$row["login"];}
			$i=1;
			while ($row=mysql_fetch_assoc($res)){
				$i=$i+1;
				if ($i==2){					
					$arr_log1_id[$shopid]=$row["uid"];			
					$arr_num1[$shopid]=$row["num"];
					$arr_log1_name[$shopid]=$row["name"]." ".$row["surname"];
					if (strlen($arr_log1_name[$shopid])==1){$arr_log1_name[$shopid]=$row["login"];}
					$arr_order2[$shopid]=$i;
				}
				if ($uid==$row["uid"]){
					$arr_log1_id[$shopid]=$row["uid"];			
					$arr_num1[$shopid]=$row["num"];
					$arr_log1_name[$shopid]=$row["name"]." ".$row["surname"];
					if (strlen($arr_log1_name[$shopid])==1){$arr_log1_name[$shopid]=$row["login"];}
					$arr_order2[$shopid]=$i;
					break;
				}
				
			}
		}	
	}
}

//Достаем данные на магазины
	$query ="select sid, title, refid from wsq_shops where sid in (".join(",",$arr_shops_id).")";
	$res = mysql_query($query);	
	if ($res!=false){
		if (mysql_num_rows($res)>0){
			while ($row=mysql_fetch_assoc($res)){
				$shop=$row["sid"];
				$arr_shop_title[$shop]=$row["title"];
				$arr_shop_refid[$shop]=0+$row["refid"]; //id захватчика магазина				
			}
		}	
	}

$shop_rating=array();
foreach ($arr_shops_id as $key => $shopid) {
	$shop=array();	
	$shop["id"]=$shopid;
	if(isset($arr_shop_title[$shopid])){$shop["title"]=$arr_shop_title[$shopid];}else{$shop["title"]="";}
	$shop["city"]="Воронеж";
	if(isset($arr_shop_refid[$shopid])){$shop["refid"]=0+$arr_shop_refid[$shopid];}else{$shop["refid"]=0;}
	if(isset($arr_shop_photo[$shopid])){$shop["photo"]=$arr_shop_photo[$shopid];}else{$shop["photo"]="";}
	if(isset($arr_bonus_to_shop[$shopid])){$shop["bonus_to_shop"]=0+$arr_bonus_to_shop[$shopid];}else{$shop["bonus_to_shop"]=0;}
	if(isset($arr_bonus_from_shop[$shopid])){$shop["bonus_from_shop"]=0+$arr_bonus_from_shop[$shopid];}else{$shop["bonus_from_shop"]=0;}
	if(isset($arr_bonus_zahvat[$shopid])){$shop["bonus_zahvat"]=0+$arr_bonus_zahvat[$shopid];}else{$shop["bonus_zahvat"]=0;}
	if(isset($arr_log1_id[$shopid])){$shop["log1_id"]=0+$arr_log1_id[$shopid];}else{$shop["log1_id"]=0;}
	if(isset($arr_log2_id[$shopid])){$shop["log2_id"]=0+$arr_log2_id[$shopid];}else{$shop["log2_id"]=0;}
	if(isset($arr_log1_name[$shopid])){$shop["log1_name"]=$arr_log1_name[$shopid];}else{$shop["log1_name"]="";}
	if(isset($arr_log2_name[$shopid])){$shop["log2_name"]=$arr_log2_name[$shopid];}else{$shop["log2_name"]="";}
	if(isset($arr_num1[$shopid])){$shop["log1_num"]=0+$arr_num1[$shopid];}else{$shop["log1_num"]=0;}
	if(isset($arr_num2[$shopid])){$shop["log2_num"]=0+$arr_num2[$shopid];}else{$shop["log2_num"]=0;}
	if(isset($arr_order2[$shopid])){$shop["log2_order"]=0+$arr_order2[$shopid];}else{$shop["log2_order"]=0;}
	$shop_rating[]=$shop;
}

$jsonData["status"]=$status;
$jsonData["time_dt"]=0+$dt;

$jsonData["shops"]=$shop_rating;

/*$jsonData["shops_id"]=$arr_shops_id;
$jsonData["shops_title"]=$arr_shop_title;
$jsonData["shops_refid"]=$arr_shop_refid; //id захватчика магазина
$jsonData["bonus_to_shop"]=$arr_bonus_to_shop;
$jsonData["bonus_from_shop"]=$arr_bonus_from_shop;
$jsonData["bonus_zahvat"]=$arr_bonus_zahvat;
$jsonData["log1_id"]=$arr_log1_id;
$jsonData["log2_id"]=$arr_log2_id;
$jsonData["log1_name"]=$arr_log1_name;
$jsonData["log2_name"]=$arr_log2_name;
$jsonData["arr_num1"]=$arr_num1;
$jsonData["arr_num2"]=$arr_num2;
$jsonData["order2"]=$arr_order2; //порядковый номер в рейтинге */

echo json_encode($jsonData, JSON_UNESCAPED_UNICODE);

mysql_close(); 
?>