<?php
//Вернуть данные со статистикой для страницы профиля
//Авторизация требуется. Информация возвращается любому обратившемуся
$uid=0;
$sid="";
if($_POST){
	$uid=0+$_POST['uid'];
	$sid=$_POST['sid'];
}else{
	$uid=0+$_GET['uid'];
	$sid=$_GET['sid'];
}
require_once("dbconfiguration.php");  //Подключаемся к базе
require_once("checksid.php");  //Проверяем пользователя
require_once("fn_gettblver.php");	  //Функция, возвращающая актуальную версию таблицы

function mysqli_result($res, $row, $field=0) {
    $res->data_seek($row);
    $datarow = $res->fetch_array();
    return $datarow[$field];
}
function getvaluefromdb($query, $rs){
	$fres=0;
	$res = mysqli_query($rs, $query);
	if ($res!=false){
	    if (mysqli_num_rows($res)>0){
		    $row=mysqli_fetch_assoc($res);
		    $fres=mysqli_result($res, 0, 0);		
		}		
	}
	return $fres;

}

$jsonData=array(); //Информация о пользователе

//Это очень плохой и неоптимальный код, но, блин, дедлайн завтра. При первой возможности его нужно оптимизировать использовать вспомогательную таблицу. 
$summ_total= 0+getvaluefromdb("SELECT sum(summ) as summ FROM wsq_transaction WHERE uid=$uid", $tmpres); //Сумма на счету пользователя за все время
$summ_today= 0+getvaluefromdb("SELECT sum(summ) as summ FROM wsq_transaction WHERE uid=$uid and date>=CURDATE( )", $tmpres);  //Сумма на счету пользователя за сегодня
$summ_month=0+getvaluefromdb("SELECT sum(summ) as summ FROM wsq_transaction WHERE uid=$uid and date> (LAST_DAY( DATE_SUB( CURDATE( ) , INTERVAL 1 MONTH ) ) + INTERVAL 1 DAY) and troper=55", $tmpres); //Сумма на счету пользователя за текущий месяц
$summ_ref_mnth=0+getvaluefromdb("SELECT sum(summ) as summ FROM wsq_transaction WHERE uid=$uid and date> (LAST_DAY( DATE_SUB( CURDATE( ) , INTERVAL 1 MONTH ) ) + INTERVAL 1 DAY) and troper=56", $tmpres); //Сумма на счету пользователя за текущий месяц

$friends=0+getvaluefromdb("SELECT friends_count FROM wsq_user_ref_str WHERE uid=$uid", $tmpres);//Друзей
$shops=0+getvaluefromdb("SELECT count(sid) as shops FROM wsq_shops WHERE refid=$uid", $tmpres);//Захвачено компаний
$buildings=0+getvaluefromdb("SELECT count (bldid) as blds FROM wsq_buildings WHERE uid=$uid", $tmpres);//Захвачено зданий
$rank=0+getvaluefromdb("SELECT rank FROM wsq_users WHERE uid=$uid", $tmpres);//ранг

$tosave=$rank_bonus_usr[$rank]-$summ_month;
if ($tosave<0){$tosave=0;}

if ($rank<4){
	$tonext=$rank_bonus_usr[$rank+1]-$summ_month;
	if ($tonext<0){$tosave=0;}
}else{
	$tonext="-";
}

$jsonData["uid"]=$uid;
$jsonData["rank"]=$rank;
$jsonData["shops"]=$shops;
$jsonData["builds"]=$buildings;
$jsonData["friends"]=$friends;
$jsonData["summ"]=$summ_total;
$jsonData["summ_today"]=$summ_today;
$jsonData["summ_for_save"]=$tosave;
$jsonData["summ_for_next"]=$tonext;
$jsonData["summ_ref"]=$summ_ref_mnth;

/*
$query = "SELECT * FROM  wsq_user_rank WHERE uid=$uid";
$res = mysql_query($query);

$flres=false;
$summ=0;
if ($res!=false){
	if (mysql_num_rows($res)>0){
		while ($row=mysql_fetch_assoc($res)){
			$jsonData = $row;
		}
		$flres=true;
	}	
}*/

$jsonData["status"]=1;
$jsonData["err"]="";
echo "[".json_encode($jsonData, JSON_UNESCAPED_UNICODE)."]";

mysqli_close($tmpres); 
?>