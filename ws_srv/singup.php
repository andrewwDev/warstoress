<?php
//Регистрируем нового пользователя
//Обнуляем переменные

$errmsg='';
$referer='';
$pwdhash='';
$login='';
$email='';
$name='';
$surname='';
$birthday='';
$ref_uid=0;
//Пароль генерируется на стороне сервера и отправляется пользователю письмом.
//
if($_POST){
	$referer=$_POST['referer'];
	$pwdhash=$_POST['pwdhash'];
	$login=$_POST['login'];
	$email=$_POST['email'];
	$name=$_POST['name'];
	$surname=$_POST['surname'];
	$birthday=$_POST['birthday']; //ЧЧ.ММ.ГГГГ
}
require_once("fn_getuidfromrefnum.php"); //Подключаем функцию, выделяющую uid по реферальному номеру
require_once("fn_generatecode.php"); //Подключаем функцию, генерирующую код активации
require_once("fn_getsid.php");  //Генератор sid (ID сессии для Android)
require_once("fn_update_ref_struct.php");  //Построить реферальное дерево над пользователем

require_once("dbconfiguration.php");  //Подключаемся к базе
global $tmpres;
$referer=mysqli_real_escape_string($tmpres, $referer);
$pwdhash=mysqli_real_escape_string($tmpres, $pwdhash);
$login=mysqli_real_escape_string($tmpres, $login);
$email=mysqli_real_escape_string($tmpres, $email);
$name=mysqli_real_escape_string($tmpres, $name);
$surname=mysqli_real_escape_string($tmpres, $surname);
$birthday=mysqli_real_escape_string($tmpres, $birthday);


//Проверяем актуальность данных о пригласившем человеке
$ref_uid=0;
if (strlen($referer)>1){
	//Вырезаем из $ref_login недопустимые символы
	$vowels = array("'", "`", "\"", "\\", "/", "--", "==","%","&","*","\n","\r","\t");
    $referer = str_replace($vowels, "", $referer);
	$ref_uid=getrefid($referer);//Декодируем id мастера.
}


	
//проверка на то, что все необходимые поля заполнены
if (strlen($login)==0){
	$errmsg = "Не задан логин.";
}elseif (strlen($login)<3){
	$errmsg = "Задан слишком короткий логин.";
}elseif (strlen($pwdhash)==0){
	$errmsg = "Не задан пароль";
}elseif (strlen($email)<3){
	$errmsg = "Не задан email.";
}elseif (strlen($name)==0){
	$errmsg = "Не задано имя.";
}elseif (strlen($surname)==0){
	$errmsg = "Не задана фамилия.";
}else{
	//Проверка на то, что все заполнено корректно
	$email_tmp = str_replace(array("@",".","-","_"), "", $email);
	$birthday_tmp = str_replace(array("/",".","-"), "", $birthday);
	//Опасные символы
	$vowels = array("'", "`", "\"", "\\", "/", "--", "==","%","#","&","*","\n","\r","\t");		
	//Проверка корректности присланных данных
	if(!preg_match("/^[\d]+$/",$birthday_tmp)){	    	
        $errmsg = "Дата рождения содержит недопустимые символы. $birthday_tmp";
        if (strlen($birthday_tmp)==0){$errmsg = "";}
    }
    
	if(!preg_match("/^[a-zA-Z0-9]+$/",$login)){
        $errmsg = "Логин может состоять только из букв английского алфавита и цифр.";
    }elseif(!preg_match("/^[a-zA-Z0-9]+$/",$email_tmp)){
        $errmsg = "Email содержит недопустимые символы.";
    }elseif(strpos($email,"--")>0){
    	$errmsg = "Email содержит недопустимые символы.";
    }elseif(strpos($email,"@")<=0){
    	$errmsg = "Email задан неверно.	";
    }elseif(strlen($birthday)>10){
		$errmsg = "Дата рождения задана неверно.";
    }elseif(preg_match("/[^(\w)|(\x7F-\xFF)|(\s)]/",$name)){
		$errmsg = "Имя содержит недопустимые символы.";
    }elseif(preg_match("/[^(\w)|(\x7F-\xFF)|(\s)]/",$surname)){
		$errmsg = "Фамилия содержит недопустимые символы.";
    }else{
    	//Проверяем, что логин и email еще не заняты
    	$query = "SELECT uid, login, email from wsq_users WHERE login='$login' or email='$email'";
		$res = mysqli_query($tmpres, $query);
			if ($res!=false){
				if (mysqli_num_rows($res)>0){
					while($row=mysqli_fetch_array($res)){
						if ($login==$row['login']){
							$errmsg=$errmsg."Логин '$login' занят. ";
						}
						if ($email==$row['email']){
							$errmsg=$errmsg."Email '$email' уже зарегистрирован. ";
						}
					}				
				}
			}

    }	
}

if (strlen($errmsg)==0){
	//Регистрируем нового пользователя.
	//$pwd=generate_password(12);
	//$pwdhash=md5($pwd);
	$code=generatecode(7);

	$sidhash=getsid();

	//Преобразуем дату к MySQLформату
	if (strlen($birthday)>0){
		$birthday_arr=explode("/", $birthday);
		if (count($birthday_arr)==3){$birthday=$birthday_arr[2]."-".$birthday_arr[1]."-".$birthday_arr[0];}
	}


	$query = "INSERT INTO wsq_users (refid,login,email,name,surname,birthday,pwdhash) values ($ref_uid, '$login', '$email','$name','$surname','$birthday','$pwdhash')";
	$res = mysqli_query($tmpres, $query);



	$query = "SELECT uid from wsq_users WHERE login='$login'";
	$res = mysqli_query($tmpres, $query);
	$uid=-1;
	if ($res!=false){
		if (mysqli_num_rows($res)>0){
			$row=mysqli_fetch_array($res);
			$uid=0+$row['uid'];
		}	
	}

	$res = mysqli_query($tmpres, "INSERT INTO wsq_user_rank (uid, shops, builds, friends, summ, summ_today, updatedate) VALUE ($uid, 0, 0, 0, 0, 0, NOW())");	//Таблица для пересчета рангов

	if ($uid==-1){
		$errmsg="При добавлении в базу данных информации о пользователе $login возникла ошибка.";
	}else{

		/////////////Отправляем email
			$subject = "WarStores: Верификация пользователя"; 
			$message = ' <p>Поздравляем, Вы успешно зарегистрировались в системе WarStores. Для верификации пользователя щелкните по <a href="http://warstores.net/ws_srv/activation.php?uid='.$uid.'&code='.$code.'">ссылке</a> или укажите код <b>'.$code.'</b> в мобильном приложении. <p>С Уважением, <br> Команда WarStores';
			$headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
			$headers .= "From: noreply@warstores.net <noreply@warstores.net>\r\n"; 
			mail($email, $subject, $message, $headers); 							 
		///////////		
			
		//Ссессия
		$ipadr=$_SERVER['REMOTE_ADDR'];
		$query = "INSERT INTO wsq_session (uid,sidhash,ip,logintime) values ($uid, '$sidhash' , '$ipadr', now())";
		$res = mysqli_query($tmpres, $query);
		//код активации
		$query = "INSERT INTO wsq_activation (uid,code,mode,created) values ($uid, '$code' , 1,now())";
		$res = mysqli_query($tmpres, $query);
		//Обновляем реф структуру пользователя
		$ref=update_ref_struct($uid);
		//Обновляем реферальную ветку, которая используется для пересчета рангов
		$ref_arr=$ref[0]; //Массив с id пользователями над этим
		for ($i=0; $i <count($ref_arr) ; $i++) { 
			mysqli_query($tmpres, "UPDATE wsq_user_ref_str SET friends_count=friends_count+1, friends".($i+1)."_count=friends".($i+1)."_count+1, friends".($i+1)."=CONCAT(friends1,',".$uid."') WHERE uid=".$ref_arr[$i]);
		}
	}
}
if (strlen($errmsg)>0){
	echo "{\"status\":\"0\", \"err\":\"$errmsg\"}";
}else{
	//Необходимые для авторизации дополнительные данные
	echo "{\"status\":\"1\", \"uid\":\"$uid\", \"sid\":\"$sidhash\", ref_id:\"$ref_uid\"}";
//		echo "[{\"status\":\"1\", \"uid\":\"$uid\", \"sid\":\"$sidhash\", ref_id:\"$ref_uid\"}]";

}

/* Закрыть соединение */
mysqli_close($tmpres);
?>