<?php
//Клиенты. Достать последние 30 записей 
$uid=0;
$sid="";
$shopid=0;
if($_POST){
	$uid=0+$_POST['uid'];
	$sid=$_POST['sid'];
	$shopid=$_POST['id'];
}else{
	$uid=$_GET['uid'];
	$sid=$_GET['sid'];
	$shopid=$_GET['id'];
}
require_once("dbconfiguration.php");  //Подключаемся к базе
require_once("checksid2.php");  //Проверяем пользователя


	$clients_arr=array(); //количество клиентов
	$newclients_arr=array(); //количество новых клиентов
	$bonus_arr=array(); //получено магазином бонусов
	$cash_arr=array(); //получено магазином копеек
	$cashback_arr=array(); //выдано магазином бонусов
	$responses_arr=array(); //отзывов
	$date_arr=array(); //дата
	$daysago_arr=array(); //прошло дней с сегодняшнего дня
	$clients=array();

	//Данные для графиков
	$query = "SELECT id, day, clients, newclients, spend_total-spend_money as bonus_to_shop, spend_money as cash_to_shop,  	received as cashback, responses, TO_DAYS(NOW()) - TO_DAYS(day) as daysago FROM wsq_shop_stat WHERE sid=$shopid ORDER BY id LIMIT 0,30";
	$res = mysqli_query($tmpres, $query);
	$j=0;
	if ($res!=false){
		if (mysqli_num_rows($res)>0){
			while ($row=mysqli_fetch_assoc($res)){
				$clients_arr[$j]=$row["clients"];
				$newclients_arr[$j]=$row["newclients"];
				$bonus_arr[$j]=$row["bonus_to_shop"];
				$cash_arr[$j]=$row["cash_to_shop"];
				$cashback_arr[$j]=$row["cashback"];
				$responses_arr[$j]=$row["responses"];
				$date_arr[$j]=$row["day"];
				$daysago_arr[$j]=$row["daysago"];
				$j=$j+1;
			}
		}	
	}

	

	$jsonData["fig_clients"]=$clients_arr; //количество клиентов
	$jsonData["fig_newclients"]=$newclients_arr; 
	$jsonData["fig_bonus_to_shop"]=$bonus_arr; 
	$jsonData["fig_cash_to_shop"]=$cash_arr; 
	$jsonData["fig_cashback"]=$cashback_arr; 
	$jsonData["fig_responses"]=$responses_arr; 
	$jsonData["fig_data"]=$date_arr; //дата
	$jsonData["fig_dayago"]=$daysago_arr; //прошло дней с сегодняшнего дня
	$jsonData["clients"]=$clients;

echo json_encode($jsonData, JSON_UNESCAPED_UNICODE);

mysqli_close($tmpres); 
?>