<?php
//Скрипт вычисления захватчиков за прошлый месяц.
//Если число покупок одинаковое, то выбирается пользователь с большим количеством потраченых денег

set_time_limit(24*60*60);//Увеличиваем максимально евремя работы скрипта в сек
$start = microtime(true); 
echo "Запущен скрипт пересчета рангов... <br>\n";

$uid=0; //ID менеджера, который провел оплату
$sid="";//

if($_POST){
	if (isset($_POST['uid'])){$uid=0+$_POST['uid'];}
	if (isset($_POST['sid'])){$sid=$_POST['sid'];}
}else{
	if (isset($_GET['uid'])) {$uid=0+$_GET['uid'];}
	if (isset($_GET['sid'])) {$sid=$_GET['sid'];}
}

require_once("dbconfiguration.php");  //Подключаемся к базе
require_once("fn_gettblver.php");	  //Функция, возвращающая актуальную версию таблицы
//require_once("checksid.php");  //Проверяем пользователя

//
$arr_uid=array(); //uid пользователя-захватчика 
$arr_num=array(); //число покупок пользователя-захватчика
$arr_sum=array(); //сколько копеек потратил пользователь-захватчик на покупки

//Вычисляем захватчиков магазина
$res = mysql_query("SELECT * FROM (SELECT sid, uid, count(sid) as num, sum(cash) as cash FROM wsq_receipt WHERE date > LAST_DAY( DATE_SUB( CURDATE( ) , INTERVAL 2 MONTH ) ) + INTERVAL 1 DAY AND date < DATE_ADD( LAST_DAY( CURDATE( ) - INTERVAL 1 MONTH ) , INTERVAL 1 DAY ) group by uid, sid ORDER BY sid,num DESC, cash DESC) as t1 GROUP BY sid");
if ($res!=false){
	if (mysql_num_rows($res)>0){
		while($row=mysql_fetch_array($res)){
			$sid=$row['sid'];
			$arr_uid[$sid]=$row['uid'];
			$arr_num[$sid]=$row['num'];
			$arr_sum[$sid]=$row['cash'];
		}
	}	
}

//Достаем всех рефералов пользователя во вспомогательный массив
$user_friends=array();
$res = mysql_query("SELECT uid,friends1,friends2,friends3,friends4,friends5 FROM wsq_user_ref_str");
if ($res!=false){
	if (mysql_num_rows($res)>0){
		while($row=mysql_fetch_array($res)){
			$userid=$row['uid'];
			$user_friends[$userid]="".$row['friends1'].",".$row['friends2'].",".$row['friends3'].",".$row['friends4'].",".$row['friends5'];
		}
	}	
}

//Стираем старых захватчиков
$res = mysql_query("UPDATE wsq_shops SET refid=0, ref_num=0, ref_summ=0");
//Пишем новых
foreach ($arr_uid as $sid => $uid) {
	$res = mysql_query("UPDATE wsq_shops SET refid=$uid, ref_num=".$arr_num[$sid].", ref_summ=".$arr_sum[$sid]." WHERE sid=$sid");
	echo "Обновлен захватчик ($uid) магазина $sid.<br>\n";
}

//////////////////Захват здания////////////////////////
$bld_uid=array(); //uid пользователя-захватчика 
$bld_num=array(); //число покупок пользователя-захватчика
$bld_sum=array(); //сколько копеек потратил пользователь-захватчик на покупки
$bld_sids=array(); //sid магазинов, входящих в здание
$bld_XY=array(); //Яндекс координаты 


//Вычисляем захватчиков здания. Если bldid=0, то в здании только один магазин
$res=mysql_query("SELECT sid, bldid, refid, YGeoLoc FROM wsq_shops WHERE bldid>0 ORDER BY bldid");
if ($res!=false){
	if (mysql_num_rows($res)>0){
		while($row=mysql_fetch_array($res)){
			
			$sid=$row['sid'];
			$bldid=$row['bldid'];

			if (isset($bld_uid[$bldid])){//В здании уже кто-то просчитан
				$bld_sids[$bldid][]=$sid;
				if (isset($arr_uid[$sid])){
					if ($arr_num[$sid]>$bld_num[$bldid]){										
							$bld_uid[$bldid]=0+$arr_uid[$sid];
							$bld_num[$bldid]=0+$arr_num[$sid];
							$bld_sum[$bldid]=0+$arr_sum[$sid];											
					}elseif($arr_num[$sid]==$bld_num[$bldid]){
						if ($arr_sum[$sid]>$bld_sum[$bldid]){
							$bld_uid[$bldid]=0+$arr_uid[$sid];
							$bld_num[$bldid]=0+$arr_num[$sid];
							$bld_sum[$bldid]=0+$arr_sum[$sid];
						}
					}
				}
			}else{//Это новое здание
				if ($row['refid']>0){//У магазина есть захватчик
					if (isset($arr_uid[$sid])){
						$bld_uid[$bldid]=0+$arr_uid[$sid];
						$bld_num[$bldid]=0+$arr_num[$sid];
						$bld_sum[$bldid]=0+$arr_sum[$sid];
						$bld_sids[$bldid]=array($sid);
						$bld_XY[$bldid]=$sid=$row['YGeoLoc'];
					}
				}
			}		
		}
	}	
}
echo "<br><br>---Здания---</br>";
//Пишем результат по зданиям
$query="";
foreach ($bld_uid as $bldid => $uid) {
	if (strlen($query)==0){
		$query="($bldid,$uid)";
	}else{
		$query=$query.",($bldid,$uid)";
	}
	echo "Обновлен захватчик здания ($uid) здания $bldid.<br>\n";
}

$res=mysql_query("DELETE FROM wsq_buildings");
$res = mysql_query("INSERT INTO wsq_buildings (bldid, uid) VALUES $query");

//////////////////Захват Сектора территории ////////////////////////
$point_uid=array(); //uid пользователя-захватчика сектора 
$point_XY=array();  //координаты точки через запятую
$point_num=array();
$point_summ=array();


//Достаем точки
$res=mysql_query("SELECT sid, refid, bldid, bld_uid, YGeoLoc, ref_num, ref_summ  FROM (SELECT t1.sid, t1.bldid, t1.refid, t1.YGeoLoc, t1.ref_num, t1.ref_summ, t2.uid as bld_uid FROM wsq_shops as t1 LEFT JOIN wsq_buildings as t2 ON t1.bldid=t2.bldid WHERE t1.bldid>0 group by bldid UNION (SELECT t1.sid, t1.bldid, t1.refid, t1.YGeoLoc, 0 as bld_uid FROM wsq_shops as t1 WHERE bldid=0)) as t WHERE refid>0");
if ($res!=false){
	if (mysql_num_rows($res)>0){
		while($row=mysql_fetch_array($res)){
			if ($row['bldid']>0){
				$point_uid[]=$row['bld_uid'];				
			}else{
				$point_uid[]=$row['refid'];		
			}
			$point_XY[]=$row['YGeoLoc'];
			$point_num[]=$row['ref_num'];
			$point_summ[]=$row['ref_summ'];
		}
	}	
}
echo "<br><br>---Сектора---</br>";
//Тут получены координаты XY в формате строки через запятую 
foreach ($point_uid as $XY) {
	echo "$XY, ";
}


echo "Скрипт выполнен<br>\n";
echo 'Время выполнения скрипта: '.(microtime(true) - $start)." сек.<br>\n";
echo "\n<br>max_time=".ini_get('max_execution_time')." сек.<br>\n";
?>