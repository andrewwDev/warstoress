<?php

function getCval($c){
	$code =array('0','1', '2', '3', '4', '5', '6','G','8','9','Q','W','E','R','T','Y','U','I','P','A','S','D','F','7','H','J','K','L','Z','X','C','V','B','N','M');
    $len=count($code);
    $res=0;
    for ($i=0;$i<$len;$i++){
    	if ($code[$i]==$c){$res=$i; break;}
    }
    return $res;
}

function getuidfromrefnum($str){	
	//Функция получает UID по реферальному номеру
	//Аналог этой функции содержится в GlobalData в Android	
	//Пользователю 1 соответствует код 123987 - это пользователь без мастера
    $len=35;//Длина массива $code
    $imax=strlen($str)-1; //Последний символ - это контрольная сумма. Она приверяется в приложении и тут ее можно вообще не смотреть
    $x=0;
    for ($i=0; $i<$imax; $i++){
        $x=$x+getCval($str[$i])*pow($len, $imax-$i-1);
    }
    return ($x-1590372);
}

function getrefid($str){
	$qrsymbols =array('@','#','0','1', '2', '3', '4', '5', '6','G','8','9','Q','W','E','R','T','Y','U','I','P','A','S','D','F','7','H','J','K','L','Z','X','C','V','B','N','M');
  //Функция получает UID пользователя в зависимости от его реф номера/email/логина или кода магазина
  //Если все символы прописные и цифры, то это QR код
  //Если @ - это email
  //Если # - это магазин (ownerid из wsq_shops)
  //Если есть одна строчная латинская буква, то это логин

	$ref_id=0; //Найденый id-шник пользователя
    global $tmpres;
	if (strpos($str,"#")===0){
		#Это магазин
		$query="SELECT ownerid FROM wsq_shops WHERE shopnick='".mysql_real_escape_string($str)."'";
		$res = mysqli_query($tmpres, $query);
		if ($res!=false){
		  if (mysqli_num_rows($res)>0){
			while($row=mysqli_fetch_array($res)){
				$ref_id=$row['ownerid'];
				break;
			}				
		  } 
		}
	}elseif ((strpos($str,"@")>0)) {
		#Это email
		$query="SELECT uid FROM wsq_users WHERE email='".mysql_real_escape_string($str)."'";
		$res = mysqli_query($tmpres, $query);
		if ($res!=false){
		  if (mysqli_num_rows($res)>0){
			while($row=mysqli_fetch_array($res)){
				$ref_id=$row['uid'];
				break;
			}				
		  } 
		}
	}else{
		$str1=str_replace($qrsymbols,"",$str);
		$len=strlen($str1);

		if ($len>0){
			#Это логин
			$query="SELECT uid FROM wsq_users WHERE login='".mysqli_real_escape_string($tmpres, $str)."'";
			$res = mysqli_query($tmpres, $query);
			if ($res!=false){
			  if (mysqli_num_rows($res)>0){
				while($row=mysqli_fetch_array($res)){
					$ref_id=$row['uid'];
					break;
				}				
			  } 
			}
		}else{
			#Это QRКод
			$ref_id_tmp=getuidfromrefnum($str); //Декодируем QR
			//Проверяем, что такой пользователь существует
			$query="SELECT uid FROM wsq_users WHERE uid=$ref_id_tmp";
			$res = mysqli_query($tmpres, $query);
			if ($res!=false){
				if (mysqli_num_rows($res)>0){
					$ref_id=$ref_id_tmp;
				}				
			} 
		}
	}	
    return $ref_id;
  }

?>