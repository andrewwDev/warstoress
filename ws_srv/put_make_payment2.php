<?php
$bordersum_cop=10000; //на сколько копеек магазин может уходить в минус
//Константы реф системы
$ref_percents[4]=array(6,6,6,6,6); //Распределение прощентов по реферальной сети Мастер
$ref_percents[3]=array(6,6,5,5,5); //Распределение прощентов по реферальной сети Платиновый
$ref_percents[2]=array(5,5,5,5,5); //Распределение прощентов по реферальной сети Золотой
$ref_percents[1]=array(5,5,5,5,5); //Распределение прощентов по реферальной сети Серебряный
$ref_percents[0]=array(5,5,5,5,5); //Распределение прощентов по реферальной сети Игрок
$percent_in_ref=6; //Процент по реф сетке максимальный
$percent_zshop=5; //Процент захват магазина
$percent_zbuilding=3; //Процент захват здания
$percent_zterritory=2; //Процент захват территории
$percent_agent=3; //Процент агенту (того, кто пригласил владельца магазина)
$percent_ref=$percent_zshop+$percent_zbuilding+$percent_zterritory+$percent_agent; 
$fine_noref_factor=0.8; //Это понижающий коэффициент для пользователя без реф-мастера
$fine_nosid_factor=1; //Это понижающий коэффициент для пользователя без команды магазина (1=отключено)

$I9=$percent_in_ref*0.01;
$ref_line_factor=(pow(1+$I9,4)+pow(1+$I9,3)+pow(1+$I9,2)+(1+$I9)+1)*$I9; //Коэффициент ветки реферальной системе.
$ref_sys_factor=($percent_ref*(1+$ref_line_factor)/100+$ref_line_factor); //Коэффициент всей системы. Умножаем его на процент пользователю и получаем сколько процентов уйдет на всю систему (без выплаты самого бонуса)
//Для 6% $ref_line_factor=0.3382255776; //Коэффициент ветки реферальной системы.
//echo "$ref_sys_factor<br>";

//

function get_shop_data($sid,$uid){ //Получить данные о магазине
	$shopdata=false;
	$isowner=false;
	$ismanager=false;
		//$query="SELECT * FROM wsq_shops where sid=$sid";
		$query="SELECT t1.*, IFNULL(t2.uid,0) as ref_build, IFNULL(t3.uid,0) as ref_terr, IFNULL(t4.refid,0) as ref_agent FROM wsq_shops as t1 LEFT JOIN wsq_buildings as t2 ON t1.bldid=t2.bldid LEFT JOIN wsq_ter_sectors as t3 ON t3.secid=t1.secid LEFT JOIN wsq_users as t4 ON t4.uid=t1.ownerid where t1.sid=$sid";
		$res = mysql_query($query);
		if ($res!=false){
			if (mysql_num_rows($res)>0){
				while ($row=mysql_fetch_assoc($res)){
						$shopdata=$row;											
						if ($shopdata["ownerid"]==$uid){$isowner=true;}
					}	
				//Если пользователь uid не является владельцем магазина, проверяем не менеджер ли он.
				$shopdata["manager"]=0;
				if ($isowner==false){
					$query="SELECT * FROM wsq_managers where shopid=$sid AND uid=$uid";
					$res = mysql_query($query);
					if ($res!=false){
						if (mysql_num_rows($res)>0){
							$ismanager=true;
							$shopdata["manager"]=$uid;
						}
					}		
				}
				//конец проверки на менеджерность
			}
		}
		//Посчитать текущий баланс магазина в копейках
		if ($shopdata!=false){
			$shopdata["balance"]=0;
			$query = "SELECT SUM(summ) FROM wsq_transaction_shop WHERE sid=$sid";
			$res = mysql_query($query);
			if ($res!=false){
				if (mysql_num_rows($res)>0){
					$shopdata["balance"]=mysql_result($res, 0);
				}
			}
			//Баланс, который в настоящих деньгах
			$shopdata["balance_money"]=0;
			$query = "SELECT SUM(summ) FROM wsq_transaction_shop WHERE sid=$sid and btype=1";
			$res = mysql_query($query);
			if ($res!=false){
				if (mysql_num_rows($res)>0){
					$shopdata["balance_money"]=mysql_result($res, 0);
				}
			}
		}
		$shopdata["title"]=str_replace("\"", "", $shopdata["title"]);	//Чтобы SQL запросы не поломались
	return $shopdata;
}

function get_user_bonuces($uid){
	//Возвращает баланс пользователя uid в копейках

	$query = "SELECT SUM(summ) FROM wsq_transaction WHERE uid=$uid";
	$balance=0;
	$res = mysql_query($query);
	if ($res!=false){
		if (mysql_num_rows($res)>0){
			$balance=mysql_result($res, 0);
		}
	}
	return $balance;
}

function get_ref_struct($uid){ //Достать реферальную структуру пользователя и 
	$ref_str="";
	$ref_rank="";
	$query = "SELECT ref_struct FROM wsq_users where uid=$uid";	
	$res = mysql_query($query);
	$fl=1;
	if ($res!=false){
		if (mysql_num_rows($res)>0){
			while ($row=mysql_fetch_assoc($res)){				
				$ref_str=$row["ref_struct"];				
				$fl=0;													
			}
		}	
	}
	if (strlen($ref_str)>0){
		$res=explode(",",$ref_str);
	}else{
		$res=false;
	}
	
	return $res;
}

function get_user_arr_data($u_arr){//Получить ранги и 
	$arr_act=array();
	$arr_rank=array();
	$fl=1;	
	if (count($u_arr)>0){
		$ulist=join(",",$u_arr);		
		$query = "SELECT uid, active, rank FROM wsq_users where uid in ($ulist)";		
		$res = mysql_query($query);		
		if ($res!=false){
			if (mysql_num_rows($res)>0){
				while ($row=mysql_fetch_assoc($res)){	
					$cid=$row["uid"];
					$arr_act[$cid]=$row["active"];	
					$arr_rank[$cid]=$row["rank"];			
					$fl=0;												
				}
			}	
		}		
	}
	if ($fl==0){
		$res=array($arr_act,$arr_rank);
		//var_dump($res);	
	}else{
		$res=false;	
	}
	return $res;
}

function make_ref_payment($ref_type, $cid, $ref_str, $summ0, $users_rank, $users_act, $ref_percents, $shopid, $uid, $rec_id, $ipadr, $paystatus){//Сгенерировать SQL запрос для оплаты реф линии и посчитать сумму выплат	
	//$ref_type	реф система клиента
	//$cid		id пользователя, на систему которого начисляются бонусы
	//$ref_uids	Собственно сама реф структура
	//$summ0	Баллы на которые идет начисление по системе

	$ref_uids=$ref_str;
	array_unshift($ref_uids, $cid);
	$query="";
	$total_ref_summ=0;
	$cid_rank=0;
	$curr_summ=0;
	//echo "<hr>Струткура клиента<br>";var_dump($ref_uids);echo "COUNT=".count($ref_uids)."<br><br> RANKS="; var_dump($users_rank); echo "<br>";
	//echo "0)".$ref_uids[0];
	//echo " (A=".$users_act[0+$ref_uids[0]]." R=".$users_rank[0+$ref_uids[0]].' summ0='.$summ0.')<br>';
	$curr_summ=$summ0;//Получили все на уровне выше
	for ($i=1;$i<count($ref_uids);$i++){		
		//echo "$i)".$ref_uids[$i];
		//echo " (A=".$users_act[0+$ref_uids[$i]]." R=".$users_rank[0+$ref_uids[$i]];
		if ((0+$ref_uids[$i])<2){break;}//Пользовтель либо не существует, либо он системный
		if (isset($users_rank[0+$ref_uids[$i]])==false){break;} //Пользователь не найден (нет данных о его ранге)
		if (isset($users_act[0+$ref_uids[$i]])==false){break;} //Пользователь не найден (нет данных о его активации)
		if ($users_act[0+$ref_uids[$i]]==0){break;}		
		if ((0+$users_rank[0+$ref_uids[$i]])<(0+$users_rank[0+$ref_uids[$i-1]])){break;}
		$cid_rank=0+$users_rank[0+$ref_uids[$i]];		
		$cur_percent=$ref_percents[$cid_rank][$i-1];		
		$summ_tmp=floor($curr_summ*$cur_percent*0.01); 	//ref_percents[r][d]-процент по реферальной сетке пользователя ранга [r] на аге [d] от начисления
		$curr_summ=$curr_summ+$summ_tmp;
		$total_ref_summ=$total_ref_summ+$summ_tmp;
		//echo " Summ=$summ_tmp %=$cur_percent cid_rank=$cid_rank)<br>";
		if ($summ_tmp>0){
			$query=$query.",($shopid, ".$ref_uids[$i].", $uid, ".$ref_uids[$i-1].", $rec_id, $ref_type, $summ_tmp, \"Реферальные начисления (по чеку №$rec_id) на выплату ".$summ0*0.01." пользователю $cid\", $paystatus, NOW(), '$ipadr')";
		}
		//echo "$query<br>";
	}

	return array($query,$total_ref_summ);

}

//Проверить достаточно ли бонусов на счету магазина
//Проверить достаточно ли бонусов на счету пользователя
//Списать бонусы со счета пользователя
//Занести бонусы на счет магазина

//Провести оплату пользователю

//Begin Transaction

//Списать бонусы со счета магазина

//Выдать бонусы пользователям
	//Списать бонусы со счета магазина
	//Дать бонус пользователю, сделавшему покупку

	//Списать бонусы со счета магазина для захватчика
	//Дать бонус захватчику магазина
	
	//Списать бонусы со счета магазина для захватчика здания
	//Дать бонус захватчику здания
	
	//Списать бонусы со счета магазина для захватчика территории
	//Дать бонус захватчику территории
	

	//Списать бонусы со счета магазина для рефералки пользователя
	//Дать бонус рефералке пользователя
	
	//Списать бонусы со счета магазина для рефералки захватчика магазина
	//Дать бонус рефералке магазина
	
	//Списать бонусы со счета магазина для рефералки захватчика здания
	//Дать бонус рефералке захватчика здания

    //Списать бонусы со счета магазина для рефералки захватчика территории
	//Дать бонус рефералке захватчика территории

//Выдать бонусы сотруднику
	//Списать бонусы со счета магазина
	//Дать бонусы сотруднику

//End Transaction




$uid=0; //ID менеджера, который провел оплату
$shopid=0; //ID магазина
$sid="";//
$qrcode=""; //QR код, полученный от пользователя
$status=1; $err=0; //Все ок;
$clientid=0; //ID клиента
$client_cash=0; //Сумма в руб, которую пользователь платит за товар
$client_bonus=0; //Бонусы в руб, которые пользователь использует для оплаты
$client_act=0;	//Активирован ли пользователь (получается дальше)
$client_rank=0;	//Ранг пользователя (получается дальше)
$ipadr=$_SERVER['REMOTE_ADDR'];
$client_cash=0; //Сумма в руб, которую пользователь платит за товар
$client_bonus=0; //Бонусы в руб, которые пользователь использует для оплаты
$sid=0;

if($_POST){
	if (isset($_POST['qrcode'])){$qrcode=$_POST['qrcode'];}
	if (isset($_POST['uid'])){$uid=0+$_POST['uid'];}
	if (isset($_POST['sid'])){$sid=$_POST['sid'];}
	if (isset($_POST['shopid'])){$shopid=0+$_POST['shopid'];}
	if (isset($_POST['clientid'])){$clientid=0+$_POST['clientid'];}
	if (isset($_POST['client_cash'])){$client_cash=0+$_POST['client_cash'];}
	if (isset($_POST['client_bonus'])){$client_bonus=0+$_POST['client_bonus'];}
}else{
	if (isset($_GET['qrcode'])) {$qrcode=$_GET['qrcode'];}
	if (isset($_GET['uid'])) {$uid=0+$_GET['uid'];}
	if (isset($_GET['sid'])) {$sid=$_GET['sid'];}
	if (isset($_GET['shopid'])) {$shopid=0+$_GET['shopid'];}
	if (isset($_GET['clientid'])) {$clientid=0+$_GET['clientid'];}
	if (isset($_GET['client_cash'])) {$client_cash=0+$_GET['client_cash'];}
	if (isset($_GET['client_bonus'])) {$client_bonus=0+$_GET['client_bonus'];}
}



require_once("dbconfiguration.php");  //Подключаемся к базе
require_once("checksid2.php");  //Проверяем пользователя
require_once("fn_gettblver.php");	  //Функция, возвращающая актуальную версию таблицы

$qrcode=mysql_real_escape_string($qrcode);
$paystatus=1; //Если 1 - то оплата выполнена, если 0 - то ожидается пополнение счета магазином
$client_bonus_balance=0;
$client_cash_cop=$client_cash*100; //Сумма в коп, которую пользователь платит за товар
$client_bonus_cop=$client_bonus*100; //Бонусы в коп, которые пользователь использует для оплаты

//Проверяем QR код на корректность. Тут должен быть 8-символьный QR без всяких спецсимволов
if (strlen($qrcode)!=8){
	$status=0; $err=13;
}
//Проверяем, имеет ли пользователь право проводить эту операцию. 
$isowner=false;
$ismanager=false;	
$shopdata=get_shop_data($shopid,$uid);
if ($shopdata["ownerid"]==$uid){$isowner=true;}	else 
if ($shopdata["manager"]==$uid){$ismanager=true;} else{
	$status=0; //Плохо. Пользователь не имеет право управлять этим магазином
	$err=7;
}
//Проверяем, достатьчно ли бонусов на счету пользователя для оплаты
if ($status==1){ 
	if ($client_bonus>0){		
		 $client_bonus_balance=get_user_bonuces($clientid); //баланс пользователя в коп
		 if ($client_bonus_balance<$client_bonus_cop){	$status=0; $err=8; } //Недостаточно бонусов на счету пользователя-клиента
	}
}

//Проверяем, можно ли совершить эту оплату бонусами (принимает ли их магазин и какой процент)
if ($status==1){ 
	//bonusborderpercent - Процент от покупки, который можно оплатить бонусами
	//bonusbordervalue - Максимальное число бонусов, которые можно отдать за покупку (в коп)	
	if ($client_bonus>0){		
		if ($shopdata["bonusbordervalue"]>0){
			if ($client_bonus_cop>$shopdata["bonusbordervalue"]){$status=0; $err=11;}
		}
		if ($shopdata["bonusborderpercent"]>0){
			if ($client_bonus/($client_bonus+$client_cash)>$shopdata["bonusborderpercent"]){$status=0; $err=12;}
		}
	}
}

//Проверяем, достаточно ли денег на счету магазина для оплаты бонусов и затрат на реф систему
if ($status==1){
	$bonus_for_user=floor(($shopdata["payvalue"]*0.01)*$client_cash_cop); //Бонус в коп
	$bonus_for_refs=ceil(($shopdata["payvalue"]*$ref_sys_factor*0.01)*$client_cash_cop); //Бонусы в реферальную систему
	$bonus_for_manager=0;  //вознаграждение сотруднику за операцию в копейках
	if ($ismanager==true){$bonus_for_manager=$shopdata["paytomanbyoper"];}
	$spend_total=$bonus_for_user+$bonus_for_refs+$bonus_for_manager;

	//if ($spend_total>$shopdata["balance"]){$status=0; $err=9;} //Недостаточно бонусов на счету магазина
	if ($spend_total>$shopdata["balance"]){$paystatus=0; } //Недостаточно бонусов на счету магазина
	if ($spend_total>($shopdata["balance"]+$bordersum_cop)){$status=0; $err=9;}

	 //Это для статистики
	$spend_money=0; //Это для статистики
	$received=0; //Это для статистики
}

//Достаем данные о пользователях, которые потом будем использовать при транзакциях
//Проверяем, прикрепился ли пользователь к кому-нибудь и 
$client_refid=1; //Первый пользователь - админ
$client_sid=0; //Первый пользователь - если пользователь не прикреплен к магазину
if ($status==1){
	$query = "SELECT refid, sid FROM wsq_users WHERE uid=$clientid";
	$res = mysql_query($query);
	if ($res!=false){
		if (mysql_num_rows($res)>0){
			while ($row=mysql_fetch_assoc($res)){
				$client_refid=$row['refid'];
				$client_sid=$row['sid'];
				
			}	
		}
	}

	//Достаем реферальные структуры
	$client_ref_str=get_ref_struct($clientid);
	$zahvat_ref_str=get_ref_struct($shopdata["refid"]);
	$zbuilding_ref_str=get_ref_struct($shopdata["ref_build"]);
	$zterritiry_ref_str=get_ref_struct($shopdata["ref_terr"]);
	$agent_ref_str=get_ref_struct($shopdata["ref_agent"]);

	//Получить массивы с ID всех затронутых пользователей, массивы с рангом и активностью
	$allusers=array();
	if ($client_ref_str!=false){$allusers=array_merge($allusers,$client_ref_str);}
	if ($zahvat_ref_str!=false){$allusers=array_merge($allusers,$zahvat_ref_str);}
	if ($zbuilding_ref_str!=false){$allusers=array_merge($allusers,$zbuilding_ref_str);}
	if ($zterritiry_ref_str!=false){$allusers=array_merge($allusers,$zterritiry_ref_str);}
	if ($agent_ref_str!=false){$allusers=array_merge($allusers,$agent_ref_str);}
	//добавляем пользователей о которых еще нужна информация
	$allusers=array_merge($allusers,array($clientid,$shopdata["refid"],$shopdata["ref_build"],$shopdata["ref_terr"],$shopdata["ref_agent"]));
	$allusers=array_unique($allusers);
	$res=get_user_arr_data($allusers);
	$users_act=$res[0];	//Массив 1 -пользователь активирован 0 -нет. $users_act[uid]
	$users_rank=$res[1];//Ранг пользователя. $users_rank[uid]
}

//ТУТ все проверки закончены и начинаются транзакции
if ($status==1){
	$received=$client_bonus_cop;
	//Создаем чек
	$res = mysql_query("INSERT INTO wsq_receipt (sid, uid, mid, cash, summ, date, qrcode) VALUE ($shopid, $clientid, $uid, $client_cash_cop, ".($client_cash_cop+$client_bonus_cop).", NOW(), '$qrcode')");
	//Достаем ID чека
	$rec_id=0;
	$res = mysql_query("SELECT rec_id FROM wsq_receipt WHERE uid=$clientid and sid=$shopid and qrcode='$qrcode' ORDER BY rec_id DESC");
	if ($res!=false){if (mysql_num_rows($res)>0){$rec_id=mysql_result($res, 0);}}
	if ($rec_id==0){$status=0; $err=10;}
	//echo "ID чека REC_id=$rec_id<br>";

	if ($client_bonus>0){//Списываем бонусы со счета пользователя в пользу магазина		
		$res = mysql_query("INSERT INTO wsq_transaction (sid,uid,mid,rec_id,troper,summ,comment,date,ip) VALUE ($shopid, $clientid, $uid, $rec_id, 54, -".abs($client_bonus_cop).", \"Оплата бонусами покупки в магазине ".$shopdata["title"]." (чек №$rec_id)\", NOW(), '$ipadr')");
		$res = mysql_query("INSERT INTO wsq_transaction_shop (sid,uid,mid,sid37,rec_id,troper,summ, btype, comment, date, ip) VALUE ($shopid, $clientid, $uid, 0, $rec_id, 4, ".abs($client_bonus_cop).", 0, \"Оплата бонусами покупки в магазине ".$shopdata["title"]." (чек №$rec_id)\", NOW(), '$ipadr')");
	}

	if ($client_cash*100>=$shopdata["payborder"]){ //пользователь отдал денег больше, чем требуется для выплаты бонусов

		//Начисление бонусов
		//Списать со счета магазина 
		$query="";
		//Пользователю
		if ($shopdata["balance_money"]>=$bonus_for_user){//снять деньги если есть
			$query="($shopid, $clientid, $uid, 0, $rec_id, 5, -".abs($bonus_for_user).", 1, \"Начисление бонусов пользователю\", NOW(), '$ipadr')";
			$shopdata["balance_money"]=$shopdata["balance_money"]-$bonus_for_user;
			$spend_money=$spend_money+$bonus_for_user; 
		}else{
			if ($shopdata["balance_money"]>0){//снять деньги если есть, но их мало
				$query="($shopid, $clientid, $uid, 0, $rec_id, 5, -".abs($shopdata["balance_money"]).", 1, \"Начисление бонусов пользователю\", NOW(), '$ipadr')";	
				$query=$query.",($shopid, $clientid, $uid, 0, $rec_id, 5, -".(abs($bonus_for_user)-abs($shopdata["balance_money"])).", 0, \"Начисление бонусов пользователю\", NOW(), '$ipadr')";								
				$spend_money=$spend_money+abs($shopdata["balance_money"]); 
				$shopdata["balance_money"]=0;
			}else{//снять бонусы
				$query="($shopid, $clientid, $uid, 0, $rec_id, 5, -".abs($bonus_for_user).", 0, \"Начисление бонусов пользователю\", NOW(), '$ipadr')";	
			}
		}
		//В рефералку
		if ($shopdata["balance_money"]>=$bonus_for_refs){//снять деньги если есть
			$query=$query.",($shopid, $clientid, $uid, 0, $rec_id, 6, -".abs($bonus_for_refs).", 1, \"Начисления в реферальную сеть\", NOW(), '$ipadr')";
			$shopdata["balance_money"]=$shopdata["balance_money"]-$bonus_for_refs;
			$spend_money=$spend_money+$bonus_for_refs; 
		}else{
			if ($shopdata["balance_money"]>0){//снять деньги если есть, но их мало
				$query=$query.",($shopid, $clientid, $uid, 0, $rec_id, 6, -".abs($shopdata["balance_money"]).", 1, \"Начисления в реферальную сеть\", NOW(), '$ipadr')";	
				$query=$query.",($shopid, $clientid, $uid, 0, $rec_id, 6, -".(abs($bonus_for_refs)-abs($shopdata["balance_money"])).", 0, \"Начисления в реферальную сеть\", NOW(), '$ipadr')";				
				$spend_money=$spend_money+abs($shopdata["balance_money"]); 
				$shopdata["balance_money"]=0;
			}else{//снять бонусы
				$query=$query.",($shopid, $clientid, $uid, 0, $rec_id, 6, -".abs($bonus_for_refs).", 0, \"Начисления в реферальную сеть\", NOW(), '$ipadr')";	
			}
		}
		//Сотруднику за проводку транзакции
		 if (($ismanager==true) and ($bonus_for_manager>0)){

			if ($shopdata["balance_money"]>=$bonus_for_manager){//снять деньги если есть
				$query=$query.",($shopid, $uid, $uid, 0, $rec_id, 10, -".abs($bonus_for_manager).", 1, \"Начисление сотруднику за проводку чека №$rec_id\", NOW(), '$ipadr')";
				$shopdata["balance_money"]=$shopdata["balance_money"]-$bonus_for_manager;
				$spend_money=$spend_money+$bonus_for_manager; 
			}else{
				if ($shopdata["balance_money"]>0){//снять деньги если есть, но их мало
					$query=$query.",($shopid, $uid, $uid, 0, $rec_id, 10, -".abs($shopdata["balance_money"]).", 1, \"Начисление сотруднику за проводку чека №$rec_id\", NOW(), '$ipadr')";	
					$query=$query.",($shopid, $uid, $uid, 0, $rec_id, 10, -".(abs($bonus_for_manager)-abs($shopdata["balance_money"])).", 0, \"Начисление сотруднику за проводку чека №$rec_id\", NOW(), '$ipadr')";									
					$spend_money=$spend_money+abs($shopdata["balance_money"]); 
					$shopdata["balance_money"]=0;
				}else{//снять бонусы
					$query=$query.",($shopid, $uid, $uid, 0, $rec_id, 10, -".abs($bonus_for_manager).", 0, \"Начисление сотруднику за проводку чека №$rec_id\", NOW(), '$ipadr')";	
				}
			}

		 }

		$res = mysql_query("INSERT INTO wsq_transaction_shop (sid,uid,mid,sid37,rec_id,troper,summ, btype, comment, date, ip) VALUES $query");	

	//Теперь то что списали с магазина начисляем пользователям
		if ($client_refid>1){$fine_noref_factor=1;} //корректируем штрафные коэффициенты для пользователя с реф-мастером
		if ($client_sid>0){$fine_nosid_factor=1;}   //корректируем штрафные коэффициенты для пользователя в команде магазина
		
		//Начисления захватчикам и агенту	
		$summ=abs(floor($bonus_for_user*$fine_noref_factor*$fine_nosid_factor));//Начисления клиенту
		$summ_zshop=floor($summ*($percent_zshop*0.01));//Начисление на бонус захватчику магазина
		$summ_zbld=floor($summ*($percent_zbuilding*0.01));//Начисление на бонус захватчику здания
		$summ_zterr=floor($summ*($percent_zterritory*0.01));//Начисление на бонус захватчику территории
		$summ_zagent=floor($summ*($percent_agent*0.01));//Начисление на бонус агенту магазина
		
		//Начисления на рефералку
		$cashback_ref=0; $summ_zs_ref=0; $summ_zb_ref=0; $summ_zt_ref=0; $summ_za_ref=0;

		//Начисление бонуса	
		$payed_summ=0;
		if (($clientid>1)and($summ>0)){if ($users_act[$clientid]==1){
			$query="($shopid, $clientid, $uid, 0, $rec_id, 55, $summ, \"Начисление бонуса из магазина ".$shopdata["title"]." (по чеку №$rec_id)\", $paystatus, NOW(), '$ipadr')";	
			if ($bonus_for_user>$summ){
				//Списать остаток
				$query=$query.",($shopid, 1, $uid, 0, $rec_id, 81, ".($bonus_for_user-$summ).", \"Остаток бонуса магазина ".$shopdata["title"]." для пользователя $clientid (по чеку №$rec_id)\", $paystatus, NOW(), '$ipadr')";	
			}
			//Проплата в реф структуру клиента, который совершил оплату
			$res1=make_ref_payment(56, $clientid, $client_ref_str, $summ, $users_rank, $users_act, $ref_percents, $shopid, $uid, $rec_id, $ipadr, $paystatus);					
			$query=$query.$res1[0];
			$payed_summ=$payed_summ+$res1[1];
			$cashback_ref=$res1[1];
			//echo "<br>1 payed_summ=<b>$payed_summ</b><br>";
		
			//Проверить что не системный пользователь, что есть что начислять, что пользователь активирован/активен
			if (($shopdata["refid"]>1)and($summ_zshop>0)){if ($users_act[$shopdata["refid"]]==1){
				$query=$query.",($shopid, ".$shopdata["refid"].", $uid, 0, $rec_id, 61, $summ_zshop, \"Начисление бонуса захватчику магазина ".$shopdata["title"]." (по чеку №$rec_id)\", $paystatus, NOW(), '$ipadr')";			
				//Проплата в реф структуру клиента, который совершил оплату
				$res1=make_ref_payment(56, $shopdata["refid"], $zahvat_ref_str, $summ_zshop, $users_rank, $users_act, $ref_percents, $shopid, $uid, $rec_id, $ipadr, $paystatus);		
				$query=$query.$res1[0];
				$payed_summ=$payed_summ+$res1[1]+$summ_zshop;
				$summ_zs_ref=$res1[1];
				//echo "<br>2 payed_summ=<b>$payed_summ</b><br>";			
			}}
			if (($shopdata["ref_build"]>1)and($summ_zbld>0)){if ($users_act[$shopdata["ref_build"]]==1){
				$query=$query.",($shopid, ".$shopdata["ref_build"].", $uid, 0, $rec_id, 62, $summ_zbld, \"Начисление бонуса захватчику здания (по чеку №$rec_id)\", $paystatus, NOW(), '$ipadr')";
				//Проплата в реф структуру клиента, который совершил оплату
				$res1=make_ref_payment(56, $shopdata["ref_build"], $zbuilding_ref_str, $summ_zbld, $users_rank, $users_act, $ref_percents, $shopid, $uid, $rec_id, $ipadr, $paystatus);		
				$query=$query.$res1[0];
				$payed_summ=$payed_summ+$res1[1]+$summ_zbld;
				$summ_zb_ref=$res1[1];
				//echo "<br>3 payed_summ=<b>$payed_summ</b><br>";
			}}
			if (($shopdata["ref_terr"]>1)and($summ_zterr>0)){if ($users_act[$shopdata["ref_terr"]]==1){
				$query=$query.",($shopid, ".$shopdata["ref_terr"].", $uid, 0, $rec_id, 63, $summ_zterr, \"Начисление бонуса захватчику территории (по чеку №$rec_id)\", $paystatus, NOW(), '$ipadr')";
				//Проплата в реф структуру клиента, который совершил оплату
				$res1=make_ref_payment(56, $shopdata["ref_terr"], $zterritiry_ref_str, $summ_zterr, $users_rank, $users_act, $ref_percents, $shopid, $uid, $rec_id, $ipadr, $paystatus);		
				$query=$query.$res1[0];
				$payed_summ=$payed_summ+$res1[1]+$summ_zterr;
				$summ_zt_ref=$res1[1];
				//echo "<br>4 payed_summ=<b>$payed_summ</b><br>";			
			}}
			if (($shopdata["ref_agent"]>1)and($summ_zagent>0)){if ($users_act[$shopdata["ref_agent"]]==1){
				$query=$query.",($shopid, ".$shopdata["ref_agent"].", $uid, 0, $rec_id, 64, $summ_zagent, \"Начисление бонуса агенту магазина ".$shopdata["title"]." (по чеку №$rec_id)\", $paystatus, NOW(), '$ipadr')";
				//Проплата в реф структуру клиента, который совершил оплату
				$res1=make_ref_payment(56, $shopdata["ref_agent"], $agent_ref_str, $summ_zagent, $users_rank, $users_act, $ref_percents, $shopid, $uid, $rec_id, $ipadr, $paystatus);		
				$query=$query.$res1[0];
				$payed_summ=$payed_summ+$res1[1]+$summ_zagent;
				$summ_za_ref=$res1[1];
				//echo "<br>5 payed_summ=<b>$payed_summ</b><br>";	
			}}
			//Проверяем есть ли остаток, и если есть - передаем его системе
			if ($bonus_for_refs>$payed_summ){
				//Списать остаток
				$query=$query.",($shopid, 1, $uid, 0, $rec_id, 82, ".($bonus_for_refs-$payed_summ).", \"Остаток выплат в реф систему (по чеку №$rec_id)\", $paystatus, NOW(), '$ipadr')";	
			}

		}}

		//Сотруднику за проводку чека
		if (($ismanager==true) and ($bonus_for_manager>0)){
			$query=$query.",($shopid, $uid, $uid, 0, $rec_id, 60, ".($bonus_for_manager).", \"Бонус от магазина за проводку чека №$rec_id\", $paystatus, NOW(), '$ipadr')";	
		}else{
			$bonus_for_manager=0;
		}
		//echo "<hr>$query";
		$res = mysql_query("INSERT INTO wsq_transaction (sid,uid,mid,refid,rec_id,troper,summ,comment, paystatus,date,ip) VALUES $query");

		//Апдейт wsq_receipt
		$res = mysql_query("UPDATE wsq_receipt SET cashback=$summ, cashback_ref=$cashback_ref, summ_zs=$summ_zshop, summ_zs_ref=$summ_zs_ref, summ_zb=$summ_zbld, summ_zb_ref=$summ_zb_ref, summ_zt=$summ_zterr, summ_zt_ref=$summ_zt_ref, summ_za=$summ_zagent, summ_za_ref=$summ_za_ref, tomanager=$bonus_for_manager, summ_into_sys=".($bonus_for_refs-$payed_summ)." WHERE rec_id=$rec_id");		

		///////////////////////////////////////////////////////////
		///           Обновляем статистику  wsq_shop_clients    ///
		///////////////////////////////////////////////////////////
		$res = mysql_query("SELECT * FROM wsq_shop_clients WHERE sid=$shopid and uid=$clientid");
		$isitnewuser=1;
		if ($res!=false){
		if (mysql_num_rows($res)>0){$isitnewuser=0;}}

		$id=0;
		$res = mysql_query("SELECT id FROM wsq_shop_stat WHERE sid=$shopid AND day=DATE_FORMAT(NOW(),'%Y-%m-%d')");
		if ($res!=false){
			if (mysql_num_rows($res)>0){
				while ($row=mysql_fetch_assoc($res)){
					$id=$row['id'];				
				}	
			}
		}
		if ($id==0){
			$res = mysql_query("INSERT INTO wsq_shop_stat (sid, day, clients, newclients, spend_total, spend_money, received, responses) VALUE ($shopid, DATE_FORMAT(NOW(),'%Y-%m-%d'), 1, $isitnewuser, $spend_total, $spend_money, $received, 0)");
		}else{
			$res = mysql_query("UPDATE wsq_shop_stat SET clients=clients+1, newclients=newclients+$isitnewuser, spend_total=spend_total+$spend_total, spend_money=spend_money+$spend_money, received=received+$received WHERE id=$id");
		}

		//Добавляем данные о покупателе в таблицу покупателей. Там стоит уникальный индекс,  поэтому дублирования не происходит
		if ($isitnewuser==1){$res = mysql_query("INSERT INTO wsq_shop_clients (sid,uid) VALUE ($shopid, $clientid)");}

		/////////////////////////////////////////////////////////////////////////
		///  Обновляем статистику wsq_user_rank (это для пересчета рангов)    ///
		/////////////////////////////////////////////////////////////////////////

		$res = mysql_query("SELECT *,updatedate=DATE_FORMAT(NOW(),'%Y-%m-%d') as thisday FROM wsq_user_rank WHERE uid=$clientid");
		$isitnewrank=1;
		$isthisday=0;
		if ($res!=false){
			if (mysql_num_rows($res)>0){
				$isitnewrank=0;
				if ($row=mysql_fetch_assoc($res)){
					$isthisday=$row['thisday'];
				}
			}
		}

		if ($isitnewrank==1){
			$res = mysql_query("INSERT INTO wsq_user_rank (uid, shops, builds, friends, summ, summ_today, updatedate) VALUE ($clientid, 0, 0, 0, $summ, $summ, NOW())");
		}else{
			if ($isthisday==1){
				$str="summ_today=summ_today+$summ";
			}else{
				$str="summ_today=$summ";
			}
			$res = mysql_query("UPDATE wsq_user_rank SET summ=summ+$summ, $str WHERE uid=$clientid");
		}

	}//Конец проверки того что оплата больше порога payborder и нужно бонусы начислять
}

$jsonData = array();
$jsonData["status"]=$status;
$jsonData["err"]=$err;

echo json_encode($jsonData, JSON_UNESCAPED_UNICODE); 

mysql_close(); 
?>