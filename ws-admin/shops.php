<?
	require_once('smarty/libs/Smarty.class.php');
	$smarty = new Smarty();
	// $smarty->debugging = true;

	$smarty->template_dir = 'smarty/templates/';
	$smarty->compile_dir = 'smarty/templates_c/';
	$smarty->config_dir = 'smarty/configs/';
	$smarty->cache_dir = 'smarty/cache/';

	$smarty->assign('username', 'Admin');
	$smarty->display('shops.tpl');
?>
