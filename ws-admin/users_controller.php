<?

require_once('smarty/libs/Smarty.class.php');
$smarty = new Smarty();
//	$smarty->debugging = true;

$smarty->template_dir = 'smarty/templates/';
$smarty->compile_dir = 'smarty/templates_c/';
$smarty->config_dir = 'smarty/configs/';
$smarty->cache_dir = 'smarty/cache/';


error_reporting(-1);
session_start();
header("Content-Type: text/html; charset=utf-8");
$homedir = "/var/www/u0413200/data/www/warstores.net/ws-panel/";
$pagenum = 0;
$mode = 0;

require_once ($homedir . 'configs/dbconfiguration.php');

function adm_users ( $pg = 0 ) {
    global $tmpres;
    $ononepg = 30; //Отображать на одной странице
    $userslist = array();
    //if ($pg>0) {
    // 	$strlimit="LIMIT ".($ononepg*($pg-1)).", ".$ononepg;
    // } else {
    // 	$strlimit='';
    // }

    $strlimit = ($pg) ? "LIMIT ".($ononepg*($pg-1)).", ".$ononepg  : '';

    //Получаем количество листов всего
    $users_num = 0;
    $sql = "SELECT count(sid) FROM `wsq_users`";
    $res = mysqli_query($tmpres, $sql);
    if ($res!=false) {
        if (mysqli_num_rows($res)>0) {
            $users_num .= mysqli_fetch_row($res)[0];
        }
    } //Всего новостей
    $pagenum = ceil($users_num/$ononepg); //Всего листов
    //
    $sql = "SELECT uid AS id, email, vk_uid, CONCAT_WS(' ', name, surname) AS name, isapproved FROM wsq_users $strlimit";
    $res = mysqli_query($tmpres, $sql) or die(mysqli_error($tmpres));

    if (mysqli_num_rows($res)>0){
        while($row = mysqli_fetch_assoc($res)){
            $userslist[]=$row;
        }
    }

    $resarr = array(
        '0' => $userslist,
        '1' => $pagenum
    );
    return $resarr;
}

class User{
    private $uid;
    private $refid;
    private $sid;
    private $login;
    private $email;
    private $name;
    private $surname;
    private $city;
    private $birthday;
    private $active;
    private $status;
    private $rank;
    private $pwdhash;
    private $lastvisit;
    private $isapproved;
    private $isshopowner;
    private $ismanager;
    private $ref_struct;
    private $ref_struct_full;
    private $vk_uid;
    private $vk_photo_100;
    private $vk_photo_big;
    private $guild_id;
    private $guild_title;
    private $notifications;
    private $phone;
    private $auth;



    public function delete(){
        global $tmpres;
        if($stmt = $tmpres->prepare("DELETE FROM wsq_users  WHERE uid=?")){
            $stmt->bind_param('s', $this->uid);
            $stmt->execute();
            $stmt->close();
        }
    }
    public function getAll(){
        print_r(get_object_vars($this));
    }

    public function __construct($uid){
        global $tmpres;
        if($stmt = $tmpres->prepare("SELECT * FROM wsq_users  WHERE uid=?")){
            $stmt->bind_param('s', $uid);
            $stmt->execute();
            $result = $stmt->get_result();
            while($row = $result->fetch_assoc()){
                foreach ($row as $key => $value ) {
                    $this->$key = $value;
                }
            }
        }
    }

    public function __get($name){
        if(property_exists($this, $name)){
            return $this->$name;
        }
    }

    public function __set($name, $value){
        if(property_exists($this, $name)){
            $this->$name = $value;
            global $tmpres;
            if($stmt = $tmpres->prepare("UPDATE wsq_users SET $name=? WHERE uid=?")){
                $stmt->bind_param('ss', $value, $this->uid);
                $stmt->execute();
                return $stmt->affected_rows;
            }
        }
    }
}

//
//$user = new User(368);
//$user->getAll();
//	$user->surname = "RRRR";
//	echo $user->surname;


function findUser($field){
    global $tmpres;

    $sql = "select uid, email, vk_uid, isapproved, CONCAT_WS(' ', name, surname) as name from wsq_users where vk_uid like '$field%' or uid='$field' or email='$field' or name like '$field%' or surname like '$field%';";
    $res = $tmpres->query($sql);
    $userslist = [];
    if ($res) {
        if ($res->num_rows > 0) {
            while($row = $res->fetch_assoc()){
                $userslist[] = $row;
            }
        }
    }else{
        return $tmpres->error;
    }

    return $userslist;
}


function getUsersByPage($page = 1){
    global $tmpres;
    $viewOnPage = 20;
    $res = [];
    if($stmt = $tmpres->prepare('SELECT uid as id, email, vk_uid, isapproved, CONCAT_WS(\' \', name, surname) as name FROM wsq_users ORDER BY uid DESC')){
        $stmt->execute();
        $stmt->bind_result($id, $email, $vk_uid, $isapproved, $name);
        while ($stmt->fetch()) {
            array_push($res, ["id" => $id, "email" => $email, "vk_uid" => $vk_uid, "isapproved" => $isapproved, "name" => $name]);

        }
        $stmt->close();
    }
    $usersCount = count($res);
    $pages = ceil($usersCount / $viewOnPage);

    $data["pages"] = $pages;


    if($page > 0 && $page < $pages + 1){
        $data["users"] = [];
        for($i = $viewOnPage * ($page - 1); $i < $viewOnPage * ($page); $i++){
            if($i < $usersCount && $res[$i]){
                array_push($data["users"], $res[$i]);
            }
        }
    }else{
        $data["users"] = [];
    }

    return $data;
}



//
	if(isset($_GET["search"])){
	    $search = $_GET["search"];
	    if(strlen($search) > 0 and $search != " "){
            $result = findUser($search);
            echo json_encode($result, JSON_OBJECT_AS_ARRAY);
        }else{
            echo json_encode(["error" => "bad query"]);
        }
    }else{
        if(isset($_GET["page"])){
            $page = $_GET["page"];
            $data = getUsersByPage($page);
        }else{
            $page = 1;
            $data = getUsersByPage($page);
		}



		$smarty->assign('userslist', $data["users"]);
		$smarty->assign('pages', $data["pages"]);
		$smarty->assign('page', $page);

		$smarty->assign('username', 'Admin');
		$smarty->display('header.tpl');
		$smarty->display('users_controller.tpl');
		$smarty->display('footer.tpl');
    }


?>
